<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2010/11/19
 * Update   : 2010/11/19
 * Description : フォーム用DB処理
 **************************************************************************/

class Validate extends CoValidate{

	function Validate() {
	}

	//引数：値　値のタイプ　値の表示名　必須　最大値　最小値
	function allCheck($vars ,$items ,$err_msg = array()) {

		foreach($items as $key => $val) {

			if( ! isset($val["nocheck"]) || $val["nocheck"] != 1) {

				//エラーメッセージ設定
				$msgtype = $val["errmsg"];

				//確認チェック用変数の設定
				$diff = array();
				if ( ! empty($val["diff"])) {
					$diff = array($vars[$val["diff"]]);
				}

				//結合データの設定
				if ( ! empty($val["join"])) {

					$join_vars = array();
					foreach ($val["join"] as $join) {
						$join_vars[] = $vars[$join];
					}

					$vars[$key] = $join_vars;
				}


				//バリデーション
				$result = $this->check($val, $msgtype, $vars[$key], $diff);
				if ($result != "") {
					$err_msg[$key] = $result;
				}
			}
		}

		return $err_msg;
	}
}
?>