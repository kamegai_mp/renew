<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2010/11/19
 * Update   : 2010/11/19
 * Description : フォーム用メール処理
 **************************************************************************/

class Mail{

	//=================================================================
	//　●メール送信
	//=================================================================
	function send($vars, $items, $lists, $tmpls = array()) {

		$param = array(
			"vars" => $vars,
			"items" => $items,
			"lists" => $lists,
		);

		//独自変数が入ってきたら変換
		preg_match("/^{%(.*)%}$/", ADMIN_MAIL_FROM, $result_adfr);
		if (isset($result_adfr[1]) && isset($vars[$result_adfr[1]])) {
			$admin_from = $vars[$result_adfr[1]];
		} else {
			$admin_from = ADMIN_MAIL_FROM;
		}

		preg_match("/^{%(.*)%}$/", USER_MAIL_TO, $result_urto);
		if (isset($result_urto[1]) && isset($vars[$result_urto[1]])) {
			$user_to = $vars[$result_urto[1]];
		} else {
			$user_to = USER_MAIL_TO;
		}

		preg_match("/^{%(.*)%}$/", USER_MAIL_FROM, $result_urfr);
		if (isset($result_urfr[1]) && isset($vars[$result_urfr[1]])) {
			$user_from = $vars[$result_urfr[1]];
		} else {
			$user_from = USER_MAIL_FROM;
		}

		//デバッグモード
		if (DEBUG_MODE === 1) {
			$admin_to = $user_to;
		} else {
			$admin_to = ADMIN_MAIL_TO;
		}

		//管理者用メール
		if (MAIL_ADMIN_SEND === true) {

			if (empty($tmpls["admin"])) {
				$tmpfile = "admin.mail";
			} else {
				$tmpfile = $tmpls["admin"];
			}

			$options = array(
				"to" => $admin_to,
				"from" => $admin_from,
				"file" => $tmpfile,
				"param" => $param,
				"return_path" => $user_to,
			);

			CoMail::send($options);
		}

		//ユーザー用メール
		if (MAIL_USER_SEND === true) {

			if (empty($tmpls["user"])) {
				$tmpfile = "user.mail";
			} else {
				$tmpfile = $tmpls["user"];
			}

			if ( ! empty($dir)) {
				$tmpfile = $dir ."/".$tmpfile;
			}

			$options = array(
				"to" => $user_to,
				"from" => $user_from,
				"file" => $tmpfile,
				"param" => $param,
				"return_path" => $admin_to,
			);

			CoMail::send($options);
		}
	}
}

?>