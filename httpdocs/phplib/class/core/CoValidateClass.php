<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2010/05/20
 * Update   : 2011/06/09
 * Description : 共通バリデーションクラス
 **************************************************************************/

class CoValidate{

	//引数：値　値のタイプ　値の表示名　必須　最大値　最小値
	//necessary, wrong, max, min, just, diff
	function check($items, $msg, $value, $diff = array()) {

		$err_msg = "";
		$util = new CoUtil();

		$defaults = array("name", "type", "max", "min", "just", "necessary", "list", "nocheck", "option", "join", "diff", "regex");
		$items = $util->initArray($items, $defaults);

		$defaults_msg = array("necessary", "max", "min", "just", "diff", "wrong", "pair");
		$msg = $util->initArray($msg, $defaults_msg);

		switch($items["type"]) {

			case "str":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif ($items["just"] != NULL && mb_strlen($value) != $items["just"]) {
					$err_msg = $msg["just"];
				} elseif ($items["max"] != NULL && mb_strlen($value) > $items["max"]) {
					$err_msg = $msg["max"];
				} elseif ($items["min"] != NULL && mb_strlen($value) < $items["min"]) {
					$err_msg = $msg["min"];
				} elseif ($items["diff"] != NULL && current($diff) != $value) {
					$err_msg = $msg["diff"];
				}

				break;

			case "int":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif ($value != "") {
					if ( ! preg_match("/^\d*$/", $value)) {
						$err_msg = $msg["wrong"];
					} elseif ($items["max"] != NULL && intval($value) > $items["max"]) {
						$err_msg = $msg["max"];
					} elseif ($items["min"] != NULL && intval($value) < $items["min"]) {
						$err_msg = $msg["min"];
					} elseif ($items["diff"] != NULL && current($diff) != $value) {
						$err_msg = $msg["diff"];
					}
				}

				break;

			case "number":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif($value != "") {
					if ( ! preg_match("/^\d*$/", $value)) {
						$err_msg = $msg["wrong"];
					} elseif ($items["just"] != NULL && mb_strlen($value) != $items["just"]) {
						$err_msg = $msg["just"];
					} elseif ($items["max"] != NULL && mb_strlen($value) > $items["max"]) {
						$err_msg = $msg["max"];
					} elseif ($items["min"] != NULL && mb_strlen($value) < $items["min"]) {
						$err_msg = $msg["min"];
					} elseif ($items["diff"] != NULL && current($diff) != $value) {
						$err_msg = $msg["diff"];
					}
				}

				break;

			case "float":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif ($value != "") {
					if ( ! preg_match("/^-?(\d|\.)*$/", $value)) {
						$err_msg = $msg["wrong"];
					} elseif ( substr_count($value, ".") > 1 ) {
						$err_msg = $msg["wrong"];
					} elseif ($items["max"] != NULL && floatval($value) > $items["max"]) {
						$err_msg = $msg["max"];
					} elseif ($items["min"] != NULL && floatval($value) < $items["min"]) {
						$err_msg = $msg["min"];
					} elseif ($items["diff"] != NULL && current($diff) != $value) {
						$err_msg = $msg["diff"];
					}
				}

				break;

			case "alpha":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif ($value != "") {
					if( ! preg_match("/^([a-zA-Z]|\n|\n\r|\r)+$/", $value)) {
						$err_msg = $msg["wrong"];
					} elseif ($items["max"] != NULL && strlen($value) > $items["max"]) {
						$err_msg = $msg["max"];
					} elseif ($items["min"] != NULL && strlen($value) < $items["min"]) {
						$err_msg = $msg["min"];
					} elseif ($items["diff"] != NULL && current($diff) != $value) {
						$err_msg = $msg["diff"];
					}
				}

				break;

			case "alphanumeric":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif ($value != "") {
					if ( ! preg_match("/^([a-zA-Z0-9])+$/", $value)) {
						$err_msg = $msg["wrong"];
					} elseif ($items["just"] != NULL && mb_strlen($value) != $items["just"]) {
						$err_msg = $msg["just"];
					} elseif ($items["max"] != NULL && mb_strlen($value) > $items["max"]) {
						$err_msg = $msg["max"];
					} elseif ($items["min"] != NULL && mb_strlen($value) < $items["min"]) {
						$err_msg = $msg["min"];
					} elseif ($items["diff"] != NULL && current($diff) != $value) {
						$err_msg = $msg["diff"];
					}
				}

				break;

			case "singlebyte":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif ($value != "") {
					if ( ! preg_match( "/^([!-~]| |\n|\n\r|\r)+$/", $value)) {
						$err_msg = $msg["wrong"];
					} elseif ($items["max"] != NULL && strlen($value) > $items["max"]) {
						$err_msg = $msg["max"];
					} elseif ($items["min"] != NULL && strlen($value) < $items["min"]) {
						$err_msg = $msg["min"];
					} elseif ($items["diff"] != NULL && current($diff) != $value) {
						$err_msg = $msg["diff"];
					}
				}

				break;

			case "hira":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif ($value != "") {
					if ( ! $util->checkZenHira($value)) {
						$err_msg = $msg["wrong"];
					} elseif ($items["just"] != NULL && mb_strlen($value) != $items["just"]) {
						$err_msg = $msg["just"];
					} elseif ($items["max"] != NULL && mb_strlen($value) > $items["max"]) {
						$err_msg = $msg["max"];
					} elseif ($items["min"] != NULL && mb_strlen($value) < $items["min"]) {
						$err_msg = $msg["min"];
					} elseif ($items["diff"] != NULL && current($diff) != $value) {
						$err_msg = $msg["diff"];
					}
				}

				break;

			case "kana":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif ($value != "") {
					if( ! $util->checkZenKana($value)) {
						$err_msg = $msg["wrong"];
					} elseif ($items["just"] != NULL && mb_strlen($value) != $items["just"]) {
						$err_msg = $msg["just"];
					} elseif ($items["max"] != NULL && mb_strlen($value) > $items["max"]) {
						$err_msg = $msg["max"];
					} elseif ($items["min"] != NULL && mb_strlen($value) < $items["min"]) {
						$err_msg = $msg["min"];
					} elseif ($items["diff"] != NULL && current($diff) != $value) {
						$err_msg = $msg["diff"];
					}
				}

				break;

			case "textarea":

				$tmp_val = preg_replace("/(\r\n|\r|\n)/", "", $value);
				if ($items["necessary"] != NULL && $tmp_val == "") {
					$err_msg = $msg["necessary"];
				} elseif ($items["just"] != NULL && mb_strlen($value) != $items["just"]) {
					$err_msg = $msg["just"];
				} elseif ($items["max"] != NULL && mb_strlen($tmp_val) > $items["max"]) {
					$err_msg = $msg["max"];
				} elseif ($items["min"] != NULL && mb_strlen($tmp_val) < $items["min"]) {
					$err_msg = $msg["min"];
				} elseif ($items["diff"] != NULL && current($diff) != $value) {
					$err_msg = $msg["diff"];
				}

				break;

			case "regex":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif($value != "") {
					if ( ! mb_ereg($items["regex"], $value)) {
						$err_msg = $msg["wrong"];
					} elseif ($items["just"] != NULL && mb_strlen($value) != $items["just"]) {
						$err_msg = $msg["just"];
					} elseif ($items["max"] != NULL && mb_strlen($value) > $items["max"]) {
						$err_msg = $msg["max"];
					} elseif ($items["min"] != NULL && mb_strlen($value) < $items["min"]) {
						$err_msg = $msg["min"];
					} elseif ($items["diff"] != NULL && $diff != $value) {
						$err_msg = $msg["diff"];
					}
				}

				break;

			case "list":

				if ($value == "") {
					if ($items["necessary"] != NULL) {
						$err_msg = $msg["necessary"];
					}
				} else {
					if ($items["list"] != NULL && is_array($items["list"])) {
						if( ! isset($items["list"][$value])) {
							$err_msg = $msg["wrong"];
						}
					} elseif ($items["max"] != NULL && $items["min"] != NULL) {
						if (false === array_search($value, range($items["min"], $items["max"]))) {
							$err_msg = $msg["wrong"];
						}
					}
				}

				break;

			case "list_optgroup":

				if ($value == "") {
					if ($items["necessary"] != NULL) {
						$err_msg = $msg["necessary"];
					}
				} else {
					if ($items["list"] != NULL && is_array($items["list"])) {
						$flag = true;
						foreach($items["list"] as $list) {
							if(isset($list["options"][$value])) {
								$flag = false;
								break;
							}
						}
						if ($flag) {
							$err_msg = $msg["wrong"];
						}
					} elseif ($items["max"] != NULL && $items["min"] != NULL) {
						if (false === array_search($value, range($items["min"], $items["max"]))) {
							$err_msg = $msg["wrong"];
						}
					}
				}

				break;



			case "array":

				if (empty($value)) {
					if ($items["necessary"] != NULL) {
						$err_msg = $msg["necessary"];
					}
				} else {

					if( $items["list"] != NULL) {

						foreach ($value as $arr_val) {

							if ( ! isset($items["list"][$arr_val])) {
								$err_msg = $msg["wrong"];
								break;
							}
						}

					} elseif ($items["max"] != NULL && $items["min"] != NULL) {

						$err_flg = 0;
						foreach ($value as $arr_val) {
							if ($arr_val > $items["max"] || $arr_val < $items["min"]) {
								$err_msg = $msg["wrong"];
								break;
							}
						}
					}
				}

				break;

			case "tel":

				if ($items["join"] != NULL && is_array($items["join"]) && count($items["join"]) == 3) {

					$tel = $value;
					if ($items["necessary"] != NULL && ($tel[0] == "" || $tel[1] == "" || $tel[2] == "")) {
						$err_msg = $msg["necessary"];
					} elseif ($tel[0] != "" || $tel[1] != "" || $tel[2] != "") {
						if ( ! preg_match("/^\d*$/", $tel[0]) || ! preg_match("/^\d*$/", $tel[1]) || ! preg_match("/^\d*$/", $tel[2])) {
							$err_msg = $msg["wrong"];
						} elseif (count($tel) == 3 && ! $util->checkTelephoneStrict($tel["0"], $tel["1"], $tel["2"])) {
							$err_msg = $msg["wrong"];
						}
					}
				} else {

					if ($value == "") {
						if ($items["necessary"] != NULL) {
							$err_msg = $msg["necessary"];
						}
					} else {

						$tel = explode("-", $value);
						if (count($tel) != 3) {
							if (! $util->checkTelephoneStrict($tel["0"])) {
								$err_msg = $msg["wrong"];
							}
						} elseif ( ! preg_match("/^\d*$/", $tel[0]) || ! preg_match("/^\d*$/", $tel[1]) || ! preg_match("/^\d*$/", $tel[2])) {
							$err_msg = $msg["wrong"];
						} elseif (! $util->checkTelephoneStrict($tel["0"], $tel["1"], $tel["2"])) {
							$err_msg = $msg["wrong"];
						}
					}
				}

				break;

			case "zip":

				if ($items["join"] != NULL && is_array($items["join"]) && count($items["join"]) == 2) {

					$zip = $value;
					if ($items["necessary"] != NULL && ( $zip[0] == "" || $zip[1] == "")) {
						$err_msg = $msg["necessary"];
					} elseif ($zip[0] != "" || $zip[1] != "") {
						if (count($zip) == 2 && ! $util->checkZip($zip[0], $zip[1])) {
							$err_msg = $msg["wrong"];
						}
					}
				} else {

					if ($value == "") {
						if ($items["necessary"] != NULL) {
							$err_msg = $msg["necessary"];
						}
					} else {

						$zip = explode("-", $value);
						if (count($zip) != 2) {
							$err_msg = $msg["wrong"];
						} elseif ( ! $util->checkZip($zip[0], $zip[1])) {
							$err_msg = $msg["wrong"];
						}
					}
				}

				break;

			case "pref":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif ($value != "") {
					$err_pref = 0;
					for ($i=1;$i<=47;$i++) {
						if ($i == $value) {
							$err_pref = 1;
						}
					}
					if ($err_pref == 0) {
						$err_msg = $msg["wrong"];
					}
				}

				break;

			case "email":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif ($value != "") {
					if ( ! $util->checkMailAddress($value)) {
						$err_msg = $msg["wrong"];
					} elseif ($items["just"] != NULL && mb_strlen($value) != $items["just"]) {
						$err_msg = $msg["just"];
					} elseif ($items["max"] != NULL && mb_strlen($value) > $items["max"]) {
						$err_msg = $msg["max"];
					} elseif ($items["min"] != NULL && mb_strlen($value) < $items["min"]) {
						$err_msg = $msg["min"];
					} elseif ($items["diff"] != NULL && current($diff) != $value) {
						$err_msg = $msg["diff"];
					}
				}

				break;

			case "pc_email":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif ($value != "") {
					if ( ! $util->checkMailAddressPc($value)) {
						$err_msg = $msg["wrong"];
					} elseif ($items["just"] != NULL && mb_strlen($value) != $items["just"]) {
						$err_msg = $msg["just"];
					} elseif ($items["max"] != NULL && mb_strlen($value) > $items["max"]) {
						$err_msg = $msg["max"];
					} elseif ($items["min"] != NULL && mb_strlen($value) < $items["min"]) {
						$err_msg = $msg["min"];
					} elseif ($items["diff"] != NULL && current($diff) != $value) {
						$err_msg = $msg["diff"];
					}
				}

				break;

			case "url":

				if ($items["necessary"] != NULL && $value == "") {
					$err_msg = $msg["necessary"];
				} elseif ($value != "") {
					if ( ! $util->checkHttpUrl($value)) {
						$err_msg = $msg["wrong"];
					} elseif ($items["just"] != NULL && mb_strlen($value) != $items["just"]) {
						$err_msg = $msg["just"];
					} elseif ($items["max"] != NULL && mb_strlen($value) > $items["max"]) {
						$err_msg = $msg["max"];
					} elseif ($items["min"] != NULL && mb_strlen($value) < $items["min"]) {
						$err_msg = $msg["min"];
					} elseif ($items["diff"] != NULL && current($diff) != $value) {
						$err_msg = $msg["diff"];
					}
				}

				break;

			case "date":

				if ($items["join"] != NULL && is_array($items["join"]) && count($items["join"]) == 3) {
					$date = $value;
				} else {
					$date = str_replace("/", "-", $value);
					$date = explode("-", $date);
				}
				if ($items["necessary"] != NULL && ( $date[0] == "" || $date[1] == "" || $date[2] == "")) {
					$err_msg = $msg["necessary"];
				} elseif ($date[0] != "" || $date[1] != "" || $date[2] != "") {
					if ($date[0] == "" || $date[1] == "" || $date[2] == "") {
						$err_msg = $msg["necessary"];
					} elseif ( ! preg_match("/^\d+$/", $date[1].$date[2].$date[0])) {
						$err_msg = $msg["wrong"];
					} elseif ( ! checkdate($date[1], $date[2], $date[0])) {
						$err_msg = $msg["wrong"];
					} elseif ($items["min"] == "now" && sprintf(date("Ymd")) > sprintf("%04d%02d%02d", $date[0], $date[1], $date[2])) {
						$err_msg = $msg["min"];
					} elseif ($items["min"] != NULL && $items["min"] != "now" && (($date[0] < $items["min"]) || ($date[1] < 1) || ($date[2] < 1))) {
						$err_msg = $msg["min"];
					} elseif ($items["max"] == "now" && sprintf(date("Ymd")) < sprintf("%04d%02d%02d", $date[0], $date[1], $date[2])) {
						$err_msg = $msg["max"];
					} elseif ($items["max"] != NULL && $items["max"] != "now" && (($date[0] > $items["max"]) || ($date[1] > 12) || ($date[2] > 31))) {
						$err_msg = $msg["max"];
					} elseif ($items["option"] != NULL) {
						$from_date = explode("-", $items["option"]);
						$from = sprintf("%04d", $from_date[0]).sprintf("%02d", $from_date[1]).sprintf("%02d", $from_date[2]);
						$to = sprintf("%04d", $date[0]).sprintf("%02d", $date[1]).sprintf("%02d", $date[2]);
						if ($to < $from) {
							$err_msg = $msg["wrong"];
						}
					}
				}

				break;

			case "date02":

				if ($items["join"] != NULL && is_array($items["join"]) && count($items["join"]) == 2) {
					$date = $value;
				} else {
					$date = str_replace("/", "-", $value);
					$date = explode("-", $date);
				}

				if ($items["necessary"] != NULL && ($date[0] == "" || $date[1] == "")) {
					$err_msg = $msg["necessary"];
				} elseif ($date[0] != "" || $date[1] != "") {
					if ($date[0] == "" || $date[1] == "") {
						$err_msg = $msg["necessary"];
					} elseif ( ! preg_match("/^\d+$/", $date[1].$date[0])) {
						$err_msg = $msg["wrong"];
					} elseif ( ! checkdate($date[1], "01", $date[0])) {
						$err_msg = $msg["wrong"];
					} elseif ($items["min"] == "now" && sprintf(date("Ym")) > sprintf("%04d%02d", $date[0], $date[1])) {
						$err_msg = $msg["min"];
					} elseif ($items["min"] != NULL && $items["min"] != "now" && (($date[0] < $items["min"]) || ($date[1] < 1))) {
						$err_msg = $msg["min"];
					} elseif ($items["max"] == "now" && sprintf(date("Ym")) < sprintf("%04d%02d", $date[0], $date[1])) {
						$err_msg = $msg["max"];
					} elseif ($items["max"] != NULL && $items["max"] != "now" && (($date[0] > $items["max"]) || ($date[1] > 12))) {
						$err_msg = $msg["max"];
					} elseif ($items["option"] != NULL) {
						$from_date = explode("-", $items["option"]);
						$from = sprintf("%04d", $from_date[0]).sprintf("%02d", $from_date[1]);
						$to = sprintf("%04d", $date[0]).sprintf("%02d", $date[1]);
						if ($to < $from) {
							$err_msg = $msg["wrong"];
						}
					}
				}

				break;
			case "datetime":

				$datetime = explode(" ",$value);

				$date = str_replace("/","-",$datetime[0]);
				$date = explode("-",$date);

				if ($datetime[0] != "" && count($date) != 3) {
					$err_msg = $msg["wrong"];
				} elseif ($items["necessary"] != NULL && ($date[0] == "" || $date[1] == "" || $date[2] == "")) {
					$err_msg = $msg["necessary"];
				} elseif ($datetime[0] != "" && ($date[0] != "" || $date[1] != "" || $date[2] != "")) {

					if ($date[0] == "" || $date[1] == "" || $date[2] == "") {
						$err_msg = $msg["wrong"];
					} elseif ( ! checkdate($date[1], $date[2], $date[0])) {
						$err_msg = $msg["wrong"];
					} elseif ($items["min"] == "now" && sprintf(date("Ymd")) > sprintf("%04d%02d%02d",$date[0],$date[1],$date[2])) {
						$err_msg = $msg["wrong"];
					} elseif ($items["min"] != NULL && $items["min"] != "now" && (($date[0] < $items["min"]) || ($date[1] < 1) || ($date[2] < 1))) {
						$err_msg = $msg["wrong"];
					} elseif ($items["max"] == "now" && sprintf(date("Ymd")) < sprintf("%04d%02d%02d",$date[0],$date[1],$date[2])) {
						$err_msg = $msg["wrong"];
					} elseif ($items["max"] != NULL && $items["max"] != "now" && (($date[0] > $items["max"]) || ($date[1] > 12) || ($date[2] > 31))) {
						$err_msg = $msg["wrong"];
					} elseif ($items["option"] != NULL) {
						$from_date = explode("-",$items["option"]);
						$from = sprintf("%04d",$from_date[0]).sprintf("%02d",$from_date[1]).sprintf("%02d",$from_date[2]);
						$to = sprintf("%04d",$date[0]).sprintf("%02d",$date[1]).sprintf("%02d",$date[2]);
						if ($to < $from) {
							$err_msg = $msg["wrong"];
						}
					}
				}

				//時間
				if ($err_msg == "" && ! empty($datetime[1])) {

					$time = explode(":",$datetime[1]);

					if (count($time) != 3) {
						$err_msg = $msg["wrong"];
					} else {

						//時
						if ( ! preg_match("/^\d{1,2}$/",$time[0])) {
							$err_msg = $msg["wrong"];
						} elseif (0 > $time[0] || 23 < $time[0]) {
							$err_msg = $msg["wrong"];
						}

						//分
						if ( ! preg_match("/^\d{1,2}$/",$time[1])) {
							$err_msg = $msg["wrong"];
						} elseif (0 > $time[1] || 59 < $time[1]) {
							$err_msg = $msg["wrong"];
						}

						//秒
						if ( ! preg_match("/^\d{1,2}$/",$time[2])) {
							$err_msg = $msg["wrong"];
						} elseif (0 > $time[2] || 59 < $time[2]) {
							$err_msg = $msg["wrong"];
						}
					}
				}

				break;

			case "privacy":

				if ($value != 1) {
					$err_msg = $msg["wrong"];
				}

				break;
		}

		if(isset($items["pair"]) && $items["pair"] == 1 && count($diff) > 0 ){
			if($value == "" && $diff[0] == ""){
				$err_msg = $msg["pair"];
			}
		}else if(isset($items["pair"]) && $items["pair"] == 2 && count($diff) > 0 ){
			if($value == "" && $diff[0] != ""){
				$err_msg = $msg["pair"];
			}
		}

		return $err_msg;
	}
}
?>