<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2013/06/26
 * Update   : 2013/06/26
 * Description : 共通DBクラス
 **************************************************************************/

class CoMysql extends CoDb{

	var $link;

	//DBへのコネクト
	function CoMysql($options) {

		$defaults = array(
		    'username' => "",
		    'password' => "",
		    'hostspec' => "",
		    'database' => "",
			'sqlport' => "",
			'table' => "",
			'stringcode' => "utf8",
		);
		$options = array_merge($defaults, $options);
		if ($options["sqlport"] != "") {
		    $options["hostspec"] .= ":".$options["sqlport"];
		}

		$this->link = mysql_connect($options["hostspec"], $options["username"], $options["password"]);
		if ( ! $this->link) {
			trigger_error('接続できませんでした', E_USER_ERROR);
			exit;
		}

		$result = mysql_select_db($options["database"], $this->link);
		if ( ! $result) {
			trigger_error('DBを設定できません', E_USER_ERROR);
			exit;
		}

		if( ! empty($options["stringcode"])) {
			$this->query("SET NAMES ". $this->escape($options["stringcode"]));
		}

		$this->table = $options["table"];
	}

	//実行
	function query($query) {

		$result = mysql_query($query, $this->link);
		if ( ! $result) {
			trigger_error('クエリを実行できません：'.$query, E_USER_ERROR);
			exit;
		}

		return $result;
	}

	function select($query) {

		$values = array();

		$result = $this->query($query);
		if (!empty($result)) {
			while ($rows = mysql_fetch_assoc($result)) {
				$values[] = $rows;
			}
		}
		return $values;
	}

	function escape($str) {
		return mysql_real_escape_string($str);
	}
}


?>