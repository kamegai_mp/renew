<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2013/06/26
 * Update   : 2013/06/26
 * Description : フレームクラス専用 共通メール送信クラス
 **************************************************************************/

class CoMail{

	//　テンプレートを読み込んでメールを送信する
	function send ($options) {

		mb_internal_encoding("UTF-8");

		$template = new CoTemplate($options["template_path"]);
		$template->assign("param", $options["param"]);

		//テンプレートファイルを読み込む
		$body = $template->fetch($options["file"]);

		//テンプレートの1行目のタイトルを抜き出す。
		preg_match("/^[^\n]*\n/", $body, $subject);
		$body = preg_replace("/^[^\n]*\n/", "", $body);

		$util = new CoUtil();
		$line = $util->decodeFormValues($body);
		$len = 0;
		$n = 100;
		$message = "";

		for ($i=0;$i<mb_strlen($line);$i+=$len) {
			for ($j=1;$j<=$n;$j++) {
				$wk = mb_substr($line, $i, $j);
				if (strlen($wk) >= $n || preg_match('/\n/', $wk)) {
					break;
				}
			}
			$len = mb_strlen($wk);
			if ( ! preg_match('/\n/', $wk)) {
				$message .= $wk."\n";
			} else {
				$message .= $wk;
			}
		}

		$options["from"] = "FROM: ". $options["from"];
		$message = preg_replace("/(\r\n|\r)/", "\n", $message);

		//CCがあれば設定する
		if (isset($options["cc"]) && is_array($options["cc"]) && count($options["cc"]) > 0) {

			$cc_header = "\nCc: ";
			$cc_mail = "";

			foreach ($options["cc"] as $cc_val) {
				if (trim($cc_val) != "") {
					$cc_mail .= trim($cc_val) .",";
				}
			}

			if ($cc_mail != "") {
				$options["from"] .= $cc_header . substr($cc_mail, 0, -1);
			}
		}

		if (empty($options["return_path"])) {

			//	メールを送信する
			if ( ! mb_send_mail($options["to"], trim($subject[0]), $message, $options["from"])) {
				trigger_error("Error function:sendMail to:".$options["to"]." sub:".$subject[0]." from:".$options["from"], E_USER_ERROR);
			}
		}else{

			//	メールを送信する
			if ( ! mb_send_mail($options["to"], trim($subject[0]), $message, $options["from"], "-f ".$options["return_path"])) {
				trigger_error("Error function:sendMail to:".$options["to"]." sub:".$subject[0]." from:".$options["from"], E_USER_ERROR);
			}
		}

		return true;
	}

	function sendPhpMailer($options){

		mb_internal_encoding("UTF-8");

		$template = new CoTemplate($options["template_path"]);
		$template->assign("param", $options["param"]);

		//テンプレートファイルを読み込む
		$body = $template->fetch($options["file"]);

		//テンプレートの1行目のタイトルを抜き出す。
		preg_match("/^[^\n]*\n/", $body, $subject);
		$body = preg_replace("/^[^\n]*\n/", "", $body);

		require_once(LIBRARY_PATH."PHPMailer/PHPMailerAutoload.php");
		$mail = new PHPMailer;

		$mail->Encoding = "7bit";
		$mail->CharSet = 'ISO-2022-JP';

		// subject
		$mail->Subject = mb_encode_mimeheader(mb_convert_encoding(trim($subject[0]), "JIS", "UTF-8"));

		// body
		$util = new CoUtil();
		$mail->Body = mb_convert_encoding($util->decodeFormValues($body), "JIS", "UTF-8");

		// from
		$mail->setFrom($options["from"], '');
		$mail->addReplyTo($options["from"], '');

		// to
		$mail->addAddress($options["to"], '');

		// file
		if ($options["send_file"] == 1) {
			if (! empty($options["param"]["vars"]["resume_file"]) && is_file(IMAGE_PATH.$options["param"]["vars"]["resume_file"])){
				$mail->addAttachment(IMAGE_PATH.$options["param"]["vars"]["resume_file"]);
			}
			if (! empty($options["param"]["vars"]["curriculum"]) && is_file(IMAGE_PATH.$options["param"]["vars"]["curriculum"])){
				$mail->addAttachment(IMAGE_PATH.$options["param"]["vars"]["curriculum"]);
			}
			if (! empty($options["param"]["vars"]["curriculum02"]) && is_file(IMAGE_PATH.$options["param"]["vars"]["curriculum02"])){
				$mail->addAttachment(IMAGE_PATH.$options["param"]["vars"]["curriculum02"]);
			}
			if (! empty($options["param"]["vars"]["curriculum03"]) && is_file(IMAGE_PATH.$options["param"]["vars"]["curriculum03"])){
				$mail->addAttachment(IMAGE_PATH.$options["param"]["vars"]["curriculum03"]);
			}
			if (! empty($options["param"]["vars"]["work"]) && is_file(IMAGE_PATH.$options["param"]["vars"]["work"])){
				$mail->addAttachment(IMAGE_PATH.$options["param"]["vars"]["work"]);
			}
		}

		if (!$mail->send()) {
			trigger_error("Error function:sendMail :".$mail->ErrorInfo, E_USER_ERROR);
		}
		return true;
	}
}
?>