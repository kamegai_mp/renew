<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2010/05/20
 * Update   : 2011/06/09
 * Description : HTML書き出し用共通関数
 **************************************************************************/

class CoHtml{

	/*.......................
		ラジオボックスのタグを生成する

		options		リスト
		selected	選択済み
		name		名前
		top			タグの前に付ける文字列
		bottom		タグの最後に付ける文字列
		labeloff	ラベルを付けない場合にフラグを立てる
		onClick		onClickタグを使用する場合
	........................*/
	function radios($params) {

		if (empty($params["options"]) || empty($params["name"])) {
			return;
		}

	    $_output = "";

		foreach ($params["options"] as $key => $val) {

			if ( ! empty($params["top"])) {
				$_output .=  $params["top"];
			}

			if (empty($params["labeloff"])) {
				$_output .= '<label';
				if ($key == $params["selected"] && isset($params["selected_class"])) {
					$_output .= ' class="'. $params["selected_class"] .'"';
				}
				$_output .= '>';
			}

			$_output .= '<input type="radio" name="'. $params["name"] .'" value="'. $key .'"';

			if ( ! empty($params["onClick"])) {
				$_output .= ' onClick="'. $params["onClick"] .'"';
			}

			if(isset($params["selected"])) {
				if ($key == $params["selected"]) {
					$_output .= ' checked="checked"';
				}
			}

		    $_output .= ' />';

		    $_output .= $val;

			if (empty($params["labeloff"])) {
				$_output .= '</label>';
			}

			if ( ! empty($params["bottom"])) {
				$_output .=  $params["bottom"];
			}

			$_output .= "\n";
		}

		print $_output;
	}


	/*.......................
		チェックボックスのタグを生成する

		options		リスト
		selected	選択済み
		name		名前
		top			タグの前に付ける文字列
		bottom		タグの最後に付ける文字列
		labeloff	ラベルを付けない場合にフラグを立てる
		onClick		onClickタグを使用する場合
	........................*/
	function checkboxes($params) {

		if (empty($params["options"]) || empty($params["name"])) {
			return;
		}

	    $_output = "";

		foreach ($params["options"] as $key => $val) {

			if ( ! empty($params["top"])) {
				$_output .=  $params["top"];
			}

			$_output .= '<input type="checkbox" name="'. $params["name"] .'[]" value="'. $key .'"';

			if (empty($params["labeloff"])) {
				$_output .= ' id="'. $params["name"] . $key .'"';
			}

			if ( ! empty($params["onClick"])) {
				$_output .= ' onClick="'. $params["onClick"] .'"';
			}

			if(isset($params["selected"])) {
				if (false !== array_search($key,$params["selected"])) {
				    $_output .= ' checked="checked"';
				}
			}

		    $_output .= ' />';

			if (empty($params["labeloff"])) {
				$_output .= '<label for="'. $params["name"].$key .'">';
			}

		    $_output .= $val;

			if (empty($params["labeloff"])) {
				$_output .= '</label>';
			}

			if ( ! empty($params["bottom"])) {
				$_output .=  $params["bottom"];
			}

			$_output .= "\n";
		}

		print $_output;
	}


	/*.......................
		プルダウンタグを生成する

		options		リスト
		selected	選択済み
	........................*/
	function options($params) {

		if (empty($params["options"])) {
			return;
		}

	    $_output = "";

		foreach ($params["options"] as $key => $val) {

			if (empty($params["optgroup"])) {

				$_output .= '<option value="'. $key .'"';

				if(isset($params["selected"])) {
					if ($key==$params["selected"]) {
					    $_output .= ' selected';
					}
				}

			    $_output .= '>'. $val ."</option>";
				$_output .= "\n";

			} else {

				$_output .= '<optgroup label="'. $val["label"] .'">';

				foreach ($val["options"] as $key2 => $val2) {
					$_output .= '<option value="'. $key2 .'"';

					if(isset($params["selected"])) {
						if ($key2==$params["selected"]) {
						    $_output .= ' selected';
						}
					}

				    $_output .= '>'. $val2 ."</option>";
					$_output .= "\n";
				}

				$_output .= '</optgroup>';
				$_output .= "\n";
			}
		}

		print $_output;
	}


	/*.......................
		hiddenタグを生成する

		options		リスト
		denys		無視リスト
	........................*/
	function hidden($params) {

		if (empty($params["options"])) {
			return;
		}

	    $_output = "";

		foreach ($params["options"] as $key => $val) {

			if(! empty($params["denys"]) && false !== array_search($key, $params["denys"])) {
				continue;
			}

			if (is_array($val)) {
				foreach ($val as $key2 => $val2) {
					if (is_array($val2)) {
						foreach ($val2 as $key3 => $val3) {
							$_output .= '<input type="hidden" name="'. $key .'['. $key2 .']['. $key3 .']" value="'. $val3 .'" />';
							$_output .= "\n";
						}
					} else {
						$_output .= '<input type="hidden" name="'. $key .'['. $key2 .']" value="'. $val2 .'" />';
						$_output .= "\n";
					}
				}
			} else {
				$_output .= '<input type="hidden" name="'. $key .'" value="'. $val .'" />';
				$_output .= "\n";
			}
		}

		print $_output;
	}

	/*..................................
		プルダウンの表示に使用する

		配列から対象キーの値を表示する
	...................................*/
	function showList($value ,$list) {

		if ($value != "") {
			if (isset($list[$value])) {
				print $list[$value];
			}
		}
	}

	/*..................................
		ラベル付きのプルダウンの表示に使用する

		配列から対象キーの値を表示する
	...................................*/
	function showListOptgroup($value ,$list) {

		if ($value != "") {
			foreach($list as $item) {
				if (isset($item["options"][$value])) {
					print $item["options"][$value];
					break;
				}
			}
		}
	}


	/*..................................
		チェックボックスの表示に使用する

		配列から対象キーの値を表示する
	...................................*/
	function showMultiList($values ,$list) {

		if ( ! empty($values)) {
			foreach ($values as $val) {
				if (isset($list[$val])) {
					print $list[$val]."<br />";
				}
			}
		}
	}
}
?>