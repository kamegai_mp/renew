<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2012/11/06
 * Update   : 2012/11/06
 * Description : ユニットテストクラス
 **************************************************************************/

class CoUnitTest{

	var $page;
	var $options;

	function CoUnitTest($options = array()) {

		$default = array(
			"pagekey" => "p",
			"url" => null,
			"pagelist" => array()
		);
		$this->options = array_merge($default, $options);

		// ページを設定
		if (isset($_GET[$this->options["pagekey"]])) {
			$this->page = $_GET[$this->options["pagekey"]];
		} else {
			$this->page = "";
		}
	}

	// 一覧ページを表示する
	function viewListPage() {

		print $this->header();
		print "<ul>";

		if (false === strpos($this->options["url"], "?")) {
			$url = $this->options["url"] ."?";
		} else {
			$url = $this->options["url"] ."&";
		}

		foreach($this->options["pagelist"] as $key => $label) {
			print '<li><a href="'. $url . $this->options["pagekey"] ."=". $key .'" target="_blank">'. $label .'</a></li>';
		}

		print "</ul>";
		print $this->footer();
	}

	// 内容をテーブル表示する
	function viewTable($vars, $errmsg) {

		print $this->header();
		print "<table border='1'>";

		foreach($vars as $key => $val) {

			$label_tag = "<th style='background:#d4d4d4'>項目</th>";
			$value_tag = "<th style='background:#f6f6f6'>入力値</th>";
			$errmsg_tag = "<th>エラー</th>";
			foreach($val as $name => $value) {
				$label_tag .= "<th style='background:#d4d4d4'>". $name ."</th>";

				if ($value != "") {
					$value_tag .= "<td style='background:#f6f6f6'>". nl2br($value) ."</td>";
				} else {
					$value_tag .= "<td style='background:#f6f6f6'>&nbsp</td>";
				}

				if (isset($errmsg[$key][$name])) {
					$errmsg_tag .= "<td style='color:red;'>". $errmsg[$key][$name] ."</td>";
				} else {
					$errmsg_tag .= "<td>&nbsp</td>";
				}
			}

			print "<tr>". $label_tag ."</tr>";
			print "<tr>". $value_tag ."</tr>";
			print "<tr>". $errmsg_tag ."</tr>";
		}

		print "</table>";
		print $this->footer();
	}

	function header() {

		return <<<DOC
		<html>
		<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		</head>
		<body>
DOC;
	}

	function footer() {

		return <<<DOC
		</body>
		</html>
DOC;
	}
}
?>