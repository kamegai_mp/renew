<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2010/05/20
 * Update   : 今日
 * Description : smartyが使用できない場合に代用として使うテンプレートクラス
 **************************************************************************/

class CoTemplate{

	var $path;
	var $element = array();

	function CoTemplate($path) {
		$this->path = $path;
	}

	//引数：key　変数名　value　変数の値
	function assign($key ,$value) {
		$this->element[$key] = $value;
	}

	function assignAll($data) {
		foreach ($data as $key => $val) {
			$this->assign($key, $val);
		}
	}

	function display($file, $data = null) {

		if ($data) {
			$this->assignAll($data);
		}

		foreach ($this->element as $key => $val) {
			${$key} = $val;
		}

		include($this->path . $file);
	}

	function getDisplay($file, $data = null) {

		if ($data) {
			$this->assignAll($data);
		}

		$items = array(
			"tmpl" => $this->path . $file,
			"data" => $this->element,
		);

		return $items;
	}

	function fetch ($file) {

		foreach ($this->element as $key => $val) {
			${$key} = $val;
		}

		ob_start();

		include($this->path . $file);

		$buff = ob_get_contents();

		ob_end_clean();

		return $buff;
	}
}
?>