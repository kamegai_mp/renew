<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2010/05/20
 * Update   : 2010/12/28
 * Description : 共通DBクラス
 **************************************************************************/

class CoPgsql extends CoDb{

	var $link;

	//DBへのコネクト
	function CoPgsql($options) {

		$defaults = array(
		    'username' => "",
		    'password' => "",
		    'hostspec' => "",
		    'database' => "",
			'sqlport' => "",
			'table' => "",
			'stringcode' => "utf8",
		);
		$options = array_merge($defaults, $options);

		$connection = "host=". $options["hostspec"] ." user=". $options["username"] ." password=". $options["password"] ." dbname=". $options["database"];
		if ($options["sqlport"] != "") {
			$connection .= " port=". $options["sqlport"];
		}

		$this->link = pg_connect($connection);
		if ( ! $this->link) {
			trigger_error('接続できませんでした', E_USER_ERROR);
			exit;
		}

		if( ! empty($options["stringcode"])) {
			$this->query("SET CLIENT_ENCODING TO '". $this->escape($options["stringcode"])."'");
		}

		$this->table = $options["table"];
	}

	//実行
	function query($query) {

		$result = pg_query($this->link, $query);
		if ( ! $result) {
			trigger_error('クエリを実行できません', E_USER_ERROR);
			exit;
		}
		return $result;
	}

	function escape($str) {
		return pg_escape_string($str);
	}

}


?>