<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2010/05/20
 * Update   : 2010/12/28
 * Description : 共通ファイルクラス
 **************************************************************************/

class CoFile{

	//=================================================================
	//　●ファイル出力
	//=================================================================
	function create ($file, $buff, $options = array()) {

		$default = array(
			"opentype" => "w",
			"chmod" => false,
			"fp" => "",
			"lockflg" => "",
			"closeflg" => "",
		);
		$options = array_merge($default, $options);

		if (empty($options["fp"])) {
			if (false === $fp = fopen($file, $options["opentype"])) {
				trigger_error("Error function:create ファイルを開けません ". $file, E_USER_ERROR);
			}
		} else {
			$fp = $options["fp"];
		}

		if (empty($fp)) {
			trigger_error("Error function:create ファイルポインタが存在しません ". $file, E_USER_ERROR);
		}

		if (empty($options["lockflg"])) {
			flock($fp, LOCK_EX);
		}

		if (false === fwrite($fp, $buff)) {
			trigger_error("Error function:create ファイルに書き込めません ". $file, E_USER_ERROR);
		}

		if (empty($options["lockflg"])) {
			flock($fp, LOCK_UN);
		}

		if (empty($options["closeflg"])) {
			fclose($fp);
		}

		if ($options["chmod"] !== false) {
			@chmod($file, $options["chmod"]);
		}
	}


	//=================================================================
	//　●アップロードファイルのエラーチェック
	//=================================================================
	function check($files, $options = array()) {

		$default = array(
			"size" 		=> 2097152,
			"filetype"	=> "jpg|jpeg|gif",
			"height"	=> "",
			"width"		=> "",
		);
		$options = array_merge($default, $options);

		if ($files['error'] != '4' && $files['error'] != '0') {
			return false;
		}

		if ($files["size"] !== 0) {

//			if ($files['error'] != '0') {
//				return false;
//			}

			//ファイルサイズ
			if ( ! empty($options["size"]) && $files["size"] > $options["size"]) {
				return false;
			}

			//拡張子
			if ( ! preg_match("/\.(". $options["filetype"].")$/i", $files['name'])) {
				return false;
			}

			//画像サイズ
			if ( ! empty($options["height"]) || ! empty($options["width"])) {
				list($width, $height) = @getimagesize($files["tmp_name"]);

				if ( ! empty($options["height"]) && $height > $options["height"]) {
					return false;
				}

				if ( ! empty($options["width"]) && $width > $options["width"]) {
					return false;
				}
			}
		}

		return true;
	}


	//=================================================================
	//　●アップロード予定ファイルを一時保管する
	//	引数：全体値(配列)、元画像変数名、テンポラリ画像変数名、アップロードファイル情報(配列)、移行先パス、オプション
	//	返値：配列
	//=========================================================================
	function up($vars, $files, $options = array()) {

		if ($files["name"]!="") {

			$default = array(
				"filename" => uniqid(),
				"auto_rename" => true,
				"overwrite" => true,
				"resize" => false,
				"ratio" => true,
				"width" => 150,
				"height" => 150,
				"no_zoom" => true,
				"key_name_ori"	=> "uploadfile_ori",
				"key_name_new"	=> "uploadfile_new",
				"up_dir_path"	=> "",
			);
			$options = array_merge($default, $options);

			$vars[$options["key_name_ori"]]  = $files["name"];
			$kaku = CoFile::getExtension($files["name"]);
			$vars[$options["key_name_new"]] = $options["filename"]. strtolower($kaku);

			if (is_file($files["tmp_name"])) {

				$handle = new Upload($files);
				if( ! $handle->uploaded) {
					return false;
				}

				// サイズ画像
//				$handle->allowed = array('image/*'); 					// 画像のみ許可
				$handle->file_overwrite = $options["overwrite"]; 		// 上書き許可
				$handle->file_auto_rename = $options["auto_rename"];	// 自動リネーム禁止
				$handle->file_src_name_body = $options["filename"]; 	// ファイル名
//				$handle->image_resize = $options["resize"];				// リサイズ許可
//				$handle->image_ratio = $options["ratio"];				// 縦横比維持
//				$handle->image_x = $options["width"];					// 横最大値
//				$handle->image_y = $options["height"];					// 縦最大値
//				$handle->image_ratio_no_zoom_in = $options["no_zoom"];	// image_x、image_yより小さいサイズは拡大禁止
				$handle->Process($options["up_dir_path"]."/");
			}
		}

		//一時保存ディレクトリから削除
		CoFile::delBuff($options["up_dir_path"]);

		return $vars;
	}

	//=================================================================
	//	アップロードファイルを本ディレクトリに移行する
	//	引数：ファイル名、一時保存ディレクトリパス、移行先パス
	//	返値：TRUE
	//=================================================================
	function move($file_path_from, $file_path_to) {

		if( ! is_file($file_path_from)) {
			trigger_error("Error function:move ファイルが存在しません ". $file_path_from, E_USER_ERROR);
		}

		//移行先が正しいかチェック
		if( ! is_dir(dirname($file_path_to))) {
			trigger_error("Error function:move 移行先ディレクトリが存在しません ". $file_path_to, E_USER_ERROR);
		}

		//本ディレクトリへ　移行
		if( ! copy( $file_path_from, $file_path_to)) {
			trigger_error("Error function:move ファイルをコピーできません ". $file_path_from ."/". $file_path_to, E_USER_ERROR);
		}

		@chmod($file_path_to, 0777);
	}


	//=================================================================
	//	データ削除の際ディレクトリから不要ファイルを削除する
	//	引数：保存ディレクトリパス、ファイル名
	//	返値：TRUE
	//=================================================================
	function delete($filepath) {
		if (is_file( $filepath)) {
			if ( ! unlink($filepath)) {
				trigger_error("Error function:delete ファイルが削除できません ". $filepath, E_USER_ERROR);
			}
		}
		return true;
	}


	//=================================================================
	//	一時保管先のディレクトリから不要ファイルを削除する
	//	引数：パス
	//	返値：配列
	//=================================================================
	function delBuff($path) {

		if( ! is_dir($path)) {
			trigger_error("Error function:delBuff ディレクトリが存在しません ". $path, E_USER_ERROR);
		}

		//古いデータの削除
		if ($dp = opendir($path)) {
			while (false !== ($filename = readdir($dp))) {
				if ($filename == "." || $filename == "..") {
					continue;
				}
				if (is_file($path.$filename) && filemtime($path.$filename) < strtotime("now -1 day", time())) {

					@unlink($path.$filename);
				}
			}
		}

		return true;
	}

	//=================================================================
	//	ファイル名から拡張子を返す
	//	引数：ファイル名
	//	返値：拡張子名
	//=================================================================
	function getExtension($filename) {

		$kaku = explode(".", $filename);

		if (count($kaku) < 2) {
			trigger_error("Error function:getExtension 拡張子がありません ". $filename, E_USER_ERROR);
		}

		return ".". end($kaku);

	}

	//=================================================================
	//	拡張子を決められた状態に変換する
	//=================================================================
	function convertExtension($kaku) {

		if (preg_match("/^(jpg|jpeg|jpe|jfif)$/i", $kaku)) {
			$kaku = "jpg";
		} elseif (preg_match("/^gif$/i", $kaku)) {
			$kaku = "gif";
		} elseif (preg_match("/^png$/i", $kaku)) {
			$kaku = "png";
		} else {
			return false;
		}

		return $kaku;
	}

	//=================================================================
	//	ファイル名から拡張子削除したファイル名を返す
	//=================================================================
	function DeleteExtension($filename) {

		$newfile = "";
		$file_array = explode(".", $filename);

		if (count($file_array) < 2) {
			trigger_error("Error function:DeleteExtension 拡張子がありません ". $filename, E_USER_ERROR);
		}

		array_pop($file_array);

		return implode("", $file_array);

	}

	//=========================================================================
	//CSVデータを多次元配列で返す
	//=========================================================================
	function getCsv($filepath, $options = array()){

		$default = array(
			"enclosure"			=> '"',
			"separate"			=> ",",
			"length"			=> null,
			"convertStringcode"	=> false,
			"to_Stringcode"		=> "UTF-8",
			"from_Stringcode"	=> "SJIS",
		);
		$options = array_merge($default,$options);

		$data = array();

		$fp = fopen($filepath,'r');
		while($row = CoFile::fgetcsv_reg($fp, $options['length'], $options['separate'], $options['enclosure'])) {

			if ($options['convertStringcode']) {
				foreach ($row as &$val) {
					$val = mb_convert_encoding($val,$options['to_Stringcode'],$options['from_Stringcode']);
				}
			}

			$data[] = $row;
		}
		fclose($fp);

		return $data;
	}

	//=================================================================
	//CSV形式のファイルを配列にして1行読みだす。
	//=================================================================
	function fgetcsv_reg(&$handle, $length = null, $d = ',', $e = '"') {

	    $d = preg_quote($d);
	    $e = preg_quote($e);
	    $line = "";
		$eof = false;

		while ($eof != true && ! feof($handle)) {

			if (empty($length)) {
				$line .= fgets($handle);
			} else {
				$line .= fgets($handle, $length);
			}

			$itemcnt = preg_match_all('/'.$e.'/', $line, $dummy);
	        if ($itemcnt % 2 == 0) {
				$eof = true;
			}
	    }

	    $csv_line = preg_replace('/(?:\r\n|[\r\n])?$/', $d, trim($line));

	    $csv_pattern = '/('. $e .'[^'. $e .']*(?:'. $e . $e .'[^'. $e .']*)*'. $e .'|[^'. $d .']*)'. $d .'/';
	    preg_match_all($csv_pattern, $csv_line, $csv_matches);

	    $csv_data = $csv_matches[1];

	    for ($i=0;$i<count($csv_data);$i++) {
	        $csv_data[$i] = preg_replace('/^'. $e .'(.*)'. $e .'$/s', '$1', $csv_data[$i]);
	        $csv_data[$i] = str_replace($e . $e, $e, $csv_data[$i]);
	    }

		if (empty($line)) {
			return false;
		}

	    return $csv_data;
	}

	//=================================================================
	//	配列データをCSV形式に変換する関数
	//=================================================================
	function convertCsv($data, $options = array()) {

		$default = array(
			"enclosure" => "\"",
			"separate" => ",",
			"break" => "\n",
			"header" => array(),
			"csvpath" => "",
			"stringcode" => "SJIS",
			"stringconvert" => "1",
		);
		$options = array_merge($default, $options);

		$csvdata = "";
		if ( ! is_array($data)) {
			return false;
		}

		//ヘッダー部分を生成
		if ( ! empty($options["header"]) && ($options["csvpath"] == "" || ($options["csvpath"] != "" && ! is_file($options["csvpath"])))) {

			$i=0;
			foreach ($options["header"] as $val) {

				if ($i > 0) {
					$csvdata .= $options["separate"];
				}

				$csvdata .= $options["enclosure"] . $val . $options["enclosure"];
				$i++;
			}

			$csvdata .= $options["break"];
		}

		//呼び出した配列をCSV形式で全て結合する。
		foreach ($data as $buff) {

			$i=0;
			foreach ($buff as $key => $val) {

				if ($i > 0) {
					$csvdata .= $options["separate"];
				}

				$val = str_replace("\"", "\"\"", $val);
				$csvdata .= $options["enclosure"] . $val . $options["enclosure"];
				$i++;
			}

			$csvdata .= $options["break"];
		}

		//SJISに変換
		if ($options["stringconvert"] == 1) {
			$csvdata = mb_convert_encoding($csvdata, $options["stringcode"], mb_detect_encoding($csvdata));
		}

		return $csvdata;
	}


	//=================================================================
	//	ダウンロード処理
	//=================================================================
	function download($buff, $filename) {

		header( 'Pragma: private' );
		header( 'Cache-Control: private' );
		header( 'Content-Type: application/octet-stream' );
		header( 'Content-Disposition: attachment; filename='. $filename );

		print $buff;
	}

}
?>