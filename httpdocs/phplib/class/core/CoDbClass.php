<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2013/06/26
 * Update   : 2013/06/26
 * Description : 共通DBクラス
 **************************************************************************/

class CoDb{

	var $table;

	//=================================================================
	//　●vars内容を登録
	//=================================================================
	function insertVars($vars, $items, $lists) {

		$reg_items = array();

		foreach ($items as $key => $val) {
			if ( ! isset($val["noregister"]) || $val["noregister"] == "") {
				if (isset($val["value"]) && $val["value"] != "") {
					$reg_items[$key]["data"] = $val["value"];
				} else {
					if ( ! empty($val["inttostr"])) {
						$reg_items[$key]["data"] = $lists[$val["inttostr"]][$vars[$key]];
//					} elseif (isset($val["type"]) && $val["type"] == "img" && empty($vars["del_".$key])) {
//						$reg_items[$key]["data"] = $vars[$key.ADD_WORD_NEW];
					} else {
						$reg_items[$key]["data"] = $vars[$key];
					}
				}

				if (isset($val["type_db"]) && $val["type_db"] != "") {
					$reg_items[$key]["type"] = $val["type_db"];
				}
			}
		}

		if (count($reg_items) > 0) {

			$this->insert($reg_items);
		}
	}


	//インサートクエリを実行する関数
	//引数　値の連想配列　オプション
	function insert($items, $options = array()) {

		$defaults = array(
			"table" => "",
			"null_flg" => "",
		);
		$options = array_merge($defaults, $options);

		if (empty($options["table"])) {
			$table = $this->table;
		} else {
			$table = $options["table"];
		}

		$field = "";
		$value = "";
		$reg_items = array();
		foreach ($items as $key => $val) {
			$field .= $key .",";

			if (isset($val["data"])) {

				if (is_array($val["data"])) {
					$val["data"] = implode(",",$val["data"]);
				}

				if ($val["data"] === "" && empty($options["null_flg"])) {
					$value .= "NULL,";
				} elseif (isset($val["type"]) && $val["type"] == 'nosingle') {
					$value .= $this->escape($val["data"]).",";
				} else {
					$value .= "'". $this->escape($val["data"]) ."',";
				}

			} else {

				if (is_array($val)) {
					$val = implode(",",$val);
				}

				if ($val === "" && empty($options["null_flg"])) {
					$value .= "NULL,";
				} else {
					$value .= "'". $this->escape($val) ."',";
				}
			}
		}

		$field = substr($field, 0, -1);
		$value = substr($value, 0, -1);

		$query = "insert into ". $table ." ($field) values ($value)";
		$this->query($query);

	}
}
?>