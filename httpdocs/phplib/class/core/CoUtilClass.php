<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2010/05/20
 * Update   : 2011/06/09
 * Description : 共通ユーティリティクラス
 **************************************************************************/

class CoUtil{

	//連想配列を変数設定の配列で初期化＆変数を結合する関数
	function initVars($vars, $items) {

		//変数設定で初期化
		foreach ($items as $key => $val) {
			if (isset($val["type"]) && $val["type"] == "img" && defined('ADD_WORD_NEW')) {
				if ( ! isset($vars[$key])) {
					$vars[$key] = "";
				}
				if ( ! isset($vars[$key.ADD_WORD_NEW])) {
					$vars[$key.ADD_WORD_NEW] = "";
				}
			} else {
				if( ! isset($vars[$key])) {
					$vars[$key] = "";
				}
			}
		}

		return $vars;
	}

	//連想配列をもう一つの配列で初期化する関数
	function initArray($vars, $items) {
		foreach ($items as $key) {
			if ( ! isset($vars[$key])) {
				$vars[$key] = "";
			}
		}
		return $vars;
	}

	//連想配列をもう一つの連想配列で初期化する関数
	function initHashVars($vars, $items) {
		foreach ($items as $key => $val) {
			if ( ! isset($option[$key])) {
				$vars[$key] = $val;
			}
		}
		return $vars;
	}

	//変数にデフォルト値を設定する
	function setInitValues($vars, $items) {
		foreach ($items as $key => $val) {
			if (isset($val["default"])) {
				if ($vars[$key] == "") {
					$vars[$key] = $val["default"];
				}
			}
		}
		return $vars;
	}

	//　●$_POSTを$varsに変換する　引数：$_POST 変換する文字コード 変換前の文字コード
	function chgForm ($post, $to_strcode = null, $from_strcode = null) {
		$form = array();
		foreach( $post as $key => $entry ){
			if( is_array( $entry ) ){
				$form[$key] = CoUtil::chgForm( $entry );
			}else{

				if ($to_strcode && $from_strcode) {
					$entry = mb_convert_encoding($entry, $to_strcode, $from_strcode);
				}

				$entry = CoUtil::encodeFormValues( $entry );
				$form[$key] = $entry;
			}
		}
		return $form;
	}

	// HTML無効化
	function encodeFormValues($data) {

		if (is_array($data)) {
			return array_map('encodeFormValues', $data);
		}

		if (get_magic_quotes_gpc()) {
			$data = stripslashes($data);
		}

		$data = mb_convert_kana($data, 'KV', mb_internal_encoding());
		$data = htmlentities($data, ENT_QUOTES, mb_internal_encoding());
		$data = trim($data);

		return $data;
	}


	// HTML有効化
	function decodeFormValues($data) {
		if (is_array($data)) {
			return array_map('decodeFormValues', $data);
		}

		$data = CoUtil::unhtmlentities($data);

		return $data;
	}


	//HTML有効化処理
	function unhtmlentities($string) {
		return html_entity_decode($string, ENT_QUOTES);
	}


	//ランダムな英数字を生成する
	function randCreatePassword($count, $diff_flg = null) {

		$return_word = "";
		$word = array(
			"a","b","c","d","e","f","g","h","i","j","k","l","m","n",
			"o","p","q","l","s","t","u","v","w","x","y","z","A","B",
			"C","D","E","F","G","H","I","J","K","L","M","N","O","P",
			"Q","R","S","T","U","V","W","X","Y","Z","1","2","3","4",
			"5","6","7","8","9","0"
		);

		if($diff_flg){

			for ($i=1;$i<$count;$i++) {
				$key = array_rand($word, 1);
				$return_word .= $word[$key];
			}

		} else {

			$items = array_rand($word, $count);
			foreach ($items as $key) {
				$return_word .= $word[$key];
			}
		}

		return $return_word;
	}

	//URLにリダイレクトする
	function location($url) {
		header("Location: ". $url);
		exit;
	}

	//「{}」で囲った文字列をphpとして実行して文字列を返す
	function strToPhpExec($str) {

		preg_match_all("/{.*?}/", $str, $matchs);

		if ( ! empty($matchs[0])) {

			foreach ($matchs[0] as $mat) {
				$val = preg_replace("/{|}/", "", $mat);
				$val = '$val = '. $val .';';
				@eval($val);
				$str = str_replace($mat, $val, $str);
			}
		}

		return $str;
	}

	//ユーザエージェントから端末のタイプを取得
	// 1:docomo 2:SB 3:au 4:smartPhone 5:PC	
	function getUserAgent() {

		$agent = "";
		if(isset($_SERVER{'HTTP_USER_AGENT'})) {
			$agent = $_SERVER{'HTTP_USER_AGENT'};
		}

		if (ereg("^DoCoMo", $agent)) {
			$agenttype = 1;
		} else if(ereg("^J-PHONE|^Vodafone|^SoftBank", $agent)) {
			$agenttype = 2;
		} else if(ereg("^UP.Browser|^KDDI", $agent)) {
			$agenttype = 3;
		} else if(ereg("iPod|iPad|iPhone|Android", $agent)) {
			$agenttype = 4;
		} else {
			$agenttype = 5;
		} 

		return $agenttype;
	}

	//ユーザエージェントがdocomoだったらSIDを付加してリダイレクトさせる
	function addSidLocation($url) {

		if (CoUtil::getUserAgent() == 1) {
			if (false === strpos($url, "?")) {
				$url .= "?".SID;
			} else {
				$url .= "&".SID;
			}
		}
		
		header("Location: ". $url);
		exit;
	}


	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//
	//　チェック関数
	//
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// 平仮名チェックUTF-8
	function checkZenHira($str) {
		return preg_match("/^(?:\xE3\x81[\x81-\xBF]|\xE3\x82[\x80-\x93]|\xE3\x83\xBC)+$/", $str);
	}

	// カタカナチェックUTF-8
	function checkZenKana($str) {
		return preg_match("/^(?:\xE3\x82[\xA1-\xBF]|\xE3\x83[\x80-\xB6]|\xE3\x83\xBC)+$/", $str);
	}

	// メールアドレスチェック
	function checkMailAddress($str) {

		$mail = explode("@", $str);

		if (count($mail) != 2) {
			return false;
		}

		if ( ! preg_match('/^(?:\w|\!|#|\$|%|&|`|\+|-|\*|\/|’|\^|{|}|\.)+$/', $mail[0])) {
			return false;
		}

		if ( ! preg_match('/^(?:\w|\!|#|\$|%|&|`|\+|-|\*|\/|’|\^|{|}|\.)+$/', $mail[1]) || preg_match('/\.\./', $mail[1])) {
			return false;
		}

		if ( ! preg_match('/^[0-9A-Za-z].*[0-9A-Za-z]$/', $mail[1])) {
			return false;
		}

		return true;
	}

	// メールアドレスチェック(PC専用)
	function checkMailAddressPc($str) {
		$c_reg =	'(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\\' .
					'\[\]\000-\037\x80-\xff])|"[^\\\\\x80-\xff\n\015"]*(?:\\\\[^\x80-\xff][' .
					'^\\\\\x80-\xff\n\015"]*)*")(?:\.(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x' .
					'80-\xff]+(?![^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff])|"[^\\\\\x80-' .
					'\xff\n\015"]*(?:\\\\[^\x80-\xff][^\\\\\x80-\xff\n\015"]*)*"))*@(?:[^(' .
					'\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\\\[\]\0' .
					'00-\037\x80-\xff])|\[(?:[^\\\\\x80-\xff\n\015\[\]]|\\\\[^\x80-\xff])*' .
					'\])(?:\.(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,' .
					';:".\\\\\[\]\000-\037\x80-\xff])|\[(?:[^\\\\\x80-\xff\n\015\[\]]|\\\\[' .
					'^\x80-\xff])*\]))*';
		// メールアドレス
		if ($str == "" || ! preg_match ("/^$c_reg$/", $str)) {
			return false;
		} else {
			return true;
		}
	}

	//URLチェック
	function checkHttpUrl($str) {
		$http_URL_regex =
			'\b(?:https?|shttp):\/\/(?:(?:[-_.!~*\'()a-zA-Z0-9;:&=+$,]|%[0-9A-Fa-f' .
			'][0-9A-Fa-f])*@)?(?:(?:[a-zA-Z0-9](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?\.)' .
			'*[a-zA-Z](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?\.?|[0-9]+\.[0-9]+\.[0-9]+\.' .
			'[0-9]+)(?::[0-9]*)?(?:\/(?:[-_.!~*\'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f]' .
			'[0-9A-Fa-f])*(?:;(?:[-_.!~*\'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-' .
			'Fa-f])*)*(?:\/(?:[-_.!~*\'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f' .
			'])*(?:;(?:[-_.!~*\'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*)*)' .
			'*)?(?:\?(?:[-_.!~*\'()a-zA-Z0-9;\/?:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])' .
			'*)?(?:#(?:[-_.!~*\'()a-zA-Z0-9;\/?:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*' .
			')?';

		return preg_match("/^$http_URL_regex$/", $str);
	}


	//携帯ドメインチェック
	function checkMobilephoneMailAddress($str) {
		$disable_mailaddress_regexp_list = array(
			'/docomo\.ne\.jp$/',
			'/ezweb\.ne\.jp$/',
			'/softbank\.ne\.jp$/',
			'/vodafone\.ne\.jp$/',
			'/pdx\.ne\.jp$/'
		);
		foreach($disable_mailaddress_regexp_list as $regexp) {
			if(preg_match( $regexp, $str)) {
				return true;
			}
		}
		return false;
	}

	//電話番号チェック
	function checkTelephoneStrict($check_num_1, $check_num_2 = "", $check_num_3 = "") {
		// 第二引数と第三引数に値がある場合
		if ($check_num_2 != "" && $check_num_3 != "") {
			// 数字以外が入っていないかをチェック
			$check_num = $check_num_1 . $check_num_2 . $check_num_3;
			if (preg_match( "/[^\d]/", $check_num)) {
				return false;
			}
			// 配列に3つをまとめる
			$check_num_array = array($check_num_1, $check_num_2, $check_num_3);
		// 第二引数と第三引数が空白の場合は、第一引数に全部入力しているパターン
		} else {
			// ハイフン無しの場合
			if (substr_count($check_num_1, "-") == 0) {
				// 桁数しか調べられないので、それでチェック
				if (preg_match("/^0\d{9}$/", $check_num_1) || preg_match("/^0[1-9]0\d{8}$/", $check_num_1)) {
					return true;
				} else {
					return false;
				}
			// ハイフン有りの場合
			} elseif (substr_count($check_num_1, "-") == 2) {

				// 配列に分けておく
				$check_num_array = explode("-", $check_num_1);
			// ハイフンが1つだったり多かったりした場合はfalse
			} else {
				return false;
			}
		}

		// 桁数が10（固定電話）の場合
		if(strlen(implode("", $check_num_array)) == 10) {

			// 固定電話の法則に従ってチェック
			if (preg_match("/^0\d{1,4}$/", $check_num_array[0]) && preg_match("/^\d{4}$/", $check_num_array[2])) {
				return true;
			} else {
				return false;
			}

		// 桁数が11（携帯電話）の場合
		} elseif (strlen(implode("", $check_num_array)) == 11) {
			// 携帯電話の法則に従ってチェック
			if (preg_match("/^0[1-9]0$/", $check_num_array[0]) && preg_match("/^\d{4}$/", $check_num_array[2])) {
				return true;
			} else {
				return false;
			}

		// 桁数が10でも11でもない場合はfalse
		} else {
			return false;
		}
	}

	//　携帯電話番号
	//　半角数値・3,4,4桁数チェック
	//-------------------------------------------------------------
	function checkMtel($mtel1, $mtel2, $mtel3) {
		if (preg_match("/^\d{3}$/", $mtel1) && preg_match("/^\d{4}$/", $mtel2) && preg_match("/^\d{4}$/", $mtel3)) {
			return true;
		} else {
			return false;
		}
	}



	//　郵便番号チェック
	//-------------------------------------------------------------
	function checkZip($zip1, $zip2) {
		if( ! preg_match("/^\d{3}$/", $zip1) || !preg_match("/^\d{4}$/", $zip2)) {
			return false;
		}
		return true;
	}


	//　●リストのキーを返す
	//-------------------------------------------------------------
	function re_list_num($list, $value) {
		foreach ($list as $key => $val) {
			if ($value == $val) {
				return $key;
			}
		}
	}

	// スマホ判定
	function isSmt() {
		$ua = $_SERVER['HTTP_USER_AGENT'];
		if ((strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false) || (strpos($ua, 'iPhone') !== false) || (strpos($ua, 'Windows Phone') !== false)) {
			return true;
		} elseif ((strpos($ua, 'Android') !== false) || (strpos($ua, 'iPad') !== false)) {
			return true;
		} else {
			return false;
		}
	}
}
?>