<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2010/11/19
 * Update   : 2010/12/08
 * Description : フォーム用ファイル処理
 **************************************************************************/

class File extends CoFile{

	function File() {
	}

	//=================================================================
	//　●投稿情報をファイル出力
	//=================================================================
	function publish($vars ,$items, $lists) {

		$filename = CoUtil::strToPhpExec(FILE_NAME);

		$header = array();
		$data = array();

		//フラグがあり、ファイルが無ければラベルを出力
		if (FILE_LABEL_FLG && ! is_file(FILE_PATH . $filename)) {
			foreach ($items as $key => $val) {
				if ( ! isset($val["noregister"]) || $val["noregister"] == "") {
					$header[] = $val["name"];
				}
			}
		}

		foreach ($items as $key => $val) {
			if ( ! isset($val["noregister"]) || $val["noregister"] == "") {
				if (isset($val["value"])) {
					$data[] = $val["value"];
				} elseif ( ! empty($val["inttostr"])) {
					$data[] = $lists[$val["inttostr"]][$vars[$key]];
				} elseif (is_array($vars[$key])) {
					$data[] = implode(",",$vars[$key]);
				} else {
					$data[] = $vars[$key];
				}
			}
		}

		//値を出力形式に変換
		$options = array(
			"enclosure" => FILE_ENCLOSURE,
			"separate" => FILE_SEPARATE,
			"break" => FILE_BREAK,
			"header" => $header,
			"stringcode" => FILE_STRINGCODE,
		);
		$buff = $this->convertCsv(array($data), $options);

		//値を出力
		$options = array(
			"opentype" => "a",
			"chmod" => 0777,
		);
		$this->create(FILE_PATH.$filename, $buff, $options);
	}

	//=================================================================
	//　●アップロードファイルをテンポラリにコピー
	//=================================================================
	function tmpfileUpload($vars, $items, $files, $err_msg = array()) {

		foreach ($items as $key => $val) {
			if (isset($val["type"]) && $val["type"] == "file") {

				if (isset($files[$key])) {

					if (! $this->check($files[$key], array("size"=>MAX_FILE_SIZE,"filetype"=>"doc|docm|docx|xls|xlsx|xlsm|ppt|pptx|pptm|pdf"))) {
						if(! empty($val["errmsg"]["over"])) {
							$err_msg[$key] = $val["errmsg"]["over"];
						} else {
							$err_msg[$key] = sprintf(Message::UPFILE_OVER, $val["name"]);
						}
					} else {

						$option = array(
							"filename"		=> uniqid() . "_tmp",
							"key_name_ori"	=> $key . ADD_WORD_ORIGNAL,
							"key_name_new"	=> $key . ADD_WORD_NEW,
							"up_dir_path"	=> IMAGE_TEMP_PATH,
						);

						$results = $this->up($vars, $files[$key], $option);

						if ($results === false) {
							if(! empty($val["errmsg"]["wrong"])) {
								$err_msg[$key] = $val["errmsg"]["wrong"];
							} else {
								$err_msg[$key] = sprintf(Message::UPFILE_WRONG, $val["name"]);
							}
							$vars[$key . ADD_WORD_NEW] = "";
							$vars[$key . ADD_WORD_ORIGNAL] = "";
						} else {
							$vars = $results;
						}
					}
				}
			}
		}

		return array($vars, $err_msg);
	}

	//=================================================================
	//　●ファイルをテンポラリから本番用にコピー
	//=================================================================
	function tmpfileMove($vars, $items, $err_msg = array()) {

		foreach ($items as $key => $val) {
			if (isset($val["type"]) && $val["type"] == "file") {

				//コピー
				if (empty($vars["del_".$key]) && ! empty($vars[$key . ADD_WORD_NEW]) && ! empty($vars[$key . ADD_WORD_ORIGNAL])) {

					$extension = $this->getExtension($vars[$key . ADD_WORD_ORIGNAL]);
					$newfile = uniqid() . $extension;

					if (isset($val["prefix"])) {
						$newfile = $val["prefix"] . $newfile;
					}

					$this->move(IMAGE_TEMP_PATH . $vars[$key . ADD_WORD_NEW], IMAGE_PATH . $newfile);

					$vars[$key] = $newfile;
				}
			}
		}

		return array($vars, $err_msg);
	}
}
?>