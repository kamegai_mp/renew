<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2013/06/26
 * Update   : 2013/06/26
 * Description : 共通モデル
 **************************************************************************/

class defaultModel{

	var $db;

	function defaultModel(){

		$options = array(
		    'username' => SQL_USER,
		    'password' => SQL_PASSWORD,
		    'hostspec' => SQL_HOSTNAME,
		    'database' => SQL_DBNAME,
			'sqlport' => SQL_PORT,
		);

		//　DB接続
		if (SQL_TYPE == "mysql") {
			$this->db = new CoMysql($options);
		} else {
			$this->db = new CoPgsql($options);
		}
	}
}
?>