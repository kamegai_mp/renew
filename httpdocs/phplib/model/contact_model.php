<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2013/06/26
 * Update   : 2013/06/26
 * Description : フォーム投稿モデル
 **************************************************************************/

class contactModel extends defaultModel{

	var $type; // お問い合わせの項目

	function contactModel($type = null){
		parent::__construct();

		$this->db->table = "t_contact";
		$this->type = $type;
	}

	//=================================================================
	//　●変数設定
	//=================================================================
	function structure() {

		$items = array(
			"mode" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"type" => array(
				"type" => "list",
				"name" => "お問い合わせの項目",
				"list" => contactModel::lists("type"),
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY_SELECT, "お問い合わせの項目"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "お問い合わせの項目"),
				),
			),
			"contents" => array(
				"type" => "textarea",
				"name" => "お問い合わせ内容",
				"max" => 3000,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "お問い合わせ内容"),
					"max" => sprintf(Message::VALIDATE_TYPE, "お問い合わせ内容", "3000文字以内"),
				),
			),
			"names" => array(
				"type" => "str",
				"name" => "お名前",
				"max" => 100,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "お名前"),
					"max" => sprintf(Message::VALIDATE_TYPE, "お名前", "100文字以内"),
				),
			),
			"tel" => array(
				"name" => "電話番号",
				"type" => "tel",
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "電話番号"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "電話番号"),
				),
				"join" => array( "tel1", "tel2", "tel3" ),
			),
			"tel1" => array(
				"nocheck" => 1,
				"noregister" => 1,
				"type" => "tel",
			),
			"tel2" => array(
				"nocheck" => 1,
				"noregister" => 1,
				"type" => "tel",
			),
			"tel3" => array(
				"nocheck" => 1,
				"noregister" => 1,
				"type" => "tel",
			),
			"email" => array(
				"type" => "email",
				"name" => "メールアドレス",
				"max" => 256,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "メールアドレス"),
					"max" => sprintf(Message::VALIDATE_TYPE, "メールアドレス", "256文字以内"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "メールアドレス"),
				),
			),
			"company" => array(
				"type" => "str",
				"name" => "御社名",
				"max" => 150,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "御社名"),
					"max" => sprintf(Message::VALIDATE_TYPE, "御社名", "150文字以内"),
				),
			),
			"privacy" => array(
				"type" => "privacy",
				"name" => "個人情報の取り扱い",
				"noregister" => 1,
				"errmsg" => array(
					"wrong" => Message::VALIDATE_WRONG_PRIVACY,
				),
			),
/*
			"url" => array(
				"type" => "url",
				"name" => "url",
				"max" => 256,
				"errmsg" => array(
					"max" => sprintf(Message::VALIDATE_TYPE, "url", "256文字以内"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "url"),
				),
			),
			"budget" => array(
				"type" => "str",
				"name" => "ご予算",
				"max" => 150,
				"errmsg" => array(
					"max" => sprintf(Message::VALIDATE_TYPE, "ご予算", "150文字以内"),
				),
			),
*/
			"request_type" => array(
				"type" => "list",
				"name" => "ご依頼予定内容",
				"list" => contactModel::lists("request_type"),
				"necessary" => 0,
				"errmsg" => array(
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "ご依頼予定内容"),
				),
			),
			"request_site_type" => array(
				"type" => "list",
				"name" => "予定サイト種別",
				"list" => contactModel::lists("request_site_type"),
				"necessary" => 0,
				"errmsg" => array(
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "予定サイト種別"),
				),
			),
		);

		$unsetList = array();
		// お問い合わせの項目によって要らないフィールドを抜く
		switch ($this->type) {

			case CONTACT_TYPE_WORK :
				break;

			case CONTACT_TYPE_RECRUIT :
				$unsetList = array("company", "request_type", "request_site_type"); // recruit
				break;

			case CONTACT_TYPE_ETC :
				$unsetList = array( "request_type", "request_site_type"); // etc
				break;

			case CONTACT_TYPE_PARTNER :
				$unsetList = array( "request_type", "request_site_type"); // partner
				break;

			case CONTACT_TYPE_PACKAGE :
				$unsetList = array( "request_type", "request_site_type"); // package
				break;				
		}

		if (count($unsetList) > 0) {
			foreach ($unsetList as $unsetItem) {
				unset($items[$unsetItem]);
			}
		}

		return $items;
	}


	//=================================================================
	//　●項目設定
	//=================================================================
	function lists($key = null) {

		$items = array(
			"type" => array(
				1 => "お仕事のご相談・ご依頼",
				2 => "採用について",
				3 => "その他お問い合わせ",
				4 => "パートナー・協業のご相談",
				5 => "無料テンプレートのお問い合わせ",				
			),
			"request_type" => array(
					1 => "新規制作",
					2 => "リニューアル",
					3 => "保守・運用",
					9 => "その他"
			),
			"request_site_type" => array(
					1 => "コーポレートサイト",
					2 => "ブランドサイト",
					3 => "キャンペーンサイト",
					4 => "メディアサイト",
					5 => "ECサイト",
					6 => "ランディングページ",
					7 => "リクルートサイト",
					99 => "その他"
			),
/*
			"pref" => array(
				1 => "北海道",
				2 => "青森県",
				3 => "岩手県",
				4 => "宮城県",
				5 => "秋田県",
				6 => "山形県",
				7 => "福島県",
				8 => "茨城県",
				9 => "栃木県",
				10 => "群馬県",
				11 => "埼玉県",
				12 => "千葉県",
				13 => "東京都",
				14 => "神奈川県",
				15 => "新潟県",
				16 => "富山県",
				17 => "石川県",
				18 => "福井県",
				19 => "山梨県",
				20 => "長野県",
				21 => "岐阜県",
				22 => "静岡県",
				23 => "愛知県",
				24 => "三重県",
				25 => "滋賀県",
				26 => "京都府",
				27 => "大阪府",
				28 => "兵庫県",
				29 => "奈良県",
				30 => "和歌山県",
				31 => "鳥取県",
				32 => "島根県",
				33 => "岡山県",
				34 => "広島県",
				35 => "山口県",
				36 => "徳島県",
				37 => "香川県",
				38 => "愛媛県",
				39 => "高知県",
				40 => "福岡県",
				41 => "佐賀県",
				42 => "長崎県",
				43 => "熊本県",
				44 => "大分県",
				45 => "宮崎県",
				46 => "鹿児島県",
				47 => "沖縄県",
			),
			"sex" => array(
				1 => "男性",
				2 => "女性"
			),
*/
		);

		if($key) {
			$items = $items[$key];
		}

		return $items;
	}


	//=================================================================
	//　●DB登録
	//=================================================================
	function insert($vars, $structures, $lists) {

		$add_items = array(
			"created" => array(
				"value" => "NOW()",
				"type_db" => "nosingle"
			),
			"updated" => array(
				"value" => "NOW()",
				"type_db" => "nosingle"
			),
		);
		$structures = array_merge($structures, $add_items);

		$this->db->insertVars($vars, $structures, $lists);
	}

	//=================================================================
	//　●メール送信
	//=================================================================
	function sendMail($vars, $lists, $mail_template_path) {

		$param = array(
			"vars" => $vars,
			"lists" => $lists
		);

		//ユーザー用メール
		$options = array(
			"to" => $vars["email"],
			"from" => USER_MAIL_FROM,
			"file" => "contact/user.mail",
			"param" => $param,
			"template_path" => $mail_template_path
		);

		CoMail::send($options);


		//管理者用メール
		$admin_to = ADMIN_MAIL_TO;
		if ($this->type == CONTACT_TYPE_RECRUIT){
			$admin_to = RECRUIT_ADMIN_MAIL_TO;
		} else if($this->type == CONTACT_TYPE_WORK){
			$admin_to = WORK_ADMIN_MAIL_TO;
		} else if($this->type == CONTACT_TYPE_PACKAGE){
			$admin_to = PACKAGE_ADMIN_MAIL_TO;
		};
		if (DEBUG_MODE == 1) {
			$admin_to = $vars["email"];
		}

		$options = array(
			"to" => $admin_to,
			"from" => ADMIN_MAIL_FROM,
			"file" => "contact/admin.mail",
			"param" => $param,
			"template_path" => $mail_template_path
		);

		CoMail::send($options);
	}

}
