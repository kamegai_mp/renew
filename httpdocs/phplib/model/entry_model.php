<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2013/06/26
 * Update   : 2013/06/26
 * Description : フォーム投稿モデル
 **************************************************************************/

class entryModel extends defaultModel{

	function entryModel(){
		parent::__construct();

		$this->db->table = "t_recruit";
	}

	//=================================================================
	//　●変数設定
	//=================================================================
	function structure() {

		$items = array(
			"mode" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"sei" => array(
				"type" => "str",
				"name" => "姓",
				"max" => 30,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "姓"),
					"max" => sprintf(Message::VALIDATE_TYPE, "姓", "30文字以内"),
				),
			),
			"mei" => array(
				"type" => "str",
				"name" => "名",
				"max" => 30,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "名"),
					"max" => sprintf(Message::VALIDATE_TYPE, "名", "30文字以内"),
				),
			),
			"k_sei" => array(
				"type" => "kana",
				"name" => "セイ",
				"max" => 30,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "セイ"),
					"max" => sprintf(Message::VALIDATE_TYPE, "セイ", "30文字以内"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_KANA, "セイ"),
				),
			),
			"k_mei" => array(
				"type" => "kana",
				"name" => "メイ",
				"max" => 30,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "メイ"),
					"max" => sprintf(Message::VALIDATE_TYPE, "メイ", "30文字以内"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_KANA, "メイ"),
				),
			),
			"birth_date" => array(
				"type" => "date",
				"name" => "生年月日",
				"necessary" => 1,
				"min" => 1900,
				"max" => "now",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "生年月日"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "生年月日"),
					"min" => sprintf(Message::VALIDATE_WRONG, "生年月日"),
					"max" => sprintf(Message::VALIDATE_WRONG, "生年月日"),
				),
				"join" => array("birth_date1","birth_date2","birth_date3"),
				"join_word" => "/",
			),
			"birth_date1" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"birth_date2" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"birth_date3" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"zip" => array(
				"name" => "郵便番号",
				"type" => "zip",
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "郵便番号"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "郵便番号"),
				),
				"join" => array( "zip1", "zip2" ),
				"join_word" => "-",
			),
			"zip1" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"zip2" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"pref" => array(
				"type" => "list",
				"name" => "都道府県",
				"list" => entryModel::lists("pref"),
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY_SELECT, "都道府県"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "都道府県"),
				),
			),
			"address" => array(
				"type" => "str",
				"name" => "市区町村・番地・建物名",
				"max" => 150,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "市区郡"),
					"max" => sprintf(Message::VALIDATE_TYPE, "市区郡", "100文字以内"),
				),
			),
			"tel01" => array(
				"name" => "電話番号(自宅)",
				"type" => "tel",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_WRONG_EITHER, "自宅・携帯"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "電話番号(自宅)"),
				),
				"join" => array( "tel01_1", "tel01_2", "tel01_3" ),
			),
			"tel01_1" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"tel01_2" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"tel01_3" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"tel02" => array(
				"name" => "電話番号(携帯)",
				"type" => "tel",
				"errmsg" => array(
					"wrong" => sprintf(Message::VALIDATE_WRONG, "電話番号(携帯)"),
				),
				"join" => array( "tel02_1", "tel02_2", "tel02_3" ),
			),
			"tel02_1" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"tel02_2" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"tel02_3" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"email" => array(
				"type" => "email",
				"name" => "メールアドレス",
				"max" => 150,
				"necessary" => 1,
				"diff" => "email_re",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "メールアドレス"),
					"max" => sprintf(Message::VALIDATE_TYPE, "メールアドレス", "150文字以内"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "メールアドレス"),
					"diff" => sprintf(Message::VALIDATE_DIFF, "メールアドレス"),
				),
			),
			"email_re" => array(
				"type" => "email",
				"name" => "メールアドレス(確認用)",
				"max" => 150,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "メールアドレス(確認用)"),
					"max" => sprintf(Message::VALIDATE_TYPE, "メールアドレス(確認用)", "150文字以内"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "メールアドレス(確認用)"),
				),
				"noregister" => 1
			),
			"job" => array(
				"type" => "list",
				"name" => "現在の状況",
				"list" => entryModel::lists("job"),
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY_SELECT, "現在の状況"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "現在の状況"),
				),
			),
			"start_date_select" => array(
				"type" => "list",
				"name" => "就業開始可能日(選択)",
				"list" => entryModel::lists("start_date_select"),
				"errmsg" => array(
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "就業開始可能日"),
				),
				"noregister" => 1
			),
			"start_date" => array(
				"type" => "date",
				"name" => "就業開始可能日",
				"min" => "now",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "就業開始可能日"),
					"min" => sprintf(Message::VALIDATE_WRONG_DATE_NOW, "就業開始可能日"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "就業開始可能日"),
				),
				"join" => array("start_date1","start_date2","start_date3"),
				"join_word" => "/",
			),
			"start_date1" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"start_date2" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"start_date3" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"resume_file" => array(
				"type" => "file",
				"name" => "履歴書",
				"prefix" => date("Y-md-Hi")."-res-",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_FILE_WORD_SELECT, "内容"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "履歴書"),
				),
			),
			"resume_text" => array(
				"type" => "textarea",
				"name" => "履歴書",
				"max" => 1000,
				"errmsg" => array(
					"max" => sprintf(Message::VALIDATE_TYPE, "履歴書", "1000文字以内"),
				),
			),
			"curriculum" => array(
				"type" => "file",
				"name" => "職務経歴書",
				"prefix" => date("Y-md-Hi")."-cur-",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_FILE_WORD_SELECT, "内容"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "職務経歴書"),
				),
			),

			"history_from" => array(
				"type" => "date02",
				"name" => "勤務歴",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "勤務歴"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "勤務歴"),
				),
				"join" => array("history_from1","history_from2"),
				"join_word" => "/",
			),
			"history_from1" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"history_from2" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"history_to" => array(
				"type" => "date02",
				"name" => "勤務歴",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "勤務歴"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "勤務歴"),
				),
				"join" => array("history_to1","history_to2"),
				"join_word" => "/",
			),
			"history_to1" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"history_to2" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"company" => array(
				"type" => "str",
				"name" => "会社名",
				"max" => 200,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "会社名"),
					"max" => sprintf(Message::VALIDATE_TYPE, "会社名", "200文字以内"),
				),
			),
			"pattern" => array(
				"type" => "list",
				"name" => "雇用形態",
				"list" => entryModel::lists("pattern"),
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY_SELECT, "雇用形態"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "雇用形態"),
				),
			),
			"job_type" => array(
				"type" => "list_optgroup",
				"name" => "職種",
				"list" => entryModel::lists("job_type"),
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY_SELECT, "職種"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "職種"),
				),
			),
			"work_contents" => array(
				"type" => "textarea",
				"name" => "業務内容",
				"max" => 1000,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "業務内容"),
					"max" => sprintf(Message::VALIDATE_TYPE, "業務内容", "1000文字以内"),
				),
			),
			"curriculum02" => array(
				"type" => "file",
				"name" => "職務経歴書",
				"prefix" => date("Y-md-Hi")."-cur-",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_FILE_WORD_SELECT, "内容"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "職務経歴書"),
				),
			),
			"history_from_02" => array(
				"type" => "date02",
				"name" => "勤務歴",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "勤務歴"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "勤務歴"),
				),
				"join" => array("history_from1_02","history_from2_02"),
				"join_word" => "/",
			),
			"history_from1_02" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"history_from2_02" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"history_to_02" => array(
				"type" => "date02",
				"name" => "勤務歴",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "勤務歴"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "勤務歴"),
				),
				"join" => array("history_to1_02","history_to2_02"),
				"join_word" => "/",
			),
			"history_to1_02" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"history_to2_02" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"company_02" => array(
				"type" => "str",
				"name" => "会社名",
				"max" => 200,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "会社名"),
					"max" => sprintf(Message::VALIDATE_TYPE, "会社名", "200文字以内"),
				),
			),
			"pattern_02" => array(
				"type" => "list",
				"name" => "雇用形態",
				"list" => entryModel::lists("pattern"),
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY_SELECT, "雇用形態"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "雇用形態"),
				),
			),
			"job_type_02" => array(
				"type" => "list_optgroup",
				"name" => "職種",
				"list" => entryModel::lists("job_type"),
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY_SELECT, "職種"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "職種"),
				),
			),
			"work_contents_02" => array(
				"type" => "textarea",
				"name" => "業務内容",
				"max" => 1000,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "業務内容"),
					"max" => sprintf(Message::VALIDATE_TYPE, "業務内容", "1000文字以内"),
				),
			),

			"curriculum03" => array(
				"type" => "file",
				"name" => "職務経歴書",
				"prefix" => date("Y-md-Hi")."-cur-",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_FILE_WORD_SELECT, "内容"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "職務経歴書"),
				),
			),
			"history_from_03" => array(
				"type" => "date02",
				"name" => "勤務歴",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "勤務歴"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "勤務歴"),
				),
				"join" => array("history_from1_03","history_from2_03"),
				"join_word" => "/",
			),
			"history_from1_03" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"history_from2_03" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"history_to_03" => array(
				"type" => "date02",
				"name" => "勤務歴",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "勤務歴"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "勤務歴"),
				),
				"join" => array("history_to1_03","history_to2_03"),
				"join_word" => "/",
			),
			"history_to1_03" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"history_to2_03" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"company_03" => array(
				"type" => "str",
				"name" => "会社名",
				"max" => 200,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "会社名"),
					"max" => sprintf(Message::VALIDATE_TYPE, "会社名", "200文字以内"),
				),
			),
			"pattern_03" => array(
				"type" => "list",
				"name" => "雇用形態",
				"list" => entryModel::lists("pattern"),
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY_SELECT, "雇用形態"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "雇用形態"),
				),
			),
			"job_type_03" => array(
				"type" => "list_optgroup",
				"name" => "職種",
				"list" => entryModel::lists("job_type"),
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY_SELECT, "職種"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "職種"),
				),
			),
			"work_contents_03" => array(
				"type" => "textarea",
				"name" => "業務内容",
				"max" => 1000,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "業務内容"),
					"max" => sprintf(Message::VALIDATE_TYPE, "業務内容", "1000文字以内"),
				),
			),
			"work" => array(
				"type" => "file",
				"name" => "作品",
				"prefix" => date("Y-md-Hi")."-wor-",
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_FILE_WORD_SELECT, "内容"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "作品"),
				),
			),
			"work_text" => array(
				"type" => "textarea",
				"name" => "作品",
				"max" => 1000,
				"errmsg" => array(
					"max" => sprintf(Message::VALIDATE_TYPE, "作品", "1000文字以内"),
				),
			),
			"target_job" => array(
				"type" => "list",
				"name" => "応募職種",
				"necessary" => 1,
				"inttostr" => "target_job",
				"list" => entryModel::lists("target_job"),
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY_SELECT, "応募職種"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "応募職種"),
				),
			),
			"privacy" => array(
				"type" => "privacy",
				"name" => "個人情報の取り扱い",
				"noregister" => 1,
				"errmsg" => array(
					"wrong" => Message::VALIDATE_WRONG_PRIVACY,
				),
			),
		);

		return $items;
	}


	//=================================================================
	//　●項目設定
	//=================================================================
	function lists($key = null) {

		$items = array(
			"pref" => array(
				1 => "北海道",
				2 => "青森県",
				3 => "岩手県",
				4 => "宮城県",
				5 => "秋田県",
				6 => "山形県",
				7 => "福島県",
				8 => "茨城県",
				9 => "栃木県",
				10 => "群馬県",
				11 => "埼玉県",
				12 => "千葉県",
				13 => "東京都",
				14 => "神奈川県",
				15 => "新潟県",
				16 => "富山県",
				17 => "石川県",
				18 => "福井県",
				19 => "山梨県",
				20 => "長野県",
				21 => "岐阜県",
				22 => "静岡県",
				23 => "愛知県",
				24 => "三重県",
				25 => "滋賀県",
				26 => "京都府",
				27 => "大阪府",
				28 => "兵庫県",
				29 => "奈良県",
				30 => "和歌山県",
				31 => "鳥取県",
				32 => "島根県",
				33 => "岡山県",
				34 => "広島県",
				35 => "山口県",
				36 => "徳島県",
				37 => "香川県",
				38 => "愛媛県",
				39 => "高知県",
				40 => "福岡県",
				41 => "佐賀県",
				42 => "長崎県",
				43 => "熊本県",
				44 => "大分県",
				45 => "宮崎県",
				46 => "鹿児島県",
				47 => "沖縄県",
			),
			"job" => array(
				1 => "在職中",
				2 => "離職中",
				3 => "就業経験なし",
			),
			"start_date_select" => array(
				1 => "西暦",
				2 => "就業開始日は未定"
			),
			"pattern" => array(
				1 => "正社員",
				2 => "契約社員（アルバイト含む）",
				3 => "その他",
			),
			"job_type" => array(
				1 => array(
					"label" => "【Webクリエイター】",
					"options" => array(
						1 => "Webプロデューサー",
						2 => "Webディレクター",
						3 => "アートディレクター",
						4 => "Webデザイナー",
						5 => "グラフィックデザイナー",
						6 => "Flashエンジニア",
						7 => "テクニカルディレクター",
						8 => "フロントエンドエンジニア",
						9 => "マークアップエンジニア",
						10 => "HTMLコーダー",
						11 => "サイト運営・編集",
						12 => "エディター・ライター",
						13 => "Webマーケティング・営業（Web）",
					),
				),
				2 => array(
					"label" => "【ITエンジニア】",
					"options" => array(
						14 => "ITコンサルタント",
						15 => "プロジェクトマネージャー",
						16 => "システムエンジニア",
						17 => "プログラマー",
						18 => "サーバーエンジニア",
						19 => "ネットワークエンジニア",
						20 => "セキュリティエンジニア",
						21 => "社内情報システム",
						22 => "システム運用・保守",
						23 => "デバッガー・テスター（IT）",
						24 => "ITマーケティング・営業（IT）",
						25 => "その他（IT）"
					),
				),
				3 => array(
					"label" => "【ゲーム・アプリ開発者】",
					"options" => array(
						26 => "スマートフォンアプリエンジニア",
						27 => "Windowsアプリエンジニア",
						28 => "ゲームプロデューサー",
						29 => "ゲームプランナー",
						30 => "ゲームディレクター",
						31 => "ゲームプログラマー",
						32 => "ゲームデザイナー",
						33 => "サウンドクリエイター",
						34 => "シナリオライター",
						35 => "ゲームマスター・運営",
						36 => "デバッガー・テスター（ゲーム）",
						37 => "マーケティング・営業（ゲーム）",
						38 => "その他（ゲーム）",
					),
				),
				4 => array(
					"label" => "【その他】",
					"options" => array(
						39 => "営業",
						40 => "企画・マーケティング",
						41 => "一般事務",
						42 => "営業事務",
						43 => "データ入力",
						44 => "テレフォンオペレーター",
						45 => "ユーザーサポート・ヘルプデスク",
						46 => "インストラクター・講師",
						47 => "総務・人事",
						48 => "経理・財務",
						49 => "秘書",
						50 => "その他",
					),
				)
			),
/*
			"target_job" => array(
				1677 => "コンテンツプランナー・ディレクター",
				2090 => "中途向け会社説明会（Webプロデューサー）",
				1763 => "Webプロデューサー",
//				1708 => "マークアップエンジニア",
//				1674 => "プログラマー",
			),
*/
		);

		// MTのエントリーと応募職種を連動させる
		$mt_filepath = dirname(dirname(__FILE__))."/mt/mt_job_list.php";
		if (is_file($mt_filepath)){
			require($mt_filepath);
			$items["target_job"] = getMtEntryJob();
		}

		if($key) {
			$items = $items[$key];
		}

		return $items;
	}


	//=================================================================
	//　●DB登録
	//=================================================================
	function insert($vars, $structures, $lists) {

		$add_items = array(
			"created" => array(
				"value" => "NOW()",
				"type_db" => "nosingle"
			),
			"updated" => array(
				"value" => "NOW()",
				"type_db" => "nosingle"
			),
		);
		$structures = array_merge($structures, $add_items);

		$reg_items = array();

		foreach ($structures as $key => $val) {
			if (! isset($val["noregister"]) || $val["noregister"] == "") {
				if (isset($val["value"]) && $val["value"] != "") {
					$reg_items[$key]["data"] = $val["value"];
				} else {
					if (! empty($val["inttostr"])) {
						$reg_items[$key]["data"] = $lists[$val["inttostr"]][$vars[$key]];
//					} elseif (isset($val["type"]) && $val["type"] == "file" && isset($vars[$key.ADD_WORD_NEW]) && empty($vars["del_".$key])) {
//						$reg_items[$key]["data"] = $vars[$key.ADD_WORD_NEW];
					} else {
						$reg_items[$key]["data"] = $vars[$key];
					}
				}

				if (isset($val["type_db"]) && $val["type_db"] != "") {
					$reg_items[$key]["type"] = $val["type_db"];
				}
			}
		}

		// 就業開始可能日が未定の場合
		if ($vars["start_date_select"] == 2) {
			$reg_items["start_date"]["data"] = $lists["start_date_select"][$vars["start_date_select"]];
		}
/*
		// 履歴書
		if (empty($reg_items["resume_file"]["data"])) {
			unset($reg_items["resume_file"]);
		} else {
			unset($reg_items["resume_text"]);
		}

		// 職務経歴書
		if (empty($reg_items["curriculum"]["data"])) {
			unset($reg_items["curriculum"]);
		} else {
			unset($reg_items["history_from"]);
			unset($reg_items["history_to"]);
			unset($reg_items["company"]);
			unset($reg_items["pattern"]);
			unset($reg_items["job_type"]);
			unset($reg_items["work_contents"]);
		}

		// 作品
		if (empty($reg_items["work"]["data"])) {
			unset($reg_items["work"]);
		} else {
			unset($reg_items["work_text"]);
		}
*/
		if (count($reg_items) > 0) {
			$this->db->insert($reg_items);
		}
	}

	//=================================================================
	//　●メール送信
	//=================================================================
	function sendMail($vars, $lists, $mail_template_path) {

		$param = array(
			"vars" => $vars,
			"lists" => $lists,
		);

		//ユーザー用メール
		$options = array(
			"to" => $vars["email"],
			"from" => RECRUIT_USER_MAIL_FROM,
			"file" => "recruit/entry/user.mail",
			"param" => $param,
			"template_path" => $mail_template_path
		);

		CoMail::send($options);


		//管理者用メール
		$admin_to = RECRUIT_ADMIN_MAIL_TO;
		if (DEBUG_MODE == 1) {
			$admin_to = $vars["email"];
		}

		$options = array(
			"to" => $admin_to,
			"from" => RECRUIT_USER_MAIL_FROM,
			"file" => "recruit/entry/admin.mail",
			"param" => $param,
			"template_path" => $mail_template_path
		);

		CoMail::send($options);

	}

	//=================================================================
	//　●メール送信 phpMailer ver
	//=================================================================
	function sendPhpMailer($vars, $lists, $mail_template_path){

		$param = array(
			"vars" => $vars,
			"lists" => $lists,
		);

		//ユーザー用メール
		$options = array(
			"to" => $vars["email"],
			"from" => RECRUIT_USER_MAIL_FROM,
			"file" => "recruit/entry/user.mail",
			"param" => $param,
			"template_path" => $mail_template_path,
			"send_file" => 0
		);

		CoMail::sendPhpMailer($options);


		//管理者用メール
		$admin_to = RECRUIT_ADMIN_MAIL_TO;
		if (DEBUG_MODE == 1) {
			$admin_to = $vars["email"];
		}

		$options = array(
			"to" => $admin_to,
			"from" => RECRUIT_USER_MAIL_FROM,
			"file" => "recruit/entry/admin.mail",
			"param" => $param,
			"template_path" => $mail_template_path,
			"send_file" => 1
		);

		CoMail::sendPhpMailer($options);
	}
}
