<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2013/06/26
 * Update   : 2013/06/26
 * Description : フォーム投稿モデル
 **************************************************************************/

class lpbrandModel extends defaultModel{

	var $type; // お問い合わせの項目

	function lpbrandModel(){
		parent::__construct();
		$this->db->table = "t_lpbrand";
	}

	//=================================================================
	//　●変数設定
	//=================================================================
	function structure() {

		$items = array(
			"mode" => array(
				"nocheck" => 1,
				"noregister" => 1
			),
			"company" => array(
				"type" => "str",
				"name" => "御社名",
				"max" => 150,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "御社名"),
					"max" => sprintf(Message::VALIDATE_TYPE, "御社名", "150文字以内"),
				),
			),
			"names" => array(
				"type" => "str",
				"name" => "お名前",
				"max" => 100,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "お名前"),
					"max" => sprintf(Message::VALIDATE_TYPE, "お名前", "100文字以内"),
				),
			),
			"tel" => array(
				"name" => "お電話番号",
				"type" => "tel",
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "お電話番号"),
					"wrong" => sprintf(Message::VALIDATE_WRONG, "お電話番号"),
				),
				"join" => array( "tel1", "tel2", "tel3" ),
			),
			"tel1" => array(
				"nocheck" => 1,
				"noregister" => 1,
				"type" => "tel",
			),
			"tel2" => array(
				"nocheck" => 1,
				"noregister" => 1,
				"type" => "tel",
			),
			"tel3" => array(
				"nocheck" => 1,
				"noregister" => 1,
				"type" => "tel",
			),
			"email" => array(
				"type" => "email",
				"name" => "メールアドレス",
				"max" => 256,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "メールアドレス"),
					"max" => sprintf(Message::VALIDATE_TYPE, "メールアドレス", "256文字以内"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "メールアドレス"),
				),
			),
			"url" => array(
				"type" => "url",
				"name" => "url",
				"max" => 256,
				"errmsg" => array(
					"max" => sprintf(Message::VALIDATE_TYPE, "url", "256文字以内"),
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "url"),
				),
			),
			"request_type" => array(
				"type" => "list",
				"name" => "ご相談内容",
				"list" => lpbrandModel::lists("request_type"),
				"errmsg" => array(
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "ご相談内容"),
				),
			),
			"request_site_type" => array(
				"type" => "list",
				"name" => "制作物の種類",
				"list" => lpbrandModel::lists("request_site_type"),
				"errmsg" => array(
					"wrong" => sprintf(Message::VALIDATE_WRONG_SELECT, "制作物の種類"),
				),
			),
			"budget" => array(
				"type" => "str",
				"name" => "ご予算",
				"max" => 150,
				"errmsg" => array(
					"max" => sprintf(Message::VALIDATE_TYPE, "ご予算", "150文字以内"),
				),
			),
			"contents" => array(
				"type" => "textarea",
				"name" => "お問い合わせ内容",
				"max" => 1000,
				"necessary" => 1,
				"errmsg" => array(
					"necessary" => sprintf(Message::VALIDATE_NECESSARY, "お問い合わせ内容"),
					"max" => sprintf(Message::VALIDATE_TYPE, "お問い合わせ内容", "1000文字以内"),
				),
			),
			"privacy" => array(
				"type" => "privacy",
				"name" => "個人情報の取り扱い",
				"noregister" => 1,
				"errmsg" => array(
					"wrong" => Message::VALIDATE_WRONG_PRIVACY,
				),
			),
		);

		return $items;
	}


	//=================================================================
	//　●項目設定
	//=================================================================
	function lists($key = null) {

		$items = array(
			"request_type" => array(
					1 => "新規制作",
					2 => "リニューアル",
					3 => "保守・運用",
					9 => "その他"
			),
			"request_site_type" => array(
					1 => "コーポレートサイト",
					2 => "ブランドサイト",
					3 => "キャンペーンサイト",
					4 => "メディアサイト",
					5 => "ECサイト",
					6 => "ランディングページ",
					7 => "リクルートサイト",
					99 => "その他"
			),
		);

		if($key) {
			$items = $items[$key];
		}

		return $items;
	}


	//=================================================================
	//　●DB登録
	//=================================================================
	function insert($vars, $structures, $lists) {

		$add_items = array(
			"created" => array(
				"value" => "NOW()",
				"type_db" => "nosingle"
			),
			"updated" => array(
				"value" => "NOW()",
				"type_db" => "nosingle"
			),
		);
		$structures = array_merge($structures, $add_items);

		$this->db->insertVars($vars, $structures, $lists);
	}

	//=================================================================
	//　●メール送信
	//=================================================================
	function sendMail($vars, $lists, $mail_template_path) {

		$param = array(
			"vars" => $vars,
			"lists" => $lists
		);

		//ユーザー用メール
		$options = array(
			"to" => $vars["email"],
			"from" => USER_MAIL_FROM,
			"file" => "lp_brand/user.mail",
			"param" => $param,
			"template_path" => $mail_template_path
		);

		CoMail::send($options);


		//管理者用メール
		$admin_to = ADMIN_MAIL_TO;
		if (DEBUG_MODE == 1) {
			$admin_to = $vars["email"];
		}

		$options = array(
			"to" => $admin_to,
			"from" => ADMIN_MAIL_FROM,
			"file" => "lp_brand/admin.mail",
			"param" => $param,
			"template_path" => $mail_template_path
		);

		CoMail::send($options);
	}

}
