<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2013/06/26
 * Update   : 2013/06/26
 * Description : フォーム投稿コントローラ
 **************************************************************************/

class lpbrandController extends defaultController{

	var $lpbrand;
	var $dir;
	var $platform_dir;

	var $vars  = array();
	var $structures = array();
	var $lists = array();
	var $err_msg = array();

	function lpbrandController() {

		//　共通Controllerの読み込み
		parent::__construct();

		//テンプレートのディレクトリを設定
		$this->dir = "lp_brand" . DIRECTORY_SEPARATOR;

		//　モデル読み込み
		include("lpbrand_model.php");
		$this->lpbrand = new lpbrandModel();

		//　項目設定
		$this->structures = $this->lpbrand->structure();
		$this->lists = $this->lpbrand->lists();

		$this->vars = $this->util->chgForm($_POST);
		$this->vars = $this->util->initVars($this->vars, $this->lpbrand->structure());

		foreach($this->structures as $key => $structures) {
			if (isset($structures["type"]) && ($structures["type"] == "tel" || $structures["type"] == "int")) {
				$this->vars[$key] = mb_convert_kana($this->vars[$key], "n");
			}
		}

		$this->template = new CoTemplate(WEB_PATH . TMPL_DIR . $this->platform_dir .  $this->dir);
	}


	//=================================================================
	//　●メイン処理
	//=================================================================
	function main() {

		if (isset($_GET["mode"]) && $_GET["mode"] == "thanks") {
			$this->thanks();
			exit;
		}

		switch($this->vars["mode"]) {
			case "entry":
				return $this->entry();

			case "confirm":
				return $this->confirm();

			default:
				return $this->input();
		}
	}

	//=================================================================
	//　●お問い合わせのindex表示
	//=================================================================
	function index() {

		$items = array(
				"template" => "index.php",
		);
		//テンプレート表示
		$this->display($items["template"], $this);
	}

	//=================================================================
	//　●お客様情報　登録
	//=================================================================
	function entry() {

		$items = array(
			"error_template" => "input.php",
			"thanks_url" => "thanks.html",
			"admin_mailtmp" => "admin.mail",
			"user_mailtmp" => "user.mail",
		);

		//入力チェック
		$this->err_msg = $this->errorCheck($this->vars, $this->structures);
		if (count($this->err_msg) > 0) {
			$this->template->display($items["error_template"], $this);
			exit;
		}

		// 分割要素を結合する。
		foreach ($this->structures as $key => $structures) {
			if(isset($structures["join"])) {
				$val = "";
				foreach($structures["join"] as $join_key) {
					$val .= $this->vars[$join_key] . "-";
				}
				$this->vars[$key] = substr($val,0,-1);
			}
		}

		//DB登録
		$this->lpbrand->insert($this->vars, $this->structures ,$this->lists);

		//メール送信
		$mail_template_path = WEB_PATH . TMPL_DIR . MAIL_TMPL_DIR;
		$this->lpbrand->sendMail($this->vars, $this->lists, $mail_template_path);

		//リダイレクト
		$this->util->location($items["thanks_url"]);
	}


	//=================================================================
	//　●お客様情報　確認
	//=================================================================
	function confirm() {

		$items = array(
			"template" => "confirm.php",
			"error_template" => "input.php",
		);

		//入力チェック
		$this->err_msg = $this->errorCheck($this->vars, $this->structures);
		if (count($this->err_msg) > 0) {
			//入力テンプレート表示
			$this->display($items["error_template"], $this);
			exit;
		}

		//テンプレート表示
		$this->display($items["template"], $this);
	}


	//=================================================================
	//　●お客様情報　入力
	//=================================================================
	function input() {

		$items = array(
			"template" => "input.php",
		);

		//デフォルトの値を設定
		$this->vars = $this->util->setInitValues($this->vars, $this->lpbrand->structure());

		//テンプレート表示
		$this->display($items["template"], $this);
	}


	//=================================================================
	//　●お客様情報　完了画面
	//=================================================================
	function thanks() {

		$items = array(
			"template" => "thanks.php",
		);

		//テンプレート表示
		$this->display($items["template"], $this);
	}


	//=================================================================
	//　●エラーチェック
	//=================================================================
	function errorCheck($vars, $structures) {

		//入力チェック
		$err_msg = $this->validate->allCheck($vars, $structures);

		//例外入力チェック
		$err_msg = $this->exceptionCheck($vars, $structures, $err_msg);

		return $err_msg;
	}

	//=================================================================
	//　●例外チェック
	//=================================================================
	function exceptionCheck($vars, $structures, $err_msg) {
		return $err_msg;
	}

	//=================================================================
	//　●テンプレート表示
	//=================================================================
	function display($template, $obj = null) {
		$this->template->display($template, $obj);
	}
}

