<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2013/06/26
 * Update   : 2013/06/26
 * Description : フォーム投稿コントローラ
 **************************************************************************/

// WPのメソッドをつかう為にヘッダーを読み込む
//require_once( WEB_PATH."/cms/wp-blog-header.php");

class entryController extends defaultController{

	var $entry, $dir, $platform_dir, $files;

	var $vars  = array();
	var $structures = array();
	var $lists = array();
	var $err_msg = array();

	function entryController($platform = null) {

		//　共通Controllerの読み込み
		parent::__construct();

		$this->files = new File();

		//テンプレートのディレクトリを設定
		$this->dir = "recruit/entry" . DIRECTORY_SEPARATOR;

		//　モデル読み込み
		include("entry_model.php");
		$this->entry = new entryModel();

		//　項目設定
		$this->structures = $this->entry->structure();
		$this->lists = $this->entry->lists();

		// 採用情報の公開記事から応募職種を取得
//		global $wpdb;
//		$results = $wpdb->get_results("SELECT ID,post_title FROM $wpdb->posts where post_type = 'recruit' AND post_status = 'publish' ORDER BY post_date DESC");
//		foreach ($results as $val) {
//			$this->lists["target_job"][$val->ID] = $val->post_title;
//		}

		//プラットフォーム毎に設定
		switch ($platform) {

			case PC_NO:
				$this->platform_dir = PC_CURRENT_DIR;
				$this->vars = $this->util->chgForm($_POST);
				break;

			case SP_NO:
				$this->platform_dir = SP_CURRENT_DIR;
				$this->vars = $this->util->chgForm($_POST);
				break;

			case MP_NO:
				$this->platform_dir = MP_CURRENT_DIR;
				$options = array(
					"to_strcode" => "UTF-8",
					"from_strcode" => "SJIS-win",
				);
				$this->vars = $this->util->chgForm($_POST, $options);
				break;

			default:
				$this->platform_dir = "";
				break;
		}

		$this->vars = $this->util->initVars($this->vars, $this->entry->structure());
		$this->vars["platform"] = $platform;

		foreach($this->structures as $key => $structures) {
			if (isset($structures["type"]) && ($structures["type"] == "tel" || $structures["type"] == "int" || $structures["type"] == "date" || $structures["type"] == "date02")) {
				$this->vars[$key] = mb_convert_kana($this->vars[$key], "n");
			}
		}

		$this->template = new CoTemplate(WEB_PATH . $this->platform_dir . TMPL_DIR . $this->dir);
	}


	//=================================================================
	//　●メイン処理
	//=================================================================
	function main() {

		switch($this->vars["mode"]) {

			case "entry":

				return $this->entry();

			case "confirm":

				return $this->confirm();

			default:

				return $this->input();
		}
	}


	//=================================================================
	//　●お客様情報　登録
	//=================================================================
	function entry() {

		$items = array(
			"error_template" => "input.php",
			"thanks_url" => "thanks.html",
			"admin_mailtmp" => "admin.mail",
			"user_mailtmp" => "user.mail",
		);

		// アップロードファイルを保存場所にコピー
		list($this->vars, $this->err_msg) = $this->files->tmpfileMove($this->vars, $this->structures);

		//入力チェック
		$this->err_msg = $this->errorCheck($this->vars, $this->structures, $this->err_msg);
		if (count($this->err_msg) > 0) {
			$this->template->display($items["error_template"], $this);
			exit;
		}

		// 分割要素を結合する。
		foreach ($this->structures as $key => $structures) {
			if(isset($structures["join"])) {
				$val = "";
				foreach($structures["join"] as $join_key) {
					if ($this->vars[$join_key] != "") {
						$val .= $this->vars[$join_key] . "-";
					}
				}
				if ($val != "") {
					$this->vars[$key] = substr($val,0,-1);
				}
			}
		}

		//DB登録
		$this->entry->insert($this->vars, $this->structures ,$this->lists);

		//メール送信
		$mail_template_path = WEB_PATH . $this->platform_dir . TMPL_DIR . MAIL_TMPL_DIR;
		$this->entry->sendPhpMailer($this->vars, $this->lists, $mail_template_path);

		//リダイレクト
		$this->util->location($items["thanks_url"]);
	}


	//=================================================================
	//　●お客様情報　確認
	//=================================================================
	function confirm() {

		$items = array(
			"template" => "confirm.php",
			"error_template" => "input.php",
		);

		//アップロードファイルをテンポラリに保存
		list($this->vars, $this->err_msg) = $this->files->tmpfileUpload($this->vars, $this->structures, $_FILES);

		//入力チェック
		$this->err_msg = $this->errorCheck($this->vars, $this->structures, $this->err_msg);
		if (count($this->err_msg) > 0) {
			//入力テンプレート表示
			$this->display($items["error_template"], $this);
			exit;
		}

		//テンプレート表示
		$this->display($items["template"], $this);
	}


	//=================================================================
	//　●お客様情報　入力
	//=================================================================
	function input() {

		$items = array(
			"template" => "input.php",
		);

		// 応募職種のデフォルト値
//		if (isset($_GET["j"]) && isset($this->lists["target_job"][$_GET["j"]])) {
//			$this->structures["target_job"]["default"] = $_GET["j"];
//		}

		//デフォルトの値を設定
		$this->vars = $this->util->setInitValues($this->vars, $this->structures);

		//テンプレート表示
		$this->display($items["template"], $this);
	}


	//=================================================================
	//　●お客様情報　完了画面
	//=================================================================
	function thanks() {

		$items = array(
			"template" => "thanks.html",
		);

		//テンプレート表示
		$this->display($items["template"], $this);
	}


	//=================================================================
	//　●エラーチェック
	//=================================================================
	function errorCheck($vars, $structures, $err_msg) {

		// 就業開始可能日　西暦なら日時は必須
		if ($vars["start_date_select"] != "2") {
			$structures["start_date"]["necessary"] = 1;
		}
/*
		// 職務経歴書
		if (empty($err_msg["curriculum"]) && empty($vars["curriculum_new"]) && ($vars["history_from1"] != "" || $vars["history_from2"] != ""
				|| $vars["history_to1"] != "" || $vars["history_to2"] != ""
				|| $vars["company"] != "" || $vars["pattern"] != "" || $vars["job_type"] != "" || $vars["work_contents"] != ""))
		{
			$structures["history_from"]["necessary"] = 1;
			$structures["history_to"]["necessary"] = 1;
			$structures["company"]["necessary"] = 1;
			$structures["pattern"]["necessary"] = 1;
			$structures["job_type"]["necessary"] = 1;
			$structures["work_contents"]["necessary"] = 1;
		}
*/
		//入力チェック
		$err_msg = $this->validate->allCheck($vars, $structures, $err_msg);

		//例外入力チェック
		$err_msg = $this->exceptionCheck($vars, $structures, $err_msg);

		return $err_msg;
	}

	//=================================================================
	//　●例外チェック
	//=================================================================
	function exceptionCheck($vars, $structures, $err_msg) {

		// 電話番号
		if ($vars["tel01_1"] == "" && $vars["tel01_2"] == "" && $vars["tel01_3"] == "" && $vars["tel02_1"] == "" && $vars["tel02_2"] == "" && $vars["tel02_3"] == "") {
			$err_msg["tel01"] = $structures["tel01"]["errmsg"]["necessary"];
		}

		// 就業開始可能日が未定なら西暦のエラーは無視
		if ($vars["start_date_select"] == "2" && isset($err_msg["start_date"])) {
			unset($err_msg["start_date"]);
		}
/*
		// 履歴書
		if (empty($err_msg["resume_file"]) && empty($vars["resume_file_new"]) && $vars["resume_text"] == "") {
			$err_msg["resume_file"] = $structures["resume_file"]["errmsg"]["necessary"];
		}

		// 職務経歴書
		if (empty($err_msg["curriculum"]) && empty($vars["curriculum_new"]) && $vars["history_from1"] == "" && $vars["history_from2"] == ""
				&& $vars["history_to1"] == "" && $vars["history_to2"] == ""
				&& $vars["company"] == "" && $vars["pattern"] == "" && $vars["job_type"] == "" && $vars["work_contents"] == "")
		{
			$err_msg["curriculum"] = $structures["curriculum"]["errmsg"]["necessary"];
		}

		// 作品・その他
		if (empty($err_msg["work"]) && empty($vars["work_new"]) && $vars["work_text"] == "") {
			$err_msg["work"] = $structures["work"]["errmsg"]["necessary"];
		}
*/
		return $err_msg;
	}

	//=================================================================
	//　●テンプレート表示
	//=================================================================
	function display($template, $obj = null) {

		if ($this->vars["platform"] == MP_NO) {
			$this->template->convertDisplay($template, "SJIS-win", $obj);
		} else {
			$this->template->display($template, $obj);
		}
	}
}

