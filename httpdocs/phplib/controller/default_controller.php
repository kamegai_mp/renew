<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2013/06/26
 * Update   : 2013/06/26
 * Description : 共通コントローラー
 **************************************************************************/

class defaultController{

	var $template;
	var $html;
	var $validate;
	var $util;

	function defaultController() {

		//　classをセット
		$this->html = new CoHtml();
		$this->validate = new Validate();
		$this->util = new CoUtil();
	}
}

