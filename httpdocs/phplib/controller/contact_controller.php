<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2013/06/26
 * Update   : 2013/06/26
 * Description : フォーム投稿コントローラ
 **************************************************************************/

class contactController extends defaultController{

	var $contact;
	var $dir;
	var $platform_dir;

	var $vars  = array();
	var $structures = array();
	var $lists = array();
	var $err_msg = array();

	function contactController($dummy) {

		//　共通Controllerの読み込み
		parent::__construct();

		//テンプレートのディレクトリを設定
		$this->dir = "contact" . DIRECTORY_SEPARATOR;

		$type = null;
		// お問い合わせの項目を決める
		switch (parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)) {

			case "/contact/work/": // work
			case "/contact/work/index.php": // work
				$type = CONTACT_TYPE_WORK;
				break;

			case "/contact/recruit/": // recruit
			case "/contact/recruit/index.php": // recruit
				$type = CONTACT_TYPE_RECRUIT;
				break;

			case "/contact/etc/": // etc
				$type = CONTACT_TYPE_ETC;
				break;

			case "/contact/partner/": // partner
			case "/contact/partner/index.php": // partner
				$type = CONTACT_TYPE_PARTNER;
				break;

			case "/contact/package/": // partner
			case "/contact/package/index.php": // partner
				$type = CONTACT_TYPE_PACKAGE;
				break;

			default :
				$type = CONTACT_TYPE_ETC; // etc
		}

		//　モデル読み込み
		include("contact_model.php");
		$this->contact = new contactModel($type);

		//　項目設定
		$this->structures = $this->contact->structure();
		$this->lists = $this->contact->lists();

		$this->vars = $this->util->chgForm($_POST);
		$this->vars['type'] = $type;
		// お問い合わせの項目が「パートナー・協業のご相談」の場合会社名を任意にする
		// パッケージの問い合わせを追加
		if (in_array($this->vars["type"], array(CONTACT_TYPE_PARTNER,CONTACT_TYPE_PACKAGE))) {
			$this->structures["company"]["necessary"] = 0;
		}

		$platform = "pc";
		$this->platform_dir = PC_CURRENT_DIR;

		// スマホの場合はチェック項目を変更する
//		if ($platform == "sp" || $this->util->isSmt()) {
//			unset($this->structures["tel"]["join"]);
//			unset($this->structures["privacy"]);
//		}

		$this->vars = $this->util->initVars($this->vars, $this->contact->structure());
		$this->vars["platform"] = $platform;

		foreach($this->structures as $key => $structures) {
			if (isset($structures["type"]) && ($structures["type"] == "tel" || $structures["type"] == "int")) {
				$this->vars[$key] = mb_convert_kana($this->vars[$key], "n");
			}
		}

		$this->template = new CoTemplate(WEB_PATH . TMPL_DIR . $this->platform_dir .  $this->dir);
	}


	//=================================================================
	//　●メイン処理
	//=================================================================
	function main() {

		if (parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) == "/contact/") {
			$this->index();
			exit;
		}

		if (isset($_GET["mode"]) && $_GET["mode"] == "thanks") {
			$this->thanks();
			exit;
		}

		switch($this->vars["mode"]) {

			case "entry":

				return $this->entry();

			case "confirm":

				return $this->confirm();

			default:

				return $this->input();
		}
	}

	//=================================================================
	//　●お問い合わせのindex表示
	//=================================================================
	function index() {

		$items = array(
				"template" => "index.php",
		);
		//テンプレート表示
		$this->display($items["template"], $this);
	}

	//=================================================================
	//　●お客様情報　登録
	//=================================================================
	function entry() {

		$items = array(
			"error_template" => "input.php",
			"thanks_url" => "/thank-you/",
			"admin_mailtmp" => "admin.mail",
			"user_mailtmp" => "user.mail",
		);

		//入力チェック
		$this->err_msg = $this->errorCheck($this->vars, $this->structures);
		if (count($this->err_msg) > 0) {
			$this->template->display($items["error_template"], $this);
			exit;
		}

		// 分割要素を結合する。
		foreach ($this->structures as $key => $structures) {
			if(isset($structures["join"])) {
				$val = "";
				foreach($structures["join"] as $join_key) {
					$val .= $this->vars[$join_key] . "-";
				}
				$this->vars[$key] = substr($val,0,-1);
			}
		}

		//DB登録
		$this->contact->insert($this->vars, $this->structures ,$this->lists);

		//メール送信
		$mail_template_path = WEB_PATH . TMPL_DIR . MAIL_TMPL_DIR;
		$this->vars['title_prefix'] = "管理者宛";
		$typeList = $this->contact->lists("type");

		if (array_key_exists($this->vars['type'], $typeList)) {
			$this->vars['title_prefix'] = $typeList[$this->vars['type']];
		}
		$this->contact->sendMail($this->vars, $this->lists, $mail_template_path);

		//リダイレクト
		$this->util->location($items["thanks_url"]);
	}


	//=================================================================
	//　●お客様情報　確認
	//=================================================================
	function confirm() {

		$items = array(
			"template" => "confirm.php",
			"error_template" => "input.php",
		);

		//入力チェック
		$this->err_msg = $this->errorCheck($this->vars, $this->structures);
		if (count($this->err_msg) > 0) {
			//入力テンプレート表示
			$this->display($items["error_template"], $this);
			exit;
		}

		//テンプレート表示
		$this->display($items["template"], $this);
	}


	//=================================================================
	//　●お客様情報　入力
	//=================================================================
	function input() {

		$items = array(
			"template" => "input.php",
		);

		//デフォルトの値を設定
		$this->vars = $this->util->setInitValues($this->vars, $this->contact->structure());

		//テンプレート表示
		$this->display($items["template"], $this);
	}


	//=================================================================
	//　●お客様情報　完了画面
	//=================================================================
	function thanks() {

		$items = array(
			"template" => "thanks.php",
		);

		//テンプレート表示
		$this->display($items["template"], $this);
	}


	//=================================================================
	//　●エラーチェック
	//=================================================================
	function errorCheck($vars, $structures) {

		//入力チェック
		$err_msg = $this->validate->allCheck($vars, $structures);

		//例外入力チェック
		$err_msg = $this->exceptionCheck($vars, $structures, $err_msg);

		return $err_msg;
	}

	//=================================================================
	//　●例外チェック
	//=================================================================
	function exceptionCheck($vars, $structures, $err_msg) {
		return $err_msg;
	}

	//=================================================================
	//　●テンプレート表示
	//=================================================================
	function display($template, $obj = null) {

		if ($this->vars["platform"] == MP_NO) {
			$this->template->convertDisplay($template, "SJIS-win", $obj);
		} else {
			$this->template->display($template, $obj);
		}
	}
}

