<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2011/07/26
 * Update   : 2011/07/26
 * Description : メッセージリスト
 **************************************************************************/

class Message{

	const VALIDATE_NECESSARY = "%sを入力してください。";
	const VALIDATE_NECESSARY_SELECT = "%sを選択してください。";
	const VALIDATE_TYPE = "%sは%sで入力してください。";
	const VALIDATE_DIFF	 = "%sが一致しません。";
	const VALIDATE_WRONG	 = "%sの入力内容をお確かめください。";
	const VALIDATE_WRONG_SELECT	 = "%sの選択内容をお確かめください。";
	const VALIDATE_WRONG_PRIVACY	 = "個人情報の取り扱いに同意してください。";
	const VALIDATE_WRONG_KANA	 = "%sはカタカナで入力してください。";
	const VALIDATE_WRONG_DATE_NOW	 = "%sには本日以降の日付を入力してください。";
	const VALIDATE_WRONG_EITHER	 = "%sのいずれかを入力してください。";
	const UPFILE_OVER = "%sは2MB以内のWord、Excel、PowerPoint、PDFファイルを選択してください。";
	const VALIDATE_FILE_WORD_SELECT = "ファイルのアップロード、もしくは%sを入力してください。";
}