<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2011/07/26
 * Update   : 2011/07/26
 * Description : エラー処理
 **************************************************************************/

class ErrorHandler{

	//=================================================================
	//　●エラーハンドラ
	//=================================================================
	static function set($errno, $errstr, $errfile, $errline){

		$errno_msg = array("1" => "E_ERROR",
							"2" => "E_WARNING",
							"4" => "E_PARSE",
							"8" => "E_NOTICE",
							"16" => "E_CORE_ERROR",
							"32" => "E_CORE_WARNING",
							"64" => "E_COMPILE_ERROR",
							"128" => "E_COMPILE_WARNING",
							"256" => "E_USER_ERROR",
							"512" => "E_USER_WARNING",
							"1024" => "E_USER_NOTICE",
		//					"2048" => "E_STRICT",
							"4096" => "E_RECOVERABLE_ERROR",
							"8191" => "E_ALL",
		);

		if( isset($errno_msg[$errno]) ){

			$error_msg = $errno_msg[$errno]."\t".$errstr."\t".$errfile."\t".$errline;
			error_log($error_msg,0);

			if( DEBUG_MODE == 1 ){
				var_dump($error_msg);
			}else{
				header( "Location:". ERROR_URL);
			}
			exit;
		}
	}

	//=================================================================
	//　●エラーハンドラ テーブルロック解除
	//=================================================================
	static function unLock($errno, $errstr, $errfile, $errline){

		$errno_msg = array("1" => "E_ERROR",
							"2" => "E_WARNING",
							"4" => "E_PARSE",
							"8" => "E_NOTICE",
							"16" => "E_CORE_ERROR",
							"32" => "E_CORE_WARNING",
							"64" => "E_COMPILE_ERROR",
							"128" => "E_COMPILE_WARNING",
							"256" => "E_USER_ERROR",
							"512" => "E_USER_WARNING",
							"1024" => "E_USER_NOTICE",
		//					"2048" => "E_STRICT",
							"4096" => "E_RECOVERABLE_ERROR",
							"8191" => "E_ALL",
		);

		if( isset($errno_msg[$errno]) ){

			$error_msg = $errno_msg[$errno]."\t".$errstr."\t".$errfile."\t".$errline;
			error_log($error_msg,0);

			//ロールバックしてテーブルロックを解除する
			$db = new CoMysql();
			$db->query("ROLLBACK");
			$db->query("UNLOCK TABLES");

			if( DEBUG_MODE == 1 ){
				var_dump($error_msg);
			}else{
				header( "Location:". ERROR_URL);
			}
			exit;
		}
	}

	//=================================================================
	//　●エラー処理実行設定
	//=================================================================
	static function main(){
		set_error_handler( array("ErrorHandler","set") );
		ini_set("error_log",ERROR_LOG_PATH);
	}
}

ErrorHandler::main();

?>