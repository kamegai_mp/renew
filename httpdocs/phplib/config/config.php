<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2013/06/26
 * Update   : 2013/06/26
 * Description : 設定ファイル
 **************************************************************************/

//===========================================================================
//　●デバッグモード
//	1:ON	2:OFF
//===========================================================================
define( "DEBUG_MODE", 2 );
//===========================================================================

//===========================================================================
//　●パス設定
//===========================================================================
//DocumentRoot
define( "WEB_PATH", dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR );

define( "SITE_PATH", "https://www.metaphase.co.jp/" );

//ライブラリディレクトリ
define( "APP_PATH", WEB_PATH . "phplib" . DIRECTORY_SEPARATOR );

//テンプレートディレクトリ
define( "TMPL_DIR", "Templates". DIRECTORY_SEPARATOR );

//コントローラーディレクトリ
define( "CONTROLLER_PATH", APP_PATH ."controller". DIRECTORY_SEPARATOR );

//モデルディレクトリ
define( "MODEL_PATH", APP_PATH ."model". DIRECTORY_SEPARATOR );

//ライブラリディレクトリ
define( "LIBRARY_PATH", APP_PATH ."library". DIRECTORY_SEPARATOR );

//エラーファイル
define( "ERROR_URL", "system_error.php" );
define( "ERROR_LOG_PATH", APP_PATH ."log/php.log" );

//メールテンプレートディレクトリ
define( "MAIL_TMPL_DIR", "mail". DIRECTORY_SEPARATOR);

//デバイス別ディレクトリ
define( "PC_CURRENT_DIR", "" );
define( "SP_CURRENT_DIR", "sp". DIRECTORY_SEPARATOR );
define( "MP_CURRENT_DIR", "" );

// デバイス別識別ナンバー
define( "PC_NO", 1);
define( "SP_NO", 2);
define( "MP_NO", 3);

//ドキュメントルートURL
// define( "DOCUMENTROOT_URL", "http://localhost/" );
// define( "DOCUMENTROOT_SSL_URL", "https://localhost/" );
//===========================================================================

//===========================================================================
//	●アップロード設定
//===========================================================================
//画像ファイルのテンポラリパス
define( "IMAGE_PATH", WEB_PATH . 'upfiles/' );
define( "IMAGE_TEMP_PATH", WEB_PATH . 'recruit/entry/tmp_upfiles/' );
define( "IMAGE_TEMP_URL", SITE_PATH . 'recruit/entry/tmp_upfiles/' );

//画像情報
define( "MAX_FILE_SIZE", 2097152 );
define( "ADD_WORD_ORIGNAL", "_orignal" );
define( "ADD_WORD_NEW", "_new" );
//===========================================================================

// お問い合わせの種別
define( "CONTACT_TYPE_WORK", 1 );
define( "CONTACT_TYPE_RECRUIT", 2 );
define( "CONTACT_TYPE_ETC", 3 );
define( "CONTACT_TYPE_PARTNER", 4 );
define( "CONTACT_TYPE_PACKAGE", 5 );
