<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2011/07/26
 * Update   : 2018/05/07
 * Description : email情報ファイル
 **************************************************************************/

//===========================================================================
//　●本番環境
//===========================================================================
// 管理者メール
define( "ADMIN_MAIL_TO", "info@metaphase.co.jp");
//define( "ADMIN_MAIL_TO", "ihara+ad@metaphase.co.jp");

define( "WORK_ADMIN_MAIL_TO", "info@metaphase.co.jp");
//define( "WORK_ADMIN_MAIL_TO", "ihara+ad@metaphase.co.jp,ihara+adad@metaphase.co.jp");

// 管理者メール FROM
define( "ADMIN_MAIL_FROM", "admin@metaphase.co.jp");

// ユーザ宛てfrom
define( "USER_MAIL_FROM",  "info@metaphase.co.jp" );

// リクルート用管理者メールアドレス
define("RECRUIT_ADMIN_MAIL_TO", "recruit@metaphase.co.jp");
//define("RECRUIT_ADMIN_MAIL_TO", "ihara+recruit@metaphase.co.jp");

// リクルート用ユーザ宛てfrom
define("RECRUIT_USER_MAIL_FROM", "recruit@metaphase.co.jp");

// パッケージ用管理者メールアドレス
define("PACKAGE_ADMIN_MAIL_TO", "m-producer@metaphase.co.jp");

//===========================================================================
