<?php
/**************************************************************************
 * author   : ihara
 * Create   : 2011/07/26
 * Update   : 2011/07/26
 * Description : 初期宣言・ライブラリ読み込み
 **************************************************************************/

//=================================================================
//　●PHP設定
//=================================================================
mb_internal_encoding( 'UTF-8' );
mb_language( 'japanese' );
error_reporting(E_ALL);
ini_set("display_errors", "On");
date_default_timezone_set('Asia/Tokyo');
//=================================================================


//=================================================================
//　●設定ファイル読み込み
//=================================================================
require_once( dirname(__FILE__)."/config.php" );
require_once( dirname(__FILE__)."/database.php" );
require_once( dirname(__FILE__)."/email.php" );
require_once( dirname(__FILE__)."/Message.php" );
require_once( dirname(__FILE__)."/error_handler.php" );
//=================================================================


//=================================================================
//　●ライブラリ読み込み
//=================================================================
foreach( glob( APP_PATH. "class/core/" . 'Co*.php' ) as $filename ) {
	if( is_file( $filename )) {
		require_once( $filename );
	}
}
require_once( APP_PATH."class/ValidateClass.php" );
require_once( APP_PATH."class/FileClass.php" );
require_once( LIBRARY_PATH."class.upload.php" );
//=================================================================


//=================================================================
//　●コントローラーディレクトリにパスを通す
//　●モデルディレクトリにパスを通す
//=================================================================
set_include_path(get_include_path() . PATH_SEPARATOR . CONTROLLER_PATH . PATH_SEPARATOR . MODEL_PATH);
//=================================================================

//=================================================================
//　●共通ControllファイルとModelファイルを読み込み
//=================================================================
require_once("default_controller.php");
require_once("default_model.php");
//=================================================================



