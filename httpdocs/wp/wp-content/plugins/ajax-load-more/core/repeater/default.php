<?php 
if (get_post_type(get_the_ID()) == 'blog') {
  $post_tag = wp_get_object_terms( get_the_ID(), 'blog-label' );
  $post_tag = $post_tag[0]->name;
  $post_date = get_the_date('Y.m.d', get_the_ID() );
  $post_introduction = get_field('blog_introduction', get_the_ID());
  $post_thumbnail = get_field('blog_thumbnail', get_the_ID());
  $post_thumbnail = $post_thumbnail['sizes']['large'];
  ?>
  <li class="columns"><a href="<?php the_permalink(); ?>">
  	<article class="article__blog">
    <div class="article__blog--image">
  		<figure style="background-image: url(<?php echo $post_thumbnail; ?>);"></figure>
    </div>
  		<div class="article__blog--head row align-middle">
  			<p class="article__blog--category columns"><span><?php echo $post_tag; ?></span></p>
  			<time class="article__blog--time fontACN columns"><?php echo $post_date; ?></time>
  		</div>
  		<div class="article__blog--main">
  			<h4 class="article__blog--title"><?php the_title(); ?></h4>
  			<p class="article__blog--text"><?php echo $post_introduction; ?></p>
  		</div>
  	</article>
  </a></li>
<?php } elseif(get_post_type(get_the_ID()) == 'news') {
  $post_tag = wp_get_object_terms( get_the_ID(), 'news-tag' );
  $post_tag = $post_tag[0]->name;
  $post_date = get_the_date('Y.m.d', get_the_ID() );
?>
  <li>
    <article class="article__news row align-middle news_list">
      <div class="columns medium-3 small-12">
        <div class="article__news--head row align-middle">
          <p class="article__news--category columns small-4 medium-6"><?php echo $post_tag; ?></p>
          <time class="article__news--time fontACN columns small-6 medium-6"><?php echo $post_date; ?></time>
        </div>
      </div>
      <div class="columns medium-9 small-12">
        <p class="article__news--link"><a href="<?php the_permalink(); ?>" class="link link__underline" title="<?php the_title(); ?>"><?php the_title(); ?></a></p>
      </div>
    </article>
  </li>
<?php } ?>