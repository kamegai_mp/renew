<?php
/* Template Name: Thank you page */
get_header(); ?>


<div class="section section__sv bg untitled"></div>

<section class="section section__contact">
	<?php if ( have_posts() ) { while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; } ?>

	<div class="section__link medium">
		<a href="<?php echo home_url(); ?>" class="link link__text" title="TOPに戻る">TOPに戻る<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a>
	</div>
</section>



<?php get_footer(); ?>
