<?php
/* Template Name: Service Concept */
get_header(); ?>


<section class="section section__sv bg">
	<h2 class="section__sv--title">サービスコンセプト</h2>

	<ul class="section__sv--menu menu align-center show-for-medium">
		<li><a href="<?php echo get_permalink(get_page_by_path('service')); ?>" class="link link__text" title="サービスコンセプト">サービスコンセプト<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('service/experiencedesign')); ?>" class="link link__text" title="エクスペリエンスデザイン">エクスペリエンスデザイン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('service/retention')); ?>" class="link link__text" title="リテンション">リテンション<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('service/businessdevelopment')); ?>" class="link link__text" title="プロダクトサービス">プロダクトサービス<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
	</ul>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><a href="<?php echo get_permalink(get_page_by_path('service')); ?>" title="サービス">サービス</a></li>
			<li><span class="show-for-sr">Current: </span>サービスコンセプト</li>
		</ul>
	</nav>
</section>

<section class="section small">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">コンセプト</h2>
			<h3 class="section--subtitle">ユーザーにとって最も心地よく、<br class="show-for-medium" />ビジネスにとって<br class="hide-for-medium" />最も効果の高いクリエイティブを提供する。</h3>
			<p class="section--headtext">サービスの立ち上げから、グロースまで、<br />いかなる事業フェイズでもメタフェイズは<br class="hide-for-medium" />クリエイティブ領域であなたをサポートします。</p>
		</div>
	</div>
	<div class="section--main medium">
		<article class="article article__service-concept">
			<figure class="article__service-concept--image">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_concept-01.png" alt="Concept" />
			</figure>
		</article>
	</div>
</section>

<section class="section medium bg__highlight">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">バリュー</h2>
			<h3 class="section--subtitle">ユーザー視点 × 実現力</h3>
			<p class="section--headtext">我々は過去15年にわたる活動で4,000以上の<br class="hide-for-medium" />クリエイティブを手がけてきました。<br />その経験によって培われたユーザー視点と<br class="hide-for-medium" />クリエイティブノウハウで、<br class="hide-for-medium" />机上の空論にならない効果的な戦略を考えます。</p>
		</div>
	</div>
	<div class="section--main medium">
		<article class="article article__service-value">
			<ul class="article__service-value--list row" data-equalizer>
				<li class="columns small-12 medium-6">
					<dl class="align-middle card card__border" data-equalizer-watch>
						<dt>ユーザー視点</dt>
						<dd>ビジネス上の目的はあれど使うのはユーザーです。ビジネスへの貢献を最大化する上でもユーザーの
						<br />満足度を高めることは重要になる為、我々はユーザー視点を常に意識し、ユーザーインタビューや
						<br />ユーザーテストなどのリサーチをもとにペルソナ設計やジャーニーマップの設計を行います。</dd>
					</dl>
				</li>
				<li class="columns small-12 medium-6">
					<dl class="align-middle card card__border" data-equalizer-watch>
						<dt>実現性</dt>
						<dd>どんなに優れた戦略を描いても実現できなければ机上の空論です。我々はこれまでのケーパビリティを活かし
						<br />戦略段階から実現方法とセットで考え、プロトタイプによる検証を行い実現性の高い戦略を実現します。</dd>
					</dl>
				</li>
			</ul>
		</article>
	</div>
</section>

<section class="section medium">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">サービス</h2>
		</div>
	</div>
	<div class="section--main medium">
		<ul class="row article__bottom-menu">
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service')); ?>" class="button button__underline expanded active" title="サービスコンセプト">サービスコンセプト<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service/experiencedesign')); ?>" class="button button__underline expanded" title="エクスペリエンスデザイン">エクスペリエンスデザイン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service/retention')); ?>" class="button button__underline expanded" title="リテンション">リテンション<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service/businessdevelopment')); ?>" class="button button__underline expanded" title="プロダクトサービス">プロダクトサービス<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		</ul>
	</div>
</section>


<?php get_footer(); ?>
