<?php
/* Template Name: Product */
get_header(); ?>


<section class="section section__sv bg">
	<h2 class="section__sv--title">プロダクト</h2>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><span class="show-for-sr">Current: </span>プロダクト</li>
		</ul>
	</nav>
</section>

<section class="section section__products">
	<div class="section--main large">
		<article class="article article__products--2col row">
			<div class="columns small-12 medium-5">
				<h4 class="article__products--title bold">無料デザインテンプレート</h4>
				<p class="article__products--text">無料でご利用いただけるレスポンシブ対応のテンプレートです。
				<br class="show-for-medium" />オンラインで一部のカラーやフォントの変更ができます。
				<br class="show-for-medium" />ダウンロードしてご利用ください。</p>
				<p class="text-left"><a href="/products/template/" class="link__text" title="詳しくはこちら">詳しくはこちら<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></p>
			</div>
			<div class="columns small-12 medium-7">
				<figure class="article__products--image">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/products/img_free-package-01.png" srcset="" alt="">
				</figure>
			</div>
		</article>
		<article class="article article__products article__products--2col row">
			<div class="columns small-12 medium-5" style="direction: ltr;">
				<h4 class="article__products--title bold">Webサイト診断</h4>
				<p class="article__products--text">Web制作のプロがノウハウを集結させた簡易診断ツールです。<br class="show-for-medium" />弊社のノウハウを用いて簡易的に問題点を抽出できるので、Webサイト改善への第一歩として、是非ご利用ください。</p>
				<p class="text-left"><a href="/siteCheck/" class="link__text" title="詳しくはこちら" target="_blank">詳しくはこちら<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></p>
			</div>
			<div class="columns small-12 medium-7">
				<figure class="article__products--image">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/products/img_sitecheck-01.png" srcset="" alt="">
				</figure>
			</div>
		</article>
		<article class="article article__products--2col row">
			<div class="article__products--column columns small-12 medium-5">
				<h4 class="article__products--title fontHVN">JOB CURATOR</h4>
				<p class="article__products--text">業界に特化した求人情報のまとめサイトを構築・管理できるシステム。
				<br class="show-for-medium" />求人情報を定期的にクロールする機能と求人情報の出稿を管理する
				<br class="show-for-medium" />機能があります。</p>
			</div>
			<div class="article__products--column columns small-12 medium-7">
				<figure class="article__products--image">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/products/img_job-curator-01.png" srcset="" alt="">
				</figure>
			</div>
		</article>
	</div>
</section>

<section class="section small bg__highlight section__contact">
	<div class="row section--head">
		<div class="columns">
			<h3 class="section--subtitle">New Product Coming soon...</h3>
			<p class="section--headtext">メタフェイズでは常に新しい挑戦を行っています。</p>
		</div>
	</div>
	<div class="section--main small">
		<article class="article__contact">
			<div class="article__contact--button">
				<a href="<?php echo get_permalink(get_page_by_path('recruit')); ?>" class="expanded button" title="採用希望の方">採用希望の方</a>
			</div>
		</article>
	</div>
</section>


<?php get_footer(); ?>
