<?php
remove_action('wp_head','wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
foreach ( array( 'rss2_head', 'commentsrss2_head', 'rss_head', 'rdf_header',
	'atom_head', 'comments_atom_head', 'opml_head', 'app_head' ) as $action ) {
	if ( has_action( $action, 'the_generator' ) )
		remove_action( $action, 'the_generator' );
}


if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   	wp_deregister_script('jquery');
  	wp_deregister_script( 'wp-embed' );
    wp_register_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js", false, null);
    wp_enqueue_script('jquery');
}
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );



/*-------------------------------------------*/
/*	metaphase_theme_setup
/*-------------------------------------------*/
// Widgets support
function neopa_theme_setup() {
	add_theme_support( 'widgets' );
}
add_action( 'after_setup_theme', 'neopa_theme_setup' );

// Register menu
function register_my_menu() {
  register_nav_menu('navigation',__( 'Navigation' ));
	register_nav_menu('footer-sitemap',__( 'Footer sitemap' ));
}
add_action( 'init', 'register_my_menu' );

// Remove post from menu
function remove_posts_menu() {
	remove_menu_page('edit.php');
}
add_action('admin_init', 'remove_posts_menu');



/*-------------------------------------------*/
/*	Get CSS / JS assets
/*-------------------------------------------*/
function neopa_get_css() {
	$filepath = glob("wp/wp-content/themes/metaphase/assets/css/app.2*.css");
	// $filepath = glob("wp-content/themes/metaphase/assets/css/app.2*.css");
	$filename = basename($filepath[count($filepath) - 1]).PHP_EOL;
	echo $filename;
}

function neopa_get_js() {
	$filepath = glob("wp/wp-content/themes/metaphase/assets/js/common.2*.js");
	// $filepath = glob("wp-content/themes/metaphase/assets/js/common.2*.js");
	$filename = basename($filepath[count($filepath) - 1]).PHP_EOL;
	echo $filename;
}




/*-------------------------------------------*/
/*	Custom post type _ add member
/*-------------------------------------------*/
add_post_type_support( 'member', 'front-end-editor' );
add_action( 'init', 'neopa_member_create_post_type', 0 );
function neopa_member_create_post_type() {
	$memberLabel = 'メンバー紹介';
	register_post_type( 'member',
		array(
		'labels' => array(
			'name' => $memberLabel,
			'singular_name' => $memberLabel
		),
		'public' => true,
		'menu_position' =>5,
		'has_archive' => true,
		'supports' => array('title','editor','excerpt','thumbnail','author'),
		'rewrite' => array('slug' => 'about/member', 'with_front' => false),
		)
	);

	// Add Custom taxonomy
	register_taxonomy(
		'member-type',
		'member',
		array(
			'hierarchical' => true,
			'update_count_callback' => '_update_post_term_count',
			'label' => $memberLabel._x(' type','admin menu'),
			'singular_label' => $memberLabel._x(' type','admin menu'),
			'public' => true,
			'show_ui' => true,
			'rewrite' => array(
				'slug' => 'member/type',
				'with_front' => false
			),
		)
	);
}

/*-------------------------------------------*/
/*	Custom post type _ add work
/*-------------------------------------------*/
add_post_type_support( 'work', 'front-end-editor' );
add_action( 'init', 'neopa_work_create_post_type', 0 );
function neopa_work_create_post_type() {
	$workLabel = '実績';
	register_post_type( 'work',
		array(
		'labels' => array(
			'name' => $workLabel,
			'singular_name' => $workLabel
		),
		'public' => true,
		'menu_position' =>5,
		'has_archive' => true,
		'supports' => array('title','editor','excerpt','thumbnail','author'),
		'rewrite' => array('slug' => 'work', 'with_front' => false),
		)
	);

	// Add Custom taxonomy
	register_taxonomy(
		'work-category',
		'work',
		array(
			'hierarchical' => true,
			'update_count_callback' => '_update_post_term_count',
			'label' => $workLabel._x(' category','admin menu'),
			'singular_label' => $workLabel._x(' category','admin menu'),
			'public' => true,
			'show_ui' => true,
			'rewrite' => array(
				'slug' => 'work/category',
				'with_front' => false
			),
		)
	);
	register_taxonomy(
		'work-process-type',
		'work',
		array(
			'hierarchical' => true,
			'update_count_callback' => '_update_post_term_count',
			'label' => $workLabel._x(' process type','admin menu'),
			'singular_label' => $workLabel._x(' process type','admin menu'),
			'public' => true,
			'show_ui' => true,
			'rewrite' => array(
				'slug' => 'work/process-type',
				'with_front' => false
			),
		)
	);
}


/*-------------------------------------------*/
/*	Custom post type _ add blog
/*-------------------------------------------*/
add_post_type_support( 'blog', 'front-end-editor' );
add_action( 'init', 'neopa_blog_create_post_type', 0 );
function neopa_blog_create_post_type() {
	$blogLabel = 'ブログ';
	register_post_type( 'blog',
		array(
		'labels' => array(
			'name' => $blogLabel,
			'singular_name' => $blogLabel
		),
		'public' => true,
		'menu_position' =>5,
		'has_archive' => true,
		'supports' => array('title','editor','excerpt','thumbnail','author'),
		'rewrite' => array('slug' => 'blog', 'with_front' => false),
		)
	);

	// Add Custom taxonomy
	register_taxonomy(
		'blog-label',
		'blog',
		array(
			'hierarchical' => true,
			'update_count_callback' => '_update_post_term_count',
			'label' => $blogLabel._x(' label','admin menu'),
			'singular_label' => $blogLabel._x(' label','admin menu'),
			'public' => true,
			'show_ui' => true,
			'rewrite' => array(
				'slug' => 'blog/label',
				'with_front' => false
			),
		)
	);
}



/*-------------------------------------------*/
/*	Custom post type _ add News
/*-------------------------------------------*/
add_post_type_support( 'news', 'front-end-editor' );
add_action( 'init', 'neopa_news_create_post_type', 0 );
function neopa_news_create_post_type() {
	$newsLabel = '最新情報';
	register_post_type( 'news',
		array(
		'labels' => array(
			'name' => $newsLabel,
			'singular_name' => $newsLabel
		),
		'public' => true,
		'menu_position' =>5,
		'has_archive' => true,
		'supports' => array('title','editor','excerpt','thumbnail','author'),
		'rewrite' => array('slug' => 'news', 'with_front' => false),
		)
	);

	register_taxonomy(
		'news-tag',
		'news',
		array(
			'hierarchical' => true,
			'update_count_callback' => '_update_post_term_count',
			'label' => $newsLabel._x(' tag','admin menu'),
			'singular_label' => $newsLabel._x(' tag','admin menu'),
			'public' => true,
			'show_ui' => true,
			'rewrite' => array(
				'slug' => 'news/tag',
				'with_front' => false
			),
		)
	);
}

/*-------------------------------------------*/
/*	Custom post type _ add Writer
/*-------------------------------------------*/
add_post_type_support( 'writer', 'front-end-editor' );
add_action( 'init', 'neopa_writer_create_post_type', 0 );
function neopa_writer_create_post_type() {
	$writerLabel = 'Writer';
	register_post_type( 'writer',
		array(
		'labels' => array(
			'name' => $writerLabel,
			'singular_name' => $writerLabel
		),
		'public' => true,
		'menu_position' =>5,
		'has_archive' => true,
		'supports' => array('title','editor','excerpt','thumbnail','author'),
		'rewrite' => array('slug' => 'writer', 'with_front' => false),
		)
	);
}



/*-------------------------------------------*/
/*	Global navigation
/*-------------------------------------------*/
class description_walker extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		 $output .= "\n<ul class=\"sub-menu dropdown\">\n";
	}

	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = esc_attr( $class_names );
		$output .= $indent . '<li class="menu--item '.$class_names.'" id="menu-item-'. $item->ID . '"' . $value .'>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		$description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

		if($depth != 0) {
			$description = $append = $prepend = "";
		}

		$item_output = $args->before;
		$item_output .= '<a class="menu--item--link" '. $attributes .'>';
		$item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
		$item_output .= $description.$args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}



/*-------------------------------------------*/
/*	Custom archive builder
/*-------------------------------------------*/
function wp_custom_archive($postType = 'post', $current_year) {
    global $wpdb, $wp_locale;
    $defaults = array(
			'post_type'     => $postType,
        'limit' => '',
        'format' => 'html', 'before' => '',
        'after' => '', 'show_post_count' => false,
        'echo' => 1
    );
    $r = wp_parse_args( $defaults );
    extract( $r, EXTR_SKIP );
    if ( '' != $limit ) {
        $limit = absint($limit);
        $limit = ' LIMIT '.$limit;
    }
    // over-ride general date format ? 0 = no: use the date format set in Options, 1 = yes: over-ride
    $archive_date_format_over_ride = 0;
    // options for daily archive (only if you over-ride the general date format)
    $archive_day_date_format = 'Y/m/d';
    // options for weekly archive (only if you over-ride the general date format)
    $archive_week_start_date_format = 'Y/m/d';
    $archive_week_end_date_format   = 'Y/m/d';
    if ( !$archive_date_format_over_ride ) {
        $archive_day_date_format = get_option('date_format');
        $archive_week_start_date_format = get_option('date_format');
        $archive_week_end_date_format = get_option('date_format');
    }
    //filters
    $where = apply_filters('customarchives_where', "WHERE post_type = '$postType' AND post_status = 'publish'", $r );
    $join = apply_filters('customarchives_join', "", $r);
    $output = '';
        $query = "SELECT YEAR(post_date) AS `year`, count(ID) as posts FROM $wpdb->posts $join $where GROUP BY YEAR(post_date) ORDER BY post_date DESC $limit";
        $key = md5($query);
        $cache = wp_cache_get( 'wp_custom_archive' , 'general');
        if ( !isset( $cache[ $key ] ) ) {
            $arcresults = $wpdb->get_results($query);
            $cache[ $key ] = $arcresults;
            wp_cache_set( 'wp_custom_archive', $cache, 'general' );
        } else {
            $arcresults = $cache[ $key ];
        }
        if ( $arcresults ) {
            $afterafter = $after;
            foreach ( (array) $arcresults as $arcresult ) {
								if ($current_year == $arcresult->year) {
									$class_active = "active";
								} else {
									$class_active = '';
								}
								$year_text = '';
								$url_year = get_post_type_archive_link($postType).'?y='.$arcresult->year;
								$output_year = get_archives_link($url_year, $year_text, $format, $before, $after);
                $url = get_post_type_archive_link($postType).'?y='.$arcresult->year;

								$year_text = '<li><a class="aside-page--tags-link '.$class_active.'" href="'.$url_year.'">'.$arcresult->year.'</a></li>';

                if ( $show_post_count )
                    $after = '&nbsp;('.$arcresult->posts.')' . $afterafter;
                // $output .= ( $arcresult->year != $temp_year ) ? $year_text : '';
                $output .= $year_text;
                $temp_year = $arcresult->year;
            }
        }
    if ( $echo )
        echo $output;
    else
        return $output;
}

/*-------------------------------------------*/
/*	Description and keyword for post_typr_archive
/*-------------------------------------------*/
function my_description($description){
  if(is_post_type_archive('member')){
    $description = 'メタフェイズのメンバーをご紹介します。ユーザー目線からUIデザイン・UXデザインを考え、将来的なビジネスの成功に貢献するためのWeb制作を行っております。メタフェイズは東京にあるWeb制作会社でUI/UXデザインの設計・Web運用まで手がけております。お困りの方は是非お問い合せ下さい。';
  }
	if(is_post_type_archive('work')){
    $description = '4000以上あるメタフェイズの実績をご紹介します。ユーザー目線からUIデザイン・UXデザインを考え、将来的なビジネスの成功に貢献するためのWeb制作を行っております。メタフェイズは東京にあるWeb制作会社でUI/UXデザインの設計・Web運用まで手がけております。お困りの方は是非お問い合せ下さい。';
  }
	if(is_post_type_archive('news')){
    $description = 'メタフェイズからの新着情報の一覧です。ユーザー目線からUIデザイン・UXデザインを考え、将来的なビジネスの成功に貢献するためのWeb制作を行っております。メタフェイズは東京にあるWeb制作会社でUI/UXデザインの設計・Web運用まで手がけております。お困りの方は是非お問い合せ下さい。';
  }
	if(is_post_type_archive('blog')){
    $description = 'カテゴリに関するブログ一覧です。ユーザー目線からUIデザイン・UXデザインを考え、将来的なビジネスの成功に貢献するためのWeb制作を行っております。メタフェイズは東京にあるWeb制作会社でUI/UXデザインの設計・Web運用まで手がけております。お困りの方は是非お問い合せ下さい。';
  }

  return $description;
}
add_filter('aioseop_description', 'my_description');

function my_keywords($keywords){
  if(is_post_type_archive()){
    $keywords = 'メタフェイズ、UIデザイン、UXデザイン、UI/UXデザイン、web制作、web運用、ホームページ、制作会社';
  }
  return $keywords;
}
add_filter('aioseop_keywords', 'my_keywords');





/*-------------------------------------------*/
/*	Rewrite rules for Tags / Surgery tags
/*-------------------------------------------*/
add_action('init', 'add_neopa_url');
function add_neopa_url(){
  add_rewrite_rule(
    '^news/tag/([^/]*)$',
    'index.php?news-tag=$matches[1]',
    'top'
  );

	add_rewrite_rule(
    '^blog/label/([^/]*)$',
    'index.php?blog-label=$matches[1]',
    'top'
  );
}
