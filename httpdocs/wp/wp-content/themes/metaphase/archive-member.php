<?php get_header();
$manager = get_term_by( 'slug', 'manager', 'member-type' );
$args = array(
	'taxonomy' => 'member-type',
	'exclude' => $manager->term_id
);
$member_cats = get_terms($args);
?>


<section class="section section__sv bg">
	<h2 class="section__sv--title">メンバー紹介</h2>

	<ul class="section__sv--menu menu align-center show-for-medium">
		<li><a href="<?php echo get_permalink(get_page_by_path('about/')); ?>" class="link link__text" title="ビジョン">ビジョン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_post_type_archive_link('member'); ?>" class="link link__text active" title="メンバー紹介">メンバー紹介<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('about/overview')); ?>" class="link link__text" title="会社情報">会社情報<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
	</ul>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><a href="<?php echo get_permalink(get_page_by_path('about')); ?>" title="会社紹介">会社紹介</a></li>
			<li><span class="show-for-sr">Current: </span>メンバー紹介</li>
		</ul>
	</nav>
</section>

<section class="section small">

  <?php foreach ($member_cats as $cat) {
		$manager_args = array(
      'post_type' => 'member',
      'orderby' => 'date',
      'posts_per_page' => -1,
      'tax_query' => array(
        array(
          'taxonomy' => 'member-type',
          'field'    => 'slug',
          'terms'    => $cat->slug,
					'operator'    => 'IN'
        ),
        array(
          'taxonomy' => 'member-type',
          'field'    => 'slug',
          'terms'    => $manager->slug,
					'operator'    => 'IN'
        )
    	),
    );
    $member_args = array(
      'post_type' => 'member',
      'orderby' => 'date',
      'posts_per_page' => -1,
      'tax_query' => array(
        array(
          'taxonomy' => 'member-type',
          'field'    => 'slug',
          'terms'    => $cat->slug,
					'operator'    => 'IN'
        ),
        array(
          'taxonomy' => 'member-type',
          'field'    => 'slug',
          'terms'    => $manager->slug,
					'operator'    => 'NOT IN'
        )
    	),
    );

		$manager_list = get_posts( $manager_args );
    $member_list = get_posts( $member_args );

		$manager_list_not_empty = false;
		if (count($manager_list) != 0) {
			$manager_list_not_empty = true;
		}
		?>

    <article class="article article__member">
  		<div class="row section--head">
  			<div class="columns">
  				<h2 class="section--title"><?php echo $cat->name; ?></h2>
  			</div>
  		</div>
      <div class="section--main">
				<?php if ($manager_list_not_empty) { ?>
					<ul class="article__posted--member row small-up-2 large-up-4">
						<?php foreach ($manager_list as $post) {
							setup_postdata( $post );
							$member_img = get_field('member_photo');
							$member_img = $member_img['sizes']['large'];
							$member_totem = get_field('member_totem');
							$member_totem = $member_totem['sizes']['large'];
							?>
							<li class="column column-block">
								<div class="card__member">
									<div class="card__member--image" style="background-image: url(<?php echo $member_img; ?>)">
										<i class="card__member--icon"><img src="<?php echo $member_totem; ?>" alt="ヤモリ" /></i>
									</div>
									<div class="card__member--text">
										<h3 class="position"><?php echo get_field('member_job_title'); ?></h3>
										<p class="name"><?php the_title(); ?></p>
									</div>
								</div>
							</li>
						<?php wp_reset_postdata(); } ?>
					</ul>
				<?php } ?>
  			<ul class="article__posted--member row small-up-2 large-up-4">
          <?php foreach ($member_list as $post) {
            setup_postdata( $post );
            $member_img = get_field('member_photo');
						$member_img = $member_img['sizes']['large'];
						$member_totem = get_field('member_totem');
						$member_totem = $member_totem['sizes']['large'];
            ?>
            <li class="column column-block">
    					<div class="card__member">
    						<div class="card__member--image" style="background-image: url(<?php echo $member_img; ?>)">
									<i class="card__member--icon"><img src="<?php echo $member_totem; ?>" alt="ヤモリ" /></i>
    						</div>
    						<div class="card__member--text">
    							<h3 class="position"><?php echo get_field('member_job_title'); ?></h3>
    							<p class="name"><?php the_title(); ?></p>
    						</div>
    					</div>
    				</li>
          <?php wp_reset_postdata(); } ?>
        </ul>
      </div>
    </article>
  <?php } ?>
</section>

<section class="section small bg__highlight section__company">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title fontHVN">採用情報</h2>
			<h3 class="section--subtitle">メタフェイズの仲間を募集しています</h3>
		</div>
	</div>
	<div class="section--main small">
		<article class="article__contact">
			<div class="article__contact--buttons">
				<ul class="row menu align-center vertical align-middle medium-horizontal">
					<li class="columns small-12"><a href="<?php echo get_permalink(get_page_by_path('recruit')); ?>" class="expanded button medium line-2" title="採用希望の方">採用希望の方</a></li>
					<li class="columns small-12"><a href="<?php echo get_permalink(get_page_by_path('recruit')); ?>" class="expanded button medium" title="パートナーフリーランスの方">パートナー<br />フリーランスの方</a></li>
				</ul>
			</div>
		</article>
	</div>
</section>


<section class="section medium">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title fontHVN">会社紹介</h2>
		</div>
	</div>
	<div class="section--main medium">
		<ul class="row article__bottom-menu">
			<li class="columns medium-4 small-12"><a href="<?php echo get_permalink(get_page_by_path('about')); ?>" class="button button__underline expanded" title="ビジョン">ビジョン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns medium-4 small-12"><a href="<?php echo get_post_type_archive_link('member'); ?>" class="button button__underline expanded active" title="メンバー紹介">メンバー紹介<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns medium-4 small-12"><a href="<?php echo get_permalink(get_page_by_path('about/overview')); ?>" class="button button__underline expanded" title="会社情報">会社情報<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		</ul>
	</div>
</section>



<?php get_footer(); ?>
