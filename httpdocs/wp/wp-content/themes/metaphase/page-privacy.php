<?php
/* Template Name: Privacy */
get_header(); ?>




<section class="section section__sv bg">
	<h2 class="section__sv--title">個人情報保護方針</h2>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><span class="show-for-sr">Current: </span>個人情報保護方針</li>
		</ul>
	</nav>
</section>

<section class="section section__privacy">
	<article class="article article__privacy" style="margin-top:0;">

		<h3 class="article__privacy--title" style="margin-bottom:1.125rem;">個人情報保護方針</h3>

		<p class="article__privacy--text" style="margin-bottom:2.125rem;">株式会社メタフェイズ
		<br />代表取締役　城野　誠大</p>

		<p class="article__privacy--text top">当社は、個人情報を大切に保護することを企業の重要な社会的使命と認識し、役員はじめ全従業者が個人情報保護に関する法規範を遵守し、次に示す当社基本方針を具現化するために、JISQ15001に準拠した個人情報保護マネジメントシステムを構築し、常に社会的要請の変化に着目しつつ個人情報保護マネジメントシステムの継続的改善に全社を挙げて取り組むことを宣言いたします。</p>
		<ul class="article__privacy--list">
			<li><span>1.</span>個人情報保護の重要性を強く認識し、当社の主力事業であるウェブ制作事業などでお預かりするお客様の大事な個人情報を取り扱う場合及び当社従業者の個人情報など当社が取得するすべての個人情報は、事前に利用目的及び提供の有無を明確にし、本人の同意を得た上で、目的の範囲内において適切に利用し、目的外利用を行わないための措置を講じます。</li>
			<li><span>2.</span>前項の措置により取得した個人情報の取扱いの全部又は一部を委託する場合、及び個人情報を第三者に提供する場合には、十分な個人情報の保護水準を満たした事業者を選定し、契約等により適切な措置を講じます。</li>
			<li><span>3.</span>安心して個人情報をご提供いただけるように、個人情報の漏えい、滅失又はき損等の危険に対して、合理的な安全対策及び予防・是正措置を講じます。</li>
			<li><span>4.</span>大切な個人情報をお預かりする企業として、個人情報保護の取扱いに関する法令、個人情報保護法に基づいて各所管官庁が作成したガイドライン・指針、JIS Q 15001をはじめとするその他の規範を遵守します。</li>
			<li><span>5.</span>個人情報を適切に利用し、またその保護を徹底するために個人情報保護マネジメントシステムの継続的な見直し及び改善を行います。</li>
			<li><span>6.</span>弊社の個人情報の取扱いに関する苦情及び相談等があった場合には、下記の個人情報に関する苦情及び相談窓口にて、迅速かつ適切な対応をいたします。</li>
		</ul>
		<h4 class="article__privacy--stitle">個人情報に関する苦情及び相談の問合せ先</h4>
		<p class="article__privacy--text">株式会社メタフェイズ
		<br />個人情報に関する苦情及び相談窓口
		<br />TEL：03-6384-5184  FAX：03-6384-5183</p>
	</article>

	<article class="article article__privacy">
		<h3 class="article__privacy--title">個人情報の取扱いについて</h3>
		<p class="article__privacy--text top">弊社はご本人様の個人情報を管理するにあたっては細心の注意を払い、以下に掲げた通りに取扱います。</p>
		<h4 class="article__privacy--stitle">1.個人情報保護管理者について</h4>
		<p class="article__privacy--text">弊社は、次の者を個人情報保護管理者に任命し、ご本人様の個人情報を適切かつ安全に管理し、外部からの不正アクセス、紛失、破壊、改ざん、漏えい等を予防及び是正するための対策を講じています。</p>
		<p class="article__privacy--text">個人情報保護管理者：塩沢<br />連絡先 03-6384-5184</p>

		<h4 class="article__privacy--stitle">2.個人情報の利用目的について</h4>
		<p class="article__privacy--text">弊社は以下に掲げる利用目的のために個人情報を収集します。</p>
		<p class="article__privacy--text">・サービスの案内、メールマガジンの配信
		<br />・弊社サービスの提供ならびにそのサービス向上
		<br />・クライアントより受託したキャンペーン商品の発送代行をおこなうため
		<br />・弊社サービスへのお問合せに電話やメールにより回答させていただくため
		<br />・弊社が主催又は共催するセミナーの参加者に対して申込内容を確認するため
		<br />・弊社が主催又は共催するセミナーでアンケートを実施するため
		<br />・従業者の人事、労務、健康管理をおこなうため
		<br />・採用応募者へ面接日時及び採用可否の結果を連絡するため
		<br />・請求書の発送
		<br />・ウェブサイト制作業者を斡旋するため
		<br />・弊社が運営するウェブサイトの会員情報管理をおこなうため</p>

		<h4 class="article__privacy--stitle">3.個人情報の提供について</h4>
		<p class="article__privacy--text">弊社は以下の場合を除いて、第三者に個人情報を提供することはございません。</p>
		<p class="article__privacy--text">・ご本人様からの同意を得ている場合
		<br />・法令等により要求された場合
		<br />・弊社業務委託先に業務を委託する場合。この際には次項の「個人情報の預託について」に従います。</p>

		<h4 class="article__privacy--stitle">4.個人情報の預託について</h4>
		<p class="article__privacy--text">弊社は事業運営上、ご本人様により良いサービスを提供するために業務の一部を外部に委託しております。その際、業務委託先に個人情報を預託することがあります。この場合、弊社は、個人情報を適切に保護できる管理体制を敷き実行していることを条件として委託先を厳選したうえで、契約等において個人情報の適正管理・機密保持などによりご本人様の個人情報の漏洩防止に必要な事項を取決め、適切な管理を実施させます。</p>

		<h4 class="article__privacy--stitle">5.個人情報の開示・訂正・削除等について</h4>
		<p class="article__privacy--text">弊社は、弊社がご本人様からお預かりしている個人情報について、ご本人様から開示・訂正・削除等のお申出があった場合、ご本人様の本人確認をおこなった後、合理的な範囲で開示・訂正・削除等をいたします。ただし、他のご本人様の生命・身体・財産その他の利益を害するおそれのある場合、または当社の業務遂行に著しく支障をきたすと判断したときは、この限りではありません。また、法令等に定めのある場合は当該法令に基づいて適切な対応をいたします。</p>

		<h4 class="article__privacy--stitle">6.本人確認について</h4>
		<p class="article__privacy--text">弊社では、以下のいずれかの方法にて本人確認をさせていただいております。</p>
		<ul class="article__privacy--list">
			<li><span>1.</span>ご本人様から既に提供を受けている電話番号にかけ直して、本人を確認する。</li>
			<li><span>2.</span>ご本人様から既に提供を受けているメールアドレスにメールを送信し、そのメールに返信して頂くことで本人であることを確認する。</li>
			<li><span>3.</span>手続き時に、ご本人のお名前と住所を確認することで、本人を確認する。</li>
		</ul>

		<h4 class="article__privacy--stitle">7.開示等の求めに応じる手続きについて</h4>
		<p class="article__privacy--text">弊社では、本人確認を行なった後、ご本人様に対して「個人情報開示等請求書」を郵送させていただきます。ご本人様より「個人情報開示等請求書」を返送いただきましたら、速やかに開示等に対して遅滞なく回答させていただきます。回答方法は、電話による回答又は請求内容への回答を文書でご郵送させていただきます。なお開示等の請求にあたり、弊社では手数料の徴収はいたしません。</p>
		<p class="article__privacy--text">なお、個人情報に関する苦情及び相談、開示等についての弊社問合わせ先は、次の通りです。</p>

		<h4 class="article__privacy--stitle">株式会社メタフェイズ
		<br />個人情報に関する苦情及び相談窓口</h4>
		<p class="article__privacy--text">住所　〒160-0022
		<br />東京都新宿区新宿5丁目17-5ラウンドクロス新宿5丁目ビル6Ｆ
		<br />TEL：03-6384-5184  FAX：03-6384-5183</p>

	</article>

</section>



<?php get_footer(); ?>
