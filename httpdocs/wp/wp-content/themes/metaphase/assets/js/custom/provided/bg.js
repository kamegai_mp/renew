!function (n) {
    function e(i) {
        if (t[i]) 
            return t[i].exports;
        var o = t[i] = {
            exports: {},
            id     : i,
            loaded : !1
        };
        return n[i].call(o.exports, o, o.exports, e),
        o.loaded = !0,
        o.exports
    }
    var t = {};
    return e.m = n,
    e.c        = t,
    e.p        = "",
    e(0)
}([
    function (n, e, t) {
        n.exports = t(1)
    },
    function (n, e, t) {
        var i;
        i = t(2),
        $(function () {
            return new i
        })
    },
    function (n, e, t) {
        var i,
            o,
            r = function (n, e) {
                return function () {
                    return n.apply(e, arguments)
                }
            };
        window.utils = t(3),
        o            = t(19),
        i            = function () {
            function n(n) {
                this.parent                 = n,
                this.windowScrollHandler    = r(this.windowScrollHandler, this),
                this.windowTouchMoveHandler = r(this.windowTouchMoveHandler, this),
                this.windowMouseMoveHandler = r(this.windowMouseMoveHandler, this),
                this.windowResizeHandler    = r(this.windowResizeHandler, this),
                this.draw                   = r(this.draw, this),
                this.container              = document.getElementById("bg"),
                this.isWebGLSupported       = !1,
                this.init()
            }
            var e,
                i,
                a,
                s,
                x,
                l,
                u;
            return u                           = 1400,
            l                                  = 100,
            a                                  = document
                .getElementById("bg")
                .getAttribute('data-src'),
            e                                  = new THREE.Vector2(1380, 800),
            x                                  = new THREE.Vector2(2e3, 1200),
            i                                  = 350,
            s                                  = 120,
            n.LOADED                           = "loaded",
            n.prototype.init                   = function () {
                return Detector.webgl
                    ? this
                        .initWebGL()
                        .then(function (n) {
                            return function () {
                                return n.start()
                            }
                        }(this))
                    : this.notSupportHandler()
            },
            n.prototype.notSupportHandler      = function () {
                var n;
                log("not supported"),
                n = document
                    .body
                    .getAttribute("class")
                    .split(" "),
                n.push("noWebGL"),
                document
                    .body
                    .setAttribute("class", n.join(" "))
            },
            n.prototype.initWebGL              = function () {
                var n,
                    e,
                    t,
                    i;
                return this.width = this.container.offsetWidth,
                this.height       = this.container.offsetHeight,
                this.renderer     = new THREE.WebGLRenderer({
                    alpha    : !0,
                    antialias: !0
                }),
                this
                    .container
                    .appendChild(this.renderer.domElement),
                null == this
                    .renderer
                    .extensions
                    .get("OES_texture_float") && null == this
                    .renderer
                    .extensions
                    .get("OES_texture_half_float")
                        ? void this.notSupportHandler()
                        : (
                            this.devicePixelRatio    = Math.min(window.devicePixelRatio || 1, 2),
                            this.renderer.setPixelRatio(this.devicePixelRatio),
                            this.scene               = new THREE.Scene,
                            this.camera              = new THREE.OrthographicCamera(-this.width / 2, this.width / 2, this.height / 2, -this.height / 2, -10, 10),
                            this.camera.target       = new THREE.Vector3(0, 0, 0),
                            this.camera.position.z   = 10,
                            this.commonPlaneGeometry = new THREE.PlaneGeometry(l, l),
                            this.isMousePosInited    = !1,
                            this.pMousePos           = new THREE.Vector2,
                            this.mouseX              = null,
                            this.mouseY              = null,
                            this.time                = 1e5 * Math.random(),
                            this.beforeTime          = (new Date).getTime(),
                            e                        = location.search.replace("?", "").split("="),
                            n                        = e[0],
                            i                        = e[1],
                            this.animalNo            = 6,
                            t                        = $.cookie("theme"),
                            "no" === n
                                ? this.animalNo      = parseInt(i)
                                : t && (this.animalNo = parseInt(t.split("-")[1])),
                            this.initBgEffect(),
                            new Promise(function (n) {
                                return function (e) {
                                    return n
                                        .initAnimal()
                                        .then(function () {
                                            window.addEventListener("resize", n.windowResizeHandler),
                                            /*window.addEventListener("mousemove", n.windowMouseMoveHandler),
                                            window.addEventListener("touchmove", n.windowTouchMoveHandler),*/
                                            window.addEventListener("scroll", n.windowScrollHandler),
                                            n.isWebGLSupported = !0,
                                            n.setParameters(),
                                            n.windowResizeHandler(),
                                            n.windowScrollHandler(),
                                            e()
                                        })
                                }
                            }(this))
                        )
            },
            n.prototype.initAnimal             = function () {
                return this.animalAnimationParam = 1,
                this
                    .loadAnimalTexture(this.animalNo)
                    .then(function (n) {
                        return function () {
                            return n.animalMaterial = new THREE.RawShaderMaterial({
                                depthTest     : !1,
                                depthWrite    : !1,
                                fragmentShader: t(21),
                                transparent   : !0,
                                uniforms      : {
                                    animationParam: {
                                        type : "1f",
                                        value: 1
                                    },
                                    blurDirection : {
                                        type : "2f",
                                        value: new THREE.Vector2
                                    },
                                    resolution    : {
                                        type : "2f",
                                        value: x
                                    },
                                    scrollY       : {
                                        type : "1f",
                                        value: window.pageYOffset
                                    },
                                    texture       : {
                                        type : "t",
                                        value: n.animalTexture
                                    },
                                    time          : {
                                        type : "1f",
                                        value: 0
                                    }
                                },
                                vertexShader  : t(20)
                            }),
                            n.animalMesh            = new THREE.Mesh(n.commonPlaneGeometry, n.animalMaterial),
                            n
                                .animalMesh
                                .scale
                                .set(x.x / l, x.y / l, 1),
                            n
                                .scene
                                .add(n.animalMesh),
                            !0
                        }
                    }(this))
            },
            n.prototype.initDatGUI             = function () {
                var n;
                n = new dat.GUI,
                n.close(),
                n
                    .add(this, "animalAnimationParam", 0, 1)
                    .onChange(function (n) {
                        return function () {
                            return n.animalMaterial.uniforms.animationParam.value = n.animalAnimationParam
                        }
                    }(this)),
                n
                    .add(this, "texPixelRatio", .2, 1)
                    .onChange(function (n) {
                        return function () {
                            return n.windowResizeHandler(),
                            n.setParameters()
                        }
                    }(this))
            },
            n.prototype.loadAnimalTexture      = function (n) {
                return new Promise(function (e) {
                    return function (t) {
                        var i;
                        i               = a + "img" + n + ".png",
                        e.animalTexture = (new THREE.TextureLoader).load(i, function (n) {
                            n.minFilter = THREE.LinearFilter,
                            t()
                        })
                    }
                }(this))
            },
            n.prototype.initBgEffect           = function () {
                var n;
                this.texPixelRatio              = .4,
                this.solverIteration            = 20,
                this.attenuation                = 1,
                this.alpha                      = 1,
                this.beta                       = 1,
                this.viscosity                  = .99,
                this.forceRadius                = 90,
                this.forceCoefficient           = 1,
                this.autoforceCoefficient       = .06,
                this.bgShaders                  = {},
                this.bgShaders.main             = new THREE.RawShaderMaterial({
                    depthTest     : !1,
                    depthWrite    : !1,
                    fragmentShader: t(23)("./renderColor" + this.animalNo + ".frag"),
                    uniforms      : {
                        dataTex         : {
                            type: "t"
                        },
                        devicePixelRatio: {
                            type : "1f",
                            value: this.devicePixelRatio
                        },
                        resolution      : {
                            type: "2f"
                        },
                        texPixelRatio   : {
                            type: "1f"
                        },
                        time            : {
                            type: "1f"
                        }
                    },
                    vertexShader  : t(22)
                }),
                this.bgShaders.updateDivergence = new THREE.RawShaderMaterial({
                    fragmentShader: t(30),
                    uniforms      : {
                        dataTex      : {
                            type: "t"
                        },
                        resolution   : {
                            type: "2f"
                        },
                        texPixelRatio: {
                            type: "1f"
                        }
                    },
                    vertexShader  : t(22)
                }),
                this.bgShaders.updatePressure   = new THREE.RawShaderMaterial({
                    fragmentShader: t(31),
                    uniforms      : {
                        alpha        : {
                            type: "1f"
                        },
                        beta         : {
                            type: "1f"
                        },
                        dataTex      : {
                            type: "t"
                        },
                        resolution   : {
                            type: "2f"
                        },
                        texPixelRatio: {
                            type: "1f"
                        }
                    },
                    vertexShader  : t(22)
                }),
                this.bgShaders.updateVelocity   = new THREE.RawShaderMaterial({
                    fragmentShader: t(32),
                    uniforms      : {
                        autoforceCoefficient: {
                            type: "1f"
                        },
                        dataTex             : {
                            type: "t"
                        },
                        forceCoefficient    : {
                            type: "1f"
                        },
                        forceRadius         : {
                            type: "1f"
                        },
                        mousePos            : {
                            type: "2f"
                        },
                        pMousePos           : {
                            type: "2f"
                        },
                        resolution          : {
                            type: "2f"
                        },
                        texPixelRatio       : {
                            type: "1f"
                        },
                        time                : {
                            type: "1f"
                        },
                        viscosity           : {
                            type: "1f"
                        }
                    },
                    vertexShader  : t(22)
                }),
                this.bgShaders.advectData       = new THREE.RawShaderMaterial({
                    fragmentShader: t(33),
                    uniforms      : {
                        attenuation  : {
                            type: "1f"
                        },
                        dataTex      : {
                            type: "t"
                        },
                        resolution   : {
                            type: "2f"
                        },
                        texPixelRatio: {
                            type: "1f"
                        }
                    },
                    vertexShader  : t(22)
                }),
                n                               = new THREE.RawShaderMaterial({
                    depthTest     : !1,
                    depthWrite    : !1,
                    fragmentShader: t(34),
                    vertexShader  : t(22)
                }),
                this.dataTexture                = new o(
                    Math.round(this.width * this.texPixelRatio),
                    Math.round(this.height * this.texPixelRatio),
                    this.renderer,
                    this.camera,
                    n,
                    n.clone(),
                    this.commonPlaneGeometry
                ),
                this.bgMesh                     = new THREE.Mesh(this.commonPlaneGeometry, this.bgShaders.main),
                this
                    .scene
                    .add(this.bgMesh)
            },
            n.prototype.setParameters          = function () {
                this.setShaderUniform("updateDivergence", "texPixelRatio", this.texPixelRatio),
                this.setShaderUniform("updatePressure", "texPixelRatio", this.texPixelRatio),
                this.setShaderUniform("updateVelocity", "texPixelRatio", this.texPixelRatio),
                this.setShaderUniform("advectData", "texPixelRatio", this.texPixelRatio),
                this.setShaderUniform("advectData", "attenuation", this.attenuation),
                this.setShaderUniform("updatePressure", "alpha", this.alpha),
                this.setShaderUniform("updatePressure", "beta", this.beta),
                this.setShaderUniform("updateVelocity", "viscosity", this.viscosity),
                this.setShaderUniform("updateVelocity", "forceRadius", this.forceRadius),
                this.setShaderUniform(
                    "updateVelocity",
                    "forceCoefficient",
                    this.forceCoefficient
                ),
                this.setShaderUniform(
                    "updateVelocity",
                    "autoforceCoefficient",
                    this.autoforceCoefficient
                )
            },
            n.prototype.setShaderResolution    = function () {
                var n,
                    e,
                    t;
                t = this.bgShaders;
                for (e in t) 
                    n = t[e],
                    this.setShaderUniform(
                        e,
                        "resolution",
                        new THREE.Vector2(this.width, this.height)
                    )
            },
            n.prototype.updateData             = function (n) {
                this.setShaderUniform(n, "dataTex", this.dataTexture.getTexture()),
                this
                    .dataTexture
                    .swapTexture(),
                this
                    .dataTexture
                    .setMeshMaterial(this.bgShaders[n]),
                this
                    .renderer
                    .render(
                        this.dataTexture.scene,
                        this.dataTexture.camera,
                        this.dataTexture.getRenderTarget()
                    )
            },
            n.prototype.setShaderUniform       = function (n, e, t) {
                this
                    .bgShaders[n]
                    .uniforms[e]
                    .value = t
            },
            n.prototype.start                  = function () {
                this.draw(),
                TweenMax.to(this, 5, {
                    animalAnimationParam: 0,
                    ease                : Cubic.easeInOut,
                    onUpdate            : function (n) {
                        return function () {
                            return n.animalMaterial.uniforms.animationParam.value = n.animalAnimationParam
                        }
                    }(this)
                })
            },
            n.prototype.draw                   = function () {
                var n,
                    e,
                    t,
                    i,
                    o;
                for (
                    n                                       = (new Date).getTime(),
                    this.time                               += n - this.beforeTime,
                    this.beforeTime                         = n,
                    this.animalMaterial.uniforms.time.value = this.time,
                    i                                       = new THREE.Vector2,
                    null != this.mouseX && (
                        this.isMousePosInited
                            ? i.set(this.mouseX, this.mouseY)
                            : (this.isMousePosInited = !0, i = this.pMousePos)
                    ),
                    this.updateData("updateDivergence"),
                    e                                       = t                                   = 0,
                    o                                       = this.solverIteration; 0 <= o
                            ? t < o
                            : t > o; e = 0 <= o
                        ? ++t
                        : --t
                ) 
                    this.updateData("updatePressure");
                this.setShaderUniform("updateVelocity", "time", this.time),
                this.setShaderUniform("updateVelocity", "mousePos", i),
                this.setShaderUniform("updateVelocity", "pMousePos", this.pMousePos),
                this.updateData("updateVelocity"),
                this.updateData("advectData"),
                this.setShaderUniform("main", "time", this.time),
                this.setShaderUniform("main", "dataTex", this.dataTexture.getTexture()),
                this
                    .renderer
                    .render(this.scene, this.camera),
                this.pMousePos               = i,
                this.requestAnimationFrameId = requestAnimationFrame(this.draw)
            },
            n.prototype.windowResizeHandler    = function (n) {
                var t,
                    o,
                    r,
                    a,
                    u,
                    c;
                null == n && (n = null),
                this.isWebGLSupported && (
                    this.width         = this.container.offsetWidth,
                    this.height        = this.container.offsetHeight,
                    this.setShaderResolution(),
                    this.dataTexture.resize(
                        Math.round(this.width * this.texPixelRatio),
                        Math.round(this.height * this.texPixelRatio)
                    ),
                    t                  = this.width / this.height,
                    r                  = 0,
                    a                  = 0,
                    t > 1
                        ? (u = this.height / e.y, o = x.y * u, c = o / x.y * x.x)
                        : (
                            u = 1.2 * this.width / e.x,
                            c = x.x * u,
                            o = c / x.x * x.y,
                            a = -s * this.width / i
                        ),
                    r                  = (this.width - c / x.x * e.x) / 2,
                    this.animalMesh.scale.set(c / l, o / l, 1),
                    this.animalMesh.position.set(r, a, 0),
                    this.camera.left   = -this.width / 2,
                    this.camera.right  = this.width / 2,
                    this.camera.top    = this.height / 2,
                    this.camera.bottom = -this.height / 2,
                    this.camera.updateProjectionMatrix(),
                    this.renderer.setSize(this.width, this.height),
                    this.renderer.setViewport(0, 0, this.width, this.height)
                )
            },
            n.prototype.windowMouseMoveHandler = function (n) {
                null == n && (n = null),
                this.mouseX = n.clientX,
                this.mouseY = n.clientY
            },
            n.prototype.windowTouchMoveHandler = function (n) {
                var e;
                null == n && (n = null),
                e           = n.touches[0],
                this.mouseX = e.clientX,
                this.mouseY = e.clientY
            },
            n.prototype.windowScrollHandler    = function (n) {
                null == n && (n = null),
                this.animalMaterial.uniforms.scrollY.value = window.pageYOffset
            },
            n
        }(),
        n.exports    = i
    },
    function (n, e, t) {
        n.exports = {
            getAndroidVersion: t(15),
            getiOSVersion    : t(15),
            isAndroid        : t(14),
            isEdge           : t(11),
            isFirefox        : t(6),
            isIE10           : t(9),
            isIE11           : t(10),
            isIE8            : t(7),
            isIE9            : t(8),
            isiPad           : t(12),
            isiPhone         : t(13),
            map              : t(5),
            preloadImg       : t(4)
        },
        t(16),
        t(17),
        t(18)
    },
    function (n, e) {
        n.exports = function (n) {
            return new Promise(function (e) {
                var t;
                return t = new Image,
                t.addEventListener("load", function (n) {
                    return t.removeEventListener("load", arguments.callee),
                    e()
                }),
                t.src = n
            })
        }
    },
    function (n, e) {
        n.exports = function (n, e, t, i, o, r) {
            var a;
            if (null == r && (r = !0), r === !0) {
                if (n < e) 
                    return i;
                if (n > t) 
                    return o
            }
            return a = (o - i) / (t - e),
            (n - e) * a + i
        }
    },
    function (n, e) {
        n.exports = navigator
            .userAgent
            .toLowerCase()
            .indexOf("firefox") !== -1
    },
    function (n, e) {
        n.exports = navigator
            .userAgent
            .toLowerCase()
            .indexOf("msie 8.0") !== -1
    },
    function (n, e) {
        n.exports = navigator
            .userAgent
            .toLowerCase()
            .indexOf("msie 9.0") !== -1
    },
    function (n, e) {
        n.exports = navigator
            .userAgent
            .toLowerCase()
            .indexOf("msie 10.0") !== -1
    },
    function (n, e) {
        n.exports = navigator
            .userAgent
            .toLowerCase()
            .indexOf("rv:11.0") !== -1
    },
    function (n, e) {
        n.exports = navigator
            .userAgent
            .toLowerCase()
            .indexOf("edge") !== -1
    },
    function (n, e) {
        n.exports = navigator
            .userAgent
            .toLowerCase()
            .indexOf("ipad") !== -1
    },
    function (n, e) {
        n.exports = navigator
            .userAgent
            .toLowerCase()
            .indexOf("iphone") !== -1
    },
    function (n, e) {
        n.exports = navigator
            .userAgent
            .toLowerCase()
            .indexOf("android") !== -1
    },
    function (n, e) {
        n.exports = function () {
            var n;
            return n = navigator
                .appVersion
                .match(/Android (\d+).(\d+).?(\d+)?;/),
            [
                parseInt(n[1], 10),
                parseInt(n[2], 10),
                parseInt(n[3] || 0, 10)
            ]
        }
    },
    function (n, e) {
        window.log = function () {
            return null != window.console
                ? null != window.console.log.bind
                    ? window
                        .console
                        .log
                        .bind(window.console)
                    : window.console.log
                : window.alert
        }()
    },
    function (n, e) {
        window.requestAnimationFrame = function (n) {
            return function () {
                return window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || function (
                    n
                ) {
                    return setTimeout(n, 1e3 / 30)
                }
            }
        }(this)()
    },
    function (n, e) {
        window.cancelAnimationFrame = function (n) {
            return function () {
                return window.cancelAnimationFrame || window.mozCancelAnimationFrame || function (
                    n
                ) {
                    return clearTimeout(n)
                }
            }
        }(this)()
    },
    function (n, e) {
        var t;
        t         = function () {
            function n(n, e, t, i, o, r, a) {
                this.width                = n,
                this.height               = e,
                this.renderer             = t,
                this.camera               = i,
                this.initShaderMaterial   = o,
                this.updateShaderMaterial = r,
                this.planeGeometry        = null != a
                    ? a
                    : null,
                this.currentTextureIndex  = 0,
                this.renderTargets        = [new THREE.WebGLRenderTarget(this.width, this.height, {
                        depthBuffer    : !1,
                        format         : THREE.RGBAFormat,
                        generateMipmaps: !1,
                        magFilter      : THREE.NearestFilter,
                        minFilter      : THREE.NearestFilter,
                        shareDepthFrom : null,
                        stencilBuffer  : !1,
                        type           : /(iPad|iPhone|iPod)/g.test(navigator.userAgent)
                            ? THREE.HalfFloatType
                            : THREE.FloatType,
                        wrapS          : THREE.ClampToEdgeWrapping,
                        wrapT          : THREE.ClampToEdgeWrapping
                    })],
                this.renderTargets[1]     = this
                    .renderTargets[0]
                    .clone(),
                null == this.planeGeometry && (
                    this.planeGeometry = new THREE.PlaneGeometry(1e4, 1e4)
                ),
                this.mesh                 = new THREE.Mesh(this.planeGeometry, this.initShaderMaterial),
                this.scene                = new THREE.Scene,
                this
                    .scene
                    .add(this.mesh),
                this
                    .renderer
                    .render(this.scene, this.camera, this.renderTargets[0]),
                this
                    .renderer
                    .render(this.scene, this.camera, this.renderTargets[1]),
                this
                    .initShaderMaterial
                    .dispose(),
                this.initShaderMaterial = null,
                this.mesh.material      = this.updateShaderMaterial,
                this
                    .renderTargets[0]
                    .texture
                    .flipY              = !1,
                this
                    .renderTargets[1]
                    .texture
                    .flipY              = !1
            }
            return n.prototype.setDefine = function (n, e) {
                this
                    .updateShaderMaterial
                    .defines[n] = e
            },
            n.prototype.initUniforms     = function (n) {
                var e,
                    t;
                for (e in n) 
                    t                = n[e],
                    this
                        .updateShaderMaterial
                        .uniforms[e] = t
                },
            n.prototype.updateUniform    = function (n, e) {
                this
                    .updateShaderMaterial
                    .uniforms[n]
                    .value = e
            },
            n.prototype.update           = function () {
                this.updateShaderMaterial.uniforms.texture.value = this.getTexture(),
                this.swapTexture(),
                this.render()
            },
            n.prototype.render           = function () {
                this
                    .renderer
                    .render(this.scene, this.camera, this.renderTargets[this.currentTextureIndex])
            },
            n.prototype.setMeshMaterial  = function (n) {
                this.mesh.material             = n,
                this.mesh.material.needsUpdate = !0
            },
            n.prototype.swapTexture      = function () {
                this.currentTextureIndex = (this.currentTextureIndex + 1) % 2
            },
            n.prototype.getTexture       = function () {
                return this
                    .getRenderTarget()
                    .texture
            },
            n.prototype.getRenderTarget  = function () {
                return this.renderTargets[this.currentTextureIndex]
            },
            n.prototype.resize           = function (n, e) {
                this
                    .renderTargets[0]
                    .setSize(n, e),
                this
                    .renderTargets[1]
                    .setSize(n, e)
            },
            n
        }(),
        n.exports = t
    },
    function (n, e) {
        n.exports = "#define GLSLIFY 1\nattribute vec3 position;\nattribute vec2 uv;\n\nuniform mat" +
                "4 projectionMatrix;\nuniform mat4 modelViewMatrix;\nuniform float scrollY;\n\n" +
                "varying vec2 vUv;\n\nvoid main() {\n  vec4 pos = modelViewMatrix * vec4(positi" +
                "on, 1.0);\n  pos.y += scrollY;\n  vUv = uv;\n  gl_Position = projectionMatrix " +
                "* pos;\n}\n"
    },
    function (n, e) {
        n.exports = "precision highp float;\n#define GLSLIFY 1\n\nuniform float time;\nuniform floa" +
                "t animationParam;\nuniform vec2 resolution;\nuniform sampler2D texture;\n\nvar" +
                "ying vec2 vUv;\n\n//\n// Description : Array and textureless GLSL 2D simplex n" +
                "oise function.\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ij" +
                "m\n//     Lastmod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima" +
                " Arts. All rights reserved.\n//               Distributed under the MIT Licens" +
                "e. See LICENSE file.\n//               https://github.com/ashima/webgl-noise\n" +
                "//\n\nvec3 mod289_2_0(vec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0" +
                ";\n}\n\nvec2 mod289_2_0(vec2 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289" +
                ".0;\n}\n\nvec3 permute_2_1(vec3 x) {\n  return mod289_2_0(((x*34.0)+1.0)*x);\n" +
                "}\n\nfloat snoise_2_2(vec2 v)\n  {\n  const vec4 C = vec4(0.211324865405187,  " +
                "// (3.0-sqrt(3.0))/6.0\n                      0.366025403784439,  // 0.5*(sqrt" +
                "(3.0)-1.0)\n                     -0.577350269189626,  // -1.0 + 2.0 * C.x\n   " +
                "                   0.024390243902439); // 1.0 / 41.0\n// First corner\n  vec2 " +
                "i  = floor(v + dot(v, C.yy) );\n  vec2 x0 = v -   i + dot(i, C.xx);\n\n// Othe" +
                "r corners\n  vec2 i1;\n  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0" +
                ".0\n  //i1.y = 1.0 - i1.x;\n  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, " +
                "1.0);\n  // x0 = x0 - 0.0 + 0.0 * C.xx ;\n  // x1 = x0 - i1 + 1.0 * C.xx ;\n  " +
                "// x2 = x0 - 1.0 + 2.0 * C.xx ;\n  vec4 x12 = x0.xyxy + C.xxzz;\n  x12.xy -= i" +
                "1;\n\n// Permutations\n  i = mod289_2_0(i); // Avoid truncation effects in per" +
                "mutation\n  vec3 p = permute_2_1( permute_2_1( i.y + vec3(0.0, i1.y, 1.0 ))\n " +
                "   + i.x + vec3(0.0, i1.x, 1.0 ));\n\n  vec3 m = max(0.5 - vec3(dot(x0,x0), do" +
                "t(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);\n  m = m*m ;\n  m = m*m ;\n\n// G" +
                "radients: 41 points uniformly over a line, mapped onto a diamond.\n// The ring" +
                " size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)\n\n  vec3 x = 2.0 " +
                "* fract(p * C.www) - 1.0;\n  vec3 h = abs(x) - 0.5;\n  vec3 ox = floor(x + 0.5" +
                ");\n  vec3 a0 = x - ox;\n\n// Normalise gradients implicitly by scaling m\n// " +
                "Approximation of: m *= inversesqrt( a0*a0 + h*h );\n  m *= 1.79284291400159 - " +
                "0.85373472095314 * ( a0*a0 + h*h );\n\n// Compute final noise value at P\n  ve" +
                "c3 g;\n  g.x  = a0.x  * x0.x  + h.x  * x0.y;\n  g.yz = a0.yz * x12.xz + h.yz *" +
                " x12.yw;\n  return 130.0 * dot(m, g);\n}\n\n\n\nfloat map_1_3(float value, flo" +
                "at inputMin, float inputMax, float outputMin, float outputMax, bool clamp) {\n" +
                "  if(clamp == true) {\n    if(value < inputMin) return outputMin;\n    if(valu" +
                "e > inputMax) return outputMax;\n  }\n\n  float p = (outputMax - outputMin) / " +
                "(inputMax - inputMin);\n  return ((value - inputMin) * p) + outputMin;\n}\n\n" +
                "\n\nvoid main(){\n  vec2 uv = vUv;\n\n  float noise1 = snoise_2_2(uv * 10.0);" +
                "\n  float noise2 = snoise_2_2(uv.ts * 30.0);\n\n  uv.x += sin(time * 0.001 + u" +
                "v.y * 40.0) * 0.02 * animationParam;\n  uv.x += sin(time * 0.004 + uv.y * 60.0" +
                ") * 0.06 * animationParam;\n  uv.x += sin(time * 0.002 + uv.y * 10.0) * 0.04 *" +
                " animationParam;\n  uv.x += sin(time * 0.0008 + uv.y * 100.0) * 0.01 * animati" +
                "onParam * noise1;\n\n  uv.y += cos(time * 0.005 + uv.y * 10.0) * 0.01 * animat" +
                "ionParam;\n  uv.y += cos(time * 0.003 + uv.y * 30.0) * 0.02 * animationParam;" +
                "\n  uv.y += cos(time * 0.002 + uv.y * 4.0) * 0.01 * animationParam;\n\n  uv.x " +
                "+= 0.3 * animationParam * map_1_3(noise1, -1.0, 1.0, 0.8, 1.2, true);\n  uv.y " +
                "+= 0.06 * animationParam * noise2;\n\n  uv += vec2(\n    snoise_2_2(uv.st * 40" +
                ".0 + 100.0 * sin(time * 0.000014)),\n    snoise_2_2(uv.ts * 30.0 + 100.0 * cos" +
                "(time * 0.00001))\n  ) * 0.0008;\n\n  vec4 texColor = texture2D(texture, uv) *" +
                " (1.0 - animationParam);\n\n  gl_FragColor = texColor;\n}\n"
    },
    function (n, e) {
        n.exports = "#define GLSLIFY 1\nattribute vec3 position;\n\nvoid main() {\n  gl_Position = " +
                "vec4(position, 1.0);\n}\n"
    },
    function (n, e, t) {
        function i(n) {
            return t(o(n))
        }
        function o(n) {
            return r[n] || function () {
                throw new Error("Cannot find module '" + n + "'.")
            }()
        }
        var r = {
            "./renderColor1.frag": 24,
            "./renderColor2.frag": 25,
            "./renderColor3.frag": 26,
            "./renderColor4.frag": 27,
            "./renderColor5.frag": 28,
            "./renderColor6.frag": 29
        };
        i.keys    = function () {
            return Object.keys(r)
        },
        i.resolve = o,
        n.exports = i,
        i.id      = 23
    },
    function (n, e) {
        n.exports = "// color 1 (アルマジロトカゲ)\nprecision highp float;\n#define GLSLIFY 1\n\nuniform fl" +
                "oat time;\nuniform float devicePixelRatio;\nuniform vec2 resolution;\nuniform " +
                "sampler2D dataTex;\n\nconst float h1 = 0.036;\nconst float h2 = 0.086;\nconst " +
                "float s1 = 0.68;\nconst float s2 = 0.7;\nconst float v1 = 0.6;\nconst float v2" +
                " = 0.7;\n\n//\n// Description : Array and textureless GLSL 2D simplex noise fu" +
                "nction.\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm\n//  " +
                "   Lastmod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima Arts. " +
                "All rights reserved.\n//               Distributed under the MIT License. See " +
                "LICENSE file.\n//               https://github.com/ashima/webgl-noise\n//\n\nv" +
                "ec3 mod289_3_0(vec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n" +
                "\nvec2 mod289_3_0(vec2 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}" +
                "\n\nvec3 permute_3_1(vec3 x) {\n  return mod289_3_0(((x*34.0)+1.0)*x);\n}\n\nf" +
                "loat snoise_3_2(vec2 v)\n  {\n  const vec4 C = vec4(0.211324865405187,  // (3." +
                "0-sqrt(3.0))/6.0\n                      0.366025403784439,  // 0.5*(sqrt(3.0)-" +
                "1.0)\n                     -0.577350269189626,  // -1.0 + 2.0 * C.x\n         " +
                "             0.024390243902439); // 1.0 / 41.0\n// First corner\n  vec2 i  = f" +
                "loor(v + dot(v, C.yy) );\n  vec2 x0 = v -   i + dot(i, C.xx);\n\n// Other corn" +
                "ers\n  vec2 i1;\n  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0\n  " +
                "//i1.y = 1.0 - i1.x;\n  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);" +
                "\n  // x0 = x0 - 0.0 + 0.0 * C.xx ;\n  // x1 = x0 - i1 + 1.0 * C.xx ;\n  // x2" +
                " = x0 - 1.0 + 2.0 * C.xx ;\n  vec4 x12 = x0.xyxy + C.xxzz;\n  x12.xy -= i1;\n" +
                "\n// Permutations\n  i = mod289_3_0(i); // Avoid truncation effects in permuta" +
                "tion\n  vec3 p = permute_3_1( permute_3_1( i.y + vec3(0.0, i1.y, 1.0 ))\n    +" +
                " i.x + vec3(0.0, i1.x, 1.0 ));\n\n  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x1" +
                "2.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);\n  m = m*m ;\n  m = m*m ;\n\n// Gradi" +
                "ents: 41 points uniformly over a line, mapped onto a diamond.\n// The ring siz" +
                "e 17*17 = 289 is close to a multiple of 41 (41*7 = 287)\n\n  vec3 x = 2.0 * fr" +
                "act(p * C.www) - 1.0;\n  vec3 h = abs(x) - 0.5;\n  vec3 ox = floor(x + 0.5);\n" +
                "  vec3 a0 = x - ox;\n\n// Normalise gradients implicitly by scaling m\n// Appr" +
                "oximation of: m *= inversesqrt( a0*a0 + h*h );\n  m *= 1.79284291400159 - 0.85" +
                "373472095314 * ( a0*a0 + h*h );\n\n// Compute final noise value at P\n  vec3 g" +
                ";\n  g.x  = a0.x  * x0.x  + h.x  * x0.y;\n  g.yz = a0.yz * x12.xz + h.yz * x12" +
                ".yw;\n  return 130.0 * dot(m, g);\n}\n\n\n\nfloat map_1_3(float value, float i" +
                "nputMin, float inputMax, float outputMin, float outputMax, bool clamp) {\n  if" +
                "(clamp == true) {\n    if(value < inputMin) return outputMin;\n    if(value > " +
                "inputMax) return outputMax;\n  }\n\n  float p = (outputMax - outputMin) / (inp" +
                "utMax - inputMin);\n  return ((value - inputMin) * p) + outputMin;\n}\n\n\nvec" +
                "3 hsv2rgb_2_4(vec3 c) {\n  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);\n  v" +
                "ec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);\n  return c.z * mix(K.xxx, cl" +
                "amp(p - K.xxx, 0.0, 1.0), c.y);\n}\n\n\n\nvoid main(){\n  vec2 uv = gl_FragCoo" +
                "rd.xy / resolution.xy / devicePixelRatio;\n  vec4 data = texture2D(dataTex, uv" +
                ");\n  vec2 velocity = data.xy;\n  float pressure = data.z;\n\n  float n1 = (sn" +
                "oise_3_2(vec2(\n    gl_FragCoord.x / devicePixelRatio * 2.0 - time * 0.1,\n   " +
                " gl_FragCoord.y / devicePixelRatio * 2.0\n  ) * 0.002) + 1.0) * 0.5;\n\n  floa" +
                "t n2 = (snoise_3_2(vec2(\n    gl_FragCoord.x / devicePixelRatio * 2.0 - time *" +
                " 0.2,\n    gl_FragCoord.y / devicePixelRatio * 2.0\n  ) * 0.01) + 1.0) * 0.5;" +
                "\n\n  n1 = max(n1, n2);\n\n  float p1 = pressure * length(velocity) * 0.3;\n  " +
                "float p2 = length(velocity) * 0.8 * 0.3;\n\n  vec4 addColor = vec4(\n    hsv2r" +
                "gb_2_4(vec3(\n      map_1_3(n1, 0.0, 1.0, h1, h2, true),\n      0.1,\n      ma" +
                "p_1_3(n1, 0.0, 1.0, 0.0, 0.2, true)\n    )), 1.0\n  ) * 0.1;\n\n  gl_FragColor" +
                " = vec4(\n    hsv2rgb_2_4(vec3(\n      map_1_3(1.0 - p2, 0.0, 1.0, h1, h2, tru" +
                "e),\n      map_1_3(p1, 0.0, 1.0, s1, s2, true),\n      map_1_3(1.0 - p2, 0.0, " +
                "1.0, v1, v2, true)\n    )), 1.0\n  ) + addColor;\n}\n"
    },
    function (n, e) {
        n.exports = "// color 2 (サイ)\nprecision highp float;\n#define GLSLIFY 1\n\nuniform float ti" +
                "me;\nuniform float devicePixelRatio;\nuniform vec2 resolution;\nuniform sample" +
                "r2D dataTex;\n\n//\n// Description : Array and textureless GLSL 2D simplex noi" +
                "se function.\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm" +
                "\n//     Lastmod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima " +
                "Arts. All rights reserved.\n//               Distributed under the MIT License" +
                ". See LICENSE file.\n//               https://github.com/ashima/webgl-noise\n/" +
                "/\n\nvec3 mod289_3_0(vec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;" +
                "\n}\n\nvec2 mod289_3_0(vec2 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289." +
                "0;\n}\n\nvec3 permute_3_1(vec3 x) {\n  return mod289_3_0(((x*34.0)+1.0)*x);\n}" +
                "\n\nfloat snoise_3_2(vec2 v)\n  {\n  const vec4 C = vec4(0.211324865405187,  /" +
                "/ (3.0-sqrt(3.0))/6.0\n                      0.366025403784439,  // 0.5*(sqrt(" +
                "3.0)-1.0)\n                     -0.577350269189626,  // -1.0 + 2.0 * C.x\n    " +
                "                  0.024390243902439); // 1.0 / 41.0\n// First corner\n  vec2 i" +
                "  = floor(v + dot(v, C.yy) );\n  vec2 x0 = v -   i + dot(i, C.xx);\n\n// Other" +
                " corners\n  vec2 i1;\n  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0." +
                "0\n  //i1.y = 1.0 - i1.x;\n  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1" +
                ".0);\n  // x0 = x0 - 0.0 + 0.0 * C.xx ;\n  // x1 = x0 - i1 + 1.0 * C.xx ;\n  /" +
                "/ x2 = x0 - 1.0 + 2.0 * C.xx ;\n  vec4 x12 = x0.xyxy + C.xxzz;\n  x12.xy -= i1" +
                ";\n\n// Permutations\n  i = mod289_3_0(i); // Avoid truncation effects in perm" +
                "utation\n  vec3 p = permute_3_1( permute_3_1( i.y + vec3(0.0, i1.y, 1.0 ))\n  " +
                "  + i.x + vec3(0.0, i1.x, 1.0 ));\n\n  vec3 m = max(0.5 - vec3(dot(x0,x0), dot" +
                "(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);\n  m = m*m ;\n  m = m*m ;\n\n// Gr" +
                "adients: 41 points uniformly over a line, mapped onto a diamond.\n// The ring " +
                "size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)\n\n  vec3 x = 2.0 *" +
                " fract(p * C.www) - 1.0;\n  vec3 h = abs(x) - 0.5;\n  vec3 ox = floor(x + 0.5)" +
                ";\n  vec3 a0 = x - ox;\n\n// Normalise gradients implicitly by scaling m\n// A" +
                "pproximation of: m *= inversesqrt( a0*a0 + h*h );\n  m *= 1.79284291400159 - 0" +
                ".85373472095314 * ( a0*a0 + h*h );\n\n// Compute final noise value at P\n  vec" +
                "3 g;\n  g.x  = a0.x  * x0.x  + h.x  * x0.y;\n  g.yz = a0.yz * x12.xz + h.yz * " +
                "x12.yw;\n  return 130.0 * dot(m, g);\n}\n\n\n\nfloat map_1_3(float value, floa" +
                "t inputMin, float inputMax, float outputMin, float outputMax, bool clamp) {\n " +
                " if(clamp == true) {\n    if(value < inputMin) return outputMin;\n    if(value" +
                " > inputMax) return outputMax;\n  }\n\n  float p = (outputMax - outputMin) / (" +
                "inputMax - inputMin);\n  return ((value - inputMin) * p) + outputMin;\n}\n\n\n" +
                "vec3 hsv2rgb_2_4(vec3 c) {\n  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);\n" +
                "  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);\n  return c.z * mix(K.xxx," +
                " clamp(p - K.xxx, 0.0, 1.0), c.y);\n}\n\n\n\nvoid main(){\n  vec2 uv = gl_Frag" +
                "Coord.xy / resolution.xy / devicePixelRatio;\n  vec4 data = texture2D(dataTex," +
                " uv);\n  vec2 velocity = data.xy;\n  float pressure = data.z;\n\n  float n1 = " +
                "(snoise_3_2(vec2(\n    gl_FragCoord.x / devicePixelRatio * 2.0 - time * 0.1,\n" +
                "    gl_FragCoord.y / devicePixelRatio * 2.0\n  ) * 0.002) + 1.0) * 0.5;\n\n  f" +
                "loat n2 = (snoise_3_2(vec2(\n    gl_FragCoord.x / devicePixelRatio * 2.0 - tim" +
                "e * 0.2,\n    gl_FragCoord.y / devicePixelRatio * 2.0\n  ) * 0.01) + 1.0) * 0." +
                "5;\n\n  n1 = max(n1, n2);\n\n  float p1 = pressure * length(velocity) * 0.3;\n" +
                "  float p2 = length(velocity) * 0.4 * 0.3;\n\n  vec4 addColor = vec4(\n    hsv" +
                "2rgb_2_4(vec3(\n      0.0,\n      0.0,\n      map_1_3(n1, 0.0, 1.0, 0.0, 0.2, " +
                "true)\n    )), 1.0\n  ) * 0.1;\n\n  gl_FragColor = vec4(\n    hsv2rgb_2_4(vec3" +
                "(\n      0.0,\n      0.0,\n      map_1_3(p2, 0.0, 1.0, 0.0, 0.4, true)\n    ))" +
                ", 1.0\n  ) + addColor;\n}\n"
    },
    function (n, e) {
        n.exports = "// color 3 (チンアナゴ)\nprecision highp float;\n#define GLSLIFY 1\n\nuniform float" +
                " time;\nuniform float devicePixelRatio;\nuniform vec2 resolution;\nuniform sam" +
                "pler2D dataTex;\n\nconst float h1 = 0.58;\nconst float h2 = 0.6;\nconst float " +
                "s1 = 0.7;\nconst float s2 = 0.8;\nconst float v1 = 0.3;\nconst float v2 = 0.42" +
                ";\n\n//\n// Description : Array and textureless GLSL 2D simplex noise function" +
                ".\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm\n//     Las" +
                "tmod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima Arts. All ri" +
                "ghts reserved.\n//               Distributed under the MIT License. See LICENS" +
                "E file.\n//               https://github.com/ashima/webgl-noise\n//\n\nvec3 mo" +
                "d289_3_0(vec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec2 " +
                "mod289_3_0(vec2 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec" +
                "3 permute_3_1(vec3 x) {\n  return mod289_3_0(((x*34.0)+1.0)*x);\n}\n\nfloat sn" +
                "oise_3_2(vec2 v)\n  {\n  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(" +
                "3.0))/6.0\n                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)\n " +
                "                    -0.577350269189626,  // -1.0 + 2.0 * C.x\n                " +
                "      0.024390243902439); // 1.0 / 41.0\n// First corner\n  vec2 i  = floor(v " +
                "+ dot(v, C.yy) );\n  vec2 x0 = v -   i + dot(i, C.xx);\n\n// Other corners\n  " +
                "vec2 i1;\n  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0\n  //i1.y " +
                "= 1.0 - i1.x;\n  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);\n  // x" +
                "0 = x0 - 0.0 + 0.0 * C.xx ;\n  // x1 = x0 - i1 + 1.0 * C.xx ;\n  // x2 = x0 - " +
                "1.0 + 2.0 * C.xx ;\n  vec4 x12 = x0.xyxy + C.xxzz;\n  x12.xy -= i1;\n\n// Perm" +
                "utations\n  i = mod289_3_0(i); // Avoid truncation effects in permutation\n  v" +
                "ec3 p = permute_3_1( permute_3_1( i.y + vec3(0.0, i1.y, 1.0 ))\n    + i.x + ve" +
                "c3(0.0, i1.x, 1.0 ));\n\n  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12." +
                "xy), dot(x12.zw,x12.zw)), 0.0);\n  m = m*m ;\n  m = m*m ;\n\n// Gradients: 41 " +
                "points uniformly over a line, mapped onto a diamond.\n// The ring size 17*17 =" +
                " 289 is close to a multiple of 41 (41*7 = 287)\n\n  vec3 x = 2.0 * fract(p * C" +
                ".www) - 1.0;\n  vec3 h = abs(x) - 0.5;\n  vec3 ox = floor(x + 0.5);\n  vec3 a0" +
                " = x - ox;\n\n// Normalise gradients implicitly by scaling m\n// Approximation" +
                " of: m *= inversesqrt( a0*a0 + h*h );\n  m *= 1.79284291400159 - 0.85373472095" +
                "314 * ( a0*a0 + h*h );\n\n// Compute final noise value at P\n  vec3 g;\n  g.x " +
                " = a0.x  * x0.x  + h.x  * x0.y;\n  g.yz = a0.yz * x12.xz + h.yz * x12.yw;\n  r" +
                "eturn 130.0 * dot(m, g);\n}\n\n\n\nfloat map_1_3(float value, float inputMin, " +
                "float inputMax, float outputMin, float outputMax, bool clamp) {\n  if(clamp ==" +
                " true) {\n    if(value < inputMin) return outputMin;\n    if(value > inputMax)" +
                " return outputMax;\n  }\n\n  float p = (outputMax - outputMin) / (inputMax - i" +
                "nputMin);\n  return ((value - inputMin) * p) + outputMin;\n}\n\n\nvec3 hsv2rgb" +
                "_2_4(vec3 c) {\n  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);\n  vec3 p = a" +
                "bs(fract(c.xxx + K.xyz) * 6.0 - K.www);\n  return c.z * mix(K.xxx, clamp(p - K" +
                ".xxx, 0.0, 1.0), c.y);\n}\n\n\n\nvoid main(){\n  vec2 uv = gl_FragCoord.xy / r" +
                "esolution.xy / devicePixelRatio;\n  vec4 data = texture2D(dataTex, uv);\n  vec" +
                "2 velocity = data.xy;\n  float pressure = data.z;\n\n  float n1 = (snoise_3_2(" +
                "vec2(\n    gl_FragCoord.x / devicePixelRatio * 2.0 - time * 0.1,\n    gl_FragC" +
                "oord.y / devicePixelRatio * 2.0\n  ) * 0.002) + 1.0) * 0.5;\n\n  float n2 = (s" +
                "noise_3_2(vec2(\n    gl_FragCoord.x / devicePixelRatio * 2.0 - time * 0.2,\n  " +
                "  gl_FragCoord.y / devicePixelRatio * 2.0\n  ) * 0.01) + 1.0) * 0.5;\n\n  n1 =" +
                " max(n1, n2);\n\n  float p1 = pressure * length(velocity) * 0.3;\n  float p2 =" +
                " length(velocity) * 0.8 * 0.3;\n\n  vec4 addColor = vec4(\n    hsv2rgb_2_4(vec" +
                "3(\n      map_1_3(n1, 0.0, 1.0, h1, h2, true),\n      0.1,\n      map_1_3(n1, " +
                "0.0, 1.0, 0.0, 0.2, true)\n    )), 1.0\n  ) * 0.1;\n\n  gl_FragColor = vec4(\n" +
                "    hsv2rgb_2_4(vec3(\n      map_1_3(1.0 - p2, 0.0, 1.0, h1, h2, true),\n     " +
                " map_1_3(p1, 0.0, 1.0, s1, s2, true),\n      map_1_3(1.0 - p2, 0.0, 1.0, v1, v" +
                "2, true)\n    )), 1.0\n  ) + addColor;\n}\n"
    },
    function (n, e) {
        n.exports = "// color 4 (ハチドリ)\nprecision highp float;\n#define GLSLIFY 1\n\nuniform float " +
                "time;\nuniform float devicePixelRatio;\nuniform vec2 resolution;\nuniform samp" +
                "ler2D dataTex;\n\nconst float h1 = 0.88;\nconst float h2 = 0.92;\nconst float " +
                "s1 = 0.42;\nconst float s2 = 0.52;\nconst float v1 = 0.8;\nconst float v2 = 0." +
                "9;\n\n//\n// Description : Array and textureless GLSL 2D simplex noise functio" +
                "n.\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm\n//     La" +
                "stmod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima Arts. All r" +
                "ights reserved.\n//               Distributed under the MIT License. See LICEN" +
                "SE file.\n//               https://github.com/ashima/webgl-noise\n//\n\nvec3 m" +
                "od289_3_0(vec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec2" +
                " mod289_3_0(vec2 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nve" +
                "c3 permute_3_1(vec3 x) {\n  return mod289_3_0(((x*34.0)+1.0)*x);\n}\n\nfloat s" +
                "noise_3_2(vec2 v)\n  {\n  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt" +
                "(3.0))/6.0\n                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)\n" +
                "                     -0.577350269189626,  // -1.0 + 2.0 * C.x\n               " +
                "       0.024390243902439); // 1.0 / 41.0\n// First corner\n  vec2 i  = floor(v" +
                " + dot(v, C.yy) );\n  vec2 x0 = v -   i + dot(i, C.xx);\n\n// Other corners\n " +
                " vec2 i1;\n  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0\n  //i1.y" +
                " = 1.0 - i1.x;\n  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);\n  // " +
                "x0 = x0 - 0.0 + 0.0 * C.xx ;\n  // x1 = x0 - i1 + 1.0 * C.xx ;\n  // x2 = x0 -" +
                " 1.0 + 2.0 * C.xx ;\n  vec4 x12 = x0.xyxy + C.xxzz;\n  x12.xy -= i1;\n\n// Per" +
                "mutations\n  i = mod289_3_0(i); // Avoid truncation effects in permutation\n  " +
                "vec3 p = permute_3_1( permute_3_1( i.y + vec3(0.0, i1.y, 1.0 ))\n    + i.x + v" +
                "ec3(0.0, i1.x, 1.0 ));\n\n  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12" +
                ".xy), dot(x12.zw,x12.zw)), 0.0);\n  m = m*m ;\n  m = m*m ;\n\n// Gradients: 41" +
                " points uniformly over a line, mapped onto a diamond.\n// The ring size 17*17 " +
                "= 289 is close to a multiple of 41 (41*7 = 287)\n\n  vec3 x = 2.0 * fract(p * " +
                "C.www) - 1.0;\n  vec3 h = abs(x) - 0.5;\n  vec3 ox = floor(x + 0.5);\n  vec3 a" +
                "0 = x - ox;\n\n// Normalise gradients implicitly by scaling m\n// Approximatio" +
                "n of: m *= inversesqrt( a0*a0 + h*h );\n  m *= 1.79284291400159 - 0.8537347209" +
                "5314 * ( a0*a0 + h*h );\n\n// Compute final noise value at P\n  vec3 g;\n  g.x" +
                "  = a0.x  * x0.x  + h.x  * x0.y;\n  g.yz = a0.yz * x12.xz + h.yz * x12.yw;\n  " +
                "return 130.0 * dot(m, g);\n}\n\n\n\nfloat map_1_3(float value, float inputMin," +
                " float inputMax, float outputMin, float outputMax, bool clamp) {\n  if(clamp =" +
                "= true) {\n    if(value < inputMin) return outputMin;\n    if(value > inputMax" +
                ") return outputMax;\n  }\n\n  float p = (outputMax - outputMin) / (inputMax - " +
                "inputMin);\n  return ((value - inputMin) * p) + outputMin;\n}\n\n\nvec3 hsv2rg" +
                "b_2_4(vec3 c) {\n  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);\n  vec3 p = " +
                "abs(fract(c.xxx + K.xyz) * 6.0 - K.www);\n  return c.z * mix(K.xxx, clamp(p - " +
                "K.xxx, 0.0, 1.0), c.y);\n}\n\n\n\nvoid main(){\n  vec2 uv = gl_FragCoord.xy / " +
                "resolution.xy / devicePixelRatio;\n  vec4 data = texture2D(dataTex, uv);\n  ve" +
                "c2 velocity = data.xy;\n  float pressure = data.z;\n\n  float n1 = (snoise_3_2" +
                "(vec2(\n    gl_FragCoord.x / devicePixelRatio * 2.0 - time * 0.1,\n    gl_Frag" +
                "Coord.y / devicePixelRatio * 2.0\n  ) * 0.002) + 1.0) * 0.5;\n\n  float n2 = (" +
                "snoise_3_2(vec2(\n    gl_FragCoord.x / devicePixelRatio * 2.0 - time * 0.2,\n " +
                "   gl_FragCoord.y / devicePixelRatio * 2.0\n  ) * 0.01) + 1.0) * 0.5;\n\n  n1 " +
                "= max(n1, n2);\n\n  float p1 = pressure * length(velocity) * 0.3;\n  float p2 " +
                "= length(velocity) * 0.8 * 0.3;\n\n  vec4 addColor = vec4(\n    hsv2rgb_2_4(ve" +
                "c3(\n      map_1_3(n1, 0.0, 1.0, h1, h2, true),\n      0.1,\n      map_1_3(n1," +
                " 0.0, 1.0, 0.0, 0.2, true)\n    )), 1.0\n  ) * 0.1;\n\n  gl_FragColor = vec4(" +
                "\n    hsv2rgb_2_4(vec3(\n      map_1_3(1.0 - p2, 0.0, 1.0, h1, h2, true),\n   " +
                "   map_1_3(p1, 0.0, 1.0, s1, s2, true),\n      map_1_3(1.0 - p2, 0.0, 1.0, v1," +
                " v2, true)\n    )), 1.0\n  ) + addColor;\n}\n";
    },
    function (n, e) {
        n.exports = "// color 5 (蝶)\nprecision highp float;\n#define GLSLIFY 1\n\nuniform float tim" +
                "e;\nuniform float devicePixelRatio;\nuniform vec2 resolution;\nuniform sampler" +
                "2D dataTex;\n\nconst float h1 = 0.54;\nconst float h2 = 0.58;\nconst float s1 " +
                "= 0.54;\nconst float s2 = 0.61;\nconst float v1 = 0.8;\nconst float v2 = 0.9;" +
                "\n\n//\n// Description : Array and textureless GLSL 2D simplex noise function." +
                "\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm\n//     Last" +
                "mod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima Arts. All rig" +
                "hts reserved.\n//               Distributed under the MIT License. See LICENSE" +
                " file.\n//               https://github.com/ashima/webgl-noise\n//\n\nvec3 mod" +
                "289_3_0(vec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec2 m" +
                "od289_3_0(vec2 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec3" +
                " permute_3_1(vec3 x) {\n  return mod289_3_0(((x*34.0)+1.0)*x);\n}\n\nfloat sno" +
                "ise_3_2(vec2 v)\n  {\n  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3" +
                ".0))/6.0\n                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)\n  " +
                "                   -0.577350269189626,  // -1.0 + 2.0 * C.x\n                 " +
                "     0.024390243902439); // 1.0 / 41.0\n// First corner\n  vec2 i  = floor(v +" +
                " dot(v, C.yy) );\n  vec2 x0 = v -   i + dot(i, C.xx);\n\n// Other corners\n  v" +
                "ec2 i1;\n  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0\n  //i1.y =" +
                " 1.0 - i1.x;\n  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);\n  // x0" +
                " = x0 - 0.0 + 0.0 * C.xx ;\n  // x1 = x0 - i1 + 1.0 * C.xx ;\n  // x2 = x0 - 1" +
                ".0 + 2.0 * C.xx ;\n  vec4 x12 = x0.xyxy + C.xxzz;\n  x12.xy -= i1;\n\n// Permu" +
                "tations\n  i = mod289_3_0(i); // Avoid truncation effects in permutation\n  ve" +
                "c3 p = permute_3_1( permute_3_1( i.y + vec3(0.0, i1.y, 1.0 ))\n    + i.x + vec" +
                "3(0.0, i1.x, 1.0 ));\n\n  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.x" +
                "y), dot(x12.zw,x12.zw)), 0.0);\n  m = m*m ;\n  m = m*m ;\n\n// Gradients: 41 p" +
                "oints uniformly over a line, mapped onto a diamond.\n// The ring size 17*17 = " +
                "289 is close to a multiple of 41 (41*7 = 287)\n\n  vec3 x = 2.0 * fract(p * C." +
                "www) - 1.0;\n  vec3 h = abs(x) - 0.5;\n  vec3 ox = floor(x + 0.5);\n  vec3 a0 " +
                "= x - ox;\n\n// Normalise gradients implicitly by scaling m\n// Approximation " +
                "of: m *= inversesqrt( a0*a0 + h*h );\n  m *= 1.79284291400159 - 0.853734720953" +
                "14 * ( a0*a0 + h*h );\n\n// Compute final noise value at P\n  vec3 g;\n  g.x  " +
                "= a0.x  * x0.x  + h.x  * x0.y;\n  g.yz = a0.yz * x12.xz + h.yz * x12.yw;\n  re" +
                "turn 130.0 * dot(m, g);\n}\n\n\n\nfloat map_1_3(float value, float inputMin, f" +
                "loat inputMax, float outputMin, float outputMax, bool clamp) {\n  if(clamp == " +
                "true) {\n    if(value < inputMin) return outputMin;\n    if(value > inputMax) " +
                "return outputMax;\n  }\n\n  float p = (outputMax - outputMin) / (inputMax - in" +
                "putMin);\n  return ((value - inputMin) * p) + outputMin;\n}\n\n\nvec3 hsv2rgb_" +
                "2_4(vec3 c) {\n  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);\n  vec3 p = ab" +
                "s(fract(c.xxx + K.xyz) * 6.0 - K.www);\n  return c.z * mix(K.xxx, clamp(p - K." +
                "xxx, 0.0, 1.0), c.y);\n}\n\n\n\nvoid main(){\n  vec2 uv = gl_FragCoord.xy / re" +
                "solution.xy / devicePixelRatio;\n  vec4 data = texture2D(dataTex, uv);\n  vec2" +
                " velocity = data.xy;\n  float pressure = data.z;\n\n  float n1 = (snoise_3_2(v" +
                "ec2(\n    gl_FragCoord.x / devicePixelRatio * 2.0 - time * 0.1,\n    gl_FragCo" +
                "ord.y / devicePixelRatio * 2.0\n  ) * 0.002) + 1.0) * 0.5;\n\n  float n2 = (sn" +
                "oise_3_2(vec2(\n    gl_FragCoord.x / devicePixelRatio * 2.0 - time * 0.2,\n   " +
                " gl_FragCoord.y / devicePixelRatio * 2.0\n  ) * 0.01) + 1.0) * 0.5;\n\n  n1 = " +
                "max(n1, n2);\n\n  float p1 = pressure * length(velocity) * 0.3;\n  float p2 = " +
                "length(velocity) * 0.3;\n\n  vec4 addColor = vec4(\n    hsv2rgb_2_4(vec3(\n   " +
                "   map_1_3(n1, 0.0, 1.0, h1, h2, true),\n      0.1,\n      map_1_3(n1, 0.0, 1." +
                "0, 0.0, 0.2, true)\n    )), 1.0\n  ) * 0.1;\n\n  gl_FragColor = vec4(\n    hsv" +
                "2rgb_2_4(vec3(\n      map_1_3(p2, 0.0, 1.0, h1, h2, true),\n      map_1_3(1.0 " +
                "- p1, 0.0, 1.0, s1, s2, true),\n      map_1_3(p1, 0.0, 1.0, v1, v2, true)\n   " +
                " )), 1.0\n  ) + addColor;\n}\n"
    },
    function (n, e) {
        n.exports = "// color 6 (カエル)\nprecision highp float;\n#define GLSLIFY 1\n\nuniform float t" +
                "ime;\nuniform float devicePixelRatio;\nuniform vec2 resolution;\nuniform sampl" +
                "er2D dataTex;\n\nconst float h1 = 0.0;\nconst float h2 = 0.034;\nconst float s" +
                "1 = 0.2;\nconst float s2 = 0.32;\nconst float v1 = 0.57;\nconst float v2 = 0.6" +
                ";\n\n//\n// Description : Array and textureless GLSL 2D simplex noise function" +
                ".\n//      Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm\n//     Las" +
                "tmod : 20110822 (ijm)\n//     License : Copyright (C) 2011 Ashima Arts. All ri" +
                "ghts reserved.\n//               Distributed under the MIT License. See LICENS" +
                "E file.\n//               https://github.com/ashima/webgl-noise\n//\n\nvec3 mo" +
                "d289_3_0(vec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec2 " +
                "mod289_3_0(vec2 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec" +
                "3 permute_3_1(vec3 x) {\n  return mod289_3_0(((x*34.0)+1.0)*x);\n}\n\nfloat sn" +
                "oise_3_2(vec2 v)\n  {\n  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(" +
                "3.0))/6.0\n                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)\n " +
                "                    -0.577350269189626,  // -1.0 + 2.0 * C.x\n                " +
                "      0.024390243902439); // 1.0 / 41.0\n// First corner\n  vec2 i  = floor(v " +
                "+ dot(v, C.yy) );\n  vec2 x0 = v -   i + dot(i, C.xx);\n\n// Other corners\n  " +
                "vec2 i1;\n  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0\n  //i1.y " +
                "= 1.0 - i1.x;\n  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);\n  // x" +
                "0 = x0 - 0.0 + 0.0 * C.xx ;\n  // x1 = x0 - i1 + 1.0 * C.xx ;\n  // x2 = x0 - " +
                "1.0 + 2.0 * C.xx ;\n  vec4 x12 = x0.xyxy + C.xxzz;\n  x12.xy -= i1;\n\n// Perm" +
                "utations\n  i = mod289_3_0(i); // Avoid truncation effects in permutation\n  v" +
                "ec3 p = permute_3_1( permute_3_1( i.y + vec3(0.0, i1.y, 1.0 ))\n    + i.x + ve" +
                "c3(0.0, i1.x, 1.0 ));\n\n  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12." +
                "xy), dot(x12.zw,x12.zw)), 0.0);\n  m = m*m ;\n  m = m*m ;\n\n// Gradients: 41 " +
                "points uniformly over a line, mapped onto a diamond.\n// The ring size 17*17 =" +
                " 289 is close to a multiple of 41 (41*7 = 287)\n\n  vec3 x = 2.0 * fract(p * C" +
                ".www) - 1.0;\n  vec3 h = abs(x) - 0.5;\n  vec3 ox = floor(x + 0.5);\n  vec3 a0" +
                " = x - ox;\n\n// Normalise gradients implicitly by scaling m\n// Approximation" +
                " of: m *= inversesqrt( a0*a0 + h*h );\n  m *= 1.79284291400159 - 0.85373472095" +
                "314 * ( a0*a0 + h*h );\n\n// Compute final noise value at P\n  vec3 g;\n  g.x " +
                " = a0.x  * x0.x  + h.x  * x0.y;\n  g.yz = a0.yz * x12.xz + h.yz * x12.yw;\n  r" +
                "eturn 130.0 * dot(m, g);\n}\n\n\n\nfloat map_1_3(float value, float inputMin, " +
                "float inputMax, float outputMin, float outputMax, bool clamp) {\n  if(clamp ==" +
                " true) {\n    if(value < inputMin) return outputMin;\n    if(value > inputMax)" +
                " return outputMax;\n  }\n\n  float p = (outputMax - outputMin) / (inputMax - i" +
                "nputMin);\n  return ((value - inputMin) * p) + outputMin;\n}\n\n\nvec3 hsv2rgb" +
                "_2_4(vec3 c) {\n  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);\n  vec3 p = a" +
                "bs(fract(c.xxx + K.xyz) * 6.0 - K.www);\n  return c.z * mix(K.xxx, clamp(p - K" +
                ".xxx, 0.0, 1.0), c.y);\n}\n\n\n\nvoid main(){\n  vec2 uv = gl_FragCoord.xy / r" +
                "esolution.xy / devicePixelRatio;\n  vec4 data = texture2D(dataTex, uv);\n  vec" +
                "2 velocity = data.xy;\n  float pressure = data.z;\n\n  float n1 = (snoise_3_2(" +
                "vec2(\n    gl_FragCoord.x / devicePixelRatio * 2.0 - time * 0.1,\n    gl_FragC" +
                "oord.y / devicePixelRatio * 2.0\n  ) * 0.002) + 1.0) * 0.5;\n\n  float n2 = (s" +
                "noise_3_2(vec2(\n    gl_FragCoord.x / devicePixelRatio * 2.0 - time * 0.2,\n  " +
                "  gl_FragCoord.y / devicePixelRatio * 2.0\n  ) * 0.01) + 1.0) * 0.5;\n\n  n1 =" +
                " max(n1, n2);\n\n  float p1 = pressure * length(velocity) * 0.3;\n  float p2 =" +
                " length(velocity) * 0.8 * 0.3;\n\n  vec4 addColor = vec4(\n    hsv2rgb_2_4(vec" +
                "3(\n      map_1_3(n1, 0.0, 1.0, h1, h2, true),\n      0.1,\n      map_1_3(n1, " +
                "0.0, 1.0, 0.0, 0.2, true)\n    )), 1.0\n  ) * 0.1;\n\n  gl_FragColor = vec4(\n" +
                "    hsv2rgb_2_4(vec3(\n      map_1_3(1.0 - p2, 0.0, 1.0, h1, h2, true),\n     " +
                " map_1_3(p1, 0.0, 1.0, s1, s2, true),\n      map_1_3(1.0 - p2, 0.0, 1.0, v1, v" +
                "2, true)\n    )), 1.0\n  ) + addColor;\n}\n"
    },
    function (n, e) {
        n.exports = "precision highp float;\n#define GLSLIFY 1\n\nuniform float texPixelRatio;\nuni" +
                "form vec2 resolution;\nuniform sampler2D dataTex;\n\nvec2 sampleVelocity_1_0(s" +
                "ampler2D tex, vec2 uv, vec2 resolution){\n  vec2 cellOffset = vec2(0.0, 0.0);" +
                "\n  vec2 multiplier = vec2(1.0, 1.0);\n\n  //free-slip boundary: the average f" +
                "low across the boundary is restricted to 0\n  //avg(uA.xy, uB.xy) dot (boundar" +
                "y normal).xy = 0\n  //walls\n  if(uv.x < 0.0) {\n    cellOffset.x = 1.0;\n    " +
                "multiplier.x = -1.0;\n\n  } else if(uv.x > 1.0) {\n    cellOffset.x = -1.0;\n " +
                "   multiplier.x = -1.0;\n  }\n\n  if(uv.y < 0.0) {\n    cellOffset.y = 1.0;\n " +
                "   multiplier.y = -1.0;\n\n  } else if(uv.y > 1.0) {\n    cellOffset.y = -1.0;" +
                "\n    multiplier.y = -1.0;\n  }\n\n  return multiplier * texture2D(tex, uv + c" +
                "ellOffset / resolution).xy;\n}\n\n\n\n\nvoid main(){\n  vec2 r = resolution * " +
                "texPixelRatio;\n  vec4 data = texture2D(dataTex, gl_FragCoord.xy / r);\n\n  ve" +
                "c2 offsetX = vec2(1.0, 0.0);\n  vec2 offsetY = vec2(0.0, 1.0);\n\n  // 上下左右の速度" +
                "\n  vec2 vLeft   = sampleVelocity_1_0(dataTex, (gl_FragCoord.xy - offsetX) / r" +
                ", r);\n  vec2 vRight  = sampleVelocity_1_0(dataTex, (gl_FragCoord.xy + offsetX" +
                ") / r, r);\n  vec2 vTop    = sampleVelocity_1_0(dataTex, (gl_FragCoord.xy - of" +
                "fsetY) / r, r);\n  vec2 vBottom = sampleVelocity_1_0(dataTex, (gl_FragCoord.xy" +
                " + offsetY) / r, r);\n\n  float divergence = ((vRight.x - vLeft.x) + (vBottom." +
                "y - vTop.y)) * 0.5;\n  gl_FragColor = vec4(data.xy, data.z, divergence);\n}\n"
    },
    function (n, e) {
        n.exports = "precision highp float;\n#define GLSLIFY 1\n\nuniform float texPixelRatio;\nuni" +
                "form float alpha;\nuniform float beta;\nuniform vec2 resolution;\nuniform samp" +
                "ler2D dataTex;\n\nfloat samplePressure_1_0(sampler2D tex, vec2 uv, vec2 resolu" +
                "tion){\n  vec2 cellOffset = vec2(0.0, 0.0);\n\n  //pure Neumann boundary condi" +
                "tions: 0 pressure gradient across the boundary\n  //dP/dx = 0\n  //walls\n  if" +
                "(uv.x < 0.0) {\n    cellOffset.x = 1.0;\n\n  } else if(uv.x > 1.0) {\n    cell" +
                "Offset.x = -1.0;\n\n  }\n\n  if(uv.y < 0.0) {\n    cellOffset.y = 1.0;\n\n  } " +
                "else if(uv.y > 1.0) {\n    cellOffset.y = -1.0;\n\n  }\n\n  return texture2D(t" +
                "ex, uv + cellOffset / resolution).z;\n}\n\n\n\n\nvoid main(){\n  vec2 r = reso" +
                "lution * texPixelRatio;\n  vec4 data = texture2D(dataTex, gl_FragCoord.xy / r)" +
                ";\n\n  // 上下左右の圧力\n  float pLeft   = samplePressure_1_0(dataTex, (gl_FragCoord" +
                ".xy - vec2(1.0, 0.0)) / r, r);\n  float pRight  = samplePressure_1_0(dataTex, " +
                "(gl_FragCoord.xy + vec2(1.0, 0.0)) / r, r);\n  float pTop    = samplePressure_" +
                "1_0(dataTex, (gl_FragCoord.xy - vec2(0.0, 1.0)) / r, r);\n  float pBottom = sa" +
                "mplePressure_1_0(dataTex, (gl_FragCoord.xy + vec2(0.0, 1.0)) / r, r);\n\n  flo" +
                "at divergence = data.w;\n  float pressure = (divergence * alpha + (pLeft + pRi" +
                "ght + pTop + pBottom)) * 0.25 * beta;\n  gl_FragColor = vec4(data.xy, pressure" +
                ", divergence);\n}\n"
    },
    function (n, e) {
        n.exports = "precision highp float;\n#define GLSLIFY 1\n\nuniform float time;\nuniform floa" +
                "t texPixelRatio;\nuniform float viscosity;\nuniform float forceRadius;\nunifor" +
                "m float forceCoefficient;\nuniform float autoforceCoefficient;\nuniform vec2 r" +
                "esolution;\nuniform sampler2D dataTex;\nuniform vec2 mousePos;\nuniform vec2 p" +
                "MousePos;\n\nfloat samplePressure_1_0(sampler2D tex, vec2 uv, vec2 resolution)" +
                "{\n  vec2 cellOffset = vec2(0.0, 0.0);\n\n  //pure Neumann boundary conditions" +
                ": 0 pressure gradient across the boundary\n  //dP/dx = 0\n  //walls\n  if(uv.x" +
                " < 0.0) {\n    cellOffset.x = 1.0;\n\n  } else if(uv.x > 1.0) {\n    cellOffse" +
                "t.x = -1.0;\n\n  }\n\n  if(uv.y < 0.0) {\n    cellOffset.y = 1.0;\n\n  } else " +
                "if(uv.y > 1.0) {\n    cellOffset.y = -1.0;\n\n  }\n\n  return texture2D(tex, u" +
                "v + cellOffset / resolution).z;\n}\n\n\n\nfloat map_2_1(float value, float inp" +
                "utMin, float inputMax, float outputMin, float outputMax, bool clamp) {\n  if(c" +
                "lamp == true) {\n    if(value < inputMin) return outputMin;\n    if(value > in" +
                "putMax) return outputMax;\n  }\n\n  float p = (outputMax - outputMin) / (input" +
                "Max - inputMin);\n  return ((value - inputMin) * p) + outputMin;\n}\n\n\n//\n/" +
                "/ Description : Array and textureless GLSL 2D simplex noise function.\n//     " +
                " Author : Ian McEwan, Ashima Arts.\n//  Maintainer : ijm\n//     Lastmod : 201" +
                "10822 (ijm)\n//     License : Copyright (C) 2011 Ashima Arts. All rights reser" +
                "ved.\n//               Distributed under the MIT License. See LICENSE file.\n/" +
                "/               https://github.com/ashima/webgl-noise\n//\n\nvec3 mod289_3_2(v" +
                "ec3 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec2 mod289_3_2" +
                "(vec2 x) {\n  return x - floor(x * (1.0 / 289.0)) * 289.0;\n}\n\nvec3 permute_" +
                "3_3(vec3 x) {\n  return mod289_3_2(((x*34.0)+1.0)*x);\n}\n\nfloat snoise_3_4(v" +
                "ec2 v)\n  {\n  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0" +
                "\n                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)\n          " +
                "           -0.577350269189626,  // -1.0 + 2.0 * C.x\n                      0.0" +
                "24390243902439); // 1.0 / 41.0\n// First corner\n  vec2 i  = floor(v + dot(v, " +
                "C.yy) );\n  vec2 x0 = v -   i + dot(i, C.xx);\n\n// Other corners\n  vec2 i1;" +
                "\n  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0\n  //i1.y = 1.0 - " +
                "i1.x;\n  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);\n  // x0 = x0 -" +
                " 0.0 + 0.0 * C.xx ;\n  // x1 = x0 - i1 + 1.0 * C.xx ;\n  // x2 = x0 - 1.0 + 2." +
                "0 * C.xx ;\n  vec4 x12 = x0.xyxy + C.xxzz;\n  x12.xy -= i1;\n\n// Permutations" +
                "\n  i = mod289_3_2(i); // Avoid truncation effects in permutation\n  vec3 p = " +
                "permute_3_3( permute_3_3( i.y + vec3(0.0, i1.y, 1.0 ))\n    + i.x + vec3(0.0, " +
                "i1.x, 1.0 ));\n\n  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot" +
                "(x12.zw,x12.zw)), 0.0);\n  m = m*m ;\n  m = m*m ;\n\n// Gradients: 41 points u" +
                "niformly over a line, mapped onto a diamond.\n// The ring size 17*17 = 289 is " +
                "close to a multiple of 41 (41*7 = 287)\n\n  vec3 x = 2.0 * fract(p * C.www) - " +
                "1.0;\n  vec3 h = abs(x) - 0.5;\n  vec3 ox = floor(x + 0.5);\n  vec3 a0 = x - o" +
                "x;\n\n// Normalise gradients implicitly by scaling m\n// Approximation of: m *" +
                "= inversesqrt( a0*a0 + h*h );\n  m *= 1.79284291400159 - 0.85373472095314 * ( " +
                "a0*a0 + h*h );\n\n// Compute final noise value at P\n  vec3 g;\n  g.x  = a0.x " +
                " * x0.x  + h.x  * x0.y;\n  g.yz = a0.yz * x12.xz + h.yz * x12.yw;\n  return 13" +
                "0.0 * dot(m, g);\n}\n\n\n\n\nvoid main(){\n  vec2 r = resolution * texPixelRat" +
                "io;\n  vec2 uv = gl_FragCoord.xy / r;\n  vec4 data = texture2D(dataTex, uv);\n" +
                "  vec2 v = data.xy;\n\n  vec2 offsetX = vec2(1.0, 0.0);\n  vec2 offsetY = vec2" +
                "(0.0, 1.0);\n\n  // 上下左右の圧力\n  float pLeft   = samplePressure_1_0(dataTex, (gl" +
                "_FragCoord.xy - offsetX) / r, r);\n  float pRight  = samplePressure_1_0(dataTe" +
                "x, (gl_FragCoord.xy + offsetX) / r, r);\n  float pTop    = samplePressure_1_0(" +
                "dataTex, (gl_FragCoord.xy - offsetY) / r, r);\n  float pBottom = samplePressur" +
                "e_1_0(dataTex, (gl_FragCoord.xy + offsetY) / r, r);\n\n  // マウス\n  vec2 mPos =" +
                " vec2(mousePos.x * texPixelRatio, r.y - mousePos.y * texPixelRatio);\n  vec2 m" +
                "PPos = vec2(pMousePos.x * texPixelRatio, r.y - pMousePos.y * texPixelRatio);\n" +
                "  vec2 mouseV = mPos - mPPos;\n  float len = length(mPos - uv * r) / forceRadi" +
                "us / texPixelRatio;\n  float d = clamp(1.0 - len, 0.0, 1.0) * length(mouseV) *" +
                " forceCoefficient;\n  vec2 mforce = d * normalize(mPos - uv * r + mouseV);\n\n" +
                "  // 自動\n  float noiseX = snoise_3_4(vec2(uv.s, time / 5000.0 + uv.t));\n  flo" +
                "at noiseY = snoise_3_4(vec2(time / 5000.0 + uv.s, uv.t));\n  float waveX = cos" +
                "(time / 1000.0 + noiseX) * sin(time / 400.0 + noiseX) * cos(time / 600.0 + noi" +
                "seX);\n  float waveY = sin(time / 500.0 + noiseY) * cos(time / 800.0 + noiseY)" +
                " * sin(time / 400.0 + noiseY);\n  waveX = map_2_1(waveX, -1.0, 1.0, -0.2, 1.2," +
                " true);\n  waveY = map_2_1(waveY, -1.0, 1.0, -0.2, 1.2, true);\n  vec2 aPos = " +
                "vec2(\n    r.x * waveX,\n    r.y * waveY\n  );\n  len = length(aPos - uv * r) " +
                "/ forceRadius / texPixelRatio / 10.0;\n  d = clamp(1.0 - len, 0.0, 1.0) * auto" +
                "forceCoefficient;\n  vec2 aforce = d * normalize(aPos - uv * r);\n\n  v += vec" +
                "2(pRight - pLeft, pBottom - pTop) * 0.5;\n  v += mforce + aforce;\n  v *= visc" +
                "osity;\n  gl_FragColor = vec4(v, data.zw);\n}\n"
    },
    function (n, e) {
        n.exports = "precision highp float;\n#define GLSLIFY 1\n\nuniform float texPixelRatio;\nuni" +
                "form float attenuation;\nuniform vec2 resolution;\nuniform sampler2D dataTex;" +
                "\n\nvec2 sampleVelocity_1_0(sampler2D tex, vec2 uv, vec2 resolution){\n  vec2 " +
                "cellOffset = vec2(0.0, 0.0);\n  vec2 multiplier = vec2(1.0, 1.0);\n\n  //free-" +
                "slip boundary: the average flow across the boundary is restricted to 0\n  //av" +
                "g(uA.xy, uB.xy) dot (boundary normal).xy = 0\n  //walls\n  if(uv.x < 0.0) {\n " +
                "   cellOffset.x = 1.0;\n    multiplier.x = -1.0;\n\n  } else if(uv.x > 1.0) {" +
                "\n    cellOffset.x = -1.0;\n    multiplier.x = -1.0;\n  }\n\n  if(uv.y < 0.0) " +
                "{\n    cellOffset.y = 1.0;\n    multiplier.y = -1.0;\n\n  } else if(uv.y > 1.0" +
                ") {\n    cellOffset.y = -1.0;\n    multiplier.y = -1.0;\n  }\n\n  return multi" +
                "plier * texture2D(tex, uv + cellOffset / resolution).xy;\n}\n\n\n\nfloat sampl" +
                "ePressure_2_1(sampler2D tex, vec2 uv, vec2 resolution){\n  vec2 cellOffset = v" +
                "ec2(0.0, 0.0);\n\n  //pure Neumann boundary conditions: 0 pressure gradient ac" +
                "ross the boundary\n  //dP/dx = 0\n  //walls\n  if(uv.x < 0.0) {\n    cellOffse" +
                "t.x = 1.0;\n\n  } else if(uv.x > 1.0) {\n    cellOffset.x = -1.0;\n\n  }\n\n  " +
                "if(uv.y < 0.0) {\n    cellOffset.y = 1.0;\n\n  } else if(uv.y > 1.0) {\n    ce" +
                "llOffset.y = -1.0;\n\n  }\n\n  return texture2D(tex, uv + cellOffset / resolut" +
                "ion).z;\n}\n\n\n\n\nvec2 bilerp(sampler2D tex, vec2 p, vec2 resolution) {\n   " +
                " vec4 ij; // i0, j0, i1, j1\n    ij.xy = floor(p - 0.5) + 0.5;\n    ij.zw = ij" +
                ".xy + 1.0;\n\n    vec4 uv = ij / resolution.xyxy;\n    vec2 d11 = sampleVeloci" +
                "ty_1_0(tex, uv.xy, resolution);\n    vec2 d21 = sampleVelocity_1_0(tex, uv.zy," +
                " resolution);\n    vec2 d12 = sampleVelocity_1_0(tex, uv.xw, resolution);\n   " +
                " vec2 d22 = sampleVelocity_1_0(tex, uv.zw, resolution);\n\n    vec2 a = p - ij" +
                ".xy;\n\n    return mix(mix(d11, d21, a.x), mix(d12, d22, a.x), a.y);\n}\n\nvoi" +
                "d main(){\n  vec2 r = resolution * texPixelRatio;\n  vec2 p = gl_FragCoord.xy " +
                "- sampleVelocity_1_0(dataTex, gl_FragCoord.xy / r, r);\n  gl_FragColor = vec4(" +
                "bilerp(dataTex, p, r) * attenuation, samplePressure_2_1(dataTex, gl_FragCoord." +
                "xy / r, r), 0.0);\n  }\n"
    },
    function (n, e) {
        n.exports = "precision mediump float;\n#define GLSLIFY 1\n\nvoid main(){\n  gl_FragColor = " +
                "vec4(0.0);\n}\n"
    }
]);