var mvHeight = function() {
	var hsize = $(window).height();
	$(".js_mvHeight").css('height',hsize + 'px');
}

$(function() {

	// mvの高さ
	mvHeight();

	// google map color change
	var map;
	var brooklyn = new google.maps.LatLng(35.693154,139.705824);
	var MY_MAPTYPE_ID = 'Pandy_style';
	function initialize() {
	var featureOpts = [
		{ "stylers": [
			{ "hue": "#000000" },
			{ "saturation": -100 }
			],
			"elementType": "all",
			"featureType": "all"
		}
	]
	var mapOptions = {
		zoom: 18,
		center: brooklyn,
		mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
		},
		mapTypeId: MY_MAPTYPE_ID,
		scrollwheel: false
	};
	map = new google.maps.Map(document.getElementById('map-canvas'),
	mapOptions);

	var marker = new google.maps.Marker({
		position: brooklyn,
		map: map,
	});
	var styledMapOptions = {
		name: 'Google Map'
	};
	var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
		map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
	}
	google.maps.event.addDomListener(window, 'load', initialize);

});

// リサイズ完了後の処理
var timer = false;
$(window).resize(function() {
	if(timer !== false){
		clearTimeout(timer);
	}
	timer = setTimeout(function() {
		mvHeight();
	}, 200);
});
