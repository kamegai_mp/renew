var wsize = function() {
	var size = $(window).width();
	return size;
}

var responsive = function(size) {
	var medium = 786;
	if(size <= 786) {
		return true;
	} else {
		return false;
	}
}

var bgHeight = function() {
	var hsize = $(window).height();
	$(".js_bgHeight").css('height',hsize + 'px');
}

var gmenuControl = function() {
	// toggleButtonのアイコン切り替え
	var $toggleButton = $(".js_toggleButton");
	var $toggleButtonMenu = $(".js_toggleButton--menu");
	var $toggleButtonClose = $(".js_toggleButton--close");
	var $toggleContents = $(".js_toggleContents");

	$toggleButtonClose.stop().fadeOut();
	$toggleButtonMenu.stop().fadeIn();

	$toggleButton.on("click",function() {
		if($toggleContents.css('display') === 'block') {
			$toggleButtonMenu.stop().fadeOut();
			$toggleButtonClose.stop().fadeIn();
		} else {
			$toggleButtonClose.stop().fadeOut();
			$toggleButtonMenu.stop().fadeIn();
		}
	});

}

$(function() {

	var js_nav_toggle = $('#js_nav_toggle');
	var js_nav_bar = $('#js_nav_bar');
	var js_logo = $('#js_logo');
	var body = $('body');

	var checkPrivacy = $('#checkPrivacy input');
	var submitButton = $('#confirm');
	submitButton.attr('disabled','disabled');

	checkPrivacy.change(function(){
		if ($(this).is(':checked')) {
			submitButton.attr('disabled',false);
		} else {
			submitButton.attr('disabled','disabled');
		}
	});


	js_nav_toggle.click(function(){
		js_nav_bar.toggleClass('active');
		js_logo.toggleClass('active');
		$(this).toggleClass('active');
		body.toggleClass('active');
	});

	$('.has_submenu').click(function(){
		$(this).toggleClass('active');
	});

	//cookieでテーマを管理
	var THEME_NAME = ['theme-01', 'theme-02', 'theme-03', 'theme-04', 'theme-05', 'theme-06'];
	var theme = THEME_NAME[ Math.floor( Math.random() * THEME_NAME.length ) ] ;

	// cookieの有効期限
	date = new Date();
	date.setTime( date.getTime() + ( 10 * 1000 ));
	// date.setTime( date.getTime() + ( 10 * 60 * 1000 ));
	$.cookie( "theme", theme , { expires: date });

	var cookieTheme = $.cookie("theme");

	// bodyclassの切り替え
	$('body').addClass(cookieTheme);

	wsize();
	gmenuControl();
	bgHeight();


	// custom tabs
	var custom_tabs_button = $('.custom_tabs--button');
	var custom_tabs_content = $('.custom_tabs--content');

	custom_tabs_button.each(function(){
		$(this).click(function(e){
			e.preventDefault

			var id = $(this).attr('id');

			custom_tabs_content.each(function(){
				$(this).hide();
			});

			if (!$(this).hasClass('active')) {
				custom_tabs_button.each(function(){
					$(this).removeClass('active');
				});

				$(this).addClass('active');
				$('#'+id+'--content').show();
			} else {
				$(this).removeClass('active');
			}

		});
	});

	var topBtn = $('.js_totop');    
	topBtn.hide();
	$(window).scroll(function () {
		if ($(this).scrollTop() > 500) {
			topBtn.fadeIn("fast");
		} else {
			topBtn.fadeOut("fast");
		}
	});


});

// リサイズ完了後の処理
var flag = false;
$(window).resize(function() {
	if(flag !== false){
		clearTimeout(flag);
	}
	flag = setTimeout(function() {
		wsize();
		gmenuControl();
		bgHeight();
	}, 200);
});
