<?php
/* Template Name: Front page */
get_header(); ?>

<section class="section section__mv row align-middle js_mvHeight"
  data-case1="<?php echo get_template_directory_uri(); ?>/assets/img/bg-01.jpg"
  data-case2="<?php echo get_template_directory_uri(); ?>/assets/img/bg-02.jpg">

	<div id="js_fadein" class="columns small-12">
		<h2 class="section__mv--title">Good experience<br />And Business growth</h2>
		<h3 class="section__mv--subtitle">我々はユーザーにとって最も心地よく、<br class="hide-for-medium" />ビジネスにとって最も効果の高い<br />クリエイティブをお届けします。</h3>
	</div>

	<div class="columns small-12 section__mv--scroll" data-magellan>
		<a href="#contents" title="下へ"><svg class="icon arrow_01"><use xlink:href="#icon-arrow_01"></use></svg></a>
		<a href="#contents" title="下へ" class="hide-for-medium"><svg class="icon arrow_02"><use xlink:href="#icon-arrow_02"></use></svg><span class="fontHVN">SCROLL DOWN</span></a>
	</div>
</section>

<section class="section bg__highlight small section__news" id="contents" data-magellan-target="contents">

	<div class="section--main row">
    <?php
    $news_args = array(
      'post_type' => 'news',
      'orderby' => 'date',
      'posts_per_page' => 3,
    );
    $news_list = get_posts( $news_args );
    ?>
		<ul class="section__news--list">
      <?php foreach ($news_list as $post) {
        setup_postdata( $post );
        $post_tag = wp_get_object_terms( $post->ID, 'news-tag' );
        $post_tag = $post_tag[0]->name;
        $post_date = get_the_date('Y.m.d', $post->ID );
        ?>
        <li>
  				<article class="article__news row align-middle">
  					<p class="article__news--category"><?php echo $post_tag; ?></p>
  					<time class="article__news--time fontHV"><?php echo $post_date; ?></time>
                   	<p class="article__news--link"><a title="<?php the_title(); ?>" href="<?php echo get_the_permalink(); ?>" class="link link__underline"><?php the_title(); ?></a></p>
  				</article>
  			</li>
      <?php wp_reset_postdata(); } ?>
		</ul>
	</div>
	<div class="section__link medium">
		<a title="一覧を見る" href="<?php echo get_post_type_archive_link('news'); ?>" class="link link__text">一覧を見る<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a>
	</div>

</section>


<section class="section section__service">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">サービス</h2>
			<h3 class="section--subtitle">デジタルとクリエイティブの力であなたのビジネスを成長させます</h3>
		</div>
	</div>

	<div class="section--main section__service--main">
    <?php if( have_rows('front_service_list') ): ?>
			<ul class="section__service--list row medium-up-4">
        <?php while ( have_rows('front_service_list') ) : the_row();
          $front_service_title = get_sub_field('front_service_title');
          $front_service_content = get_sub_field('front_service_content');
          $front_service_image = get_sub_field('front_service_image');
          $front_service_image = $front_service_image['sizes']['large'];
          // $front_service_image_retina = get_sub_field('front_service_image_retina');
          // $front_service_image_retina = $front_service_image_retina['sizes']['large'];
        ?>
        <li class="columns small-12 medium-expand">
  				<article class="article article__service">
  					<h4 class="article__service--title show-for-medium"><?php echo $front_service_title; ?></h4>
  					<div class="article__service--box row align-middle">
  						<div class="columns small-5 medium-12">
  							<figure class="article__service--figure">
  								<img src="<?php echo $front_service_image; ?>" class="small" alt="<?php echo $front_service_title; ?>" />
  							</figure>
  						</div>
  						<div class="columns small-7 medium-12">
  							<h4 class="article__service--title hide-for-medium"><?php echo $front_service_title; ?></h4>
                <?php echo $front_service_content; ?>
  						</div>
  					</div>
  				</article>
  			</li>
        <?php endwhile; ?>
      </ul>
    <?php endif; ?>
		<div class="section__link">
			<a href="<?php echo get_permalink(get_page_by_path('service')); ?>" title="詳細を見る" class="link link__text">詳細を見る<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a>
		</div>
	</div>
</section>

<section class="section bg section__works">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title fontHVN">実績</h2>
			<h3 class="section--subtitle">累計4000サイト以上 <br class="hide-for-medium" />ありとあらゆる実績があります</h3>
		</div>
	</div>


	<div class="section--main section__works--main">

    <?php get_template_part( 'includes/front-work' ); ?>

    <?php if( have_rows('clients_list') ): ?>
      <section class="section__clients">
  			<h2 class="section__clients--title">主なクライアント</h2>
  			<div class="section__clients--main"><div class="section__clients--mainInner">
  				<ul class="section__clients--list row align-center">
            <?php while ( have_rows('clients_list') ) : the_row();
              $client_name = get_sub_field('client_name');
              $client_logo = get_sub_field('client_logo');
              $client_logo = $client_logo['sizes']['large'];
              $client_subtitle_1 = get_sub_field('client_subtitle_1');
              $client_subtitle_2 = get_sub_field('client_subtitle_2');
            ?>
            <li class="columns medium-expand">
              <img src="<?php echo $client_logo; ?>" alt="<?php echo $client_name; ?>" />
            </li>
            <?php endwhile; ?>
          </ul>
        </div></div>
      </section>
    <?php endif; ?>
	</div>
</section>

<section class="section section__products">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">プロダクト</h2>
		</div>
	</div>

	<div class="section--main section--top">
		<article class="article row">
			<div class="columns small-12 medium-4">
				<figure class="article__products--image">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/products/img_front-free-package-01.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/products/img_front-free-package-01@2x.png" alt="無料デザインテンプレート" />
				</figure>
				<h4 class="article__products--title bold">無料デザインテンプレート</h4>
				<p class="article__products--text">無料でご利用いただけるレスポンシブ対応のテンプレートです。
				<br class="show-for-medium" />オンラインで一部のカラーやフォントの変更ができます。
				<br class="show-for-medium" />ダウンロードしてご利用ください。</p>
				<p class="text-left"><a href="/products/template/" title="詳しくはこちら" class="link__text">詳しくはこちら<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></p>
			</div>
			<div class="columns small-12 medium-4">
				<figure class="article__products--image">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/products/img_front-sitecheck.png" alt="Webサイト診断" />
				</figure>
				<h4 class="article__products--title bold">Webサイト診断</h4>
				<p class="article__products--text">Web制作のプロがノウハウを集結させた簡易診断ツールです。<br class="show-for-medium" />弊社のノウハウを用いて簡易的に問題点を抽出できるので、Webサイト改善への第一歩として、是非ご利用ください。</p>
				<p class="text-left"><a href="/siteCheck/" title="詳しくはこちら" class="link__text" target="_blank">詳しくはこちら<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></p>
			</div>
			<div class="columns small-12 medium-4">
				<figure class="article__products--image">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/products/img_front-job-curator-01.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/products/img_front-job-curator-01@2x.png" alt="JOB CURATOR" />
				</figure>
				<h4 class="article__products--title fontHVN bold">JOB CURATOR</h4>
				<p class="article__products--text">業界に特化した求人情報のまとめサイトを構築・管理できるシステム。
				<br class="show-for-medium" />求人情報を定期的にクロールする機能と求人情報の出稿を管理する
				<br class="show-for-medium" />機能があります。</p>
				<!-- <p class="article__products--link"><a href="http://job-curator.jp/" title="プロダクトサイトを見る" target="_blank" class="link link__text">プロダクトサイトを見る<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></p> -->
			</div>
		</article>
	</div>
</section>

<section class="section bg section__blog">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">ブログ</h2>
		</div>
	</div>
	<div class="section--main">
    <?php get_template_part( 'includes/front-blog' ); ?>
	</div>
</section>

<section class="section section__company">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title fontHVN">会社情報</h2>
		</div>
	</div>
	<div class="section--main">
		<article class="article__map">
			<div id="map-canvas" class="article__map--googlemap"></div>
		</article>
		<article class="article__company row column">
			<h4 class="article__company--title">株式会社メタフェイズ metaphase, co.,ltd.</h4>
			<div class="article__company--main row align-justify">
				<div class="article__company--address columns large-6 small-12">
					<p class="list list--icon-line"><i></i>東京都新宿区新宿5-17-5 ラウンドクロス新宿5丁目ビル6F</p>
					<p class="list list--icon-line"><i></i>都営新宿線 東京メトロ丸ノ内線 東京メトロ副都心線『新宿三丁目』駅『E2』<br />出口直結
					<span class="add">JR線 『新宿』駅より徒歩15分</span></p>
				</div>
				<div class="article__company--information columns large-6 small-12">
					<p class="list list--icon-line"><i></i>TEL：03-6384-5184</p>
					<div class="article__company--sns01 row align-middle">
						<div class="article__company--sns large-4 large-pull-8 small-6 columns">
							<ul class="article__company--list menu large-vertical">
								<li><a href="http://recruit.metaphase.co.jp/" title="RECRUIT SITE" target="_blank"><i><svg class="icon metaphase"><use xlink:href="#icon-metaphase"></use></svg></i>RECRUIT SITE</a></li>
								<li class="show-for-medium"><a href="https://ja-jp.facebook.com/metaphase.co.jp" title="公式Facebook" target="_blank"><i><svg class="icon facebook"><use xlink:href="#icon-facebook"></use></svg></i>公式Facebook</a></li>
							</ul>
						</div>
						<div class="article__company--banner large-8 large-push-4 small-6 columns">
							<a href="http://www.metaphase.co.jp/column/" title="PR BLOG" target="_blank" class="expanded button"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_metaphase-pr-blog.png" alt="PR BLOG" /></a>
						</div>
					</div>
					<div class="article__company--sns02">
						<ul class="article__company--list bottom menu">
							<li class="hide-for-medium"><a href="https://ja-jp.facebook.com/metaphase.co.jp" title="公式Facebook" target="_blank"><i><svg class="icon facebook"><use xlink:href="#icon-facebook"></use></svg></i><span class="show-for-medium">公式</span>Facebook</a></li>
							<li><a href="https://www.instagram.com/metaphase_pr/" target="_blank" title="公式Instagram"><i><svg class="icon instagram"><use xlink:href="#icon-instagram"></use></svg></i><span class="show-for-medium">公式</span>Instagram</a></li>
							<li><a href="https://twitter.com/metaphase_co_jp" target="_blank" title="公式Twitter"><i><svg class="icon twitter"><use xlink:href="#icon-twitter"></use></svg></i><span class="show-for-medium">公式</span>Twitter</a></li>
							<li><a href="#" target="_blank" title="公式Tumblr"><i><svg class="icon tumblr"><use xlink:href="#icon-tumblr"></use></svg></i><span class="show-for-medium">公式</span>Tumblr</a></li>
						</ul>
					</div>
				</div>
			</div>
		</article>

		<div class="section__link">
			<a href="<?php echo get_permalink(get_page_by_path('about')); ?>" title="詳細を見る" class="link link__text">詳細を見る<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a>
		</div>
	</div>
</section>

<section class="section small bg__highlight section__contact">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title fontHVN">お問い合わせ</h2>
			<h3 class="section--subtitle">まずはお気軽にご相談ください</h3>
		</div>
	</div>
	<div class="section--main small">
		<article class="article__contact">
			<div class="article__contact--button">
				<a href="<?php echo get_permalink(get_page_by_path('contact/work')); ?>" title="お問い合わせはこちら" class="expanded button">お問い合わせはこちら</a>
			</div>
			<p class="article__contact--link"><a href="<?php echo get_permalink(get_page_by_path('contact')); ?>" title="採用企業やパートナー様はこちら" class="link link__text">採用企業やパートナー様はこちら<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></p>
		</article>
	</div>
</section>


<?php get_footer(); ?>
