<?php $work_args = array(
  'post_type' => 'work',
  'orderby' => 'date',
  'posts_per_page' => 3,
);

$work_list = get_posts( $work_args ); ?>
<ul class="section__works--list row small-up-1 medium-up-3">
  <?php foreach ($work_list as $post) {
    setup_postdata( $post );

    $post_cat = wp_get_object_terms( $post->ID, 'work-category' );
    $post_cat = $post_cat[0]->name;
    $post_image = get_field('work_top_image');
    $post_image = $post_image['sizes']['large'];
    $post_client = get_field('work_client_name', $post->ID);
    ?>
      <li class="column"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        <article class="article article__works">
          <figure class="article__works--image" style="background-image: url(<?php echo $post_image; ?>);"></figure>
          <div class="article__works--text">
            <h5 class="article__works--subtitle"><?php echo $post_client; ?><br /><small><?php echo $post_cat; ?></small></h5>
            <h4 class="article__works--title"><?php the_title(); ?></h4>
          </div>
        </article>
      </a></li>
  <?php wp_reset_postdata(); } ?>
</ul>

<div class="section__link large">
  <a href="<?php echo get_post_type_archive_link('work'); ?>" class="link link__text">一覧を見る<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a>
</div>
