<?php
$queried_object = get_queried_object();
$label_args = array(
	'taxonomy' => 'blog-label'
);
$labels_list = get_terms($label_args);

$writer_args = array(
  'post_type' => 'writer',
  'posts_per_page' => -1,
);
$writer_list = get_posts( $writer_args );
?>

<aside class="aside">
  <section class="section section__aside">
    <div class="section__aside--head">
      <h2 class="section__aside--title">カテゴリー</h2>
    </div>
    <div class="section__aside--main">
      <article class="article article__category">
        <ul class="article__category--list">
          <?php foreach ($labels_list as $label) { ?>
            <li><a href="<?php echo get_term_link($label->term_id); ?>" class="link link__text" title="<?php echo $label->name; ?>"><?php echo $label->name; ?><svg class="icon arrow_08"><use xlink:href="#icon-arrow_08"></use></svg></a></li>
          <?php } ?>
        </ul>
      </article>
    </div>
  </section>
  <section class="section section__aside">
    <div class="section__aside--head">
      <h2 class="section__aside--title">ライター</h2>
    </div>
    <div class="section__aside--main medium">
      <article class="article article__posted">
        <ul class="article__posted--member row small-up-1 large-up-1">
          <?php foreach ($writer_list as $post) {
						setup_postdata( $post );
            $writer_image = get_field('writer_image');
						$writer_image = $writer_image['sizes']['large'];
						$writer_content = wp_trim_words( $post->post_content, 24, "" );
						$writer_totem = get_field('writer_totem');
						$writer_totem = $writer_totem['sizes']['large'];
            ?>
            <li class="column column-block">
              <div class="card__member row align-middle">
                <div class="columns small-6 medium-12">
                  <div class="card__member--image" style="background-image: url(<?php echo $writer_image; ?>)">
										<i class="card__member--icon"><img src="<?php echo $writer_totem; ?>" alt="ヤモリ" /></i>
                  </div>
                </div>
                <div class="columns small-6 medium-12">
                  <div class="card__member--text">
                    <p class="name"><?php echo $post->post_title; ?></p>
                    <p class="profile"><?php echo $writer_content; ?> </p>
                    <p class="link-bottom"><a href="<?php echo get_the_permalink($post->ID); ?>" class="link link__text" title="ブログを見る">ブログを見る<svg class="icon arrow_08"><use xlink:href="#icon-arrow_08"></use></svg></a></p>
                  </div>
                </div>
              </div>
            </li>
          <?php wp_reset_postdata(); } ?>
        </ul>
      </article>
    </div>
  </section>
</aside>
