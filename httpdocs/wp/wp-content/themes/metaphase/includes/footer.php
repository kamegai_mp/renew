<footer id="footer" class="footer">

  <div class="button__totop js_totop" data-magellan>
    <a href="#top" title="ページトップへ"><svg class="icon arrow_09"><use xlink:href="#icon-arrow_09"></use></svg></a>
  </div>

  <div class="row align-middle">
    <div class="columns large-2 medium-12 small-12 show-for-large">
      <h2 class="footer--title image image__full"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_metaphase.svg" alt=""></a></h2>
    </div>
    <div class="columns large-10 medium-12 small-12 footer--wrap">
      <div class="footer--nav">
        <ul class="nav nav__gnav align-right row small-up-2 fontHVN">
          <li class="columns"><a href="<?php echo get_permalink(get_page_by_path('about')); ?>" title="会社紹介">会社紹介</a></li>
          <li class="columns"><a href="<?php echo get_permalink(get_page_by_path('service')); ?>" title="サービス">サービス</a></li>
          <li class="columns"><a href="<?php echo get_post_type_archive_link('work'); ?>" title="実績">実績</a></li>
          <li class="columns"><a href="<?php echo get_permalink(get_page_by_path('products')); ?>" title="プロダクト">プロダクト</a></li>
          <li class="columns"><a href="<?php echo get_post_type_archive_link('blog'); ?>" title="ブログ">ブログ</a></li>
          <li class="columns"><a href="<?php echo get_permalink(get_page_by_path('recruit')); ?>" title="採用情報">採用情報</a></li>
          <li class="columns"><a href="<?php echo get_permalink(get_page_by_path('privacypolicy')); ?>" title="個人情報保護方針">個人情報保護方針</a></li>
          <li class="columns"><a href="https://ja-jp.facebook.com/metaphase.co.jp" target="_blank" title="Facebook"><svg class="icon facebook"><use xlink:href="#icon-facebook"></use></svg><span class="show-for-small-only">Facebook</span></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="row column">
    <p class="footer--copyright fontACN">Copyright© METAPHASE Co.,Ltd All rights reserved.</p>
  </div>

</footer>
