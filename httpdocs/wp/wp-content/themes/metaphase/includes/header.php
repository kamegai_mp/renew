<header id="header" class="header">
  <div class="row align-center">
    <div class="columns">

      <h1 id="js_logo" class="header--title image image__full"><a href="<?php echo home_url(); ?>" title="Metaphase">
        <svg class="logo icon-logo_metaphase"><use xlink:href="#icon-logo_metaphase"></use></svg>
      </a></h1>
    </div>

    <div class="nav--toggle--holder columns" data-responsive-toggle="js_nav" data-hide-for="medium">
      <button id="js_nav_toggle" class="nav--toggle" type="button" data-toggle>
        <svg class='icon icon-menu'><use xlink:href='#icon-menu'></use></svg>
        <svg class='icon icon-close'><use xlink:href='#icon-close'></use></svg>
      </button>
    </div>

    <div id="js_nav_bar" class="nav--bar columns shrink">

      <div id="js_nav" class="nav--holder">
        <nav class="nav">
          <ul class="menu vertical medium-horizontal">
            <li class="has_submenu">
              <span>会社紹介<svg class="icon arrow_04"><use xlink:href="#icon-arrow_04"></use></svg></span>
              <ul class="nav--submenu">
                <li><a href="<?php echo get_permalink(get_page_by_path('about')); ?>" title="ビジョン">ビジョン</a></li>
                <li><a href="<?php echo get_post_type_archive_link('member'); ?>" title="メンバー紹介">メンバー紹介</a></li>
                <li><a href="<?php echo get_permalink(get_page_by_path('about/overview')); ?>" title="会社情報">会社情報</a></li>
              </ul>
            </li>
            <li class="has_submenu">
              <span>サービス<svg class="icon arrow_04"><use xlink:href="#icon-arrow_04"></use></svg></span>
              <ul class="nav--submenu">
                <li><a href="<?php echo get_permalink(get_page_by_path('service')); ?>" title="サービスコンセプト">サービスコンセプト</a></li>
                <li><a href="<?php echo get_permalink(get_page_by_path('/service/experiencedesign/')); ?>" title="エクスペリエンスデザイン">エクスペリエンスデザイン</a></li>
                <li><a href="<?php echo get_permalink(get_page_by_path('service/retention')); ?>" title="リテンション">リテンション</a></li>
                <li><a href="<?php echo get_permalink(get_page_by_path('service/businessdevelopment')); ?>" title="プロダクトサービス">プロダクトサービス</a></li>
              </ul>
            </li>
            <li><a href="<?php echo get_post_type_archive_link('work'); ?>" title="実績">実績</a></li>
            <li><a href="<?php echo get_permalink(get_page_by_path('products')); ?>" title="プロダクト">プロダクト</a></li>
            <li class="has_submenu">
              <span>ブログ<svg class="icon arrow_04"><use xlink:href="#icon-arrow_04"></use></svg></span>
              <ul class="nav--submenu">
                <li><a href="<?php echo get_post_type_archive_link('blog'); ?>" title="ブログ">ブログ</a></li>
                <li><a href="http://www.metaphase.co.jp/column/" title="PR BLOG" target="_blank">PR BLOG</a></li>
              </ul>
            </li>
            <li><a href="<?php echo get_permalink(get_page_by_path('recruit')); ?>" title="採用情報">採用情報</a></li>
            <li><a href="<?php echo get_permalink(get_page_by_path('contact')); ?>" class="nav--button" title="お問い合わせ">お問い合わせ</a></li>
          </ul>
        </nav>
      </div>
    </div>

  </div>
</header>
