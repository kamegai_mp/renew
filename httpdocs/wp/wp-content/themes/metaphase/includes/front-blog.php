<?php $blog_args = array(
  'post_type' => 'blog',
  'orderby' => 'date',
  'posts_per_page' => 4,
);
$blog_list = get_posts( $blog_args ); ?>
<ul class="row small-up-1 medium-up-2 large-up-4 section__blog--list">
  <?php foreach ($blog_list as $post) {
    setup_postdata( $post );
    $post_tag = wp_get_object_terms( get_the_ID(), 'blog-label' );
		$post_tag = $post_tag[0]->name;
		$post_date = get_the_date('Y.m.d', get_the_ID() );
		$post_introduction = get_field('blog_introduction');
    $post_thumbnail = get_field('blog_thumbnail');
    $post_thumbnail = $post_thumbnail['sizes']['large'];
    ?>
    <li class="columns"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
			<article class="article__blog">
				<div class="article__blog--image">
					<figure style="background-image: url(<?php echo $post_thumbnail; ?>);"></figure>
				</div>
				<div class="article__blog--head row align-middle">
					<p class="article__blog--category columns"><span><?php echo $post_tag; ?></span></p>
					<time class="article__blog--time fontACN columns"><?php echo $post_date; ?></time>
				</div>
				<div class="article__blog--main">
					<h4 class="article__blog--title"><?php the_title(); ?></h4>
					<p class="article__blog--text"><?php echo $post_introduction; ?></p>
				</div>
			</article>
		</a></li>
  <?php wp_reset_postdata(); } ?>
</ul>

<div class="section__link large">
  <a href="<?php echo get_post_type_archive_link('blog'); ?>" class="link link__text" title="一覧を見る">一覧を見る<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a>
</div>
