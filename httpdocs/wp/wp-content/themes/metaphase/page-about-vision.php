<?php
/* Template Name: About vision */
get_header(); ?>


<section class="section section__sv bg">
	<h2 class="section__sv--title">ビジョン</h2>

	<ul class="section__sv--menu menu align-center show-for-medium">
		<li><a href="<?php echo get_permalink(get_page_by_path('about')); ?>" class="link link__text active" title="ビジョン">ビジョン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_post_type_archive_link('member'); ?>" class="link link__text" title="メンバー紹介">メンバー紹介<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('about/overview')); ?>" class="link link__text" title="会社情報">会社情報<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
	</ul>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><a href="<?php echo get_permalink(get_page_by_path('about')); ?>" title="会社紹介">会社紹介</a></li>
			<li><span class="show-for-sr">Current: </span>ビジョン</li>
		</ul>
	</nav>
</section>

<section class="section small">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">ミッション</h2>
			<h3 class="section--subtitle">クライアント、ユーザー、パートナー、<br class="hide-for-medium" />社員やその家族。<br />私たちが関わる全ての人の幸せに貢献する。</h3>
		</div>
	</div>
</section>

<section class="section medium bg__highlight">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">ビジョン</h2>
			<h3 class="section--subtitle">ユーザーには幸せを。ビジネスには成果を。<br />両者にとって価値があるクリエイティブを<br class="hide-for-medium" />創造し続ける。</h3>
			<p class="section--headtext">私たちは単にモノを作るだけでなく、作ったモノがもたらす価値にこだわります。
			<br class="show-for-medium" />既成の概念・制約に捉われることなく、クライアントとユーザーにとって
			<br class="show-for-medium" />真に有益なクリエイティブやサービスを生み出す企業を目指します。</p>
		</div>
	</div>
</section>

<section class="section medium">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title fontHVN">会社紹介</h2>
		</div>
	</div>
	<div class="section--main medium">
		<ul class="row article__bottom-menu">
			<li class="columns medium-4 small-12"><a href="<?php echo get_permalink(get_page_by_path('about')); ?>" class="button button__underline expanded active" title="ビジョン">ビジョン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns medium-4 small-12"><a href="<?php echo get_post_type_archive_link('member'); ?>" class="button button__underline expanded" title="メンバー紹介">メンバー紹介<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns medium-4 small-12"><a href="<?php echo get_permalink(get_page_by_path('about/overview')); ?>" class="button button__underline expanded" title="会社情報">会社情報<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		</ul>
	</div>
</section>


<?php get_footer(); ?>
