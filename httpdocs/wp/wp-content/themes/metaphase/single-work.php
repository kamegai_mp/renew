<?php get_header();
$post_cat = wp_get_object_terms( $post->ID, 'work-category' );
$post_cat = $post_cat[0]->name;

$post_top_image = get_field('work_top_image', $post->ID);
$post_image_desktop = get_field('work_image_desktop', $post->ID);
$post_image_phone = get_field('work_image_phone', $post->ID);
$post_image_desktop_large = get_field('work_image_desktop_large', $post->ID);
$post_image_phone_large = get_field('work_image_phone_large', $post->ID);
$post_top_image = $post_top_image['sizes']['large'];
$post_image_desktop = $post_image_desktop['sizes']['large'];
$post_image_phone = $post_image_phone['sizes']['large'];
$post_image_desktop_large = $post_image_desktop_large['sizes']['large'];
$post_image_phone_large = $post_image_phone_large['sizes']['large'];

$post_client = get_field('work_client_name', $post->ID);
$post_agency = get_field('work_agency_name', $post->ID);
$post_release_date = get_field('work_release_date', $post->ID);
$post_header_title = get_field('work_header_title', $post->ID);
$post_description = get_field('work_description', $post->ID);
$post_process_description = get_field('work_process_description', $post->ID);
$post_website_url = get_field('work_website_url', $post->ID);
?>




<section class="section section__works--detail head">
	<article class="article article__works">
		<figure class="article__works--image" style="background-image: url(<?php echo $post_top_image; ?>);"></figure>
		<div class="article__works--table-box">
			<div class="article__works--text">
				<div class="inner">
					<h2 class="article__works--title"><?php the_title(); ?><br /><span style="font-size:0.7em;"><?php echo $post_cat; ?></span></h2>
					<h3 class="article__works--subtitle">
						<div><strong class="fontBSC bold">CLIENT:</strong><?php echo $post_client; ?></div>
						<div><strong class="fontBSC bold">AGENCY:</strong><?php echo $post_agency; ?></div>
					</h3>
					<p class="article__works--released fontBSC bold">Released:<?php echo $post_release_date; ?></p>
				</div>
			</div>
		</div>
	</article>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><a href="<?php echo get_post_type_archive_link('work'); ?>" title="実績">実績</a></li>
			<li><span class="show-for-sr">Current: </span><?php the_title(); ?></li>
		</ul>
	</nav>
</section>

<section class="section section__posted">
	<article class="section__posted--article article article__posted">
		<div class="article__posted--inner">
			<div class="section--head">
				<h2 class="section--title">プロジェクト概要</h2>
				<h3 class="section--subtitle"><?php echo $post_header_title; ?></h3>
				<p class="section--headtext"><?php echo $post_description; ?></p>
			</div>
		</div>
	</article>

	<article class="section__posted--article article article__posted">
		<div class="article__posted--inner">
			<div class="section--head">
				<h2 class="section--title">プロセス</h2>
			</div>
			<div class="section--main">
				<?php if( have_rows('work_process') ): ?>
					<ul class="article__posted--process-flow row small-up-3 large-up-6 align-center">
			    <?php while ( have_rows('work_process') ) : the_row();
						$process = get_sub_field('work_process_elem');
						$process_name = $process->name;
						$process_image = get_field('process_image', 'work-process-type_'.$process->term_id);
						$process_image_large = get_field('process_image_large', 'work-process-type_'.$process->term_id);
						$process_image = $process_image['sizes']['large'];
						$process_image_large = $process_image_large['sizes']['large'];
					?>
						<li class="column column-block">
							<figure>
								<img src="<?php echo $process_image; ?>" srcset="<?php echo $process_image_large; ?>" alt="<?php echo $process_name; ?>" />
								<figcaption><?php echo $process_name; ?></figcaption>
							</figure>
						</li>
			    <?php endwhile; ?>
					</ul>
				<?php endif; ?>

				<div class="article__posted--text">
					<p class="center">
						<?php echo $post_process_description; ?>
					</p>
				</div>
			</div>
		</div>
	</article>

	<article class="section__posted--article article article__posted">
		<div class="article__posted--inner">
			<div class="section--head">
				<h2 class="section--title">ギャラリー</h2>
			</div>
			<div class="section--main">
				<div class="article__posted--gallery">
					<div class="row">
						<?php if ($post_image_desktop) { ?>
							<div class="pc columns small-12 large-8">
								<div class="img-wrap">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/img_gallery-pc.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/img_gallery-pc@2x.png" alt="<?php the_title(); ?>" />
									<div class="img-capture" style="background-image: url(<?php echo $post_image_desktop; ?>)"></div>
								</div>
							</div>
						<?php } ?>
						<?php if ($post_image_phone) { ?>
							<div class="sp columns small-12 large-4">
								<div class="img-wrap">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/img_gallery-sp.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/img_gallery-sp@2x.png" alt="<?php the_title(); ?>" />
									<div class="img-capture" style="background-image: url(<?php echo $post_image_phone; ?>)"></div>
								</div>
							</div>
						<?php } ?>
					</div>
					<p class="article__posted--link"><a href="<?php echo $post_website_url; ?>" class="link link__text" title="サイトを見る" target="_blank">サイトを見る<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></p>
				</div>


				<?php if( have_rows('work_gallery') ): ?>
					<div class="article__posted--gallery-detail">
						<div class="article__posted--swiper swiper-container">
							<div class="contents swiper-wrapper">
					    <?php while ( have_rows('work_gallery') ) : the_row();
							$gallery_image = get_sub_field('work_gallery_image');
							$gallery_image = $gallery_image['sizes']['large'];
							$gallery_title = get_sub_field('work_gallery_title');
							$gallery_text = get_sub_field('work_gallery_text');
							?>
								<div class="detail swiper-slide">
									<div class="row">
										<div class="columns small-12 medium-6">
											<figure>
												<img src="<?php echo $gallery_image; ?>" alt="<?php echo $gallery_title; ?>" />
											</figure>
										</div>
										<div class="columns small-12 medium-6">
											<h3 class="title"><?php echo $gallery_title; ?></h3>
											<div class="text">
												<?php echo $gallery_text; ?>
											</div>
										</div>
									</div>
								</div>
					    <?php endwhile; ?>
						</div>
					</div>
					<div class="swiper-next-prev">
						<div class="swiper-button-next"></div>
						<div class="swiper-button-prev"></div>
						<div class="pager swiper-pagination"></div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</article>

	<article class="section__posted--article article article__posted">
		<div class="article__posted--inner">
			<div class="section--head">
				<h2 class="section--title">プロジェクトメンバー</h2>
			</div>
			<div class="section--main">
				<?php if( have_rows('work_members_list') ): ?>
					<ul class="article__posted--member row small-up-2 large-up-4">
			    <?php while ( have_rows('work_members_list') ) : the_row();
						$member = get_sub_field('work_member');
						$member_name = $member->post_title;
						$member_job_title = get_field('member_job_title', $member->ID);
						$member_image = get_field('member_photo', $member->ID);
						$member_image = $member_image['sizes']['large'];
						$member_totem = get_field('member_totem', $member->ID);
						$member_totem = $member_totem['sizes']['large'];
					?>
						<li class="column column-block">
							<div class="card__member">
								<div class="card__member--image" style="background-image: url(<?php echo $member_image; ?>)">
									<i class="card__member--icon"><img src="<?php echo $member_totem; ?>" alt="ヤモリ" /></i>
								</div>
								<div class="card__member--text">
									<h3 class="position"><?php echo $member_name; ?></h3>
									<p class="name"><?php echo $member_job_title; ?></p>
								</div>
							</div>
						</li>
			    <?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
	</article>

	<article class="section__posted--article article article__posted">
		<div class="section--head">
			<h2 class="section--title">その他の実績</h2>
		</div>
		<div class="section--main">
			<?php $custom_args = array(
			  'post_type' => 'work',
			  'post_status' => 'publish',
			  'orderby' => 'date',
			  'has_password' => false,
				'posts_per_page' => 3,
			);
			$lastest_list = get_posts( $custom_args ); ?>

			<ul class="section__works--list row small-up-1 medium-up-3">
				<?php foreach ($lastest_list as $post) {
					setup_postdata( $post );
					$post_cat = wp_get_object_terms( $post->ID, 'work-category' );
	        $post_cat = $post_cat[0]->name;
	        $post_image = get_field('work_top_image', $post->ID);
	        $post_image = $post_image['sizes']['large'];
	        $post_client = get_field('work_client_name', $post->ID);
					?>
						<li class="column"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<article class="article article__works">
								<figure class="article__works--image" style="background-image: url(<?php echo $post_image; ?>);"></figure>
								<div class="article__works--text">
									<h5 class="article__works--subtitle"><?php echo $post_client; ?><br /><small><?php echo $post_cat; ?></small></h5>
									<h4 class="article__works--title"><?php the_title(); ?></h4>
								</div>
							</article>
						</a></li>
				<?php wp_reset_postdata(); } ?>
			</ul>

			<div class="section__link large">
				<a href="<?php echo get_post_type_archive_link('work'); ?>" class="link link__text" title="もっと見る">もっと見る<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a>
			</div>
		</div>

	</article>
</section>

<?php get_footer(); ?>
