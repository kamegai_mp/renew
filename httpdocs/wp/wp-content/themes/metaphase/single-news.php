<?php get_header();
$post_tag = wp_get_object_terms( $post->ID, 'news-tag' );
$post_tag_id = $post_tag[0]->term_id;
$post_tag = $post_tag[0]->name;
$post_date = get_the_date('Y.m.d', $post->ID );
$post_top_image = get_field('news_top_image');
$post_image = get_field('news_content_image');
$post_top_image = $post_top_image['sizes']['large'];
$post_image = $post_image['sizes']['large'];
?>


<section class="section section__news--detail_head head">
	<article class="article article__news--detail">
		<figure class="article__news--image" style="background-image: url(<?php echo $post_top_image; ?>);"></figure>
		<div class="article__news--table-box">
			<div class="article__news--text">
				<div class="inner">
					<h2 class="title"><?php echo get_the_title(); ?></h2>
					<p class="news_category"><?php echo $post_tag; ?></p>
					<p class="date"><?php echo $post_date; ?></p>
				</div>
			</div>
		</div>
	</article>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><a href="<?php echo get_post_type_archive_link('news'); ?>" title="最新情報">最新情報</a></li>
			<li><span class="show-for-sr">Current: </span><?php echo get_the_title(); ?></li>
		</ul>
	</nav>
</section>

<section class="section section__news--detail">
	<article class="article__share_sns row align-right">
		<p class="columns small-8 medium-3 share">この記事をシェアする</p>
		<div class="columns small-4 medium-2 sns">
			<ul class="menu">
				<li><a class="share_fb" data-href="<?php echo get_the_permalink(); ?>" data-caption="<?php echo get_the_title(); ?>" title="Facebook"><i><svg class="icon facebook"><use xlink:href="#icon-facebook"></use></svg></i></a></li>
				<li>
					<?php $twitterText = get_the_title().' - '.get_bloginfo('description'); ?>
					<a title="Twitter" href='https://twitter.com/intent/tweet?text=<?php echo $twitterText; ?> <?php echo wp_get_shortlink(); ?>'>
						<i><svg class="icon twitter"><use xlink:href="#icon-twitter"></use></svg></i>
					</a>
				</li>
			</ul>
		</div>
	</article>

	<article class="article__news">
		<div class="row columns article__edition">
      <?php if ( have_posts() ) { while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
      <?php endwhile; } ?>
			<figure class="article__content--content_img" style="background-image: url(<?php echo $post_image; ?>);"></figure>
		</div>
	</article>

	<article class="article__share_sns row align-right">
		<p class="columns small-8 medium-3 share">この記事をシェアする</p>
		<div class="columns small-4 medium-2 sns">
			<ul class="menu">
				<li><a class="share_fb" data-href="<?php echo get_the_permalink(); ?>" data-caption="<?php echo get_the_title(); ?>" title="Facebook"><i><svg class="icon facebook"><use xlink:href="#icon-facebook"></use></svg></i></a></li>
				<li>
					<a title="Twitter" href='https://twitter.com/intent/tweet?text=<?php echo $twitterText; ?> <?php echo wp_get_shortlink(); ?>'>
						<i><svg class="icon twitter"><use xlink:href="#icon-twitter"></use></svg></i>
					</a>
				</li>
			</ul>
		</div>
	</article>

	<div class="section__link medium">
		<a href="<?php echo get_post_type_archive_link('news'); ?>" class="link link__text" title="一覧へもどる">一覧へもどる<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a>
	</div>

</section>

<?php get_footer(); ?>
