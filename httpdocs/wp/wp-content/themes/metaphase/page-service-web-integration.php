<?php
/* Template Name: Service Web integration */
get_header(); ?>


<section class="section section__sv bg">
	<h2 class="section__sv--title">エクスペリエンスデザイン</h2>

	<ul class="section__sv--menu menu align-center vertical large-horizontal show-for-medium">
		<li><a href="<?php echo get_permalink(get_page_by_path('service')); ?>" class="link link__text" title="サービスコンセプト">サービスコンセプト<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('service/experiencedesign')); ?>" class="link link__text active" title="エクスペリエンスデザイン">エクスペリエンスデザイン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('service/retention')); ?>" class="link link__text" title="リテンション">リテンション<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('service/businessdevelopment')); ?>" class="link link__text" title="プロダクトサービス">プロダクトサービス<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
	</ul>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><a href="<?php echo get_permalink(get_page_by_path('service')); ?>" title="サービス">サービス</a></li>
			<li><span class="show-for-sr">Current: </span>エクスペリエンスデザイン</li>
		</ul>
	</nav>
</section>

<section class="section small">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title only-small-noborder">サービスメニュー</h2>
		</div>
	</div>
	<div class="section--main">
		<article class="article article__service-menu">
			<ul class="article__service-menu--list row small-up-1 medium-up-3" data-equalizer>
				<li class="column column-block">
					<dl class="align-middle article__service-menu--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-01.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-01@2x.png" alt="" />
								<figcaption>ユーザーリサーチ</figcaption>
							</figure>
						</dt>
						<dd>ユーザー行動調査やアクセス解析によってターゲットユーザーのペルソナや抱えている課題を考察します。</dd>
					</dl>
				</li>
				<li class="column column-block">
					<dl class="align-middle article__service-menu--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-02.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-02@2x.png" alt="" />
								<figcaption>KPI設計</figcaption>
							</figure>
						</dt>
						<dd>各種マーケティング施策において最大の費用対効果を実現するために、定量測定可能な評価指標とアクションプランを設計します。</dd>
					</dl>
				</li>
				<li class="column column-block">
					<dl class="align-middle article__service-menu--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-03.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-03@2x.png" alt="" />
								<figcaption>戦略立案・策定</figcaption>
							</figure>
						</dt>
						<dd>クライアントビジネスを論理的に分解し、本質的な課題や道筋を抽出することでWebを基点とした具体的戦略をアウトプットします。</dd>
					</dl>
				</li>
				<li class="column column-block">
					<dl class="align-middle article__service-menu--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-04.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-04@2x.png" alt="" />
								<figcaption>ビジュアルデザイン</figcaption>
							</figure>
						</dt>
						<dd>広告クリエイティブからUIなど幅広い領域において、ユーザー、ビジネスの双方にとって最適なインターフェイスをデザインします。</dd>
					</dl>
				</li>
				<li class="column column-block">
					<dl class="align-middle article__service-menu--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-05.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-05@2x.png" alt="" />
								<figcaption>開発</figcaption>
							</figure>
						</dt>
						<dd>ユーザーにとって快適な操作体験を実現したフロントエンドやWebアプリケーションを開発します。</dd>
					</dl>
				</li>
				<li class="column column-block">
					<dl class="align-middle article__service-menu--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-06.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-06@2x.png" alt="" />
								<figcaption>改善・運用</figcaption>
							</figure>
						</dt>
						<dd>KPI指標を基に実証データによる仮説・検証サイクルを運用、継続的な効果改善を通じ、クライアントの事業成長を支援します。</dd>
					</dl>
				</li>
			</ul>
		</article>
	</div>
</section>

<section class="section small bg__highlight">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">サービスフロー</h2>
			<h3 class="section--subtitle">スクラップ＆ビルドを繰り返す工程で<br class="show-for-only-small" />品質を高めます。</h3>
		</div>
	</div>
	<div class="section--main medium">
		<article class="article article__service-flow">
			<ul class="article__service-flow--list row small-up-1 medium-up-4" data-equalizer>
				<li class="column column-block">
					<dl class="align-middle article__service-flow--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-01.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-01@2x.png" alt="" />
								<figcaption>プロジェクト要件定義</figcaption>
							</figure>
						</dt>
						<dd>
							<ul class="list">
								<li class="list--icon-circle"><i></i>プロジェクトゴールの設定</li>
								<li class="list--icon-circle"><i></i>KGI設計</li>
								<li class="list--icon-circle"><i></i>プロジェクトスコープの定義</li>
								<li class="list--icon-circle"><i></i>制作/開発仕様の定義</li>
                                <li class="list--icon-circle"><i></i>スケジュール策定</li>
                                <li class="list--icon-circle"><i></i>プロジェクトルールの定義</li>
                                <li class="list--icon-circle"><i></i>プロジェクト体制の構築</li>
							</ul>
						</dd>
					</dl>
				</li>
                <li class="column column-block">
					<dl class="align-middle article__service-flow--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-01.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-01@2x.png" alt="" />
								<figcaption>戦略</figcaption>
							</figure>
						</dt>
						<dd>
							<ul class="list">
								<li class="list--icon-circle"><i></i>ビジネスロードマップの整理</li>
                                <li class="list--icon-circle"><i></i>マーケットリサーチ</li>
                                <li class="list--icon-circle"><i></i>データ分析</li>
                                <li class="list--icon-circle"><i></i>ターゲット定義</li>
                                <li class="list--icon-circle"><i></i>課題抽出</li>
                                <li class="list--icon-circle"><i></i>マーケティング戦略立案・策定</li>
                                <li class="list--icon-circle"><i></i>KPI設計</li>
							</ul>
						</dd>
					</dl>
				</li>
				<li class="column column-block">
					<dl class="align-middle article__service-flow--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-02.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-02@2x.png" alt="" />
								<figcaption>設計</figcaption>
							</figure>
						</dt>
						<dd>
							<ul class="list">
								<li class="list--icon-circle"><i></i>ペルソナ設計</li>
                                <li class="list--icon-circle"><i></i>コミュニケーションシナリオ策定</li>
                                <li class="list--icon-circle"><i></i>構造・導線設計</li>
                                <li class="list--icon-circle"><i></i>画面設計</li>
                                <li class="list--icon-circle"><i></i>コンテンツプランニング</li>
                                <li class="list--icon-circle"><i></i>ビジュアルコンセプト策定</li>
                                <li class="list--icon-circle"><i></i>マークアップ設計</li>
							</ul>
						</dd>
					</dl>
				</li>
				<li class="column column-block">
					<dl class="align-middle article__service-flow--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-03.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-03@2x.png" alt="" />
								<figcaption>開発</figcaption>
							</figure>
						</dt>
						<dd>
							<ul class="list">
								<li class="list--icon-circle"><i></i>プロトタイプ検証</li>
                                <li class="list--icon-circle"><i></i>ビジュアルデザイン</li>
                                <li class="list--icon-circle"><i></i>CSS/HTMLコーディング</li>
                                <li class="list--icon-circle"><i></i>システム設計</li>
                                <li class="list--icon-circle"><i></i>プログラム実装（Javascript実装/CMS実装）</li>
                                <li class="list--icon-circle"><i></i>テスト検証・公開作業</li>
                                <li class="list--icon-circle"><i></i>ガイドライン作成</li>
							</ul>
						</dd>
					</dl>
				</li>
			</ul>
		</article>
	</div>
</section>

<section class="section small">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">マネジャー紹介</h2>
		</div>
	</div>
	<div class="section--main medium">
		<article class="article article__member-voice row">
			<div class="article__member-voice--head">
				<div class="image" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/sample/img_member-voice-01.png);"></div>
				<div class="inner">
					<h3>綺麗に整っているだけじゃなく、クリエイティブがもたらす効果や体験にまでこだわる。</h3>
					<p class="text-1">マネージャー</p>
					<p class="text-2">塩沢 彰吾</p>
				</div>
			</div>
			<ul class="article__member-voice--list">
				<li>
					<dl class="article__member-voice--dl">
						<dt><i class="icon icon__q">Q</i>エクスペリエンスデザインとは一言でどんなサービスか？</dt>
						<dd><i class="icon icon__a">A</i>一言で言うと、ユーザーの体験を計画し、計画された体験を実現できるようなクリエイティブを提供するサービスです。私たちはこれまでもWebサイトやWebシステムのデザインを手がけてきましたが、エクスペリエンスデザインは単にモノを作るだけでなく、それを使う人の体験まで考えて、その結果としてクライアントのビジネスに貢献することを目指して運営しています。</dd>
					</dl>
				</li>
				<li>
					<dl class="article__member-voice--dl">
						<dt><i class="icon icon__q">Q</i>何を得意としているのか？</dt>
						<dd><i class="icon icon__a">A</i>エクスペリエンスデザインと聞くとユーザー調査やカスタマージャーニーマップの作成など調査・分析や、プランニングの仕事を想像される方も多いと思いますが、得意分野ということで言えばやはり「制作」です。ただ、私たちは制作だけでなく戦略のプランニングやUXの検討からお手伝いさせて頂きますので、プランニングしたものを正しく具現化することが強みと言えると考えています。</dd>
					</dl>
				</li>
				<li>
					<dl class="article__member-voice--dl">
						<dt><i class="icon icon__q">Q</i>今後どこに向かっていくのか？</dt>
						<dd><i class="icon icon__a">A</i>昨今、デザインへの期待は今までよりも高まってきています。プロダクトアウトな視点ではなく、顧客体験をいかに良くしていくかが重要になってきているからだと思います。そんな流れの中で、私たちも自分たちが行うデザインで、ユーザーにとって良いものであることはもちろん、クライアントのビジネスを加速させられるようなパートナーになっていきたいと思っています。</dd>
					</dl>
				</li>
			</ul>
		</article>
	</div>
</section>

<section class="section small bg__highlight section__contact">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">お問い合わせ</h2>
			<h3 class="section--subtitle">まずはお気軽にご相談ください</h3>
		</div>
	</div>
	<div class="section--main small">
		<article class="article__contact">
			<div class="article__contact--button">
				<a href="<?php echo get_permalink(get_page_by_path('contact')); ?>" class="expanded button" title="お問い合わせはこちら">お問い合わせはこちら</a>
			</div>
			<p class="article__contact--link"><a href="<?php echo get_permalink(get_page_by_path('contact/partner')); ?>" class="link link__text" title="採用企業やパートナー様はこちら">採用企業やパートナー様はこちら<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></p>
		</article>
	</div>
</section>

<section class="section medium">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">サービス</h2>
		</div>
	</div>
	<div class="section--main medium">
		<ul class="row article__bottom-menu">
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service')); ?>" class="button button__underline expanded" title="サービスコンセプト">サービスコンセプト<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service/experiencedesign')); ?>" class="button button__underline expanded active" title="エクスペリエンスデザイン">エクスペリエンスデザイン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service/retention')); ?>" class="button button__underline expanded" title="リテンション">リテンション<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service/businessdevelopment')); ?>" class="button button__underline expanded" title="プロダクトサービス">プロダクトサービス<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		</ul>
	</div>
</section>


<?php get_footer(); ?>
