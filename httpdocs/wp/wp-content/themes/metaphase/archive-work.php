<?php get_header();
global $query_string;

$category_args = array(
	'taxonomy' => 'work-category'
);
$category_list = get_terms($category_args);

$custom_args = array(
  'post_type' => 'work',
  'post_status' => 'publish',
  'orderby' => 'date',
  'has_password' => false,
	'posts_per_page' => 9,
  'paged' => $paged,
);
if (isset($_POST['work_category']) && $_POST['work_category'] != '') {
	$work_category_filter = $_POST['work_category'];

  $tax_args = array(
    'tax_query' => array(
      array(
        'taxonomy' => 'work-category',
        'field'    => 'slug',
        'terms'    => $work_category_filter,
      ),
    ),
  );
  $custom_args = array_merge($custom_args, $tax_args);
}

$myworks = new WP_Query( $custom_args );
?>



<section class="section section__sv bg">
	<h2 class="section__sv--title">実績</h2>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><span class="show-for-sr">Current: </span>実績</li>
		</ul>
	</nav>
</section>

<section class="section section__works">
	<div class="section__works--search">
		<form action="<?php echo home_url(); ?>/work" method="post">
			<article class="article article__search select">
				<label for="work_category" class="article__search--title">サイトの種類</label>
				<div class="article__search--select">
					<select id="work_category" name="work_category" class="custom-select">
            <option value=''>すべて</option>
            <?php foreach ($category_list as $category) {
              if ($work_category_filter == $category->slug) {
                $selected = "selected";
              } else {
                $selected = '';
              }
              ?>
              <option value="<?php echo $category->slug; ?>" <?php echo $selected; ?>><?php echo $category->name; ?></option>
            <?php } ?>
					</select>
				</div>
				<br class="hide-for-medium">
				<div class="article__search--button">
					<button type="submit" class="button button__border">絞り込む</button>
				</div>
			</article>
		</form>
	</div>

	<div class="section--main section__works--main">
    <?php if($myworks->have_posts()) : ?>
			<ul class="section__works--list row small-up-1 medium-up-3">
			<?php while($myworks->have_posts()) : $myworks->the_post();
      $post_cat = wp_get_object_terms( $post->ID, 'work-category' );
      $post_cat = $post_cat[0]->name;
      $post_image = get_field('work_top_image', $post->ID);
			$post_image = $post_image['sizes']['large'];
      $post_client = get_field('work_client_name', $post->ID);
			?>
        <li class="column"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
          <article class="article article__works">
            <figure class="article__works--image" style="background-image: url(<?php echo $post_image; ?>);"></figure>
            <div class="article__works--text">
              <h5 class="article__works--subtitle"><?php echo $post_client; ?><br /><small><?php echo $post_cat; ?></small></h5>
              <h4 class="article__works--title"><?php the_title(); ?></h4>
            </div>
          </article>
        </a></li>
			<?php endwhile; ?>
			</ul>
		<?php endif; ?>

  </div>

	<div class="section__works--pagenation text-center fontHVN">
    <?php wp_pagenavi(array( 'query' => $myworks)); ?>
	</div>
</section>




<?php get_footer(); ?>
