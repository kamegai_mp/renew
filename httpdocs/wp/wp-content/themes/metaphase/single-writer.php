<?php get_header();
$queried_object = get_queried_object();

// $custom_args = array(
//   'post_type' => 'blog',
//   'post_status' => 'publish',
//   'orderby' => 'date',
//   'has_password' => false,
// 	'posts_per_page' => 2,
// );
// $myblogs = new WP_Query( $custom_args );
?>


<section class="section section__sv bg">
	<h2 class="section__sv--title">ブログ</h2>
</section>


<section class="section section__2columns">
	<div class="row" data-equalizer>
		<div class="columns small-12 medium-9" data-equalizer-watch>

			<section class="section section__breadcrumb">
				<nav class="nav__breadcrumb row">
					<ul class="breadcrumbs columns">
						<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
						<li><span class="show-for-sr">Current: </span>ブログ</li>
					</ul>
				</nav>
			</section>

			<section class="section section__blog small">
				<div class="section--main blog_loader">
          <?php echo do_shortcode(' [ajax_load_more container_type="ul" css_classes="row small-up-1 medium-up-2 large-up-3 section__blog--list" post_type="blog" posts_per_page="9" meta_key="blog_writer" meta_value="'.$queried_object->ID.'" meta_compare="IN" transition_container="false" button_label="もっと見る"] '); ?>
        </div>
      </section>
		</div>


		<div class="columns small-12 medium-3">
			<?php get_template_part( 'includes/blog-aside' ); ?>
		</div>
	</div>
</section>


<?php get_footer(); ?>
