<?php
/* Template Name: Service Products */
get_header(); ?>


<section class="section section__sv bg">
	<h2 class="section__sv--title">プロダクトサービス</h2>

	<ul class="section__sv--menu menu align-center vertical large-horizontal show-for-medium">
		<li><a href="<?php echo get_permalink(get_page_by_path('service')); ?>" class="link link__text" title="サービスコンセプト">サービスコンセプト<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('service/experiencedesign')); ?>" class="link link__text" title="エクスペリエンスデザイン">エクスペリエンスデザイン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('service/retention')); ?>" class="link link__text" title="リテンション">リテンション<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('service/businessdevelopment')); ?>" class="link link__text active" title="プロダクトサービス">プロダクトサービス<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
	</ul>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><a href="<?php echo get_permalink(get_page_by_path('service')); ?>" title="サービス">サービス</a></li>
			<li><span class="show-for-sr">Current: </span>プロダクトサービス</li>
		</ul>
	</nav>
</section>

<section class="section small">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">新しいチャレンジ</h2>
            <div class="section--main small">
            <p class="section--headtext">メタフェイズでは自社プロダクトの開発、新規事業の立ち上げへのチャレンジしています。<br class="show-for-medium">
クライアント企業に向けた受託制作事業を柱としながら、一方で新たな事業モデルを持つことにより、受託制作で得た<br class="show-for-medium">
スキル・技術を新規事業に活用し、新規事業で得たノウハウを受託制作に還元していく、<br class="show-for-medium">
相互に質の高いサービスを提供していくことを目指しています。</p>
<p class="article__contact--link"><a href="/products/" class="link link__text" title="プロダクトを見る">プロダクトを見る<svg class="icon arrow_03"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow_03"></use></svg></a></p>
            </div><!--/.section--main medium-->
            
		</div>
	</div>
</section>

<section class="section small">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">マネジャー紹介</h2>
		</div>
	</div>
	<div class="section--main medium">
		<article class="article article__member-voice row">
			<div class="article__member-voice--head">
				<div class="image" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/sample/img_member-voice-03.png);"></div>
				<div class="inner">
					<h3>制作会社だからこそできる<br class="show-for-medium">プロダクト開発に<br class="show-for-medium">チャレンジする</h3>
					<p class="text-1">取締役</p>
					<p class="text-2">折本 裕司</p>
				</div>
			</div>
			<ul class="article__member-voice--list">
				<li>
					<dl class="article__member-voice--dl">
						<dt><i class="icon icon__q">Q</i>どうしてメタフェイズがプロダクトを開発するのか？</dt>
						<dd><i class="icon icon__a">A</i>まず事業モデルを複数持つことで、底堅い経営基盤をつくるという目的があります。<br>
デジタルマーケティングの領域は技術トレンドの変化も早く、外部環境の影響を受けやすいため、持続的な成長を支えるためにも新しいチャレンジは必要不可欠だと考えています。<br>
また将来的には受託制作事業とのシナジーも期待しています。自社事業のPDCAに関するノウハウを受託制作にも還元していくことで、クライアントのビジネス成果に深くコミットできる受託制作を展開できるとも考えています。</dd>
					</dl>
				</li>
				<li>
					<dl class="article__member-voice--dl">
						<dt><i class="icon icon__q">Q</i>どんな体制で開発を進めるのか？</dt>
						<dd><i class="icon icon__a">A</i>2015年に事業開発室という「事業の立ち上げ、インキュベーションを専任で行うチーム」を設けました。社内から上がった事業アイデアを具現化し、事業立ち上げ初期の運用まで行うのが、この事業開発室のミッションです。<br>
事業採算が合う目処がつけば、新しい事業部を設立し、参画してくれるメンバーを社内から募る流れとなります。<br>
現在、事業開発室に所属するメンバーは通常の受託制作は行わず、新しいプロダクトの開発を専任する体制になっています。<br>
デザイナーにマークアップエンジニア、サーバーサイドエンジニアが所属しており、そのメンバーで全ての制作開発を進めています。<br>
少人数のチームで開発を進めますので、何度も意見交換をして、仮説をスピーディにモックアップにおこし、すぐに再検討を行うサイクルを何度も繰り返しながら開発を進めています。</dd>
					</dl>
				</li>
				<li>
					<dl class="article__member-voice--dl">
						<dt><i class="icon icon__q">Q</i>どういうプロダクトを開発していくのか？</dt>
						<dd><i class="icon icon__a">A</i>明確な定義はありませんが、制作会社として得た経験を活かしてプロダクトの開発していきたいと考えています。<br>
その切り口は様々あって、技術的なトレンドから発想しても良いですし、制作業界の課題を解決するという視点から考えても良いのですが、制作会社であること十分に生かしていきたいと思っています。</dd>
					</dl>
				</li>
			</ul>
		</article>
	</div>
</section>

<section class="section small bg__highlight section__contact">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">お問い合わせ</h2>
			<h3 class="section--subtitle">まずはお気軽にご相談ください</h3>
		</div>
	</div>
	<div class="section--main small">
		<article class="article__contact">
			<div class="article__contact--button">
				<a href="<?php echo get_permalink(get_page_by_path('contact')); ?>contact" class="expanded button" title="お問い合わせはこちら">お問い合わせはこちら</a>
			</div>
			<p class="article__contact--link"><a href="<?php echo get_permalink(get_page_by_path('contact/partner')); ?>" class="link link__text" title="採用企業やパートナー様はこちら">採用企業やパートナー様はこちら<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></p>
		</article>
	</div>
</section>

<section class="section medium">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">サービス</h2>
		</div>
	</div>
	<div class="section--main medium">
		<ul class="row article__bottom-menu">
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service')); ?>" class="button button__underline expanded" title="サービスコンセプト">サービスコンセプト<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service/experiencedesign')); ?>" class="button button__underline expanded" title="エクスペリエンスデザイン">エクスペリエンスデザイン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service/retention')); ?>" class="button button__underline expanded" title="リテンション">リテンション<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service/businessdevelopment')); ?>" class="button button__underline expanded active" title="プロダクトサービス">プロダクトサービス<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		</ul>
	</div>
</section>



<?php get_footer(); ?>
