<?php
/* Template Name: Service Web retention */
get_header(); ?>

<section class="section section__sv bg">
	<h2 class="section__sv--title">リテンション</h2>

	<ul class="section__sv--menu menu align-center vertical large-horizontal show-for-medium">
		<li><a href="<?php echo get_permalink(get_page_by_path('service')); ?>" class="link link__text" title="サービスコンセプト">サービスコンセプト<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('service/experiencedesign')); ?>" class="link link__text" title="エクスペリエンスデザイン">エクスペリエンスデザイン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('service/retention')); ?>" class="link link__text active" title="リテンション">リテンション<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('service/businessdevelopment')); ?>" class="link link__text" title="プロダクトサービス">プロダクトサービス<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
	</ul>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>">TOP</a></li>
			<li><a href="<?php echo get_permalink(get_page_by_path('service')); ?>">サービス</a></li>
			<li><span class="show-for-sr">Current: </span>リテンション</li>
		</ul>
	</nav>
</section>

<section class="section small">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">サービスメニュー</h2>
		</div>
	</div>
	<div class="section--main medium">
		<article class="article article__service-menu">
			<ul class="article__service-menu--list row small-up-1 medium-up-3" data-equalizer>
				<li class="column column-block">
					<dl class="align-middle article__service-menu--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-01.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-01@2x.png" alt="" />
								<figcaption>Webサイト保守・運用</figcaption>
							</figure>
						</dt>
						<dd>多数のWebサイトの構築・運用経験を活かした実践的な運用プロセス・スキルとノウハウを持つことで、単純なサイトメンテナンスではない「サイト醸成」を目的にした運用サービスを提供しています。</dd>
					</dl>
				</li>
				<li class="column column-block">
					<dl class="align-middle article__service-menu--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-07.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_servicemenu-07@2x.png" alt="" />
								<figcaption>Webマスター支援</figcaption>
							</figure>
						</dt>
						<dd>大型サイトの構築・運用経験に基づき、Web制作以外の面からもWebマスターのサポートを行います。各種施策の検討から、ガイドライン策定・品質管理まで幅広く対応しています。</dd>
					</dl>
				</li>
			</ul>
		</article>
	</div>
</section>

<section class="section small bg__highlight">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">サービスフロー</h2>
			<h3 class="section--subtitle">スクラップ＆ビルドを繰り返す設計工程で品質を高めます。</h3>
		</div>
	</div>
	<div class="section--main medium">
		<article class="article article__service-flow">
			<ul class="article__service-flow--list row small-up-1 medium-up-3" data-equalizer>
				<li class="column column-block">
					<dl class="align-middle article__service-flow--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-01.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-01@2x.png" alt="" />
								<figcaption>運用要件定義</figcaption>
							</figure>
						</dt>
						<dd>
							<ul class="list">
								<li class="list--icon-circle"><i></i>現状の運用体制、Webサイト管理・更新にまつわる機能面などを確認します。</li>
								<li class="list--icon-circle"><i></i>現状抱えている運用上の課題に対し、改善案を策定します。</li>
								<li class="list--icon-circle"><i></i>プロモーション計画やシーズナリーイベントなどの計画を確認し、年間の稼働予測を立てます。</li>
								<li class="list--icon-circle"><i></i>運用引き継ぎのための会議体や進行方法をまとめ、運用定義を行います。</li>
							</ul>
						</dd>
					</dl>
				</li>
				<li class="column column-block">
					<dl class="align-middle article__service-flow--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-02.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-02@2x.png" alt="" />
								<figcaption>試験運用</figcaption>
							</figure>
						</dt>
						<dd>
							<ul class="list">
								<li class="list--icon-circle"><i></i>3〜4ヵ月の間を試験運用期間として、当初の計画に基づいた業務及び施策を実行します。</li>
								<li class="list--icon-circle"><i></i>試験運用中に作業フローの見直しなども行い、本稼働に向けた体制づくりを行います。</li>
								<li class="list--icon-circle"><i></i>場合により、運用チーム向けの用語集やルール集を策定し、効率化を行います。</li>
							</ul>
						</dd>
					</dl>
				</li>
				<li class="column column-block">
					<dl class="align-middle article__service-flow--dl" data-equalizer-watch>
						<dt>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-03.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/service/img_serviceflow-03@2x.png" alt="" />
								<figcaption>本運用</figcaption>
							</figure>
						</dt>
						<dd>
							<ul class="list">
								<li class="list--icon-circle"><i></i>試験運用を経て、本格運用に入ります。</li>
								<li class="list--icon-circle"><i></i>月次の稼働報告の他、取り決めに応じたレポーティングを実施します。</li>
								<li class="list--icon-circle"><i></i>必要に応じ、制作以外の部分での業務協力なども行います。業務内容の変更・追加等のご相談はいつでも受け付けております。</li>
							</ul>
						</dd>
					</dl>
				</li>
			</ul>
		</article>
	</div>
</section>

<section class="section small">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">マネジャー紹介</h2>
		</div>
	</div>
	<div class="section--main medium">
		<article class="article article__member-voice row">
			<div class="article__member-voice--head">
				<div class="image" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/sample/img_member-voice-02.png);"></div>
				<div class="inner">
					<h3>クライアントだけでなくお客様に寄り添うサービスでありたい</h3>
					<p class="text-1">マネージャー/シニアディレクター</p>
					<p class="text-2">佐久間 絢</p>
				</div>
			</div>
			<ul class="article__member-voice--list">
				<li>
					<dl class="article__member-voice--dl">
						<dt><i class="icon icon__q">Q</i>リテンション事業部の強みは何か？</dt>
						<dd><i class="icon icon__a">A</i>私たちはクライアントのWeb運用をお手伝いさせて頂いています。そういった形態もあり長期的かつ深いお付き合いをさせて頂いているクライアントがほとんどです。なので強みとすればそこで培った経験です。事業やクライアント組織の背景を正しく理解し、最適なサービス・サポートを提供できます。</dd>
					</dl>
				</li>
				<li>
					<dl class="article__member-voice--dl">
						<dt><i class="icon icon__q">Q</i>どんなクライアントに適しているのか？</dt>
						<dd><i class="icon icon__a">A</i>制作の手が足りないクライアントはもちろんのこと、Webマスターの手が足りないなど、Webの取り組みがうまく進んでいない、活用できていないクライアント様にこそ、価値のあるサービスを提供できると考えています。</dd>
					</dl>
				</li>
				<li>
					<dl class="article__member-voice--dl">
						<dt><i class="icon icon__q">Q</i>今後どこに向かっていくのか？</dt>
						<dd><i class="icon icon__a">A</i>やはり、クライアントと一緒に同じ目標を追いかけ続ける組織になっていきたいですね。制作パートナーとしてではなく同じチームとして、同じ目標に取り組んでいきたいです。そして、その中でもユーザー（クライアントのお客様）にもちゃんと寄り添っていきたいです。第三者の立場であるからこそ、お客様の事だけを考える事も出来るので、その視点を持ち続けることは私たちのサービスとして大切なことだと考えています。</dd>
					</dl>
				</li>
			</ul>
		</article>
	</div>
</section>

<section class="section small bg__highlight section__contact">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title fontHVN">お問い合わせ</h2>
			<h3 class="section--subtitle">まずはお気軽にご相談ください</h3>
		</div>
	</div>
	<div class="section--main small">
		<article class="article__contact">
			<div class="article__contact--button">
				<a href="<?php echo get_permalink(get_page_by_path('contact')); ?>" class="expanded button" title="お問い合わせはこちら">お問い合わせはこちら</a>
			</div>
			<p class="article__contact--link"><a href="<?php echo get_permalink(get_page_by_path('contact/partner')); ?>" class="link link__text" title="採用企業やパートナー様はこちら">採用企業やパートナー様はこちら<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></p>
		</article>
	</div>
</section>

<section class="section medium">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">サービス</h2>
		</div>
	</div>
	<div class="section--main medium">
		<ul class="row article__bottom-menu">
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service')); ?>" class="button button__underline expanded" title="サービスコンセプト">サービスコンセプト<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service/experiencedesign')); ?>" class="button button__underline expanded" title="エクスペリエンスデザイン">エクスペリエンスデザイン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service/retention')); ?>" class="button button__underline expanded active" title="リテンション">リテンション<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns large-3 medium-6 small-12"><a href="<?php echo get_permalink(get_page_by_path('service/businessdevelopment')); ?>" class="button button__underline expanded" title="プロダクトサービス">プロダクトサービス<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		</ul>
	</div>
</section>

<?php get_footer(); ?>
