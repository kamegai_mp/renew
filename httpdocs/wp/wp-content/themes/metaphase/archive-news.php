<?php get_header();

if( isset($_GET['y']) ) {
  $year = intval(htmlspecialchars($_GET['y']));
} else {
  $year = date("Y");
}
?>


<section class="section section__sv bg">
	<h2 class="section__sv--title">最新情報</h2>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><span class="show-for-sr">Current: </span>最新情報</li>
		</ul>
	</nav>
</section>

<section class="section section__news">
	<div class="section__news--years">
		<div class="section--main row align-center">
			<div class="columns shrink">
				<ul class="section__news--years_list menu">
					<?php wp_custom_archive( 'news', $year );  ?>
				</ul>
			</div>
		</div>
	</div>

	<div class="section row section__news--main">

    <?php echo do_shortcode(' [ajax_load_more container_type="ul" css_classes="section__news--list detail_lists" post_type="news" posts_per_page="2" year="'.$year.'" scroll="false" transition_container="false" button_label="もっと見る"] '); ?>

	</div>
</section>

<section class="section section__news--filter">
	<h3 class="subtitle">期間別で絞り込む</h3>
	<div class="section__news--years_sp row">
		<div class="columns small-12">
			<ul class="section__news--years_list row columns">
				<?php wp_custom_archive( 'news' );  ?>
			</ul>
		</div>
	</div>
</section>

<?php get_footer(); ?>
