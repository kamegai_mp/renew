<?php get_header();
$queried_object = get_queried_object();
?>

<section class="section section__sv bg">
	<h2 class="section__sv--title">ブログ</h2>
	<p class="section__sv--category"><?php echo $queried_object->name; ?></p>
</section>


<section class="section section__2columns">
	<div class="row" >
		<div class="columns small-12 medium-9">

			<section class="section section__breadcrumb">
				<nav class="nav__breadcrumb row">
					<ul class="breadcrumbs columns">
						<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
						<li><a href="<?php echo get_post_type_archive_link('blog'); ?>" title="ブログ">ブログ</a></li>
						<li><span class="show-for-sr">Current: </span><?php echo $queried_object->name; ?></li>
					</ul>
				</nav>
			</section>

			<section class="section section__blog small">
				<div class="section--main blog_loader">
          <?php echo do_shortcode(' [ajax_load_more container_type="ul" css_classes="row small-up-1 medium-up-2 large-up-3 section__blog--list" post_type="blog" posts_per_page="9" taxonomy="blog-label" taxonomy_terms="'.$queried_object->slug.'" taxonomy_operator="IN" transition_container="false" button_label="もっと見る"] '); ?>
        </div>
      </section>
		</div>


		<div class="columns small-12 medium-3 section__2columns--aside">
			<?php get_template_part( 'includes/blog-aside' ); ?>
		</div>

	</div>
</section>


<?php get_footer(); ?>
