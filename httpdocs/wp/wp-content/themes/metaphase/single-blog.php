<?php get_header();
$blog_tag = wp_get_object_terms( get_the_ID(), 'blog-label' );
$blog_tag_id = $blog_tag[0]->term_id;
$blog_tag = $blog_tag[0]->name;
$blog_date = get_the_date('Y.m.d', get_the_ID() );
$blog_introduction = get_field('blog_introduction');
$blog_detail_title = get_field('blog_detail_title');
$blog_top_image = get_field('blog_top_image');
$blog_top_image = $blog_top_image['sizes']['large'];
$blog_writer = get_field('blog_writer');


$custom_args = array(
  'post_type' => 'blog',
  'post_status' => 'publish',
  'orderby' => 'date',
  'has_password' => false,
	'posts_per_page' => 3,
);
$lastest_list = get_posts( $custom_args );
?>



<section class="section section__blog--detail_head head">
	<article class="article article__blog--detail">
		<figure class="article__blog--image_top" style="background-image: url(<?php echo $blog_top_image; ?>);"></figure>
		<div class="article__blog--table-box">
			<div class="article__blog--text_top">
				<div class="inner">
					<h2 class="title"><?php echo $blog_detail_title; ?></h2>
					<p class="news_category"><?php echo $blog_tag; ?></p>
					<p class="date"><?php echo $blog_date; ?></p>
				</div>
			</div>
		</div>
	</article>
</section>


<div class="section section__2columns">
	<div class="row">
		<div class="columns small-12 medium-9">

			<section class="section section__breadcrumb">
				<nav class="nav__breadcrumb row">
					<ul class="breadcrumbs columns">
						<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
						<li><a href="<?php echo get_post_type_archive_link('blog'); ?>" title="ブログ">ブログ</a></li>
						<li><a href="<?php echo get_term_link($blog_tag_id); ?>" title="UX・イベント"><?php echo $blog_tag; ?></a></li>
						<li><span class="show-for-sr">Current: </span><?php the_title(); ?></li>
					</ul>
				</nav>
			</section>

			<section class="section section__blog small">
				<div class="section--main">
					<article class="article__share_sns row align-right">
						<p class="columns small-8 medium-5 share">この記事をシェアする</p>
						<div class="columns small-4 medium-2 sns">
							<ul class="menu">
								<li><a class="share_fb" data-href="<?php echo get_the_permalink(); ?>" data-caption="<?php echo get_the_title(); ?>" title="Facebook"><i><svg class="icon facebook"><use xlink:href="#icon-facebook"></use></svg></i></a></li>
								<li>
                  <?php $twitterText = get_the_title().' - '.get_bloginfo('description'); ?>
                  <a title="Twitter" href='https://twitter.com/intent/tweet?text=<?php echo $twitterText; ?> <?php echo wp_get_shortlink(); ?>'>
                    <i><svg class="icon twitter"><use xlink:href="#icon-twitter"></use></svg></i>
                  </a>
                </li>
							</ul>
						</div>
					</article>

					<article class="article__blog detail">
						<div class="row columns article__edition">
              <?php if ( have_posts() ) { while ( have_posts() ) : the_post(); ?>
    						<?php the_content(); ?>
    					<?php endwhile; } ?>
            </div>
					</article>
				</div>
				<article class="article__share_sns row align-right">
					<p class="columns small-8 medium-5 share">この記事をシェアする</p>
					<div class="columns small-4 medium-2 sns">
						<ul class="menu">
              <li><a class="share_fb" data-href="<?php echo get_the_permalink(); ?>" data-caption="<?php echo get_the_title(); ?>" title="Facebook"><i><svg class="icon facebook"><use xlink:href="#icon-facebook"></use></svg></i></a></li>

              <li>
                <a title="Twitter" href='https://twitter.com/intent/tweet?text=<?php echo $twitterText; ?> <?php echo wp_get_shortlink(); ?>'>
                  <i><svg class="icon twitter"><use xlink:href="#icon-twitter"></use></svg></i>
                </a>
              </li>
						</ul>
					</div>
				</article>
			</section>


			<?php
			$blog_writer_job_title = get_field('writer_job_title', $blog_writer->ID);
			$blog_writer_image = get_field('writer_image', $blog_writer->ID);
			$blog_writer_image = $blog_writer_image['sizes']['large'];
			$blog_writer_name = $blog_writer->post_title;
			$blog_writer_content = $blog_writer->post_content;
      $blog_writer_totem = get_field('writer_totem', $blog_writer->ID);
      $blog_writer_totem = $blog_writer_totem['sizes']['large'];
			?>

			<section class="section section__blog small">
				<article class="row align-center align-middle section__blog--writer">
					<div class="columns small-6 medium-4">
						<div class="card__member--image" style="background-image: url(<?php echo $blog_writer_image; ?>)">
              <i class="card__member--icon"><img src="<?php echo $blog_writer_totem; ?>" alt="ヤモリ" /></i>
						</div>
					</div>
					<div class="columns small-12 medium-8">
						<p class="name_job"><strong><?php echo $blog_writer_name; ?></strong> / <?php echo $blog_writer_job_title; ?></p>
						<p class="profile"><?php echo $blog_writer_content; ?></p>
					</div>
				</article>
			</section>
			<?php wp_reset_postdata(); ?>

			<section class="section section__blog medium">
				<h2 class="section--title">最新のブログ記事</h2>
				<ul class="row small-up-1 medium-up-2 large-up-3 section__blog--list latest">
					<?php foreach ($lastest_list as $post) {
            setup_postdata( $post );
						$post_tag = wp_get_object_terms( get_the_ID(), 'blog-label' );
    				$post_tag = $post_tag[0]->name;
    				$post_date = get_the_date('Y.m.d', get_the_ID() );
    				$post_introduction = get_field('blog_introduction');
            $post_thumbnail = get_field('blog_thumbnail');
            $post_thumbnail = $post_thumbnail['sizes']['large'];
            ?>
						<li class="columns"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<article class="article__blog">
								<div class="article__blog--image">
									<figure style="background-image: url(<?php echo $post_thumbnail; ?>)"></figure>
								</div>
								<div class="article__blog--head row align-middle">
									<p class="article__blog--category columns"><span><?php echo $post_tag; ?></span></p>
									<time class="article__blog--time fontACN columns"><?php echo $post_date; ?></time>
								</div>
								<div class="article__blog--main">
									<h4 class="article__blog--title"><?php the_title(); ?></h4>
									<p class="article__blog--text"><?php echo $post_introduction; ?></p>
								</div>
							</article>
						</a></li>
          <?php wp_reset_postdata(); } ?>
				</ul>
			</section>
		</div>

		<div class="columns small-12 medium-3 section__2columns--aside">
			<?php get_template_part( 'includes/blog-aside' ); ?>
		</div>
	</div>
</div>



<?php get_footer(); ?>
