<?php
/* Template Name: Contact page */
get_header(); ?>

<section class="section section__sv bg">
	<h2 class="section__sv--title">お問い合わせ</h2>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><span class="show-for-sr">Current: </span>お問い合わせ</li>
		</ul>
	</nav>
</section>

<section class="section section__contact">
	<div class="section--head">
		<h2 class="section--title">お気軽にご相談ください</h2>
	</div>
	<div class="section--main">
		<article class="article article__contact expanded">
			<ul class="article__contact--menu small-up-1 medium-up-1 large-up-3 row" data-equalizer>
				<li class="columns">
					<dl class="align-middle card card__border" data-equalizer-watch>
						<dt>クライアント・エージェンシー様</dt>
						<dd>
							<div class="card__border--text">
								<p>案件のご相談やお見積もりの<br />ご依頼はこちら</p>
								<div class="card__border--button">
									<a href="/contact/work/" class="expanded button" title="お問い合わせはこちら">お問い合わせはこちら</a>
								</div>
							</div>
						</dd>
					</dl>
				</li>
				<li class="columns">
					<dl class="align-middle card card__border" data-equalizer-watch>
						<dt>フリーランス・パートナー様</dt>
						<dd>
							<div class="card__border--text">
								<p>サービスのご紹介や協業の<br />ご相談はこちら</p>
								<div class="card__border--button">
									<a href="/contact/partner/" class="expanded button" title="お問い合わせはこちら">お問い合わせはこちら</a>
								</div>
							</div>
						</dd>
					</dl>
				</li>
				<li class="columns">
					<dl class="align-middle card card__border" data-equalizer-watch>
						<dt>採用希望者様</dt>
						<dd>
							<div class="card__border--text">
								<p>採用情報に関する<br />ご連絡はこちら</p>
								<div class="card__border--button">
									<a href="/contact/recruit/" class="expanded button" title="お問い合わせはこちら">お問い合わせはこちら</a>
								</div>
							</div>
						</dd>
					</dl>
				</li>
			</ul>
		</article>
	</div>
</section>

<?php get_footer(); ?>
