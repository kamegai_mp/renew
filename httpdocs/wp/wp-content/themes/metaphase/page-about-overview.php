<?php
/* Template Name: About overview */
get_header(); ?>


<section class="section section__sv bg">
	<h2 class="section__sv--title">会社情報</h2>

	<ul class="section__sv--menu menu align-center show-for-medium">
		<li><a href="<?php echo get_permalink(get_page_by_path('about')); ?>" class="link link__text" title="ビジョン">ビジョン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_post_type_archive_link('member'); ?>" class="link link__text" title="メンバー紹介">メンバー紹介<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		<li><a href="<?php echo get_permalink(get_page_by_path('about/overview')); ?>" class="link link__text active" title="会社情報">会社情報<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
	</ul>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><a href="<?php echo get_permalink(get_page_by_path('about')); ?>" title="会社紹介">会社紹介</a></li>
			<li><span class="show-for-sr">Current: </span>会社情報</li>
		</ul>
	</nav>
</section>

<section class="section small">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">会社情報</h2>
		</div>
	</div>
	<div class="section--main medium">
		<article class="article article__overview">
			<ul class="article__overview--list">
				<li>
					<dl class="article__overview--detail row">
						<dt class="columns medium-2 small-12">商号</dt>
						<dd class="columns medium-10 small-12">
							<p>株式会社メタフェイズ　<br class="hide-for-medium" />metaphase,co.,ltd</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__overview--detail row">
						<dt class="columns medium-2 small-12">設立</dt>
						<dd class="columns medium-10 small-12">
							<p>2001年2月7日</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__overview--detail row">
						<dt class="columns medium-2 small-12">本社所在地</dt>
						<dd class="columns medium-10 small-12">
							<div class="row align-bottom">
								<p class="columns medium-10 small-12">〒160-0022<br />東京都新宿区新宿5-17-5　ラウンドクロス新宿5丁目ビル6F</p>
								<p class="columns medium-2 small-12 right"><a href="https://goo.gl/maps/odXqofjYL2E2" target="_blank" class="link link__text" title="アクセス">アクセス<svg class="icon arrow_03"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow_03"></use></svg></a></p>
							</div>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__overview--detail row">
						<dt class="columns medium-2 small-12">連絡先</dt>
						<dd class="columns medium-10 small-12">
							<ul class="inline-block">
								<li>TEL:03-6384-5184</li>
								<li>FAX:03-6384-5183</li>
							</ul>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__overview--detail row">
						<dt class="columns medium-2 small-12">資本金</dt>
						<dd class="columns medium-10 small-12">
							<p>30,000,000円</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__overview--detail row">
						<dt class="columns medium-2 small-12">役員</dt>
						<dd class="columns medium-10 small-12">
							<ul class="member">
								<li class="row">
									<p class="columns medium-2 small-4">代表取締役</p>
									<p class="columns">城野 誠大</p>
								</li>
							</ul>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__overview--detail row">
						<dt class="columns medium-2 small-12">社員数</dt>
						<dd class="columns medium-10 small-12">
							<p>55名（東京50名、上海5名）</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__overview--detail row">
						<dt class="columns medium-2 small-12">取引銀行</dt>
						<dd class="columns medium-10 small-12">
							<p>みずほ銀行 新宿支店<span class="show-for-medium"> / </span><br class="hide-for-medium" />三菱東京UFJ銀行 新宿支店</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__overview--detail row">
						<dt class="columns medium-2 small-12">関連会社</dt>
						<dd class="columns medium-10 small-12">
							<div class="row align-bottom">
								<p class="columns medium-9 small-12">上海銘塔飛信息技術有限公司（上海メタフェイズ）</p>
							</div>

						</dd>
					</dl>
				</li>

			</ul>
		</article>
	</div>
</section>

<section class="section medium bg__highlight">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title">沿革</h2>
		</div>
	</div>
	<div class="section--main medium">
		<article class="article article__history">

			<ul class="article__history--list">
				<li>
					<dl class="article__history--detail">
						<dt><strong class="fontHVN">2000</strong>年</dt>
						<dd>
							<p>個人事務所スタジオ・メタフェイズとして<br clear="hide-for-medium" />活動を開始</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__history--detail">
						<dt><strong class="fontHVN">2001</strong>年</dt>
						<dd>
							<p>有限会社スタジオ・メタフェイズを設立</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__history--detail">
						<dt><strong class="fontHVN">2002</strong>年</dt>
						<dd>
							<p>株式会社メタフェイズに組織変更</p>
							<p>webインテグレーション事業を開始</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__history--detail">
						<dt><strong class="fontHVN">2005</strong>年</dt>
						<dd>
							<p>ネットリサーチ事業を開始</p>
							<p>アンケート配信システム「アンとケイト」を<br clear="hide-for-medium" />リリース</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__history--detail">
						<dt><strong class="fontHVN">2007</strong>年</dt>
						<dd>
							<p>ネットリサーチ事業を開始</p>
							<p>アンケート配信システム「アンとケイト」を<br clear="hide-for-medium" />株式会社ボーダーズへ営業譲渡</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__history--detail">
						<dt><strong class="fontHVN">2008</strong>年</dt>
						<dd>
							<p>上海にてメタフェイズ100%独資法人</p>
							<p>上海銘塔飛信息有限公司（上海メタフェイズ）を設立</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__history--detail">
						<dt><strong class="fontHVN">2012</strong>年</dt>
						<dd>
							<p>ユーザー投稿型コンテンツ制作パッケージ</p>
							<p>「BuzzForm」リリース</p>
							<p class="small">※現在はサービス終了</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__history--detail">
						<dt><strong class="fontHVN">2013</strong>年</dt>
						<dd>
							<p>ソーシャルボタン作成ツール</p>
							<p>「Biscuit」リリース</p>
							<p class="small">※現在はサービス終了</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__history--detail">
						<dt><strong class="fontHVN">2014</strong>年</dt>
						<dd>
							<p>人材アフィリエイト事業を開始</p>
							<p>「転職.TV」をリリース</p>
							<p class="small">※現在はサービス終了</p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__history--detail">
						<dt><strong class="fontHVN">2015</strong>年</dt>
						<dd>
							<p>求人情報のまとめサイトを構築・管理できる<br clear="hide-for-medium" />サービス</p>
							<p>JOB CURATORをリリース</p>
							<p class="small"><a href="http://job-curator.jp/" target="_blank" class="link link--text" title="Web site">Web site<svg class="icon new_window"><use xlink:href="#icon-new_window"></use></svg></a></p>
						</dd>
					</dl>
				</li>
				<li>
					<dl class="article__history--detail">
						<dt><strong class="fontHVN">2016</strong>年</dt>
						<dd>
							<p>現在</p>
						</dd>
					</dl>
				</li>
			</ul>
		</article>
	</div>

</section>

<section class="section medium">
	<div class="row section--head">
		<div class="columns">
			<h2 class="section--title fontHVN">会社紹介</h2>
		</div>
	</div>
	<div class="section--main medium">
		<ul class="row article__bottom-menu">
			<li class="columns medium-4 small-12"><a href="<?php echo get_permalink(get_page_by_path('about')); ?>" class="button button__underline expanded" title="ビジョン">ビジョン<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns medium-4 small-12"><a href="<?php echo get_post_type_archive_link('member'); ?>" class="button button__underline expanded" title="メンバー紹介">メンバー紹介<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
			<li class="columns medium-4 small-12"><a href="<?php echo get_permalink(get_page_by_path('about/overview')); ?>" class="button button__underline expanded active" title="会社情報">会社情報<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a></li>
		</ul>
	</div>
</section>


<?php get_footer(); ?>
