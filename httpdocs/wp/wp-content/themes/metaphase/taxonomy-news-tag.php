<?php get_header();
$queried_object = get_queried_object();
$custom_args = array(
  'post_type' => 'news',
  'post_status' => 'publish',
  'orderby' => 'date',
  'has_password' => false,
	'posts_per_page' => -1,
  'tax_query' => array(
    array(
      'taxonomy' => 'news-tag',
      'field'    => 'slug',
      'terms'    => $queried_object->slug,
    )
  ),
);

if( isset($_GET['y']) ) {
  $year = intval(htmlspecialchars($_GET['y']));

  $custom_args_date = array(
    'date_query' => array(
  		array(
  			'year'  => $year
  		)
  	)
  );
} else {
	$custom_args_date = array(
    'date_query' => array(
  		array(
  			'year'  => 2017
  		)
  	)
  );
}
$custom_args = array_merge($custom_args, $custom_args_date);
$mynews = new WP_Query( $custom_args );
?>


<section class="section section__sv bg">
	<h2 class="section__sv--title">最新情報</h2>
</section>

<section class="section section__breadcrumb">
	<nav class="nav__breadcrumb row">
		<ul class="breadcrumbs columns">
			<li><a href="<?php echo home_url(); ?>" title="TOP">TOP</a></li>
			<li><span class="show-for-sr">Current: </span>最新情報</li>
		</ul>
	</nav>
</section>

<section class="section section__news">
	<div class="section__news--years">
		<div class="section--main row align-center">
			<div class="columns shrink">
				<ul class="section__news--years_list menu">
					<?php wp_custom_archive( 'news' );  ?>
				</ul>
			</div>
		</div>
	</div>

	<div class="section row section__news--main">
		<?php if($mynews->have_posts()) : ?>
			<ul class="section__news--list detail_lists">
			<?php while($mynews->have_posts()) : $mynews->the_post();
				$post_tag = wp_get_object_terms( $post->ID, 'news-tag' );
				$post_tag = $post_tag[0]->name;
				$post_date = get_the_date('Y.m.d', $post->ID );
			?>
				<li>
					<article class="article__news row align-middle news_list">
						<div class="columns medium-3 small-12">
							<div class="article__news--head row align-middle">
								<p class="article__news--category columns small-4 medium-6"><?php echo $post_tag; ?></p>
								<time class="article__news--time fontACN columns small-6 medium-6"><?php echo $post_date; ?></time>
							</div>
						</div>
						<div class="columns medium-9 small-12">
							<p class="article__news--link"><a href="<?php the_permalink(); ?>" class="link link__underline" title="<?php the_title(); ?>"><?php the_title(); ?></a></p>
						</div>
					</article>
				</li>
			<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</div>

	<div class="section__link medium">
		<a href="<?php echo get_post_type_archive_link('news'); ?>" class="link link__text" title="もっと見る">もっと見る<svg class="icon arrow_03"><use xlink:href="#icon-arrow_03"></use></svg></a>
	</div>
</section>

<section class="section section__news--filter">
	<h3 class="subtitle">期間別で絞り込む</h3>
	<div class="section__news--years_sp row">
		<div class="columns small-12">
			<ul class="section__news--years_list row columns">
				<?php wp_custom_archive( 'news' );  ?>
			</ul>
		</div>
	</div>
</section>

<?php get_footer(); ?>
