<?php
/* Template Name: Contact form */
get_header(); ?>

<section class="section section__sv bg">
	<h2 class="section__sv--title"><?php echo get_field('form_page_title'); ?></h2>
</section>

<section class="section section__contact expanded">
	<div class="section--head">
		<h2 class="section--title"><?php the_title(); ?></h2>
	</div>
	<div class="section--main">
		<article class="article article__contact expanded">

			<div class="row">
			  <div class="columns">
					<?php if ( have_posts() ) { while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; } ?>
				</div>
			</div>

		</article>
	</div>
</section>

<?php get_footer(); ?>
