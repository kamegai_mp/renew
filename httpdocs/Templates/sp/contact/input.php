<!DOCTYPE HTML>
<html lang="ja"><!-- InstanceBegin template="/Templates/base_sp.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="utf-8">
<meta name="keywords" content="資料請求,お問い合わせ,Web,制作会社,デザイン,ブランディング,システム開発,メタフェイズ,上海">
<meta name="description" content="メタフェイズへの資料請求・お問い合わせ。Web制作、Webデザインならメタフェイズ。IT領域においてクリエイティブをコアに据えた事業を展開し、有益なサービスを生み出せる企業を目指します。">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="format-detection" content="telephone=no">
<link rel="shortcut icon" href="/meta.ico">
<title>お問い合わせ｜株式会社メタフェイズ</title>
<meta property="og:title" content="Web制作・デザイン会社　株式会社メタフェイズ">
<meta property="og:description" content="Web制作、Webデザインならメタフェイズ。IT領域においてクリエイティブをコアに据えた事業を展開し、有益なサービスを生み出せる企業を目指します。">
<meta property="og:url" content="http://www.metaphase.co.jp/">
<meta property="og:image" content="http://www.metaphase.co.jp/images/meta_ogp.png">
<meta property="og:type" content="website">
<meta name="apple-mobile-web-app-title" content="メタフェイズ" />
<link rel="apple-touch-icon-precomposed" href="/apple-touch-icon-precomposed.png">
<script type="text/javascript" src="/scripts/sp/common/jquery.js"></script>
<script type="text/javascript" src="/scripts/sp/common/css_browser_selector.js"></script>
<script type="text/javascript" src="/scripts/sp/common/fixHeight.js"></script>
<script type="text/javascript" src="/scripts/sp/common/common.js"></script>
<link rel="stylesheet" href="/css/sp/common/format.css">
<link rel="stylesheet" href="/css/sp/common/base.css">
<link rel="stylesheet" href="/css/sp/common/contents.css">
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" href="/css/sp/contact/local.css">
<script type="text/javascript">
$(function(){
	// custom check/radio
	var checkObj = [$(".radioList01 input[type='radio']")];

	function check(obj){
		jQuery.each(obj,function(){
			if($(this).is(':checked')){
				$(this).parent().addClass('c_on');
			}else{
				$(this).parent().removeClass('c_on');
			}
		});
	}
	for(var i = 0;i < checkObj.length; ++i){
		check(checkObj[i]);
	}
	$('input[type="radio"]').change(function(){
		for(var i = 0;i < checkObj.length; ++i){
			check(checkObj[i]);
		}
	});
});
</script>
<!-- InstanceEndEditable -->
<!-- InstanceParam name="categoryID" type="text" value="contact" -->
<!-- InstanceParam name="pageID" type="text" value="index" -->
<!-- InstanceParam name="title" type="text" value="お問い合わせ｜株式会社メタフェイズ" -->
<!-- InstanceParam name="keywords" type="text" value="資料請求,お問い合わせ,Web,制作会社,デザイン,ブランディング,システム開発,メタフェイズ,上海" -->
<!-- InstanceParam name="description" type="text" value="メタフェイズへの資料請求・お問い合わせ。Web制作、Webデザインならメタフェイズ。IT領域においてクリエイティブをコアに据えた事業を展開し、有益なサービスを生み出せる企業を目指します。" -->
<!-- InstanceParam name="ogurl" type="text" value="" -->
</head>
<body>
<div id="wrapper">
<header id="gheader">
<p class="logo"><a href="/"><img src="/images/sp/common/header_parts_logo_01.png" alt="metaphase" width="147" height="26"></a></p>
<p class="blogIco"><a href="/column/" target="_blank"><img src="/images/sp/common/header_parts_blog_01.png" width="28" height="30" alt="Blog"></a></p>
<p class="menuBtn"><a href="javascript:;"><img src="/images/sp/common/header_parts_menu_01.png" width="28" height="30" alt="Menu"></a></p>
<nav id="gnav">
<ul>
<li><a href="/"><img src="/images/sp/common/gnav_menu_01.png" width="102" height="15" alt="HOME(ホーム)"></a></li>
<li><a href="/press/"><img src="/images/sp/common/gnav_menu_02.png" width="106" height="15" alt="NEWS（お知らせ）"></a></li>
<li><a href="/company/"><img src="/images/sp/common/gnav_menu_03.png" width="155" height="15" alt="COMPANY（会社案内）"></a></li>
<li><a href="https://www.metaphase.co.jp/contact/"><img src="/images/sp/common/gnav_menu_04.png" width="169" height="15" alt="CONTACT（お問い合わせ）"></a></li>
</ul>
<p class="close"><a href="javascript:;"><img src="/images/sp/common/gnav_btn_close_01.png" width="54" height="10" alt="CLOSE"></a></p>
</nav>
</header>
<section id="container"><!-- InstanceBeginEditable name="contents" -->
<div id="contents">
<div class="lowerTitle">
<h1><img src="/images/sp/contact/index_title_01.png" width="141" height="23" alt="CONTACT"><span>お問い合わせ</span></h1>
<!-- /.lowerTitle --></div>

<div class="sectionTypeA01">
<form action="" method="post">
<h2><?php $html->showList($vars["type"],$lists["type"])?></h2>
<p class="text01">下記の入力欄に必要事項をご記入の上、『確認する』ボタンをタップして次の画面へお進みください。</p>
<?php if ($vars["type"] == CONTACT_TYPE_PARTNER) {?>
<p class="text01">
パートナー・協業のご相談につきましては内容により必要に応じてご連絡させて頂きます。
<br>お答えできない場合もございますので、何卒ご了承いただきますようお願い致します。</p>
<?php } ?>
<dl class="dList01">
  <?php if (in_array($vars["type"], array(CONTACT_TYPE_WORK, CONTACT_TYPE_ETC, CONTACT_TYPE_PARTNER))) {?>
  <dt>会社名<?php if ($vars["type"] == CONTACT_TYPE_PARTNER) {?><span class="any">任意</span><?php } else {?><span class="req">必須</span><?php } ?></dt>
  <dd>
    <?php if(! empty($err_msg["company"])):?><p class="error"><?php echo $err_msg["company"]?></p><?php endif?>
    <input class="inputTxtType01 w624" type="text" name="company" value="<?php echo $vars["company"]?>">
  </dd>
  <?php } ?>
  <dt>お名前<span class="req">必須</span></dt>
  <dd>
    <?php if(! empty($err_msg["names"])):?><p class="error"><?php echo $err_msg["names"]?></p><?php endif?>
    <input class="inputTxtType01" type="text" name="names" value="<?php echo $vars["names"]?>">
  </dd>
  <dt>電話番号<span class="req">必須</span></dt>
  <dd>
    <?php if(! empty($err_msg["tel"])):?><p class="error"><?php echo $err_msg["tel"]?></p><?php endif?>
    <input class="inputTxtType01" type="tel" name="tel" value="<?php echo $vars["tel"]?>">
  </dd>
  <dt>メールアドレス<span class="req">必須</span></dt>
  <dd>
    <?php if(! empty($err_msg["email"])):?><p class="error"><?php echo $err_msg["email"]?></p><?php endif?>
    <input class="inputTxtType01" type="email" name="email" value="<?php echo $vars["email"]?>">
  </dd>
  <?php if (in_array($vars["type"], array(CONTACT_TYPE_WORK, CONTACT_TYPE_PARTNER))) {?>
  <dt>URL<span class="any">任意</span></dt>
  <dd>
    <?php if(! empty($err_msg["url"])):?><p class="error"><?php echo $err_msg["url"]?></p><?php endif?>
    <input class="inputTxtType01 w624" type="text" name="url" value="<?php echo $vars["url"]?>">
  </dd>
  <?php } ?>
  <?php if ($vars["type"] == CONTACT_TYPE_WORK) {?>
  <dt>ご依頼予定内容<span class="any">任意</span></dt>
  <dd>
    <?php if(! empty($err_msg["request_type"])):?><p class="error"><?php echo $err_msg["request_type"]?></p><?php endif?>
    <select name="request_type" id="" class="customSelect01">
	<?php $html->options(array("options"=>$lists["request_type"],"selected"=>$vars["request_type"]))?>
	</select>
  </dd>
  <dt>ご予算<span class="any">任意</span></dt>
  <dd>
    <?php if(! empty($err_msg["budget"])):?><p class="error"><?php echo $err_msg["budget"]?></p><?php endif?>
    <input class="inputTxtType01 w624" type="text" name="budget" value="<?php echo $vars["budget"]?>">
  </dd>
  <dt>予定サイト種別<span class="any">任意</span></dt>
  <dd>
    <?php if(! empty($err_msg["request_site_type"])):?><p class="error"><?php echo $err_msg["request_site_type"]?></p><?php endif?>
    <select name="request_site_type" id="" class="customSelect01">
      <?php $html->options(array("options"=>$lists["request_site_type"],"selected"=>$vars["request_site_type"]))?>
    </select>
  </dd>
  <?php } ?>
  <dt>お問い合わせ内容<span class="req">必須</span></dt>
  <dd>
    <?php if(! empty($err_msg["contents"])):?><p class="error"><?php echo $err_msg["contents"]?></p><?php endif?>
    <textarea class="textAreaType01" cols="10" rows="10" name="contents"><?php echo $vars["contents"]?></textarea>
  </dd>
</dl>

<div class="personalAreaWrap">
<div class="personalArea">
<p class="ttl"><em>個人情報の取り扱いについて</em></p>
<p class="alR">株式会社メタフェイズ</p>
<p class="alR">代表取締役 城野 誠大</p>
<dl>
<dt>1.（取得及び利用目的）</dt>
<dd>株式会社メタフェイズ（以下「弊社」という）は、弊社採用募集に応募されるみなさまの氏名、メールアドレスなどの個人を識別できる情報を取得します。取得の目的は、採用面接のご連絡であり、それ以外の目的で利用いたしません。</dd>
<dt>2.（預託・提供）</dt>
<dd>弊社は、法令で定める場合を除き個人を特定できる情報（氏名、メールアドレス等）を取得目的の範囲を超えて当該個人の同意なしに第三者への提供や預託はいたしません。</dd>
<dt>3.（情報の開示・訂正・削除）</dt>
<dd>弊社は、個人情報（採用評価情報等を除く）の開示、訂正、追加又は削除、利用目的の通知、利用又は提供の拒否についての依頼を受けた場合は、弊社の手続きに従って速やかに対応します。</dd>
<dt>4.（個人情報提供の任意性）</dt>
<dd>個人情報の弊社への提供は、お客様の同意に基づく任意ですが、必要な情報が提供されない場合、適正な応募者の採用選考並びに採用面接のご連絡ができない場合もありうることをご承知ください。</dd>
<dt>5.（問合せ情報の廃棄）</dt>
<dd>お問合せいただいき採用応募していただいたものの、残念ながら採用にいたらなかった場合、本ページで取得いたしました個人情報は、弊社で責任を持って廃棄致します。</dd>
<dt>6.（個人情報保護管理者及び問合せ先）</dt>
<dd>当社では、個人情報を適切に保護するための管理者を下記の者が担当いたしております。また個人情報管理に関する問い合わせや開示、訂正または削除の依頼は、下記までご連絡下さい。<br>
株式会社メタフェイズ<br>
個人情報に関する苦情及び相談窓口<br>
〒160-0022　東京都新宿区新宿5-17-5 ラウンドクロス新宿5丁目ビル6F<br>
個人情報保護管理者　折本 裕司<br>
E-mail：<a href="mailto:%70%72%69%76%61%63%79%40%6D%65%74%61%70%68%73%65.%63%6f.%6a%70"><img width="103" height="9" src="/images/sp/contact/mod_mailAddress_01.gif" width="178" height="23" alt="メールアドレス画像"></a><br>
TEL：03-6384-5184　FAX：03-6384-5183<br>
</dd>
</dl>
<!-- /.personalArea --></div>
<!-- /.personalAreaWrap --></div>

<p class="btnType02"><button type="submit">同意し確認画面へ</button></p>
<input type="hidden" name="mode" value="confirm" />
</form>
<!-- /.sectionTypeA01 --></div>

<!-- /#contents --></div>
<!-- InstanceEndEditable --><!-- /#container --></section>
<footer id="gfooter">
<ul class="shareLinks">
<li><a href="https://ja-jp.facebook.com/metaphase.co.jp" target="_blank"><img src="/images/sp/common/footer_parts_sns_01.gif" width="78" height="20" alt="Facebook"></a></li>
<li><a href="https://twitter.com/metaphase_co_jp" target="_blank"><img src="/images/sp/common/footer_parts_sns_02.gif" width="63" height="20" alt="Twitter"></a></li>
<li><a href="https://instagram.com/metaphase_pr/" target="_blank"><img src="/images/sp/common/footer_parts_sns_03.gif" width="79" height="20" alt="instagram"></a></li>
</ul>
<p class="pageTop jsScroll"><a href="#wrapper"><img src="/images/sp/common/footer_parts_pagetop_01.png" width="57" height="25" alt="PAGE TOP"></a></p>
<p class="pcLink"><a href="<?php echo preg_replace("/\?.*$/","",$_SERVER["REQUEST_URI"])?>?tml=pc"><img src="/images/sp/common/footer_parts_pcLink_01.png" width="69" height="16" alt="PCサイト"></a></p>
<p class="copy"><small>Copyright&copy; METAPHASE Co.,Ltd All rights reserved.</small></p>
</footer>
<!-- /#wrapper --></div>
</body>
<!-- InstanceEnd --></html>
