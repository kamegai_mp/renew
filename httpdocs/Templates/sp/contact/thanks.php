<!DOCTYPE HTML>
<html lang="ja"><!-- InstanceBegin template="/Templates/base_sp.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="utf-8">
<meta name="keywords" content="資料請求,お問い合わせ,Web,制作会社,デザイン,ブランディング,システム開発,メタフェイズ,上海">
<meta name="description" content="メタフェイズへの資料請求・お問い合わせ。Web制作、Webデザインならメタフェイズ。IT領域においてクリエイティブをコアに据えた事業を展開し、有益なサービスを生み出せる企業を目指します。">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="format-detection" content="telephone=no">
<link rel="shortcut icon" href="/meta.ico">
<title>お問い合わせ｜株式会社メタフェイズ</title>
<meta property="og:title" content="Web制作・デザイン会社　株式会社メタフェイズ">
<meta property="og:description" content="Web制作、Webデザインならメタフェイズ。IT領域においてクリエイティブをコアに据えた事業を展開し、有益なサービスを生み出せる企業を目指します。">
<meta property="og:url" content="http://www.metaphase.co.jp/">
<meta property="og:image" content="http://www.metaphase.co.jp/images/meta_ogp.png">
<meta property="og:type" content="website">
<meta name="apple-mobile-web-app-title" content="メタフェイズ" />
<link rel="apple-touch-icon-precomposed" href="/apple-touch-icon-precomposed.png">
<script type="text/javascript" src="/scripts/sp/common/jquery.js"></script>
<script type="text/javascript" src="/scripts/sp/common/css_browser_selector.js"></script>
<script type="text/javascript" src="/scripts/sp/common/fixHeight.js"></script>
<script type="text/javascript" src="/scripts/sp/common/common.js"></script>
<link rel="stylesheet" href="/css/sp/common/format.css">
<link rel="stylesheet" href="/css/sp/common/base.css">
<link rel="stylesheet" href="/css/sp/common/contents.css">
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" href="/css/sp/contact/local.css">
<!-- InstanceEndEditable -->
<!-- InstanceParam name="categoryID" type="text" value="contact" -->
<!-- InstanceParam name="pageID" type="text" value="confirm" -->
<!-- InstanceParam name="title" type="text" value="お問い合わせ｜株式会社メタフェイズ" -->
<!-- InstanceParam name="keywords" type="text" value="資料請求,お問い合わせ,Web,制作会社,デザイン,ブランディング,システム開発,メタフェイズ,上海" -->
<!-- InstanceParam name="description" type="text" value="メタフェイズへの資料請求・お問い合わせ。Web制作、Webデザインならメタフェイズ。IT領域においてクリエイティブをコアに据えた事業を展開し、有益なサービスを生み出せる企業を目指します。" -->
<!-- InstanceParam name="ogurl" type="text" value="" -->
</head>
<body>
<div id="wrapper">
<header id="gheader">
<p class="logo"><a href="/"><img src="/images/sp/common/header_parts_logo_01.png" alt="metaphase" width="147" height="26"></a></p>
<p class="blogIco"><a href="/column/" target="_blank"><img src="/images/sp/common/header_parts_blog_01.png" width="28" height="30" alt="Blog"></a></p>
<p class="menuBtn"><a href="javascript:;"><img src="/images/sp/common/header_parts_menu_01.png" width="28" height="30" alt="Menu"></a></p>
<nav id="gnav">
<ul>
<li><a href="/"><img src="/images/sp/common/gnav_menu_01.png" width="102" height="15" alt="HOME(ホーム)"></a></li>
<li><a href="/press/"><img src="/images/sp/common/gnav_menu_02.png" width="106" height="15" alt="NEWS（お知らせ）"></a></li>
<li><a href="/company/"><img src="/images/sp/common/gnav_menu_03.png" width="155" height="15" alt="COMPANY（会社案内）"></a></li>
<li><a href="https://www.metaphase.co.jp/contact/"><img src="/images/sp/common/gnav_menu_04.png" width="169" height="15" alt="CONTACT（お問い合わせ）"></a></li>
</ul>
<p class="close"><a href="javascript:;"><img src="/images/sp/common/gnav_btn_close_01.png" width="54" height="10" alt="CLOSE"></a></p>
</nav>
</header>
<section id="container"><!-- InstanceBeginEditable name="contents" -->
<div id="contents" class="thanks">
<div class="lowerTitle">
<h1><img src="/images/sp/contact/index_title_01.png" width="141" height="23" alt="CONTACT"><span>お問い合わせ</span></h1>
<!-- /.lowerTitle --></div>

<div class="sectionTypeA01 alC">
<p class="text01">お問い合わせいただきありがとうございました。<br>後日、担当者からご連絡させていただきます。</p>
<?php if ($vars["type"] == CONTACT_TYPE_PARTNER) {?>
<p class="text01">パートナー・協業のご相談につきましては<br>内容により必要に応じてご連絡させて頂きます。
</p>

<?php } ?>
<p class="btnType02"><a href="/">HOMEへ</a></p>
<!-- /.sectionTypeA01 --></div>

<!-- /#contents --></div>
<!-- InstanceEndEditable --><!-- /#container --></section>
<footer id="gfooter">
<ul class="shareLinks">
<li><a href="https://ja-jp.facebook.com/metaphase.co.jp" target="_blank"><img src="/images/sp/common/footer_parts_sns_01.gif" width="78" height="20" alt="Facebook"></a></li>
<li><a href="https://twitter.com/metaphase_co_jp" target="_blank"><img src="/images/sp/common/footer_parts_sns_02.gif" width="63" height="20" alt="Twitter"></a></li>
<li><a href="https://instagram.com/metaphase_pr/" target="_blank"><img src="/images/sp/common/footer_parts_sns_03.gif" width="79" height="20" alt="instagram"></a></li>
</ul>
<p class="pageTop jsScroll"><a href="#wrapper"><img src="/images/sp/common/footer_parts_pagetop_01.png" width="57" height="25" alt="PAGE TOP"></a></p>
<p class="pcLink"><a href="<?php echo preg_replace("/\?.*$/","",$_SERVER["REQUEST_URI"])?>?tml=pc"><img src="/images/sp/common/footer_parts_pcLink_01.png" width="69" height="16" alt="PCサイト"></a></p>
<p class="copy"><small>Copyright&copy; METAPHASE Co.,Ltd All rights reserved.</small></p>
</footer>
<!-- /#wrapper --></div>
</body>
<!-- InstanceEnd --></html>
