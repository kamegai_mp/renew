
<!DOCTYPE HTML>
<html lang="ja"><!-- InstanceBegin template="/Templates/base.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="utf-8">
<meta name="description" content="メタフェイズへの資料請求・お問い合わせ。Web制作、Webデザインならメタフェイズ。IT領域においてクリエイティブをコアに据えた事業を展開し、有益なサービスを生み出せる企業を目指します。">
<meta name="keywords" content="資料請求,お問い合わせ,Web,制作会社,デザイン,ブランディング,システム開発,メタフェイズ,上海">
<link rel="shortcut icon" href="/meta.ico">


<meta property="og:title" content="Web制作・デザイン会社　株式会社メタフェイズ">
<meta property="og:description" content="Web制作、Webデザインならメタフェイズ。IT領域においてクリエイティブをコアに据えた事業を展開し、有益なサービスを生み出せる企業を目指します。">

<meta property="og:url" content="http://www.metaphase.co.jp/">
<meta property="og:image" content="http://www.metaphase.co.jp/images/meta_ogp.png">
<meta property="og:type" content="website">




<title>お問い合わせ｜株式会社メタフェイズ</title>

<link rel="stylesheet" href="/css/common/format.css">
<link rel="stylesheet" href="/css/common/base.css">
<link rel="stylesheet" href="/css/common/contents.css">
<link rel="stylesheet" href="/css/common/cate_uni.css">
<script type="text/javascript" src="/scripts/common/jquery.js"></script>
<script type="text/javascript" src="/scripts/common/css_browser_selector.js"></script>
<script type="text/javascript" src="/scripts/common/nav.js"></script>
<script type="text/javascript" src="/scripts/common/common.js"></script>
<script type="text/javascript" src="/scripts/common/viewport.js"></script>
<script type="text/javascript" src="/scripts/common/toSp.js"></script>
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" href="/css/common/mCustomScrollbar.css">
<script src="/scripts/common/customSelect.js"></script>
<script src="/scripts/common/mCustomScrollbar.js"></script>
<script type='text/javascript'>
$(function () {
	// custom select
	$('.customSelect01').customSelect();

	// custom check/radio
	var checkObj = [$(".radioList01 input[type='radio']"),$(".checkboxType01 input")];

	function check(obj){
		jQuery.each(obj,function(){
			if($(this).is(':checked')){
				$(this).parent().addClass('c_on');
			}else{
				$(this).parent().removeClass('c_on');
			}
		});
	}
	for(var i = 0;i < checkObj.length; ++i){
		check(checkObj[i]);
	}
	$('input[type="radio"],input[type="checkbox"]').change(function(){
		for(var i = 0;i < checkObj.length; ++i){
			check(checkObj[i]);
		}
	});

	// custom scroll
	$('.personalArea').mCustomScrollbar({
		scrollButtons:{
			enable:true
		}
	});
});
</script>
<style>
.error{
color:#FF0000;
}
</style>
<!-- InstanceEndEditable -->
<!-- InstanceParam name="categoryID" type="text" value="contact" -->
<!-- InstanceParam name="pageID" type="text" value="index" -->
<!-- InstanceParam name="title" type="text" value="お問い合わせ｜株式会社メタフェイズ" -->
<!-- InstanceParam name="keywords" type="text" value="資料請求,お問い合わせ,Web,制作会社,デザイン,ブランディング,システム開発,メタフェイズ,上海" -->
<!-- InstanceParam name="description" type="text" value="メタフェイズへの資料請求・お問い合わせ。Web制作、Webデザインならメタフェイズ。IT領域においてクリエイティブをコアに据えた事業を展開し、有益なサービスを生み出せる企業を目指します。" -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-4728089-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/6707003.js"></script>
<!-- End of HubSpot Embed Code -->

</head>

<body id="contact">
<div class="toSpSite">
<p><a href="/?tml=sp"><img src="/images/common/mod_btn_spLink_01.png" alt="スマートフォン版へ表示切替"></a></p>
<!-- /.toSpSite --></div>
<div id="wrapper">

<div id="sideArea">
<p id="logo"><a href="http://www.metaphase.co.jp/"><img src="/images/common/header_logo_01.png" width="136" height="23" alt="metaphase"></a></p>
<nav id="gnav">
<ul>
<li><a href="http://www.metaphase.co.jp/service/">SERVICE</a>
<div class="innerNav">
<ul>
<li><a href="http://www.metaphase.co.jp/service/">サービス</a></li>
<li><a href="http://www.metaphase.co.jp/service/integration.html">Webインテグレーション</a></li>
<li><a href="http://www.metaphase.co.jp/service/retention.html">Webリテンション</a></li>
<li><a href="http://www.metaphase.co.jp/service/curation.html">クロール型求人サイト構築</a></li>
</ul></div></li>
<li><a href="http://www.metaphase.co.jp/works/">WORK</a></li>
<li><a href="http://www.metaphase.co.jp/company/">COMPANY</a>
<div class="innerNav">
<ul>
<li><a href="http://www.metaphase.co.jp/company/">会社案内</a></li>
<li><a href="http://www.metaphase.co.jp/company/mission.html">ミッション</a></li>
<li><a href="http://www.metaphase.co.jp/company/history.html">沿革</a></li>
<li><a href="http://www.metaphase.co.jp/company/access.html">地図・アクセス</a></li>
</ul></div></li>
<li><a href="http://www.metaphase.co.jp/member/">MEMBER</a></li>
<li><a href="http://www.metaphase.co.jp/recruit/">RECRUIT</a>
<div class="innerNav">
<ul>
<li><a href="http://www.metaphase.co.jp/recruit/">採用情報</a></li>
<li><a href="http://metaphase-pr.tumblr.com/" target="_blank">フォトギャラリー</a></li>
</ul></div></li>
<li><a href="https://www.metaphase.co.jp/contact/">CONTACT</a></li>
</ul>
</nav>

<p class="access"><a href="http://www.metaphase.co.jp/company/access.html"><img src="/images/common/mod_txt_side_01.png" width="72" height="17" alt="ACCESS"></a></p>
<p class="blog"><a href="http://www.metaphase.co.jp/column/" target="_blank"><img src="/images/common/mod_ico_blog.png" width="79" height="14" alt="PR BLOG"></a></p>
<ul class="snsBtn">
<li><a href="https://ja-jp.facebook.com/metaphase.co.jp" target="_blank"><img src="/images/common/mod_ico_facebook.png" width="19" height="19" alt="Facebook"></a></li>
<li><a href="https://twitter.com/metaphase_co_jp" target="_blank"><img src="/images/common/mod_ico_twitter.png" width="19" height="19" alt="Twitter"></a></li>
</ul>
<!-- / #sideArea --></div>

<div id="index">
<div id="container">


<section id="contents"><!-- InstanceBeginEditable name="contents" -->
<div class="contentsInner01">
<h1 class="pageTitle01"><img src="/images/contact/contact_title_01.png" width="323" height="55" alt="CONTACT"><span>各種お問い合わせ一覧</span></h1>

<ul class="btnList02">
<li><a href="/contact/work/">お仕事のご相談・ご依頼</a></li>
<li><a href="/contact/partner/">パートナー・協業のご相談</a></li>
<li><a href="/contact/recruit/">採用について</a></li>
<li><a href="/contact/etc/">その他お問い合わせ</a></li>
</ul>

<!-- /.contentsInner01 --></div>
<!-- InstanceEndEditable --><!-- / #contents --></section>

<!-- / #container --></div>

<div id="topicPath">
<ul>
<li><a href="http://www.metaphase.co.jp/">HOME</a></li>
<!-- InstanceBeginEditable name="topicPath" -->
<li>お問い合わせ</li>
<!-- InstanceEndEditable -->
</ul>
</div>
<footer id="gfooter">
<div class="footerInner">
<ul class="fUnityNav">
<li><a href="http://www.metaphase.co.jp/privacy/">個人情報保護方針</a></li>
<li><a href="https://www.metaphase.co.jp/contact/">お問い合わせ</a></li>
</ul>
<p class="copy"><img src="/images/common/mod_txt_copy_01.gif" width="237" height="13" alt="Copyright&copy; METAPHASE Co.,Ltd All rights reserved."></p>
<!-- /.footerInner --></div>
</footer>
<!-- / #index --></div>
<!-- / #wrapper --></div>
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'GQM827ZXA9';
var yahoo_retargeting_label = '';
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 932812472;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/932812472/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
<!-- InstanceEnd --></html>
