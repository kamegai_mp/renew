<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="user-scalable=0, width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta name="description" content="Web制作、ブランドサイトならメタフェイズ。戦略からデザインまで常にユーザー目線でビジネスをサポートします。">
<meta name="keywords" content="Web,制作会社,デザイン,ブランドサイト,システム開発,メタフェイズ">
<link href="common/css/style.css" rel="stylesheet">
<link href="common/css/slick.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Oswald:600' rel='stylesheet'>
<link href='http://fonts.googleapis.com/css?family=Roboto:700' rel='stylesheet'>
<link href='http://fonts.googleapis.com/css?family=Archivo+Narrow' rel='stylesheet'>
<script src="common/js/jquery-3.1.1.min.js"></script>
<script src="common/js/common.js"></script>
<script src="common/js/animate.js"></script>
<script src="common/js/slick.min.js"></script>
<title>株式会社メタフェイズ</title>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-4728089-1', 'auto');
  ga('send', 'pageview');

$(function(){
	$("#back").click(function(){
		document.form1.mode.value = "back";
	});
});
</script>
</head>
<body>
<div id="container">

	<main id="main">

		<section id="section08" class="form_section section">
			<h2>お問い合わせ</h2>
			<p class="text">以下の内容でよろしければ「送信する」ボタンを押して下さい。</p>

			<div class="form_area">
				<form action="./#section08" method="post" name="form1">
					<table>
						<tr>
							<th>御社名</th>
							<td><?php echo $vars["company"]?></td>
						</tr>
						<tr>
							<th>お名前</th>
							<td><?php echo $vars["names"]?></td>
						</tr>
						<tr>
							<th>お電話番号</th>
							<td><?php echo $vars["tel1"]?> - <?php echo $vars["tel2"]?> - <?php echo $vars["tel3"]?></td>
						</tr>
						<tr>
							<th>メールアドレス</th>
							<td><?php echo $vars["email"]?></td>
						</tr>
						<tr>
							<th>URL（任意）</th>
							<td><?php echo $vars["url"]?></td>
						</tr>
						<tr>
							<th>ご相談内容（任意）</th>
							<td><?php $html->showList($vars["request_type"],$lists["request_type"])?></td>
						</tr>
						<tr>
							<th>制作物の種類（任意）</th>
							<td><?php $html->showList($vars["request_site_type"],$lists["request_site_type"])?></td>
						</tr>
						<tr>
							<th>ご予算（任意）</th>
							<td><?php echo $vars["budget"]?></td>
						</tr>
						<tr>
							<th>お問い合わせ内容</th>
							<td><?php echo nl2br($vars["contents"])?></td>
						</tr>
					</table>

					<p class="submit_btn"><input type="submit" value="戻る" id="back">&nbsp;&nbsp;<input type="submit" value="送信する"></p>

<input type="hidden" name="mode" value="entry" />
<?php $html->hidden(array("options"=>$vars,"denys"=>array("mode")))?>

				</form>
			</div>
		</section>
	</main>

	<footer id="footer">
		<p class="logo"><img src="common/img/footer_logo_01.png" alt="web creative agency metaphase"></p>
		<small>Copyright&copy; METAPHASE Co.,Ltd All rights reserved.</small>
	</footer>
</div>

</body>
</html>
