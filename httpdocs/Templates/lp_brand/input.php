<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="user-scalable=0, width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta name="description" content="Web制作、ブランドサイトならメタフェイズ。戦略からデザインまで常にユーザー目線でビジネスをサポートします。">
<meta name="keywords" content="Web,制作会社,デザイン,ブランドサイト,システム開発,メタフェイズ">
<link href="common/css/style.css" rel="stylesheet">
<link href="common/css/slick.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Oswald:600' rel='stylesheet'>
<link href='http://fonts.googleapis.com/css?family=Roboto:700' rel='stylesheet'>
<link href='http://fonts.googleapis.com/css?family=Archivo+Narrow' rel='stylesheet'>
<script src="common/js/jquery-3.1.1.min.js"></script>
<script src="common/js/common.js"></script>
<script src="common/js/animate.js"></script>
<script src="common/js/slick.min.js"></script>
<title>株式会社メタフェイズ</title>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-4728089-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="container">

	<header id="header">
		<div id="header_top">
			<p class="logo"><img src="common/img/header_logo_01.png" alt="web creative agency metaphase"></p>
			<div class="contact">
				<p class="tel"><a href="tel:0363845184">03-6384-5184</a></p>
				<p class="btn bottom"><a href="#section08">お問い合わせ</a></p>
			</div>
		</div>

		<div id="mainvisual" class="abox">
			<p id="mainvisual_item01" class="sub_title">ブランド担当者さま、マーケティング担当者さま、<br class="display_sp">販促担当者さま必見！</p>
			<h1 id="mainvisual_item02" class="title"><img src="common/img/mv_head.png" alt="ブランドサイトで潜在顧客をつかむ4つの秘訣"></h1>
			<p id="mainvisual_item03" class="lead_text">メタフェイズでは過去のプロジェクトを通じて得た<br class="display_sp">ノウハウを活かし、<br class="display_pc">
				クライアント企業にとって最も価値の<br class="display_sp">高い、潜在顧客を育成する<br class="display_pc">
				ブランドサイトを提案しています。</p>
		</div>

		<div id="client_area">
			<p>株式会社メタフェイズでは<br class="display_sp">様々なクライアント企業のWebサイト制作を行っています。</p>
			<ul>
				<li><img src="common/img/mv_logo_01.png" alt="常盤薬品"></li>
				<li><img src="common/img/mv_logo_02.png" alt="RECRUIT"></li>
				<li><img src="common/img/mv_logo_03.png" alt="KIRIN"></li>
				<li><img src="common/img/mv_logo_04.png" alt="ヒューマンアカデミー"></li>
				<li><img src="common/img/mv_logo_05.png" alt="ドクターシーラボ"></li>
				<li><img src="common/img/mv_logo_06.png" alt="Columbia"></li>
				<li><img src="common/img/mv_logo_07.png" alt="odakyu GROUP"></li>
			</ul>
		</div>
	</header>

	<main id="main">
		<section id="section01" class="target_section section">
			<div id="section01_abox01" class="abox">
				<h2>
					<span id="section01_item01" class="title">ブランドサイトのターゲットは誰か？</span>
					<span id="section01_item02" class="sub_title">見落とされがちな潜在顧客との<br class="display_sp">コミュニケーション</span>
				</h2>
				<p id="section01_item03" class="lead_text">ブランドサイトには商品に関する基本的な情報が必要です。消費者がそのブランドに興味関心を<br class="display_pc">
				持って訪れた際に要求を満たすだけの商品情報がなくてはなりません。<br>
				一方、見落とされがちなブランドへの関心の低い層とのコミュニケーションです。<br>
				潜在顧客との接点づくりという事業にとって重要なテーマがブランドサイト設計から<br class="display_pc">
				抜け落ち、商品カタログサイト化してしまう事例が多く見受けられます。</p>
			</div>

			<div id="section01_abox02" class="abox display_pc">
				<p id="section01_item04"><img src="common/img/section01_img_01.gif" alt="ブランドサイト　顕在顧客層 ブランドに興味を持ち、積極的に情報収集をする　潜在顧客層 ブランドへの関心が薄く、積極的に情報収集はしない"></p>
				<p id="section01_item05"><img src="common/img/section01_img_02.png" alt="商品情報の発信を通じたコミュニケーションができる"></p>
				<p id="section01_item06"><img src="common/img/section01_img_03.png" alt="商品情報の発信では接点を持つことができない"></p>
			</div>

			<div id="section01_abox02_sp" class="abox display_sp">
				<p id="section01_item04_sp"><img src="common/img/section01_img_01_sp.gif" alt="ブランドサイト　顕在顧客層 ブランドに興味を持ち、積極的に情報収集をする　潜在顧客層 ブランドへの関心が薄く、積極的に情報収集はしない"></p>
				<p id="section01_item05_sp"><img src="common/img/section01_img_02_sp.png" alt="商品情報の発信を通じたコミュニケーションができる"></p>
				<p id="section01_item06_sp"><img src="common/img/section01_img_03_sp.png" alt="商品情報の発信では接点を持つことができない"></p>
			</div>

			<p class="btm_text">メタフェイズでは潜在顧客との<br class="display_sp">コミュニケーションにも目を向けた<br>ブランドサイトづくりをご提案します。</p>
		</section>

		<section id="section02" class="approach_section section">
			<div class="heading_area">
				<h2>メタフェイズの考える「潜在顧客を引き込む」アプローチ</h2>
			</div>

			<section id="section02_01">
				<div id="section02_abox01" class="abox">
					<p id="section02_item01" class="num_title">01</p>
					<h3>
						<span id="section02_item02" class="title"><span class="quote left">&#8220;</span>ブランドとの最初の<br class="display_sp">接点をつくる<br class="display_sp"><em id="section02_item03">「コンテンツデザイン」</em><span class="quote right">&#8221;</span></span>
					</h3>
					<p id="section02_item04" class="lead_text">まだ商品やサービスに関心のない潜在顧客と接点を持つためには<br class="display_pc">
						彼らのニーズを把握し、ブランドとの接点となり得るコンテンツを用意することが大切です。<br>
						メタフェイズでは潜在顧客のインサイトをユーザーインタビューや<br class="display_pc">
						検索impなどから類推した上でコンテンツデザインをします。</p>
				</div>

				<div id="section02_abox02" class="display_pc">
					<p id="section02_item05"><img src="common/img/section02_img_01.gif" alt="潜在顧客層"></p>
					<div id="section02_abox02_01" class="abox">
						<ul>
							<li id="section02_item06"><img src="common/img/section02_img_02.png" alt="脱水症状"></li>
							<li id="section02_item07"><img src="common/img/section02_img_03.png" alt="二日酔い"></li>
							<li id="section02_item08"><img src="common/img/section02_img_04.png" alt="子育て"></li>
							<li id="section02_item09"><img src="common/img/section02_img_05.png" alt="風邪"></li>
							<li id="section02_item10"><img src="common/img/section02_img_06.png" alt="乾燥"></li>
						</ul>
						<p id="section02_item11"><span>水分補給に関するワード</span></p>
						<p id="section02_item12"><img src="common/img/section02_img_08.png" alt="潜在顧客の中にある周辺ニーズを拾い、コンテンツ化"></p>
					</div>
				</div>

				<div id="section02_abox02_sp" class="display_sp">
					<p id="section02_item05_sp"><img src="common/img/section02_img_01_sp.gif" alt="潜在顧客層"></p>
					<div id="section02_abox02_01_sp" class="abox">
						<ul>
							<li id="section02_item06_sp"><img src="common/img/section02_img_02_sp.png" alt="脱水症状"></li>
							<li id="section02_item08_sp"><img src="common/img/section02_img_04_sp.png" alt="子育て"></li>
							<li id="section02_item09_sp"><img src="common/img/section02_img_05_sp.png" alt="風邪"></li>
						</ul>
						<p id="section02_item11_sp"><span>水分補給に関するワード</span></p>
						<p id="section02_item12_sp"><img src="common/img/section02_img_08_sp.png" alt="潜在顧客の中にある周辺ニーズを拾い、コンテンツ化"></p>
					</div>
				</div>
			</section>

			<section id="section02_02">
				<div id="section02_abox03" class="abox">
					<p id="section02_item13" class="num_title">02</p>
					<h3>
						<span id="section02_item14" class="title"><span class="quote left">&#8220;</span><em id="section02_item15">2次流入</em>を誘う<br class="display_sp">ソーシャルメディア連携<span class="quote right">&#8221;</span></span>
					</h3>
					<p id="section02_item16" class="lead_text">新たなユーザーとの接点獲得のための方法の１つとして、ブランドサイト訪問者の<br class="display_pc">
						ソーシャルグラフの活用があります。ブランドサイト内でソーシャルメディアと連携した<br class="display_pc">
						アクションを促すことにより、ユーザーのソーシャルグラフを経由した情報伝播、<br class="display_pc">
						２次流入を期待できます。広告出稿コストをかけることなく、<br class="display_pc">
						新規ターゲットへアプローチを行えます。</p>
				</div>

				<div id="section02_abox04" class="abox display_pc">
					<p id="section02_item17"><img src="common/img/section02_img_09.gif" alt="ソーシャルメディア連携 ブランドサイト"></p>
					<p id="section02_item18"><img src="common/img/section02_img_10.png" alt="インスタグラム"></p>
					<p id="section02_item19"><img src="common/img/section02_img_11.png" alt="フェイスブック"></p>
					<p id="section02_item20"><img src="common/img/section02_img_12.png" alt="ツイッター"></p>
					<p id="section02_item21"><img src="common/img/section02_img_13.png" alt="インスタグラム"></p>
					<p id="section02_item22"><img src="common/img/section02_img_14.png" alt="フェイスブック"></p>
					<p id="section02_item23"><img src="common/img/section02_img_15.png" alt="フェイスブック"></p>
					<p id="section02_item24"><img src="common/img/section02_img_16.png" alt="ツイッター"></p>
					<p id="section02_item25"><img src="common/img/section02_img_17.png" alt="2次流入"></p>
				</div>

				<div id="section02_abox04_sp" class="abox display_sp">
					<p id="section02_item17_sp"><img src="common/img/section02_img_09_sp.gif" alt="ソーシャルメディア連携 ブランドサイト"></p>
					<p id="section02_item18_sp"><img src="common/img/section02_img_10_sp.png" alt="インスタグラム"></p>
					<p id="section02_item19_sp"><img src="common/img/section02_img_11_sp.png" alt="フェイスブック"></p>
					<p id="section02_item20_sp"><img src="common/img/section02_img_12_sp.png" alt="ツイッター"></p>
					<p id="section02_item25_sp"><img src="common/img/section02_img_17_sp.png" alt="2次流入"></p>
				</div>
			</section>

			<section id="section02_03">
				<div id="section02_abox05" class="abox">
					<p id="section02_item26" class="num_title">03</p>
					<h3>
						<span id="section02_item27" class="title"><span class="quote left">&#8220;</span>ブランド理解を<br class="display_sp"><em id="section02_item28">促すための表現</em><span class="quote right">&#8221;</span></span>
					</h3>
					<p id="section02_item29" class="lead_text">潜在顧客層はブランドへの興味関心も薄く、ブランド（商品やサービス）に関する記事を<br class="display_pc">
						深く読み込んでくれるわけではありません。情報ニーズの低いユーザーに対しては<br class="display_pc">
						わかりやすい表現を駆使して、理解を促すことが求められます。<br class="display_pc">
						複雑になりやすい「効果」や「開発背景」をよりわかりやすく伝える<br class="display_pc">
						配慮を行っています。</p>
				</div>

				<div id="section02_abox06" class="abox">
					<p id="section02_item30"><img src="common/img/section02_img_18.gif" alt=""></p>
				</div>
			</section>

			<section id="section02_04">
				<div id="section02_abox07" class="abox">
					<p id="section02_item31" class="num_title">04</p>
					<h3>
						<span id="section02_item32" class="title"><span class="quote left">&#8220;</span><em id="section02_item33">ジブンゴト化</em><br class="display_sp">を誘う仕掛け<span class="quote right">&#8221;</span></span>
					</h3>
					<p id="section02_item34" class="lead_text">ブランドサイトと潜在顧客が接触した段階では、ブランドは信頼を得ていません。<br>
						そのブランドを通じてどんなベネフィットをえられるかを具体的に<br class="display_pc">
						想起させるメッセージが必要になります。</p>
				</div>

				<div id="section02_abox08" class="abox display_pc">
					<p id="section02_item35"><img src="common/img/section02_img_19.png" alt=""></p>
					<p id="section02_item36"><img src="common/img/section02_img_20.png" alt=""></p>
					<p id="section02_item37"><img src="common/img/section02_img_21.png" alt="商品・サービスの解説"></p>
					<p id="section02_item38"><img src="common/img/section02_img_20.png" alt=""></p>
					<p id="section02_item39"><img src="common/img/section02_img_22.png" alt="効果の説明"></p>
					<p id="section02_item40"><img src="common/img/section02_img_23.png" alt=""></p>
					<p id="section02_item41"><img src="common/img/section02_img_24.png" alt="体験価値の提示"></p>
				</div>

				<div id="section02_abox08_sp" class="abox display_sp">
					<p id="section02_item35_sp"><img src="common/img/section02_img_19_sp.png" alt=""></p>
					<p id="section02_item36_sp"><img src="common/img/section02_img_20_sp.png" alt=""></p>
					<p id="section02_item37_sp"><img src="common/img/section02_img_21_sp.png" alt="商品・サービスの解説"></p>
					<p id="section02_item38_sp"><img src="common/img/section02_img_20_sp.png" alt=""></p>
					<p id="section02_item39_sp"><img src="common/img/section02_img_22_sp.png" alt="効果の説明"></p>
					<p id="section02_item40_sp"><img src="common/img/section02_img_23_sp.png" alt=""></p>
					<p id="section02_item41_sp"><img src="common/img/section02_img_24_sp.png" alt="体験価値の提示"></p>
				</div>

				<p class="btm_text">ユーザーは体験価値を感じて、<br class="display_sp">初めてそのブランドをジブンゴト化します。</p>
			</section>
		</section>

		<section id="section03" class="contact_section section">
			<h2>お問い合わせ</h2>
			<p class="text">過去のブランドサイト構築実績についても<br class="display_sp">対面でご案内します。<br>
				Webサイトに課題をお感じの際は<br class="display_sp">お気軽にお問い合わせください。</p>
			<p class="btn bottom"><a href="#section08">お問い合わせはこちら</a></p>
			<p class="tel"><a href="tel:0363845184">TEL 03-6384-5184</a></p>
		</section>

		<section id="section04" class="result_section section">
			<h2>メタフェイズの制作実績</h2>
			<div class="list clear">
				<div class="item abox">
					<div class="item_inner">
						<p><img src="common/img/section04_img_01.jpg" alt=""></p>
						<p>眠眠打破ブランドサイト</p>
					</div>
				</div>
				<div class="item abox">
					<div class="item_inner">
						<p><img src="common/img/section04_img_02.jpg" alt=""></p>
						<p>舞妓ふぁん。プロモーションサイト</p>
					</div>
				</div>
				<div class="item abox">
					<div class="item_inner">
						<p><img src="common/img/section04_img_03.jpg" alt=""></p>
						<p>NOV30周年記念サイト</p>
					</div>
				</div>
				<div class="item abox">
					<div class="item_inner">
						<p><img src="common/img/section04_img_04.jpg" alt=""></p>
						<p>ヒューマンアカデミー日本語学校<br>佐賀校プロモーションサイト</p>
					</div>
				</div>
				<div class="item abox">
					<div class="item_inner">
						<p><img src="common/img/section04_img_05.jpg" alt=""></p>
						<p>BE&#64;BRICKシリーズ30</p>
					</div>
				</div>
				<div class="item abox">
					<div class="item_inner">
						<p><img src="common/img/section04_img_06.jpg" alt=""></p>
						<p>Satolax ブランドサイト</p>
					</div>
				</div>
				<div class="item abox">
					<div class="item_inner">
						<p><img src="common/img/section04_img_07.jpg" alt=""></p>
						<p>SANA 暖冷美容 プロモーションサイト</p>
					</div>
				</div>
				<div class="item abox">
					<div class="item_inner">
						<p><img src="common/img/section04_img_08.jpg" alt=""></p>
						<p>コロンビア公式オンラインショップ</p>
					</div>
				</div>
				<div class="item abox">
					<div class="item_inner">
						<p><img src="common/img/section04_img_09.jpg" alt=""></p>
						<p>東京美容外科WEBサイト</p>
					</div>
				</div>
				<div class="item abox">
					<div class="item_inner">
						<p><img src="common/img/section04_img_10.jpg" alt=""></p>
						<p>小田急バスサービスサイト</p>
					</div>
				</div>
				<div class="item abox">
					<div class="item_inner">
						<p><img src="common/img/section04_img_11.jpg" alt=""></p>
						<p>ANA Expeience Japan</p>
					</div>
				</div>
				<div class="item abox">
					<div class="item_inner">
						<p><img src="common/img/section04_img_12.jpg" alt=""></p>
						<p>株式会社シーエーシーコーポレートサイト</p>
					</div>
				</div>
			</div>
		</section>

		<section id="section05" class="contact_section section">
			<h2>お問い合わせ</h2>
			<p class="text">過去のブランドサイト構築実績についても<br class="display_sp">対面でご案内します。<br>
				Webサイトに課題をお感じの際は<br class="display_sp">お気軽にお問い合わせください。</p>
			<p class="btn bottom"><a href="#section08">お問い合わせはこちら</a></p>
			<p class="tel"><a href="tel:0363845184">TEL 03-6384-5184</a></p>
		</section>

		<section id="section06" class="cost_section section">
			<h2>ブランドサイト構築の費用例</h2>
			<div class="cost_table">
				<table>
					<tr>
						<th colspan="2">某化粧品ブランドサイト構築</th>
					</tr>
					<tr>
						<td>
							<dl>
								<dt>制作ボリューム</dt>
								<dd>約  <span class="num">15</span> ページ</dd>
								<dt>編集・ライティング</dt>
								<dd>約  <span class="num">10</span> ページ</dd>
								<dt>プロジェクト期間</dt>
								<dd>約  <span class="num">2.5</span>  ヶ月</dd>
							</dl>
						</td>
						<td>
							<dl>
								<dt>費用合計</dt>
								<dd><span class="num">3,000,000</span>円</dd>
							</dl>
						</td>
					</tr>
				</table>
			</div>
		</section>

		<section id="section07" class="info_section section">
			<h2>メタフェイズ会社情報</h2>
			<div class="info_tab">
				<div class="tab">
					<ul>
						<li>会社概要</li>
						<li>事業内容</li>
						<li>取引先一覧</li>
					</ul>
				</div>
				<div class="content_wrap">
					<div class="content">
						<div class="outline_table">
							<table>
								<tr>
									<th>商　号</th>
									<td>株式会社メタフェイズ　metaphase co.,ltd.</td>
								</tr>
								<tr>
									<th>設　立</th>
									<td>2001年2月7日</td>
								</tr>
								<tr>
									<th>本社所在地</th>
									<td>〒160-0022<br>東京都新宿区新宿5-17-5 <br class="display_sp">ラウンドクロス新宿5丁目ビル6F</td>
								</tr>
								<tr>
									<th>連絡先</th>
									<td>03-6384-5184</td>
								</tr>
								<tr>
									<th>資本金</th>
									<td>30,000,000円</td>
								</tr>
								<tr>
									<th>役　員</th>
									<td>代表取締役	 城野 誠大（シロノ マサヒロ）</td>
								</tr>
								<tr>
									<th>社員数</th>
									<td>55名</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="content">
						<div class="business_list">
							<dl>
								<dt>Webインテグレーション事業部</dt>
								<dd>企業のビジネス課題を解決するため、ノウハウを培った様々な職種のスペシャリスト達が集まり、付加価値のあるWebサイトを提案・制作します。</dd>
								<dt>Webリテンション事業部</dt>
								<dd>一括したWebサイトの運用を依頼できるアウトソース先だけでなく、サイトの最終目標を理解した上で信頼できる更新・運用・サポート業務を行っています。</dd>
								<dt>クロール型求人サイト構築事業</dt>
								<dd>Web業界で培ってきた長年の経験とノウハウを活かし、業界に特化した人材集客サービスと、集客に適した課金システム・及び求人検索エンジンのOEM提供を行っています。</dd>
							</dl>
						</div>
					</div>
					<div class="content">
						<div class="client_list clear">
							<ul>
								<li>株式会社再春館製薬所</li>
								<li>株式会社コロンビアスポーツウェアジャパン</li>
								<li>常盤薬品工業株式会社</li>
								<li>株式会社ノエビア</li>
								<li>コニカミノルタ株式会社</li>
								<li>株式会社ドクターシーラボ</li>
								<li>大塚製薬株式会社</li>
								<li>ヒューマンアカデミー株式会社</li>
								<li>キリン株式会社</li>
								<li>東京急行電鉄</li>
								<li>株式会社エイチ・アイ・エス</li>
								<li class="etc">他、数百社</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="section08" class="form_section section">
			<h2>お問い合わせ</h2>
			<p class="text">過去のブランドサイト構築実績についても対面でご案内します。<br>
				Webサイトに課題をお感じの際はお気軽にお問い合わせください。</p>
			<p class="tel"><a href="tel:0363845184">TEL 03-6384-5184</a></p>

			<div class="form_area">
				<form action="./#section08" method="post">
					<table>
						<tr>
							<th>御社名</th>
							<td><?php if(! empty($err_msg["company"])):?><p class="error"><?php echo $err_msg["company"]?></p><?php endif?><input type="text" name="company" value="<?php echo $vars["company"]?>" placeholder="入力してください"></td>
						</tr>
						<tr>
							<th>お名前</th>
							<td><?php if(! empty($err_msg["names"])):?><p class="error"><?php echo $err_msg["names"]?></p><?php endif?><input type="text" name="names" value="<?php echo $vars["names"]?>" placeholder="入力してください"></td>
						</tr>
						<tr>
							<th>お電話番号</th>
							<td><?php if(! empty($err_msg["tel"])):?><p class="error"><?php echo $err_msg["tel"]?></p><?php endif?>
								<input type="tel" name="tel1" value="<?php echo $vars["tel1"]?>" placeholder="入力してください" class="input_tel"> －
								<input type="tel" name="tel2" value="<?php echo $vars["tel2"]?>" placeholder="入力してください" class="input_tel"> －
								<input type="tel" name="tel3" value="<?php echo $vars["tel3"]?>" placeholder="入力してください" class="input_tel">
							</td>
						</tr>
						<tr>
							<th>メールアドレス</th>
							<td><?php if(! empty($err_msg["email"])):?><p class="error"><?php echo $err_msg["email"]?></p><?php endif?><input type="email" name="email" value="<?php echo $vars["email"]?>" placeholder="入力してください"></td>
						</tr>
						<tr>
							<th>URL（任意）</th>
							<td><?php if(! empty($err_msg["url"])):?><p class="error"><?php echo $err_msg["url"]?></p><?php endif?><input type="url" name="url" value="<?php echo $vars["url"]?>" placeholder="入力してください"></td>
						</tr>
						<tr>
							<th>ご相談内容（任意）</th>
							<td><?php if(! empty($err_msg["request_type"])):?><p class="error"><?php echo $err_msg["request_type"]?></p><?php endif?>
								<select name="request_type" id="">
									<option value="">選択してください</option>
									<?php $html->options(array("options"=>$lists["request_type"],"selected"=>$vars["request_type"]))?>
								</select>
							</td>
						</tr>
						<tr>
							<th>制作物の種類（任意）</th>
							<td><?php if(! empty($err_msg["request_site_type"])):?><p class="error"><?php echo $err_msg["request_site_type"]?></p><?php endif?>
								<select name="request_site_type" id="">
									<option value="">選択してください</option>
									<?php $html->options(array("options"=>$lists["request_site_type"],"selected"=>$vars["request_site_type"]))?>
								</select>
							</td>
						</tr>
						<tr>
							<th>ご予算（任意）</th>
							<td><?php if(! empty($err_msg["budget"])):?><p class="error"><?php echo $err_msg["budget"]?></p><?php endif?><input type="text" name="budget" value="<?php echo $vars["budget"]?>" placeholder="入力してください"></td>
						</tr>
						<tr>
							<th>お問い合わせ内容</th>
							<td><?php if(! empty($err_msg["contents"])):?><p class="error"><?php echo $err_msg["contents"]?></p><?php endif?><textarea name="contents" id="" placeholder="入力してください"><?php echo $vars["contents"]?></textarea></td>
						</tr>
					</table>

					<div class="personal_area_wrap">
						<div class="personal_area">
							<p class="ttl">個人情報の取り扱いについて</p>
							<div class="name">
								<p>株式会社メタフェイズ</p>
								<p>代表取締役 城野 誠大</p>
							</div>
							<dl>
								<dt>1.（取得及び利用目的）</dt>
								<dd>株式会社メタフェイズ（以下「弊社」という）は、弊社の商品サービスに関するご案内を希望されるみなさまの氏名、メールアドレスなどの個人を識別できる情報を取得します。取得の目的は、弊社の商品サービスのご案内を送付させていただくためであり、それ以外の目的で利用いたしません。</dd>
								<dt>2.（預託・提供）</dt>
								<dd>弊社は、法令で定める場合を除き個人を特定できる情報（氏名、メールアドレス等）を取得目的の範囲を超えて当該個人の同意なしに第三者への提供や預託はいたしません。</dd>
								<dt>3.（情報の開示・訂正・削除）</dt>
								<dd>弊社は、個人情報の開示、訂正、追加又は削除、利用目的の通知、利用又は提供の拒否についての依頼を受けた場合は、弊社の手続きに従って速やかに対応します。</dd>
								<dt>4.（個人情報提供の任意性）</dt>
								<dd>個人情報の弊社への提供は、お客様の同意に基づく任意ですが、必要な情報が提供されない場合、弊社の商品サービスをご案内できない場合もありうることをご承知ください。</dd>
								<dt>5.（個人情報保護管理者及び問合せ先）</dt>
								<dd>当社では、個人情報を適切に保護するための管理者を下記の者が担当いたしております。また個人情報管理に関する問い合わせや開示、訂正または削除の依頼は、下記までご連絡下さい。<br>
								株式会社メタフェイズ<br>
								個人情報に関する苦情及び相談窓口<br>
								〒160-0022　東京都新宿区新宿5-17-5 ラウンドクロス新宿5丁目ビル6F<br>
								個人情報保護管理者　折本 裕司<br>
								E-mail：<a href="mailto:%70%72%69%76%61%63%79%40%6D%65%74%61%70%68%73%65.%63%6f.%6a%70"><img src="common/img/mailaddress.gif" alt="メールアドレス画像"></a><br>
								TEL：03-6384-5184　FAX：03-6384-5183<br>
								</dd>
							</dl>
						</div>
					</div>

					<div class="personal_check">
						<?php if(! empty($err_msg["privacy"])):?><p class="error"><?php echo $err_msg["privacy"]?></p><?php endif?>
						<p class="checkbox"><input type="checkbox" name="privacy" value="1" id="privacy"<?php if($vars["privacy"]==1){echo " checked";}?>><label for="privacy">個人情報の取り扱いに同意する</label></p>
					</div>

					<p class="submit_btn"><input type="submit" value="確認する"></p>

					<input type="hidden" name="mode" value="confirm" />
				</form>
			</div>
		</section>
	</main>

	<footer id="footer">
		<p class="logo"><img src="common/img/footer_logo_01.png" alt="web creative agency metaphase"></p>
		<small>Copyright&copy; METAPHASE Co.,Ltd All rights reserved.</small>
	</footer>
</div>

</body>
</html>
