<!doctype html>
<html class="no-js" lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>プロフィール</title>

<link rel="icon" sizes="16x16 32x32 48x48 64x64" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/favicon.ico"/>
<!--
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/favicon-16x16.png">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
-->

<link href="//fonts.googleapis.com/css?family=Archivo+Narrow" rel="stylesheet">
<link rel="stylesheet" href="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/css/app.20170502-03.css">

<!-- All in One SEO Pack 2.3.12 by Michael Torbert of Semper Fi Web Design[220,237] -->
<link rel="canonical" href="<?php echo SITE_PATH?>recruit/apply/" />
<meta property="og:title" content="プロフィール" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo SITE_PATH?>recruit/apply/" />
<meta property="og:image" content="<?php echo SITE_PATH?>wp/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" />
<meta property="og:site_name" content="メンバー紹介" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="プロフィール" />
<meta name="twitter:image" content="<?php echo SITE_PATH?>wp/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" />
<meta itemprop="image" content="<?php echo SITE_PATH?>wp/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" />
<!-- /all in one seo pack -->
<link rel='dns-prefetch' href='//ajax.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel='stylesheet' id='contact-form-7-css'  href='<?php echo SITE_PATH?>wp/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.7' type='text/css' media='all' />
<link rel='stylesheet' id='cf7cf-style-css'  href='<?php echo SITE_PATH?>wp/wp-content/plugins/cf7-conditional-fields/style.css?ver=1.1' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-confirm-css'  href='<?php echo SITE_PATH?>wp/wp-content/plugins/contact-form-7-add-confirm/includes/css/styles.css?ver=4.6.2' type='text/css' media='all' />
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'></script>
<link rel='https://api.w.org/' href='<?php echo SITE_PATH?>wp-json/' />
<link rel='shortlink' href='https://goo.gl/Tm6FNH' />
<link rel="alternate" type="application/json+oembed" href="<?php echo SITE_PATH?>wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.metaphase.co.jp%2Frecruit%2Fapply%2F" />
<link rel="alternate" type="text/xml+oembed" href="<?php echo SITE_PATH?>wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.metaphase.co.jp%2Frecruit%2Fapply%2F&#038;format=xml" />
<style>
.error{
color:#ff0000;
}
</style>
<script src="https://use.typekit.net/cum0daj.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-4728089-1', 'auto');
ga('send', 'pageview');
</script>

</head>
<body id="top">

<section id="bg" data-src="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/mainVisual/" class="section section__bg"></section>
<svg style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<symbol id="icon-new_window" viewBox="0 0 10 10">
<title>new_window</title>
<path class="st0" d="M9,9H1V1h2V0H0v10h10V7H9V9z M6,0v1h2v1h1v2h1V0H6z M7,3h1V2H7V3z M6,4h1V3H6V4z M5,5h1V4H5V5z M4,6h1V5H4V6z M3,7h1V6H3V7z"/>
</symbol>
<symbol id="icon-arrow_01" viewBox="0 0 52 52">
<title>arrow_01</title>
<path class="st0" d="M26,0C11.6,0,0,11.6,0,26c0,14.4,11.6,26,26,26c14.4,0,26-11.6,26-26C52,11.6,40.4,0,26,0z M31,33h-1v1h-1v1
h-1v1h-1v1h-1v-1h-1v-1h-1v-1h-1v-1h-1v-1h1v1h1v1h1v1h1V15h1v20h1v-1h1v-1h1v-1h1V33z M26,35v1h1v-1H26z"/>
</symbol>
<symbol id="icon-arrow_02" viewBox="0 0 18 44">
<title>arrow_02</title>
<path class="st0" d="M10,0H8v40H6v2h2v2h2v-2h2v-2h-2V0z M4,38v2h2v-2H4z M12,40h2v-2h-2V40z M14,38h2v-2h-2V38z M2,36v2h2v-2H2z
M0,34v2h2v-2H0z M16,34v2h2v-2H16z"/>
</symbol>
<symbol id="icon-arrow_03" viewBox="0 0 17 7">
<title>arrow_03</title>
<path class="st0" d="M16,3V2h-1v1H0v1h15v1h1V4h1V3H16z M14,6h1V5h-1V6z M14,1v1h1V1H14z M13,0v1h1V0H13z M13,7h1V6h-1V7z"/>
</symbol>
<symbol id="icon-arrow_04" viewBox="0 0 9 6">
<title>arrow_04</title>
<path class="st0" d="M8,0v1H7v1H6v1H5v1H4V3H3V2H2V1H1V0H0v1v1h1v1h1v1h1v1h1v1h1V5h1V4h1V3h1V2h1V1V0H8z"/>
</symbol>
<symbol id="icon-arrow_05" viewBox="0 0 8 13">
<title>arrow_05</title>
<path class="st0" d="M8,6.5L1.4,0L0,1.4l5.1,5l-5.1,5L1.4,13l5.1-5l0,0L8,6.5z"/>
</symbol>
<symbol id="icon-arrow_06" viewBox="0 0 8 13">
<title>arrow_06</title>
<path class="st0" d="M0,6.5L6.5,13L8,11.5l-5.1-5l5.1-5L6.5,0L1.4,5l0,0L0,6.5z"/>
</symbol>
<symbol id="icon-arrow_07" viewBox="0 0 15 9">
<title>arrow_07</title>
<path class="st0" d="M14,0v1h-1v1h-1v1h-1v1h-1v1H9v1H8v1H7V6H6V5H5V4H4V3H3V2H2V1H1V0H0v0.5V2h1v1h1v1h1v1h1v1h1v1h1v1h1v1h1V8h1V7h1V6h1V5h1V4h1V3h1V2h1V0.5V0H14z"/>
</symbol>
<symbol id="icon-arrow_08" viewBox="0 0 4 7">
<title>arrow_08</title>
<path class="st0" d="M1,6h1V5H1V6z M1,1v1h1V1H1z M0,0v1h1V0H0z M0,7h1V6H0V7z M3,3v1h1V3H3z M2,2v1h1V2H2z M2,5h1V4H2V5z"/>
</symbol>
<symbol id="icon-arrow_09" viewBox="0 0 22 46">
<title>arrow_09</title>
<path class="st0" d="M12,2V0h-2v2H8v2h2v42h2V4h2V2H12z M14,4v2h2V4H14z M6,6h2V4H6V6z M4,8h2V6H4V8z M16,6v2h2V6H16z M2,10h2V8H2V10z M18,8v2h2V8H18z M20,10v2h2v-2H20z M0,12h2v-2H0V12z"/>
</symbol>
<symbol id="icon-menu" viewBox="0 0 52 42">
<title>menu</title>
<path class="st0" d="M0,0v4h52V0H0z M0,23h52v-4H0V23z M0,42h52v-4H0V42z"/>
</symbol>
<symbol id="icon-close" viewBox="0 0 40.3 40.3">
<title>close</title>
<polygon class="st0" points="40.3,36.8 23.7,20.2 40.3,3.5 36.8,0 20.2,16.6 3.5,0 0,3.5 16.6,20.2 0,36.8 3.5,40.3 20.2,23.7
36.8,40.3 	"/>
</symbol>

<symbol id="icon-metaphase" viewBox="0 0 117.9 118">
<title>metaphase</title>
<path class="st0" d="M117.9,59c0,32.6-26.4,59-59,59C26.4,118,0,91.6,0,59C0,26.4,26.4,0,59,0C91.5,0,117.9,26.4,117.9,59z
M103.4,80c-4.1-0.9-10.8-2.4-15.7-6.6c-1.1-1-3-2.2-6.1-3.2c-3.1-1-8.3-2.2-11.6-1.2c1.3,0.9,2.8,3.5,2.8,5.8
c0,8.8-11.8,10.7-17.9,10.7c-8.7,0-16-4.2-17.4-8c-0.6-1.5-2.2-3.2-4-4c-3.1-1.5-7.6-1.4-10.1-0.3c-2.6,1-6.2,2-8.3,2.2
c-2.1,0.2-3.7,0.2-5.8,1.4c-2.1,1.1-2,3.3-1.5,4.7C16.4,101.2,36.1,115,59,115c20.7,0,38.8-11.3,48.4-28
C109.7,82.7,107.5,80.9,103.4,80z M114.7,54C112.1,25.4,88.2,3,59,3C31.2,3,8.1,23.4,3.8,50c-0.5,2,0.1,3.1,1.4,3.6
c3.5,1.2,16.2-0.5,22.1,4.4c1.8,1.4,2.8,2.3,3.4,3.5c0.6,1.2,2.6,4.3,8.9,4.3c1.8,0,4.1-0.7,5.2-1.1c1.1-0.5,2.6-0.9,3.9-1.1
c-1.3-1.3-1.8-3.3-1.8-4.6c0-2.9,3.5-7.2,11.1-8.5c-3.1-0.7-5.4-0.7-7.6-0.3c-2.2,0.5-9.6,1.2-14.5-1.5c-2.7-1.5-5.4-3.5-5.4-6.7
c0-3.2,4.1-7.4,13.9-7.4c3,0,8.1,0.5,12.2,3.9c1.2,1,4.7,2.9,10.5,2.9c1.8,0,3.5-0.4,4.9-0.7c1.4-0.3,3-0.4,4.7-0.4
c6.6,0,15.8,3,15.8,9.1c0,3.2-3.7,7-10.8,8c3.7,1.1,8.1,1.8,12.3,1.1c4.2-0.7,7.4-1.3,12.5-0.5c2.1,0.3,5.5,0.3,6.5-0.1
C113.9,57.3,114.9,56.9,114.7,54z"/>
</symbol>
<symbol id="icon-facebook" viewBox="0 0 6.9 13.9">
<title>facebook</title>
<path class="st0" d="M2,13.9V7.6H0V5.1h2V3.3C2,1.2,3.3,0,5.1,0c0.9,0,1.6,0.1,1.8,0.1v2.2H5.6c-1,0-1.2,0.5-1.2,1.2v1.6h2.3
L6.5,7.6h-2v6.4H2z"/>
</symbol>
<symbol id="icon-instagram" viewBox="0 0 44 44">
<title>instagram</title>
<path class="st0" d="M43.9,13.4c-0.1-2.1-0.3-4.1-1.2-6.1c-1.5-3.6-4.3-5.7-8-6.7c-1.7-0.4-3.4-0.5-5.1-0.5c-0.1,0-0.3,0-0.4-0.1
H14.8c-0.5,0-0.9,0.1-1.4,0.1c-2.1,0.1-4.1,0.3-6.1,1.2c-3.6,1.5-5.7,4.3-6.7,8c-0.4,1.7-0.5,3.4-0.5,5.1c0,0.1,0,0.3-0.1,0.4v14.5
c0,0.5,0.1,0.9,0.1,1.4c0.1,2.1,0.3,4.1,1.2,6.1c1.5,3.6,4.3,5.7,8,6.7c1.7,0.4,3.4,0.5,5.1,0.5c0.1,0,0.3,0,0.4,0.1h14.5
c0.5,0,0.9-0.1,1.4-0.1c2.1-0.1,4.1-0.3,6.1-1.2c3.6-1.5,5.7-4.3,6.7-8c0.4-1.7,0.5-3.4,0.5-5.1c0-0.1,0-0.3,0.1-0.4V14.8
C44,14.3,43.9,13.8,43.9,13.4z M39.9,30.9c0,1.1-0.2,2.2-0.5,3.2c-0.9,3.2-3,5-6.2,5.5c-1.9,0.3-3.8,0.3-5.7,0.4
c-2.6,0.1-5.1,0.1-7.7,0c-2.6,0-5.2-0.1-7.8-0.2c-2.3-0.1-4.3-0.9-5.9-2.7c-1.1-1.3-1.7-2.8-1.9-4.4C4.1,31,4,29.2,4,27.5
C4,24.9,4,22.4,4,19.8c0-2.6,0.1-5.2,0.2-7.8c0.1-2.3,0.9-4.3,2.7-5.9C8.2,5,9.7,4.5,11.3,4.3C13,4.1,14.8,4,16.5,4
C19.1,4,21.6,4,24.2,4c2.6,0,5.2,0.1,7.8,0.2c2.3,0.1,4.3,0.9,5.9,2.7c1.1,1.3,1.7,2.8,1.9,4.4c0.2,1.7,0.2,3.4,0.3,5.2
c0,1.8,0,3.7,0,5.5c0,0,0,0,0.1,0C40,25,40,27.9,39.9,30.9z M22,10.7c-6.3,0-11.3,5-11.3,11.3c0,6.2,5,11.3,11.3,11.3
c6.3,0,11.3-5,11.3-11.3C33.3,15.8,28.3,10.7,22,10.7z M22,29.3c-4,0-7.3-3.3-7.3-7.3c0-4,3.3-7.3,7.4-7.3c4,0,7.3,3.3,7.2,7.4
C29.3,26,26,29.3,22,29.3z M33.7,7.6c-1.4,0-2.6,1.2-2.6,2.6c0,1.5,1.2,2.7,2.7,2.7c1.5,0,2.7-1.2,2.7-2.7
C36.4,8.8,35.2,7.6,33.7,7.6z"/>
</symbol>

<symbol id="icon-twitter" viewBox="0 0 13 10">
<title>twitter</title>
<path class="st0" d="M4.1,10c4.9,0,7.6-3.8,7.6-7.2c0-0.1,0-0.2,0-0.3c0.5-0.4,1-0.8,1.3-1.3c-0.5,0.2-1,0.3-1.5,0.4
c0.6-0.3,1-0.8,1.2-1.4c-0.5,0.3-1.1,0.5-1.7,0.6C10.5,0.3,9.8,0,9,0C7.5,0,6.3,1.1,6.3,2.5c0,0.2,0,0.4,0.1,0.6
C4.2,3,2.2,2,0.9,0.5C0.7,0.8,0.5,1.3,0.5,1.7c0,0.9,0.5,1.6,1.2,2.1c-0.4,0-0.8-0.1-1.2-0.3c0,0,0,0,0,0c0,1.2,0.9,2.2,2.1,2.5
C2.4,6.1,2.2,6.1,2,6.1c-0.2,0-0.3,0-0.5,0c0.3,1,1.3,1.7,2.5,1.8C3,8.5,1.9,8.9,0.6,8.9c-0.2,0-0.4,0-0.6,0C1.2,9.6,2.6,10,4.1,10"
/>
</symbol>

<symbol id="icon-tumblr" viewBox="0 0 7 12">
<title>tumblr</title>
<path class="st0" d="M1.4,8.7l0-3.6c-0.5,0-0.9,0-1.4,0C0,4.5,0,4,0,3.4C1,3.1,1.9,2,2.3,0.8C2.4,0.3,2.4,0,2.4,0S4,0,4.3,0
c0,1,0,2.1,0,3.1c0.8,0,1.5,0,2.3,0v2C5.9,5,5.1,5,4.3,5c0,0.4,0,0.8,0,1.1c0,0.8,0,1.5,0,2.3c0,2,1.9,1.2,2.1,1.1L7,11.4
C6.5,12,1.4,13.2,1.4,8.7z"/>
</symbol>

<symbol id="icon-logo_metaphase" viewBox="0 0 160 32">
<title>logo_metaphase</title>
<path d="M39.467 13.76h-14.72v10.347h3.733v-8.107h4.587v8.107h3.947v-8.107h2.24c2.027 0 2.56 0.427 2.56 1.92v6.187h3.733v-6.613c0-3.093-2.667-3.733-6.080-3.733zM46.187 18.987c0.107 3.2 2.453 5.227 7.36 5.227h5.547v-2.027h-5.12c-2.987 0-3.627-0.533-3.627-2.027h8.747v-2.133h-8.64c0-1.6 1.067-2.027 3.627-2.027h5.013v-2.133h-5.547c-2.667-0.107-7.36 0.427-7.36 5.12zM66.453 11.307l-3.733-1.387v3.84h-2.667v2.027h2.667v5.12c0 2.027 1.707 3.2 4.373 3.2h3.413v-2.133h-2.453c-1.28 0-1.707-1.067-1.707-2.027v-4.267h4.053v-2.027h-4.053l0.107-2.347zM79.573 13.76h-7.893v2.133h6.4c2.773 0 3.52 0.427 3.52 2.027h-5.12c-3.947 0-5.76 0.853-5.76 3.2 0.107 1.92 1.707 3.093 4.48 3.093h10.24v-6.507c0-3.947-4.16-3.947-5.867-3.947zM81.707 22.080h-4.907c-1.28 0-2.133-0.32-2.133-1.173 0-1.067 1.173-1.173 2.347-1.173h4.587l0.107 2.347zM93.867 13.76h-7.36v12.8h3.733v-2.347h3.413c4.907 0 7.36-1.813 7.36-5.12 0-3.627-3.093-5.333-7.147-5.333zM91.733 21.973h-1.493v-6.080h2.453c3.84 0 4.16 1.28 4.267 2.987 0.107 1.92-0.64 3.093-5.227 3.093zM108.8 13.76h-3.307v-2.453l-3.627-1.387v14.187h3.627v-8.107h3.093c2.133 0 2.667 0.533 2.667 1.813v6.293h3.84v-6.613c-0.107-3.2-3.307-3.733-6.293-3.733zM124.587 13.76h-7.893v2.133h6.4c2.773 0 3.52 0.427 3.52 2.027h-5.12c-3.947 0-5.76 0.853-5.76 3.2 0.107 1.92 1.707 3.093 4.48 3.093h10.24v-6.507c0-3.947-4.16-3.947-5.867-3.947zM126.72 22.080h-4.907c-1.28 0-2.133-0.32-2.133-1.173 0-1.067 1.173-1.173 2.347-1.173h4.587l0.107 2.347zM140.373 17.813h-3.52c-1.28 0-1.813-0.32-1.92-0.96-0.107-0.747 0.533-1.067 2.027-1.067h7.36v-2.133h-8.213c-3.413 0-5.12 1.067-5.12 3.307 0 3.2 3.84 2.88 5.013 2.88h3.413c1.387 0 2.133 0.32 2.133 1.067s-0.747 1.067-2.133 1.067h-8.107v2.027h9.813c2.453 0 3.947-1.387 3.947-3.413 0.107-2.24-2.24-2.773-4.693-2.773zM153.387 15.893h5.013v-2.133h-5.547c-2.667 0-7.36 0.533-7.36 5.227 0.107 3.2 2.453 5.227 7.36 5.227h5.547v-2.027h-5.12c-2.987 0-3.627-0.533-3.627-2.027h8.747v-2.133h-8.64c0-1.707 1.067-2.133 3.627-2.133zM26.987 7.68l0.64-2.987 0.64 2.987h1.067l1.173-4.053h-1.067l-0.64 2.88-0.64-2.88h-1.067l-0.533 2.88-0.64-2.88h-1.173l1.173 4.053h1.067zM32.853 7.787c0.64 0 1.173-0.213 1.493-0.64 0.213-0.213 0.32-0.533 0.427-0.747h-1.067c-0.107 0.107-0.107 0.213-0.213 0.32s-0.32 0.213-0.533 0.213-0.427 0-0.533-0.107c-0.213-0.213-0.427-0.427-0.427-0.853h2.88c0-0.32 0-0.64 0-0.853 0-0.32-0.213-0.64-0.32-0.853-0.213-0.32-0.427-0.427-0.64-0.64-0.32-0.107-0.64-0.213-0.96-0.213-0.533 0-1.067 0.213-1.387 0.533-0.32 0.427-0.533 0.96-0.533 1.6 0 0.747 0.213 1.28 0.64 1.6 0.107 0.427 0.64 0.64 1.173 0.64zM32.213 4.587c0.107-0.107 0.32-0.213 0.64-0.213 0.213 0 0.427 0.107 0.64 0.213s0.213 0.32 0.32 0.64h-1.813c-0.107-0.213 0-0.427 0.213-0.64zM36.267 7.147c0.107 0.213 0.32 0.32 0.427 0.427 0.213 0.107 0.427 0.213 0.853 0.213 0.533 0 0.96-0.213 1.28-0.64s0.427-0.96 0.427-1.6-0.107-1.067-0.427-1.493c-0.32-0.427-0.747-0.533-1.28-0.533-0.32 0-0.533 0.107-0.747 0.213-0.107 0.107-0.32 0.213-0.427 0.427v-2.027h-1.067v5.44h1.067l-0.107-0.427zM36.373 4.907c0.107-0.32 0.427-0.533 0.747-0.533 0.427 0 0.64 0.213 0.747 0.533 0.107 0.213 0.107 0.427 0.107 0.747s-0.107 0.64-0.213 0.853c-0.107 0.213-0.32 0.32-0.64 0.32s-0.533-0.107-0.747-0.32c0-0.213-0.107-0.533-0.107-0.853s0.107-0.533 0.107-0.747zM43.627 7.787c0.64 0 1.173-0.213 1.493-0.747 0.213-0.32 0.32-0.64 0.32-0.96h-1.067c0 0.213-0.107 0.427-0.213 0.533s-0.32 0.213-0.533 0.213c-0.32 0-0.533-0.213-0.747-0.533-0.107-0.213-0.107-0.427-0.107-0.747s0-0.533 0.107-0.747c0.107-0.32 0.427-0.533 0.747-0.533 0.213 0 0.427 0.107 0.533 0.213s0.107 0.213 0.107 0.427h1.067c-0.107-0.533-0.213-0.96-0.533-1.173s-0.747-0.32-1.28-0.32c-0.64 0-1.067 0.213-1.387 0.533-0.32 0.427-0.533 0.96-0.533 1.6s0.107 1.173 0.427 1.493c0.533 0.533 0.96 0.747 1.6 0.747zM47.253 5.76c0-0.32 0-0.533 0.107-0.747 0.107-0.32 0.427-0.427 0.853-0.427h0.107c0 0 0.107 0 0.213 0v-1.067h-0.107c-0.32 0-0.533 0.107-0.747 0.213-0.107 0.107-0.213 0.32-0.427 0.533v-0.64h-0.96v4.053h1.067l-0.107-1.92zM50.88 7.787c0.64 0 1.173-0.213 1.493-0.64 0.213-0.213 0.32-0.533 0.427-0.747h-1.067c-0.107 0.107-0.107 0.213-0.213 0.32s-0.32 0.213-0.533 0.213-0.427 0-0.533-0.107c-0.213-0.213-0.427-0.427-0.427-0.853h2.88c0-0.32 0-0.64 0-0.853 0-0.32-0.107-0.64-0.32-0.853-0.213-0.32-0.427-0.427-0.747-0.64-0.32-0.107-0.64-0.213-0.96-0.213-0.64 0-1.067 0.213-1.387 0.533-0.32 0.427-0.533 0.96-0.533 1.6 0 0.747 0.213 1.28 0.64 1.6 0.213 0.427 0.747 0.64 1.28 0.64zM50.133 4.587c0.107-0.107 0.32-0.213 0.64-0.213 0.213 0 0.427 0.107 0.64 0.213s0.213 0.32 0.32 0.64h-1.813c0-0.213 0.107-0.427 0.213-0.64zM55.36 5.12c-0.107 0-0.213 0.107-0.427 0.107h-0.32c-0.427 0.107-0.747 0.107-0.96 0.213-0.427 0.213-0.533 0.64-0.533 1.067s0.107 0.64 0.32 0.853c0.213 0.213 0.533 0.32 0.853 0.32s0.533-0.107 0.747-0.213c0.213 0 0.427-0.107 0.533-0.32 0 0.107 0 0.213 0 0.213 0 0.107 0 0.107 0.107 0.213h1.173v-0.107c-0.107 0-0.107-0.107-0.107-0.107s0-0.107-0.107-0.32c0-0.213 0-0.32 0-0.427v-1.813c0-0.533-0.213-0.853-0.533-0.96-0.32-0.213-0.747-0.213-1.173-0.213-0.64 0-1.173 0.213-1.387 0.533-0.213 0.107-0.32 0.427-0.32 0.747h0.96c0-0.107 0.107-0.32 0.107-0.32 0.107-0.107 0.32-0.213 0.533-0.213s0.427 0 0.533 0.107 0.213 0.213 0.213 0.32c0 0.107-0.107 0.213-0.213 0.32zM55.573 6.080c0 0.32-0.107 0.64-0.32 0.747s-0.427 0.213-0.64 0.213c-0.107 0-0.32 0-0.32-0.107-0.107-0.107-0.107-0.213-0.107-0.427s0.107-0.32 0.213-0.427c0.107-0.107 0.213-0.107 0.533-0.107h0.213c0.107 0 0.213 0 0.32-0.107 0.107 0 0.107-0.107 0.213-0.107v0.32h-0.107zM58.667 2.453h-1.067v1.173h-0.533v0.747h0.533v2.453c0 0.213 0.107 0.427 0.213 0.533 0.213 0.213 0.533 0.32 1.067 0.213h0.533v-0.64h-0.213c-0.213 0-0.32 0-0.427-0.107 0 0-0.107-0.107-0.107-0.32v-2.133h0.64v-0.747h-0.64v-1.173zM61.12 2.133h-1.067v0.96h1.067v-0.96zM61.12 3.627h-1.067v4.053h1.067v-4.053zM64.32 7.68l1.493-4.053h-1.173l-0.853 2.987-0.853-2.987h-1.173l1.493 4.053h1.067zM68.053 7.787c0.64 0 1.173-0.213 1.493-0.64 0.213-0.213 0.32-0.533 0.427-0.747h-1.067c-0.107 0.107-0.107 0.213-0.213 0.32s-0.32 0.213-0.533 0.213c-0.213 0-0.427 0-0.533-0.107-0.213-0.213-0.427-0.427-0.427-0.853h2.88c0-0.32 0-0.64 0-0.853 0-0.32-0.107-0.64-0.32-0.853-0.213-0.32-0.427-0.427-0.64-0.64-0.32-0.107-0.64-0.213-0.96-0.213-0.64 0-1.067 0.213-1.387 0.533-0.32 0.427-0.533 0.96-0.533 1.6 0 0.747 0.213 1.28 0.64 1.6 0.107 0.427 0.64 0.64 1.173 0.64zM67.413 4.587c0.107-0.107 0.32-0.213 0.64-0.213 0.213 0 0.427 0.107 0.64 0.213s0.213 0.32 0.32 0.64h-1.813c-0.107-0.213 0-0.427 0.213-0.64zM74.56 5.12c-0.107 0-0.213 0.107-0.427 0.107h-0.32c-0.427 0.107-0.747 0.107-0.96 0.213-0.427 0.213-0.533 0.64-0.533 1.067s0.107 0.64 0.32 0.853c0.213 0.213 0.533 0.32 0.853 0.32s0.533-0.107 0.747-0.213c0.32 0 0.427-0.107 0.64-0.32 0 0.107 0 0.213 0 0.213 0 0.107 0 0.107 0.107 0.213h1.173v-0.107c-0.107 0-0.107-0.107-0.107-0.107s-0.107-0.107-0.107-0.32c0-0.213 0-0.32 0-0.427v-1.813c0-0.533-0.213-0.853-0.533-0.96-0.32-0.213-0.747-0.213-1.173-0.213-0.64 0-1.173 0.213-1.387 0.533-0.32 0.107-0.427 0.427-0.427 0.747h0.96c0-0.107 0.107-0.32 0.107-0.32 0.107-0.107 0.32-0.213 0.533-0.213s0.427 0 0.533 0.107 0.213 0.213 0.213 0.32c0.107 0.107 0 0.213-0.213 0.32zM74.88 6.080c0 0.32-0.107 0.64-0.32 0.747s-0.427 0.213-0.64 0.213c-0.107 0-0.32 0-0.32-0.107-0.107-0.107-0.107-0.213-0.107-0.427s0.107-0.32 0.213-0.427 0.213-0.107 0.533-0.107h0.213c0.107 0 0.213 0 0.32-0.107 0.107 0 0.107-0.107 0.213-0.107l-0.107 0.32zM78.293 7.68c0.32 0 0.64-0.107 0.853-0.213 0.107-0.107 0.213-0.213 0.32-0.427v0.213c0 0.427 0 0.64-0.107 0.853-0.213 0.32-0.427 0.427-0.853 0.427-0.32 0-0.427 0-0.533-0.107s-0.107-0.213-0.213-0.32h-1.173c0 0.427 0.213 0.747 0.533 0.96s0.747 0.32 1.28 0.32c0.853 0 1.387-0.213 1.707-0.64 0.213-0.213 0.32-0.64 0.32-1.173v-3.947h-1.067v0.533c-0.107-0.32-0.32-0.533-0.64-0.64-0.107-0.107-0.32-0.107-0.533-0.107-0.533 0-0.96 0.213-1.28 0.64s-0.427 0.96-0.427 1.493c0 0.64 0.107 1.173 0.427 1.493 0.427 0.427 0.853 0.64 1.387 0.64zM77.76 4.907c0.107-0.32 0.427-0.533 0.747-0.533s0.533 0.107 0.64 0.32c0.107 0.213 0.213 0.533 0.213 0.853 0 0.427-0.107 0.747-0.213 0.853-0.213 0.213-0.427 0.32-0.64 0.32-0.32 0-0.64-0.213-0.747-0.533-0.107-0.213-0.107-0.427-0.107-0.64s0-0.427 0.107-0.64zM83.093 7.787c0.64 0 1.173-0.213 1.493-0.64 0.213-0.213 0.32-0.533 0.427-0.747h-1.067c-0.107 0.107-0.107 0.213-0.213 0.32s-0.32 0.213-0.533 0.213-0.427 0-0.533-0.107c-0.427-0.213-0.533-0.533-0.533-0.853h2.88c0-0.32 0-0.64 0-0.853 0-0.32-0.107-0.64-0.32-0.853-0.213-0.32-0.427-0.427-0.747-0.64-0.32-0.107-0.64-0.213-0.96-0.213-0.64 0-1.067 0.213-1.387 0.533-0.32 0.533-0.533 1.067-0.533 1.707 0 0.747 0.213 1.28 0.64 1.6s0.853 0.533 1.387 0.533zM82.453 4.587c0.107-0.107 0.32-0.213 0.64-0.213 0.213 0 0.427 0.107 0.64 0.213s0.213 0.32 0.32 0.64h-1.92c0.107-0.213 0.107-0.427 0.32-0.64zM86.613 5.44c0-0.213 0-0.427 0.107-0.64 0.107-0.32 0.32-0.427 0.747-0.427 0.32 0 0.427 0.107 0.533 0.32 0.107 0.107 0.107 0.32 0.107 0.533v2.453h1.067v-2.773c0-0.533-0.107-0.853-0.427-1.067-0.213-0.213-0.64-0.32-0.96-0.32s-0.64 0.107-0.853 0.213c-0.107 0.107-0.213 0.213-0.427 0.427v-0.533h-0.96v4.053h1.067v-2.24zM91.84 7.787c0.64 0 1.173-0.213 1.493-0.747 0.213-0.32 0.32-0.64 0.32-0.96h-1.067c0 0.213-0.107 0.427-0.213 0.533s-0.32 0.213-0.533 0.213c-0.32 0-0.533-0.213-0.747-0.533-0.107-0.213-0.107-0.427-0.107-0.747s0-0.533 0.107-0.747c0.107-0.32 0.32-0.533 0.747-0.533 0.213 0 0.427 0.107 0.533 0.213s0.107 0.213 0.107 0.427h1.067c-0.107-0.533-0.213-0.96-0.533-1.173s-0.747-0.32-1.28-0.32c-0.64 0-1.067 0.213-1.387 0.533-0.32 0.427-0.533 0.96-0.533 1.6s0.107 1.173 0.427 1.493c0.533 0.533 0.96 0.747 1.6 0.747zM95.36 8.107c0 0.107-0.107 0.213-0.213 0.213s-0.107 0.107-0.213 0.107-0.213 0-0.32 0h-0.107v0.853c0.107 0 0.213 0 0.213 0 0.107 0 0.107 0 0.213 0 0.427 0 0.747-0.107 0.96-0.32 0.107-0.213 0.32-0.64 0.64-1.387l1.387-3.947h-1.067l-0.853 2.987-0.853-2.987h-1.173l1.493 4.267c0 0 0 0.107-0.107 0.213zM12.373 8.107c-5.973 0-10.773 4.907-10.773 10.88s4.8 10.88 10.773 10.88 10.773-4.907 10.773-10.88c-0.107-5.973-4.907-10.88-10.773-10.88zM21.12 24.213c-1.813 3.093-5.013 5.12-8.853 5.12-4.16 0-7.787-2.56-9.28-6.187-0.107-0.32-0.107-0.64 0.213-0.853 0.427-0.213 0.64-0.213 1.067-0.213s1.067-0.213 1.493-0.427c0.427-0.213 1.28-0.213 1.813 0.107 0.32 0.107 0.64 0.427 0.747 0.747 0.213 0.747 1.6 1.493 3.2 1.493 1.067 0 3.307-0.32 3.307-1.92 0-0.427-0.32-0.853-0.533-1.067 0.64-0.213 1.6 0 2.133 0.213s0.853 0.427 1.067 0.64c0.96 0.747 2.133 1.067 2.88 1.173 0.853 0 1.173 0.32 0.747 1.173zM22.187 18.773c-0.213 0.107-0.853 0.107-1.173 0-0.96-0.107-1.493 0-2.24 0.107s-1.6 0-2.24-0.213c1.28-0.213 1.92-0.853 1.92-1.493 0-1.173-1.707-1.707-2.88-1.707-0.32 0-0.64 0-0.853 0.107-0.32 0.107-0.533 0.107-0.853 0.107-1.067 0-1.707-0.32-1.92-0.533-0.747-0.64-1.707-0.747-2.24-0.747-1.813 0-2.56 0.747-2.56 1.387 0 0.533 0.533 0.96 0.96 1.28 0.853 0.533 2.24 0.32 2.667 0.32 0.427-0.107 0.853-0.107 1.387 0-1.387 0.213-2.027 1.067-2.027 1.6 0 0.213 0.107 0.64 0.32 0.853-0.213 0-0.533 0.107-0.747 0.213s-0.533 0.213-0.96 0.213c-1.173 0-1.493-0.533-1.6-0.747s-0.32-0.427-0.64-0.64c-1.067-0.853-3.413-0.533-4.053-0.853-0.213-0.107-0.32-0.32-0.213-0.64 0.747-4.907 5.013-8.64 10.027-8.64 5.333 0 9.707 4.16 10.133 9.387 0.107 0.533 0 0.533-0.213 0.64z"></path>
</symbol>
</svg>

<section class="page">
<header id="header" class="header">
<div class="row align-center">
<div class="columns">

<h1 id="js_logo" class="header--title image image__full"><a href="<?php echo SITE_PATH?>" title="Metaphase">
<svg class="logo icon-logo_metaphase"><use xlink:href="#icon-logo_metaphase"></use></svg>
</a></h1>
</div>

<div class="nav--toggle--holder columns" data-responsive-toggle="js_nav" data-hide-for="medium">
<button id="js_nav_toggle" class="nav--toggle" type="button" data-toggle>
<svg class='icon icon-menu'><use xlink:href='#icon-menu'></use></svg>
<svg class='icon icon-close'><use xlink:href='#icon-close'></use></svg>
</button>
</div>

<div id="js_nav_bar" class="nav--bar columns shrink">

<div id="js_nav" class="nav--holder">
<nav class="nav">
<ul class="menu vertical medium-horizontal">
<li class="has_submenu">
<span>会社紹介<svg class="icon arrow_04"><use xlink:href="#icon-arrow_04"></use></svg></span>
<ul class="nav--submenu">
<li><a href="<?php echo SITE_PATH?>about/" title="ビジョン">ビジョン</a></li>
<li><a href="<?php echo SITE_PATH?>about/member/" title="メンバー紹介">メンバー紹介</a></li>
<li><a href="<?php echo SITE_PATH?>about/overview/" title="会社情報">会社情報</a></li>
</ul>
</li>
<li class="has_submenu">
<span>サービス<svg class="icon arrow_04"><use xlink:href="#icon-arrow_04"></use></svg></span>
<ul class="nav--submenu">
<li><a href="<?php echo SITE_PATH?>service/" title="サービスコンセプト">サービスコンセプト</a></li>
<li><a href="<?php echo SITE_PATH?>service/experiencedesign/" title="エクスペリエンスデザイン">エクスペリエンスデザイン</a></li>
<li><a href="<?php echo SITE_PATH?>service/retention/" title="リテンション">リテンション</a></li>
<li><a href="<?php echo SITE_PATH?>service/businessdevelopment/" title="プロダクトサービス">プロダクトサービス</a></li>
</ul>
</li>
<li><a href="<?php echo SITE_PATH?>work/" title="実績">実績</a></li>
<li><a href="<?php echo SITE_PATH?>products/" title="プロダクト">プロダクト</a></li>
<li class="has_submenu">
<span>ブログ<svg class="icon arrow_04"><use xlink:href="#icon-arrow_04"></use></svg></span>
<ul class="nav--submenu">
<li><a href="<?php echo SITE_PATH?>blog/" title="ブログ">ブログ</a></li>
<li><a href="http://www.metaphase.co.jp/column/" title="PR BLOG" target="_blank">PR BLOG</a></li>
</ul>
</li>
<li><a href="http://recruit.metaphase.co.jp" title="採用情報">採用情報</a></li>
<li><a href="<?php echo SITE_PATH?>contact/" class="nav--button" title="お問い合わせ">お問い合わせ</a></li>
</ul>
</nav>
</div>
</div>

</div>
</header>

<main id="main" class="main">

<section class="section section__sv bg">
<h2 class="section__sv--title">エントリー	</h2>
</section>

<section class="section section__contact expanded">
<div class="section--head">
<h2 class="section--title">プロフィール</h2>
</div>
<div class="section--main">
<article class="article article__contact expanded">
<div role="form" class="wpcf7" id="wpcf7-f599-p588-o1" lang="ja" dir="ltr">
<div class="screen-reader-response"></div>
<form action="" method="post" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
<section class="section small section__recruit-form">
<div class="section--main">
<article class="article article__contact expanded">
<ul class="article__contact--form">
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="name-sei">お名前</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["sei"])):?><span class="error"><?php echo $err_msg["sei"]?></span><?php endif?>
<?php if(! empty($err_msg["mei"])):?><?php if(! empty($err_msg["sei"])):?><br><?php endif?><span class="error"><?php echo $err_msg["mei"]?></span><?php endif?>
<div class="row">
<div class="columns medium-4 small-6">
<span class="wpcf7-form-control-wrap name-sei"><input type="text" name="sei" value="<?php echo $vars["sei"]?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="name-sei" aria-required="true" aria-invalid="false" placeholder="姓" /></span>
</div>
<div class="columns medium-4 small-6">
<span class="wpcf7-form-control-wrap name-mei"><input type="text" name="mei" value="<?php echo $vars["mei"]?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="name-mei" aria-required="true" aria-invalid="false" placeholder="名" /></span>
</div>
</div>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="name-sei-kana">フリガナ</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["k_sei"])):?><span class="error"><?php echo $err_msg["k_sei"]?></span><?php endif?>
<?php if(! empty($err_msg["k_mei"])):?><?php if(! empty($err_msg["k_sei"])):?><br><?php endif?><span class="error"><?php echo $err_msg["k_mei"]?></span><?php endif?>
<div class="row">
<div class="columns medium-4 small-6">
<span class="wpcf7-form-control-wrap name-sei-kana"><input type="text" name="k_sei" value="<?php echo $vars["k_sei"]?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="name-sei-kana" aria-required="true" aria-invalid="false" placeholder="セイ" /></span>
</div>
<div class="columns medium-4 small-6">
<span class="wpcf7-form-control-wrap name-mei-kana"><input type="text" name="k_mei" value="<?php echo $vars["k_mei"]?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="name-mei-kana" aria-required="true" aria-invalid="false" placeholder="メイ" /></span>
</div>
</div>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="birthday-year">生年月日（半角数字）</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["birth_date"])):?><span class="error"><?php echo $err_msg["birth_date"]?></span><?php endif?>
<ul class="row align-top">
<li class="multifield columns medium-2">
<span class="wpcf7-form-control-wrap birthday-year"><input type="number" name="birth_date1" value="<?php echo $vars["birth_date1"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-required wpcf7-validates-as-number" id="birthday-year" min="1900" step="1" aria-required="true" aria-invalid="false" placeholder="1970" /></span>
</li>
<li class="separator shrink">年</li>
<li class="columns medium-2">
<span class="wpcf7-form-control-wrap birthday-month"><input type="number" name="birth_date2" value="<?php echo $vars["birth_date2"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="birthday-month" min="01" max="12" step="1" aria-invalid="false" placeholder="01" /></span>
</li>
<li class="separator shrink">月</li>
<li class="columns medium-2">
<span class="wpcf7-form-control-wrap birthday-day"><input type="number" name="birth_date3" value="<?php echo $vars["birth_date3"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="birthday-day" min="01" max="31" step="1" aria-invalid="false" placeholder="01" /></span>
</li>
<li class="separator shrink">日</li>
</ul></div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="post-1">郵便番号（半角数字）</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["zip"])):?><span class="error"><?php echo $err_msg["zip"]?></span><?php endif?>
<ul class="row align-top">
<li class="multifield columns medium-2">
<span class="wpcf7-form-control-wrap post-1"><input type="number" name="zip1" value="<?php echo $vars["zip1"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-required wpcf7-validates-as-number required" id="post-1" min="0" max="999" step="1" aria-required="true" aria-invalid="false" placeholder="154" /></span>
</li>
<li class="separator shrink">-</li>
<li class="columns medium-2">
<span class="wpcf7-form-control-wrap post-2"><input type="number" name="zip2" value="<?php echo $vars["zip2"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="post-2" min="0" max="9999" step="1" aria-invalid="false" placeholder="0054" /></span>
</li>
</ul></div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="prefectures">お住まいの都道府県</label>
</div>
<div class="columns small-12 medium-8">
<div class="wrap-select">
<?php if(! empty($err_msg["pref"])):?><span class="error"><?php echo $err_msg["pref"]?></span><?php endif?>
<span class="wpcf7-form-control-wrap prefectures">
<select name="pref" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required contact-select" aria-required="true" aria-invalid="false">
<option value="">選択してください</option>
<?php $html->options(array("options"=>$lists["pref"],"selected"=>$vars["pref"]))?>
</select></span>
</div>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="address">市区町村・番地・建物名</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["address"])):?><span class="error"><?php echo $err_msg["address"]?></span><?php endif?>
<span class="wpcf7-form-control-wrap address"><input type="text" name="address" value="<?php echo $vars["address"]?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="address" aria-required="true" aria-invalid="false" placeholder="入力してください" /></span>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<p class="card__form--title">お電話番号（半角数字）<br /><small>※自宅・携帯のいずれかを入力してください</small></p>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["tel01"])):?><span class="error"><?php echo $err_msg["tel01"]?></span><?php endif?>
<?php if(! empty($err_msg["tel02"])):?><?php if(! empty($err_msg["tel01"])):?><br><?php endif?><span class="error"><?php echo $err_msg["tel02"]?></span><?php endif?>
<ul class="row align-top">
<li class="shrink"><label for="tel-home-1">自宅</label></li>
<li class="multifield columns">
<span class="wpcf7-form-control-wrap tel-home-1"><input type="tel" name="tel01_1" value="<?php echo $vars["tel01_1"]?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" id="tel-home-1" aria-required="true" aria-invalid="false" placeholder="123" /></span>
</li>
<li class="separator shrink">-</li>
<li class="columns">
<span class="wpcf7-form-control-wrap tel-home-2"><input type="tel" name="tel01_2" value="<?php echo $vars["tel01_2"]?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" id="tel-home-2" aria-invalid="false" placeholder="123" /></span>
</li>
<li class="separator shrink">-</li>
<li class="columns">
<span class="wpcf7-form-control-wrap tel-home-3"><input type="tel" name="tel01_3" value="<?php echo $vars["tel01_3"]?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" id="tel-home-3" aria-invalid="false" placeholder="123" /></span>
</li>
</ul>
<ul class="row align-top">
<li class="shrink"><label for="tel-mobile-1">携帯</label></li>
<li class="multifield columns">
<span class="wpcf7-form-control-wrap tel-mobile-1"><input type="tel" name="tel02_1" value="<?php echo $vars["tel02_1"]?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" id="tel-mobile-1" aria-invalid="false" placeholder="123" /></span></p>
<li class="separator shrink">-</li>
<li class="columns">
<span class="wpcf7-form-control-wrap tel-mobile-2"><input type="tel" name="tel02_2" value="<?php echo $vars["tel02_2"]?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" id="tel-mobile-2" aria-invalid="false" placeholder="123" /></span></p>
<li class="separator shrink">-</li>
<li class="columns">
<span class="wpcf7-form-control-wrap tel-mobile-3"><input type="tel" name="tel02_3" value="<?php echo $vars["tel02_3"]?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" id="tel-mobile-3" aria-invalid="false" placeholder="123" /></span>
</ul>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="email">メールアドレス</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["email"])):?><span class="error"><?php echo $err_msg["email"]?></span><?php endif?>
<span class="wpcf7-form-control-wrap email"><input type="email" name="email" value="<?php echo $vars["email"]?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="email" aria-required="true" aria-invalid="false" placeholder="入力してください" /></span>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="email-confirme">メールアドレス<br /><small>※確認のため再入力してください</small></label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["email_re"])):?><span class="error"><?php echo $err_msg["email_re"]?></span><?php endif?>
<span class="wpcf7-form-control-wrap email-confirme"><input type="email" name="email_re" value="<?php echo $vars["email_re"]?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="email-confirme" aria-required="true" aria-invalid="false" placeholder="入力してください" /></span>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="state">現在の状況</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["job"])):?><span class="error"><?php echo $err_msg["job"]?></span><br><?php endif?>
<span class="wpcf7-form-control-wrap state"><span class="wpcf7-form-control wpcf7-radio">
<?php $html->radios(array("options"=>$lists["job"],"selected"=>$vars["job"],"name"=>"job","top"=>'<span class="wpcf7-list-item">',"bottom"=>"</span>"))?>
</span></span>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="plan">就業開始可能日</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["start_date_select"])):?><span class="error"><?php echo $err_msg["start_date_select"]?></span><?php endif?>
<?php if(! empty($err_msg["start_date"])):?><br><span class="error"><?php echo $err_msg["start_date"]?></span><?php endif?>
<div id="group-plan-date" data-class="wpcf7cf_group">
<ul class="row align-top wrap-radio_secondary">
<li id="has_plan" class="multifield columns medium-2">
<p><span class="wpcf7-form-control-wrap start-date-year"><input type="number" name="start_date1" value="<?php echo $vars["start_date1"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-required wpcf7-validates-as-number" id="start-date-year" min="1900" step="1" aria-required="true" aria-invalid="false" placeholder="1970" /></span>
</li>
<li class="separator shrink">年</li>
<li class="columns medium-2">
<span class="wpcf7-form-control-wrap start-date-month"><input type="number" name="start_date2" value="<?php echo $vars["start_date2"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="start-date-month" min="01" max="12" step="1" aria-invalid="false" placeholder="01" /></span>
</li>
<li class="separator shrink">月</li>
<li class="columns medium-2">
<span class="wpcf7-form-control-wrap start-date-day"><input type="number" name="start_date3" value="<?php echo $vars["start_date3"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="start-date-day" min="01" max="31" step="1" aria-invalid="false" placeholder="01" /></span>
</li>
<li class="separator shrink">日</li>
</ul>
</div>
<ul class="wrap-radio">
<li>
<span class="wpcf7-form-control-wrap plan"><span class="wpcf7-form-control wpcf7-checkbox" id="noplan"><span class="wpcf7-list-item first last"><label><input type="checkbox" name="start_date_select" value="2"<?php if($vars["start_date_select"]==2):?> checked<?php endif?> /><span class="wpcf7-list-item-label">就業開始日は未定</span></label></span></span></span>
</li>
</ul></div>
</div>
</li>
</ul>
</article></div>
</section>
<section class="section small section__recruit-form">
<div class="row section--head">
<div class="columns">
<h2 class="section--title">応募職種</h2>
</div>
</div>
<div class="section--main">
<article class="article article__contact expanded">
<ul class="article__contact--form">
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="job_category">応募職種</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["target_job"])):?><span class="error"><?php echo $err_msg["target_job"]?></span><?php endif?>
<div class="wrap-select large">
<span class="wpcf7-form-control-wrap job_category">
<select name="target_job" class="wpcf7-form-control wpcf7-select" aria-invalid="false">
<option value="">選択してください</option>
<?php $html->options(array("options"=>$lists["target_job"],"selected"=>$vars["target_job"]))?>
</select></span>
</div>
</div>
</div>
</li>
</ul>
</article></div>
</section>
<section class="section small section__recruit-form">
<div class="row section--head">
<div class="columns">
<h2 class="section--title">履歴書</h2>
<p class="section--headtext">ご自身で作成した履歴書ファイルをアップロードするか、入力フォームに直接ご入力ください。<br /><small>※面接の際、必要になります。可能な限りご入力ください。</small></p>
</div>
</div>
<div class="section--main">
<article class="article article__contact expanded">
<ul class="article__contact--form">
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<p class="card__form--title">履歴書（任意）</p>
</div>
<div class="columns small-12 medium-8 select-file">
<?php if(! empty($err_msg["resume_file"])):?><span class="error"><?php echo $err_msg["resume_file"]?></span><?php endif?>
<?php if(! empty($err_msg["resume_text"])):?><br><span class="error"><?php echo $err_msg["resume_text"]?></span><?php endif?>
<div class="row align-middle select-file__box">
<div class="shrink">
<label class="select-file__button button" for="open-file_1"><span>ファイルを選択</span></label>
</div>
<div class="shrink">
<span class="wpcf7-form-control-wrap open-file_1"><input type="file" name="resume_file" size="40" class="wpcf7-form-control wpcf7-file js_file-input" id="open-file_1" aria-invalid="false" /></span></p>
<div class="js_file-text">選択されていません</div>
</div>
</div>
<?php if(! empty($vars["resume_file_new"])): ?>
<span class="fileName"><a href="<?php echo IMAGE_TEMP_URL?><?php echo $vars["resume_file_new"]?>" target="_blank"><?php echo $vars["resume_file_orignal"]?></a></span><br>
<input type="hidden" name="resume_file_new" value="<?php echo $vars["resume_file_new"]?>" />
<input type="hidden" name="resume_file_orignal" value="<?php echo $vars["resume_file_orignal"]?>" />
<?php endif ?>
<p>（Word、Excel、PowerPoint、PDF、2MBまで）</p>
<div class="select-file__form">
<span class="wpcf7-form-control-wrap inquiry"><textarea name="resume_text" cols="40" rows="30" class="wpcf7-form-control wpcf7-textarea" id="inquiry" aria-invalid="false" placeholder="入力してください"><?php echo $vars["resume_text"]?></textarea></span>
</div>
</div>
</div>
</li>
</ul>
</article></div>
</section>
<section class="section small section__recruit-form">
<div class="row section--head">
<div class="columns">
<h2 class="section--title">職務経歴書</h2>
<p class="section--headtext">ご自身で作成した職務経歴書ファイルをアップロードするか、入力フォームに直接ご入力ください。<br />入力フォームには、直近3つまでご入力ください。</p>
</div>
</div>
<div class="section--main">
<article class="article article__contact border expanded">
<h3 class="article__contact--title">職務経歴１（任意）</h3>
<ul class="article__contact--form">
<li>
<div class="card__form row">
<div class="columns small-12 medium-8 medium-offset-3 select-file">
<?php if(! empty($err_msg["curriculum"])):?><span class="error"><?php echo $err_msg["curriculum"]?></span><?php endif?>
<div class="row align-middle select-file__box">
<div class="shrink">
<label class="select-file__button button" for="open-file_2"><span>ファイルを選択</span></label>
</div>
<div class="shrink">
<span class="wpcf7-form-control-wrap open-file_2"><input type="file" name="curriculum" size="40" class="wpcf7-form-control wpcf7-file js_file-input" id="open-file_2" aria-invalid="false" /></span></p>
<div class="js_file-text">選択されていません</div>
</div>
</div>
<?php if(! empty($vars["curriculum_new"])): ?>
<span class="fileName"><a href="<?php echo IMAGE_TEMP_URL?><?php echo $vars["curriculum_new"]?>" target="_blank"><?php echo $vars["curriculum_orignal"]?></a></span><br>
<input type="hidden" name="curriculum_new" value="<?php echo $vars["curriculum_new"]?>" />
<input type="hidden" name="curriculum_orignal" value="<?php echo $vars["curriculum_orignal"]?>" />
<?php endif ?>
<p>（Word、Excel、PowerPoint、PDF、2MBまで）</p>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_1_yers_service-year">勤務歴</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["history_from"])):?><span class="error"><?php echo $err_msg["history_from"]?></span><?php endif?>
<?php if(! empty($err_msg["history_to"])):?><br><span class="error"><?php echo $err_msg["history_to"]?></span><?php endif?>
<ul class="row align-top">
<li class="multifield columns medium-2">
<span class="wpcf7-form-control-wrap history_1_yers_service-year"><input type="number" name="history_from1" value="<?php echo $vars["history_from1"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="history_1_yers_service-year" min="1900" step="1" aria-invalid="false" placeholder="1970" /></span>
</li>
<li class="separator shrink">年</li>
<li class="columns medium-2">
<span class="wpcf7-form-control-wrap history_1_yers_service-month"><input type="number" name="history_from2" value="<?php echo $vars["history_from2"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="history_1_yers_service-month" min="01" max="12" step="1" aria-invalid="false" placeholder="01" /></span>
</li>
<li class="separator shrink">月</li>
</ul>
<ul class="row align-top">
<li class="multifield columns medium-2">
<span class="wpcf7-form-control-wrap history_1_yers_end_service-year"><input type="number" name="history_to1" value="<?php echo $vars["history_to1"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="history_1_yers_end_service-year" min="1900" step="1" aria-invalid="false" placeholder="1970" /></span>
</li>
<li class="separator shrink">年</li>
<li class="columns medium-2">
<span class="wpcf7-form-control-wrap history_1_yers_end_service-month"><input type="number" name="history_to2" value="<?php echo $vars["history_to2"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="history_1_yers_end_service-month" min="01" max="12" step="1" aria-invalid="false" placeholder="01" /></span>
</li>
<li class="separator shrink">月</li>
</ul></div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_1_company_name">会社名</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["company"])):?><span class="error"><?php echo $err_msg["company"]?></span><?php endif?>
<span class="wpcf7-form-control-wrap history_1_company_name"><input type="text" name="company" value="<?php echo $vars["company"]?>" size="40" class="wpcf7-form-control wpcf7-text" id="history_1_company_name" aria-invalid="false" placeholder="入力してください" /></span>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_1_employment_status">雇用形態</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["pattern"])):?><span class="error"><?php echo $err_msg["pattern"]?></span><?php endif?>
<div class="wrap-select large">
<span class="wpcf7-form-control-wrap history_1_employment_status">
<select name="pattern" class="wpcf7-form-control wpcf7-select contact-select" id="history_1_employment_status" aria-invalid="false">
<option value="">選択してください</option>
<?php $html->options(array("options"=>$lists["pattern"],"selected"=>$vars["pattern"]))?>
</select></span>
</div>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_1_job_category">職種</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["job_type"])):?><span class="error"><?php echo $err_msg["job_type"]?></span><?php endif?>
<div class="wrap-select large">
<span class="wpcf7-form-control-wrap history_1_job_category">
<select name="job_type" class="wpcf7-form-control wpcf7-select contact-select has_optgroup" id="history_1_job_category" aria-invalid="false">
<option value="">選択してください</option>
<?php $html->options(array("options"=>$lists["job_type"],"selected"=>$vars["job_type"],"optgroup"=>1))?>
</select></span>
</div>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_1_occupation">業務内容</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["work_contents"])):?><span class="error"><?php echo $err_msg["work_contents"]?></span><?php endif?>
<span class="wpcf7-form-control-wrap history_1_occupation"><textarea name="work_contents" cols="40" rows="30" class="wpcf7-form-control wpcf7-textarea" id="history_1_occupation" aria-invalid="false" placeholder="ご自身で作成したファイルがない場合、こちらにご入力ください"><?php echo $vars["work_contents"]?></textarea></span>
</div>
</div>
</li>
</ul>
</article>
<article class="article article__contact border expanded">
<h3 class="article__contact--title">職務経歴２（任意）</h3>
<ul class="article__contact--form">
<li>
<div class="card__form row">
<div class="columns small-12 medium-8 medium-offset-3 select-file">
<?php if(! empty($err_msg["curriculum02"])):?><span class="error"><?php echo $err_msg["curriculum02"]?></span><?php endif?>
<div class="row align-middle select-file__box">
<div class="shrink">
<label class="select-file__button button" for="open-file_3"><span>ファイルを選択</span></label>
</div>
<div class="shrink">
<span class="wpcf7-form-control-wrap open-file_3"><input type="file" name="curriculum02" size="40" class="wpcf7-form-control wpcf7-file js_file-input" id="open-file_3" aria-invalid="false" /></span></p>
<div class="js_file-text">選択されていません</div>
</div>
</div>
<?php if(! empty($vars["curriculum02_new"])): ?>
<span class="fileName"><a href="<?php echo IMAGE_TEMP_URL?><?php echo $vars["curriculum02_new"]?>" target="_blank"><?php echo $vars["curriculum02_orignal"]?></a></span><br>
<input type="hidden" name="curriculum02_new" value="<?php echo $vars["curriculum02_new"]?>" />
<input type="hidden" name="curriculum02_orignal" value="<?php echo $vars["curriculum02_orignal"]?>" />
<?php endif ?>
<p>（Word、Excel、PowerPoint、PDF、2MBまで）</p>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_2_yers_service-year">勤務歴</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["history_from_02"])):?><span class="error"><?php echo $err_msg["history_from_02"]?></span><?php endif?>
<?php if(! empty($err_msg["history_to_02"])):?><br><span class="error"><?php echo $err_msg["history_to_02"]?></span><?php endif?>
<ul class="row align-top">
<li class="multifield columns medium-2">
<span class="wpcf7-form-control-wrap history_2_yers_service-year"><input type="number" name="history_from1_02" value="<?php echo $vars["history_from1_02"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="history_2_yers_service-year" min="1900" step="1" aria-invalid="false" placeholder="1970" /></span>
</li>
<li class="separator shrink">年</li>
<li class="columns medium-2">
<span class="wpcf7-form-control-wrap history_2_yers_service-month"><input type="number" name="history_from2_02" value="<?php echo $vars["history_from2_02"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="history_2_yers_service-month" min="01" max="12" step="1" aria-invalid="false" placeholder="01" /></span>
</li>
<li class="separator shrink">月</li>
</ul>
<ul class="row align-top">
<li class="multifield columns medium-2">
<span class="wpcf7-form-control-wrap history_2_yers_end_service-year"><input type="number" name="history_to1_02" value="<?php echo $vars["history_to1_02"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="history_2_yers_end_service-year" min="1900" step="1" aria-invalid="false" placeholder="1970" /></span>
</li>
<li class="separator shrink">年</li>
<li class="columns medium-2">
<span class="wpcf7-form-control-wrap history_2_yers_end_service-month"><input type="number" name="history_to2_02" value="<?php echo $vars["history_to2_02"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="history_2_yers_end_service-month" min="01" max="12" step="1" aria-invalid="false" placeholder="01" /></span>
</li>
<li class="separator shrink">月</li>
</ul></div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_2_company_name">会社名</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["history_from_02"])):?><span class="error"><?php echo $err_msg["history_from_02"]?></span><?php endif?>
<span class="wpcf7-form-control-wrap history_2_company_name">
<input type="text" name="company_02" value="<?php echo $vars["company_02"]?>" size="40" class="wpcf7-form-control wpcf7-text" id="history_2_company_name" aria-invalid="false" placeholder="入力してください" /></span>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_2_employment_status">雇用形態</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["pattern_02"])):?><span class="error"><?php echo $err_msg["pattern_02"]?></span><?php endif?>
<div class="wrap-select large">
<span class="wpcf7-form-control-wrap history_2_employment_status">
<select name="pattern_02" class="wpcf7-form-control wpcf7-select contact-select" id="history_2_employment_status" aria-invalid="false">
<option value="">選択してください</option>
<?php $html->options(array("options"=>$lists["pattern"],"selected"=>$vars["pattern_02"]))?>
</select></span>
</div>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_2_job_category">職種</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["job_type_02"])):?><span class="error"><?php echo $err_msg["job_type_02"]?></span><?php endif?>
<div class="wrap-select large">
<span class="wpcf7-form-control-wrap history_2_job_category">
<select name="job_type_02" class="wpcf7-form-control wpcf7-select contact-select has_optgroup" id="history_2_job_category" aria-invalid="false">
<option value="">選択してください</option>
<?php $html->options(array("options"=>$lists["job_type"],"selected"=>$vars["job_type_02"],"optgroup"=>1))?>
</select></span>
</div>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_2_occupation">業務内容</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["work_contents_02"])):?><span class="error"><?php echo $err_msg["work_contents_02"]?></span><?php endif?>
<span class="wpcf7-form-control-wrap history_2_occupation"><textarea name="work_contents_02" cols="40" rows="30" class="wpcf7-form-control wpcf7-textarea" id="history_2_occupation" aria-invalid="false" placeholder="ご自身で作成したファイルがない場合、こちらにご入力ください"><?php echo $vars["work_contents_02"]?></textarea></span>
</div>
</div>
</li>
</ul>
</article>
<article class="article article__contact border expanded">
<h3 class="article__contact--title">職務経歴３（任意）</h3>
<ul class="article__contact--form">
<li>
<div class="card__form row">
<div class="columns small-12 medium-8 medium-offset-3 select-file">
<?php if(! empty($err_msg["curriculum03"])):?><span class="error"><?php echo $err_msg["curriculum03"]?></span><?php endif?>
<div class="row align-middle select-file__box">
<div class="shrink">
<label class="select-file__button button" for="open-file_4"><span>ファイルを選択</span></label>
</div>
<div class="shrink">
<span class="wpcf7-form-control-wrap open-file_4"><input type="file" name="curriculum03" size="40" class="wpcf7-form-control wpcf7-file js_file-input" id="open-file_4" aria-invalid="false" /></span></p>
<div class="js_file-text">選択されていません</div>
</div>
</div>
<?php if(! empty($vars["curriculum03_new"])): ?>
<span class="fileName"><a href="<?php echo IMAGE_TEMP_URL?><?php echo $vars["curriculum03_new"]?>" target="_blank"><?php echo $vars["curriculum03_orignal"]?></a></span><br>
<input type="hidden" name="curriculum03_new" value="<?php echo $vars["curriculum03_new"]?>" />
<input type="hidden" name="curriculum03_orignal" value="<?php echo $vars["curriculum03_orignal"]?>" />
<?php endif ?>
<p>（Word、Excel、PowerPoint、PDF、2MBまで）</p>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_3_yers_service-year">勤務歴</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["history_from_03"])):?><span class="error"><?php echo $err_msg["history_from_03"]?></span><?php endif?>
<?php if(! empty($err_msg["history_to_03"])):?><br><span class="error"><?php echo $err_msg["history_to_03"]?></span><?php endif?>
<ul class="row align-top">
<li class="multifield columns medium-2">
<span class="wpcf7-form-control-wrap history_3_yers_end_service-year"><input type="number" name="history_from1_03" value="<?php echo $vars["history_from1_03"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="history_3_yers_end_service-year" min="1900" step="1" aria-invalid="false" placeholder="1970" /></span>
</li>
<li class="separator shrink">年</li>
<li class="columns medium-2">
<span class="wpcf7-form-control-wrap history_3_yers_end_service-month"><input type="number" name="history_from2_03" value="<?php echo $vars["history_from2_03"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="history_3_yers_end_service-month" min="01" max="12" step="1" aria-invalid="false" placeholder="01" /></span>
</li>
<li class="separator shrink">月</li>
</ul>
<ul class="row align-top">
<li class="multifield columns medium-2">
<span class="wpcf7-form-control-wrap history_3_yers_end_service-year"><input type="number" name="history_to1_03" value="<?php echo $vars["history_to1_03"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="history_3_yers_end_service-year" min="1900" step="1" aria-invalid="false" placeholder="1970" /></span>
</li>
<li class="separator shrink">年</li>
<li class="columns medium-2">
<span class="wpcf7-form-control-wrap history_3_yers_end_service-month"><input type="number" name="history_to2_03" value="<?php echo $vars["history_to2_03"]?>" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" id="history_3_yers_end_service-month" min="01" max="12" step="1" aria-invalid="false" placeholder="01" /></span>
</li>
<li class="separator shrink">月</li>
</ul></div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_3_company_name">会社名</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["company_03"])):?><span class="error"><?php echo $err_msg["company_03"]?></span><?php endif?>
<span class="wpcf7-form-control-wrap history_3_company_name"><input type="text" name="company_03" value="<?php echo $vars["company_03"]?>" size="40" class="wpcf7-form-control wpcf7-text" id="history_3_company_name" aria-invalid="false" placeholder="入力してください" /></span>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_3_employment_status">雇用形態</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["pattern_03"])):?><span class="error"><?php echo $err_msg["pattern_03"]?></span><?php endif?>
<div class="wrap-select large">
<span class="wpcf7-form-control-wrap history_3_employment_status">
<select name="pattern_03" class="wpcf7-form-control wpcf7-select contact-select" id="history_3_employment_status" aria-invalid="false">
<option value="">選択してください</option>
<?php $html->options(array("options"=>$lists["pattern"],"selected"=>$vars["pattern_03"]))?>
</select></span>
</div>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_3_job_category">職種</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["job_type_03"])):?><span class="error"><?php echo $err_msg["job_type_03"]?></span><?php endif?>
<div class="wrap-select large">
<span class="wpcf7-form-control-wrap history_3_job_category">
<select name="job_type_03" class="wpcf7-form-control wpcf7-select contact-select has_optgroup" id="history_3_job_category" aria-invalid="false">
<option value="">選択してください</option>
<?php $html->options(array("options"=>$lists["job_type"],"selected"=>$vars["job_type_03"],"optgroup"=>1))?>
</select></span>
</div>
</div>
</div>
</li>
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<label class="card__form--title" for="history_3_occupation">業務内容</label>
</div>
<div class="columns small-12 medium-8">
<?php if(! empty($err_msg["work_contents_03"])):?><span class="error"><?php echo $err_msg["work_contents_03"]?></span><?php endif?>
<span class="wpcf7-form-control-wrap history_3_occupation"><textarea name="work_contents_03" cols="40" rows="30" class="wpcf7-form-control wpcf7-textarea" id="history_3_occupation" aria-invalid="false" placeholder="ご自身で作成したファイルがない場合、こちらにご入力ください"><?php echo $vars["work_contents_03"]?></textarea></span>
</div>
</div>
</li>
</ul>
</article></div>
</section>
<section class="section small section__recruit-form">
<div class="row section--head">
<div class="columns">
<h2 class="section--title">作品・PR文</h2>
<p class="section--headtext">ご自身で作成したファイルをアップロードするか、入力フォームに直接ご入力ください。</p>
</div>
</div>
<div class="section--main">
<?php if(! empty($err_msg["work"])):?><span class="error"><?php echo $err_msg["work"]?></span><?php endif?>
<?php if(! empty($err_msg["work_text"])):?><br><span class="error"><?php echo $err_msg["work_text"]?></span><?php endif?>
<article class="article article__contact expanded">
<ul class="article__contact--form">
<li>
<div class="card__form row align-center">
<div class="columns small-12 medium-2">
<p class="card__form--title">作品（任意）</p>
</div>
<div class="columns small-12 medium-8 select-file">
<div class="row align-middle select-file__box">
<div class="shrink">
<label class="select-file__button button" for="open-file_5"><span>ファイルを選択</span></label>
</div>
<div class="shrink">
<span class="wpcf7-form-control-wrap open-file_5"><input type="file" name="work" size="40" class="wpcf7-form-control wpcf7-file js_file-input" id="open-file_5" aria-invalid="false" /></span></p>
<div class="js_file-text">選択されていません</div>
</div>
</div>
<?php if(! empty($vars["work_new"])): ?>
<span class="fileName"><a href="<?php echo IMAGE_TEMP_URL?><?php echo $vars["work_new"]?>" target="_blank"><?php echo $vars["work_orignal"]?></a></span><br>
<input type="hidden" name="work_new" value="<?php echo $vars["work_new"]?>" />
<input type="hidden" name="work_orignal" value="<?php echo $vars["work_orignal"]?>" />
<?php endif ?>
<p>（Word、Excel、PowerPoint、PDF、2MBまで）</p>
<div class="select-file__form">
<span class="wpcf7-form-control-wrap inquiry_2"><textarea name="work_text" cols="40" rows="30" class="wpcf7-form-control wpcf7-textarea" id="inquiry_2" aria-invalid="false" placeholder="入力してください"><?php echo $vars["work_text"]?></textarea></span>
</div>
</div>
</div>
</li>
</ul>
</article></div>
</section>
<section class="section small section__recruit-form">
<div class="row align-center">
<div class="columns small-12 medium-10">
<div class="card__privacy">
<h3 class="card__privacy--title">個人情報の取り扱いについて</h3>
<p>株式会社メタフェイズ<br />代表取締役 城野 誠大</p>
<p>1.（取得及び利用目的）<br />
株式会社メタフェイズ（以下「弊社」という）は、弊社採用募集に応募されるみなさまの氏名、メールアドレスなどの個人を識別できる情報を取得します。取得の目的は、採用面接のご連絡であり、それ以外の目的で利用いたしません。</p>
<p>2.（預託・提供）<br />
弊社は、法令で定める場合を除き個人を特定できる情報（氏名、メールアドレス等）を取得目的の範囲を超えて当該個人の同意なしに第三者への提供や預託はいたしません。</p>
<p>3.（情報の開示・訂正・削除）<br />
弊社は、個人情報（採用評価情報等を除く）の開示、訂正、追加又は削除、利用目的の通知、利用又は提供の拒否についての依頼を受けた場合は、弊社の手続きに従って速やかに対応します。</p>
<p>4.（個人情報提供の任意性）<br />
個人情報の弊社への提供は、お客様の同意に基づく任意ですが、必要な情報が提供されない場合、適正な応募者の採用選考並びに採用面接のご連絡ができない場合もありうることをご承知ください。</p>
<p>5.（問合せ情報の廃棄）<br />
お問合せいただいき採用応募していただいたものの、残念ながら採用にいたらなかった場合、本ページで取得いたしました個人情報は、弊社で責任を持って廃棄致します。</p>
<p>6.（個人情報保護管理者及び問合せ先）<br />
当社では、個人情報を適切に保護するための管理者を下記の者が担当いたしております。また個人情報管理に関する問い合わせや開示、訂正または削除の依頼は、下記までご連絡下さい。<br />
株式会社メタフェイズ<br />
個人情報に関する苦情及び相談窓口<br />
〒160-0022　東京都新宿区新宿5-17-5 ラウンドクロス新宿5丁目ビル6F<br />
TEL：03-6384-5184　FAX：03-6384-5183</p>
</div>
<div class="article__contact--privacy-check">
<?php if(! empty($err_msg["privacy"])):?><span class="error"><?php echo $err_msg["privacy"]?></span><br><?php endif?>
<span class="wpcf7-form-control-wrap checkPrivacy"><span class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required" id="checkPrivacy"><span class="wpcf7-list-item first last"><label><input type="checkbox" name="privacy" value="1"<?php if($vars["privacy"]==1){echo " checked";}?> /><span class="wpcf7-list-item-label">個人情報の取扱に同意する</span></label></span></span></span>
</div>
</div>
</div>

<div class="article__contact--form-button-parent">
<div class="article__contact--form-button">
<input type="submit" value="確認画面へ" class="wpcf7-form-control wpcf7-submit button expanded" />
</div>
</div>

</section>
<div class="wpcf7-response-output wpcf7-display-none"></div>

<input type="hidden" name="mode" value="confirm" />

</form></div>
</article>
</div>
</section>

</main>
<footer id="footer" class="footer">

<div class="button__totop js_totop" data-magellan>
<a href="#top" title="ページトップへ"><svg class="icon arrow_09"><use xlink:href="#icon-arrow_09"></use></svg></a>
</div>

<div class="row align-middle">
<div class="columns large-2 medium-12 small-12 show-for-large">
<h2 class="footer--title image image__full"><a href="<?php echo SITE_PATH?>"><img src="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/img/logo_metaphase.svg" alt=""></a></h2>
</div>
<div class="columns large-10 medium-12 small-12 footer--wrap">
<div class="footer--nav">
<ul class="nav nav__gnav align-right row small-up-2 fontHVN">
<li class="columns"><a href="<?php echo SITE_PATH?>about/" title="会社紹介">会社紹介</a></li>
<li class="columns"><a href="<?php echo SITE_PATH?>service/" title="サービス">サービス</a></li>
<li class="columns"><a href="<?php echo SITE_PATH?>work/" title="実績">実績</a></li>
<li class="columns"><a href="<?php echo SITE_PATH?>products/" title="プロダクト">プロダクト</a></li>
<li class="columns"><a href="<?php echo SITE_PATH?>blog/" title="ブログ">ブログ</a></li>
<li class="columns"><a href="http://recruit.metaphase.co.jp" title="採用情報">採用情報</a></li>
<li class="columns"><a href="<?php echo SITE_PATH?>privacypolicy/" title="個人情報保護方針">個人情報保護方針</a></li>
<li class="columns"><a href="https://ja-jp.facebook.com/metaphase.co.jp" target="_blank" title="Facebook"><svg class="icon facebook"><use xlink:href="#icon-facebook"></use></svg><span class="show-for-small-only">Facebook</span></a></li>
</ul>
</div>
</div>
</div>
<div class="row column">
<p class="footer--copyright fontACN">Copyright© METAPHASE Co.,Ltd All rights reserved.</p>
</div>

</footer>
</section>

<script src="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/js/app.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/js/common.20170301-00.js"></script>
<script src="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/js/custom/provided/bgLib.js"></script>
<script src="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/js/custom/provided/bg.js"></script>
<script src="<?php echo SITE_PATH?>wp/wp-content/themes/metaphase/assets/js/custom/provided/jquery.customSelect.js"></script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<script>
window.fbAsyncInit = function() {
FB.init({
appId      : '2227587850799098',
xfbml      : true,
version    : 'v2.8'
});
FB.AppEvents.logPageView();
};
(function(d, s, id){
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) {return;}
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/ja_JP/sdk.js";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


$(document).ready(function(){
$('.share_fb').click(function(){
var dhref = $(this).attr('data-href');
FB.ui({
method: 'share',
href: dhref,
}, function(response){});
});
});
</script>

</body>
</html>
